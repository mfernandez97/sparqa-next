const path = require('path')
const { PHASE_DEVELOPMENT_SERVER } = require('next/constants')
const withLess = require("next-with-less")
const withImages = require('next-images')
const withTM = require('next-transpile-modules')(['front-end-core']); // modules to be transpiled

module.exports = (phase, { defaultConfig }) => {
  console.log('__dirname', __dirname)
  
  return withTM(
    withImages(
      withLess({
        cssModules: true,
        reactStrictMode: true,
        lessLoaderOptions: {
          sourceMap: true
        }
      })
    )
  )
}
