import { useTheme, useMediaQuery } from '@material-ui/core'

const useMuiMediaQuery = (direction, breakpoint) => {
  const theme = useTheme()

  if (direction === 'down') {
    return useMediaQuery(theme.breakpoints.down(breakpoint))
  }

  if (direction === 'up') {
    return useMediaQuery(theme.breakpoints.up(breakpoint))
  }

  if (direction === 'only') {
    return useMediaQuery(theme.breakpoints.only(breakpoint))
  }
}

export default useMuiMediaQuery