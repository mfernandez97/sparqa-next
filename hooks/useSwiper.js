import { useRef, useState } from 'react'

const useSwiper = () => {
  const swiperRef = useRef(null)
  const [isBeginning, setIsBeginning] = useState(true)
  const [isEnd, setIsEnd] = useState(false)
  const [currentIndex, setCurrentIndex] = useState()

  const onSlideChange = (swiper) => {
    setIsBeginning(swiper.isBeginning)
    setIsEnd(swiper.isEnd)
  }

  const realIndexChange = (swiper) => setCurrentIndex(swiper.realIndex)

  return { swiperRef, onSlideChange, realIndexChange, isBeginning, isEnd, currentIndex }
}

export default useSwiper