import { useState, useEffect } from 'react'

const useObservable = (observer, defaultValue = {}, deps = []) => {
  const [value, setValue] = useState(defaultValue)

  useEffect(() => {
    const sub = observer.subscribe(setValue)
    return () => sub.unsubscribe() // clean up when component is destroyed
  }, deps) // these shouldn't ever really change but you know

  return value
}

export default useObservable
