import { _last } from 'libraries/lodash'

export const toInvalidResponse = json => json

export const toUser = function (data) {
  const {
    mail: email,
    firstName,
    lastName,
    roles = [],
    uri = '',
    appPin = null,
    analyticsId,
    marketingConsent
  } = data
  const userId = _last(uri.split(':'))
  return {
    email,
    firstName,
    lastName,
    name: `${firstName} ${lastName}`,
    passwordStatus: 'Password never changed',
    pin: appPin,
    roles,
    uri,
    userId,
    analyticsId,
    marketingConsent,
    isSubscribed: roles.includes('subscribed'),
    isFreemium: !roles.includes('subscribed') && roles.includes('free-tier')
  }
}

export const toUserAuth = function (data) {
  const { isExternalAuth } = data
  return { isExternalAuth }
}
/**
 * email: "user@basement.com"
 * partnerCode: "string" - undefined if no partner
 * uuid: "uu-i-d"
 */
export const toCustomerDetails = function (data) {
  const { data: { email, uuid, partner: partnerRaw } } = data
  const partner = partnerRaw
    ? {
      name: partnerRaw.name,
      logoLg: partnerRaw.logoLargePath,
      logoSm: partnerRaw.logoSmallPath,
      servicedAccounts: partnerRaw.servicedAccounts,
      code: partnerRaw.code
    }
    : {}
  return { email, uuid, partner }
}
