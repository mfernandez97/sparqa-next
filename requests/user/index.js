import { RequestDefinition } from 'front-end-core/core/Api'
import { URL_USER_AUTH, URL_USER, URL_CUSTOMER_DETAILS } from 'constants/urls'
import { USER, USER_AUTH, CUSTOMER_DETAILS } from 'constants/requests'
import { toUser, toUserAuth, toCustomerDetails, toInvalidResponse } from 'requests/user/transform'

export const User = new RequestDefinition({
  name: USER,
  url: URL_USER,
  transform: toUser,
  transformInvalid: toInvalidResponse
})

export const UserAuth = new RequestDefinition({
  name: USER_AUTH,
  url: URL_USER_AUTH,
  transform: toUserAuth,
  transformInvalid: toInvalidResponse
})

export const CustomerDetails = new RequestDefinition({
  name: CUSTOMER_DETAILS,
  url: URL_CUSTOMER_DETAILS,
  transform: toCustomerDetails,
  transformInvalid: toInvalidResponse
})
