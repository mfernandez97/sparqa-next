export const toInvalidResponse = json => json

export const toDocumentSeoMap = data => data.data

export const toSectionSeo = data => data.data

export const toTopicQuestionSeo = data => data.data

export const toBundleSeo = data => data.data
