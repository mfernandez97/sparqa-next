import { RequestDefinition } from 'core/api/ApiService'
import { DOCUMENT_SEO, SECTION_SEO, TOPIC_QUESTION_SEO, BUNDLE_SEO } from 'constants/requests'
import { toDocumentSeoMap, toSectionSeo, toTopicQuestionSeo, toInvalidResponse, toBundleSeo } from './transform'
import { URL_DOCUMENT_URL_MAPPING, URL_SECTION_URL_MAPPING, URL_QUESTION_URL_MAPPING, URL_BUNDLE_URL_MAPPING } from 'constants/urls'

export const DocumentSeoMappings = new RequestDefinition({
  name: DOCUMENT_SEO,
  url: URL_DOCUMENT_URL_MAPPING,
  transform: toDocumentSeoMap,
  transformInvalid: toInvalidResponse
})

export const SectionSeo = new RequestDefinition({
  name: SECTION_SEO,
  url: URL_SECTION_URL_MAPPING,
  transform: toSectionSeo,
  transformInvalid: toInvalidResponse
})

export const TopicQuestionSeo = new RequestDefinition({
  name: TOPIC_QUESTION_SEO,
  url: URL_QUESTION_URL_MAPPING,
  transform: toTopicQuestionSeo,
  transformInvalid: toInvalidResponse
})

export const BundleSeo = new RequestDefinition({
  name: BUNDLE_SEO,
  url: URL_BUNDLE_URL_MAPPING,
  transform: toBundleSeo,
  transformInvalid: toInvalidResponse
})
