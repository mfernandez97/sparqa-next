import { stringify } from 'qs'

export const login = ({ username, password }) => {
  const url = '/auth/login/authenticatePwd'
  const params = stringify({
    username,
    password,
    rememberMe: true
  })

  return fetch(url, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: params
  })
}
