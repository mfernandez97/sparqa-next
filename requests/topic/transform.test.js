/* eslint-env jest */
import { mockTopicContentFromEndpoint, mockTopicContent } from '__testdata__'
import { toTopicContent } from './transform'

describe('toTopicContent', () => {
  it('transforms topic content response json to the correct format', () => {
    expect(toTopicContent(mockTopicContentFromEndpoint)).toStrictEqual(mockTopicContent)
  })
})
