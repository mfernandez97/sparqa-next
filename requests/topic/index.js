import { RequestDefinition } from 'core/api/ApiService'
import { URL_TOPICS, URL_TOPIC_CONTENT, URL_TOPIC_QUESTIONS, URL_TOPIC_SEARCH, URL_TOPIC_ANSWERS } from 'constants/urls'
import { TOPICS, TOPIC_CONTENT, TOPIC_QUESTIONS, TOPIC_SEARCH, TOPIC_ANSWERS } from 'constants/requests'
import { toInvalidResponse, toInvalidTopicContent, toTopics, toTopicContent, toTopicQuestions, toTopicSearch, toTopicAnswers } from 'requests/topic/transform'

export const Topics = new RequestDefinition({
  name: TOPICS,
  url: URL_TOPICS,
  transform: toTopics,
  transformInvalid: toInvalidResponse
})

export const TopicContent = new RequestDefinition({
  name: TOPIC_CONTENT,
  url: URL_TOPIC_CONTENT,
  transform: toTopicContent,
  transformInvalid: toInvalidTopicContent
})

export const TopicQuestions = new RequestDefinition({
  name: TOPIC_QUESTIONS,
  url: URL_TOPIC_QUESTIONS,
  transform: toTopicQuestions,
  transformInvalid: toInvalidResponse
})

export const TopicAnswers = new RequestDefinition({
  name: TOPIC_ANSWERS,
  url: URL_TOPIC_ANSWERS,
  transform: toTopicAnswers,
  transformInvalid: toInvalidResponse
})

export const TopicSearch = new RequestDefinition({
  name: TOPIC_SEARCH,
  url: URL_TOPIC_SEARCH,
  transform: toTopicSearch,
  transformInvalid: toInvalidResponse
})
