import { _map, _forEach, _mapValues, _trim, _get, _lowerCase } from 'libraries/lodash'
import debug from 'front-end-core/core/debug'
import { toHtml, toFacet, touch, deepPush } from 'helpers/requests'

export const toInvalidResponse = json => json

export const toInvalidTopicContent = json => {
  const getMessage = json => {
    const message = _get(json, 'data.message', null)
    if (!message) {
      return null
    }
    // TODO: this needs to change asap so we're not matching on long, arbitrary
    // strings; backend needs to return something like "code" field.
    switch (_lowerCase(message)) {
      case 'you dont have access to view this section of q a':
        return 'This section is not available for NatWest Mentor users. For further information contact <a href=\'mailto:info@sparqa.com\'>info@sparqa.com</a>.'
      default:
        return message
    }
  }
  return {
    ...json,
    message: getMessage(json)
  }
}

export const toTopics = data => {
  debug.time('toTopics')
  data = data.data
  const nodes = {}
  let root
  const ntouch = touch.bind(null, nodes)
  const npush = deepPush.bind(null, nodes)
  _forEach(data, (r, index) => {
    const { id, title, bookmarkId, overview = null, description = null, parentId } = _mapValues(r, String)
    const depth = Number(r.depth)

    if (index === 0) {
      root = id
      ntouch(id, { id, title, bookmarkId, overview, description, path: [], children: [], descendants: [], depth })
    } else if (id && nodes.hasOwnProperty(parentId)) { // eslint-disable-line
      const parentPath = nodes[parentId].path
      const child = {
        id,
        title,
        bookmarkId,
        overview: overview ? toHtml(overview) : overview,
        description: _trim(description),
        active: !!r.active,
        path: [...parentPath, id],
        children: [],
        descendants: [id],
        parentId,
        depth
      }
      ntouch(id, child)
      npush([parentId, 'children'], child)
      parentPath.forEach(p => npush([p, 'descendants'], id))
    }
  })
  debug.timeEnd('toTopics')
  return { nodes, root }
}

export const toTopicContent = data => {
  data = data.data
  const questions = {}
  const topics = {}
  const qpush = deepPush.bind(null, topics)
  _forEach(data, q => {
    const { qa_id: id, sectionId: topicId, tocId: qtocId, bookmarkId, questionNumber } = _mapValues(q, String)
    const tocId = bookmarkId || qtocId
    if (tocId) {
      qpush(topicId, tocId)
      questions[tocId] = {
        id,
        question: toHtml(q.question, 'span', false, ['question']),
        answer: '<span>N/A</span>',
        tocId,
        topicId,
        questionNumber
      }
    }
  })
  return { questions, topics }
}

export const toTopicQuestions = data => {
  const content = data.data
  const nodes = {}
  const topics = {}
  const all = []
  content.forEach(
    ({ parent_id: topicId, id, questionNumber, title: question }) => {
      touch(topics, [topicId], []).push(id)
      nodes[id] = {
        topicId,
        id,
        questionNumber,
        question
      }
      all.push(id)
    }
  )
  return { all, nodes, topics }
}

export const toTopicAnswers = data => {
  const content = data.data
  const answers = content.reduce((a, n) =>
    ({
      ...a,
      [n.bookmarkId]:
      {
        answer: toHtml(n.answer)
      }
    }),
  {})
  return { answers }
}

export const toTopicSearch = json => {
  const { data } = json
  return {
    resultCount: data.totalNumberResults,
    results: _map(data.results || [], toTopicsSearchResult),
    facets: _map(data.facets || [], toFacet)
  }
}

const toTopicsSearchResult = result => {
  const question = toHtml(
    `<search-result-question>${result.questionExcerpt}</search-result-question>`
  )
  const answer = toHtml(
    `<search-result-answer>${result.answerExcerpt}</search-result-answer>`
  )
  return {
    question,
    answer,
    topicId: result.parent_id.toString(),
    questionNumber: result.question_num.toString(),
    tocId: result.bookmarkId || result.tocID,
    s2: result.section_root_id
  }
}
