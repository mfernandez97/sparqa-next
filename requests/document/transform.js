import { _map, _get, _head, _isNumber, _lowerCase } from 'libraries/lodash'
import { toHtml, toFacet } from 'helpers/requests'

export const toInvalidResponse = json => json

export const toSavedDocumentInterviewRawText = json => json.data

export const toDocumentInterview = json => json.data

export const toSavedDocumentInterview = json => json

// This assumes we've only requested one topic for now, even though the BE endpoint supports multiple
export const toTopicDocumentsList = json => {
  const { data } = json
  const thisTopic = _head(data)
  return thisTopic ? thisTopic.documents.map(document => toDocument({ data: document })) : []
}

const documentMap = ({
  id,
  isStatic,
  isPublished,
  title,
  lowerLevelTopics = [],
  topLevelTopic = { title: 'Uncategorized', bookmarkId: null },
  overview,
  documentType,
  price,
  salesBulletPoints,
  resourceQuestions,
  images,
  seoUrl,
  type,
  videoUrl,
  availableFiles = []
}) => ({
  id,
  isStatic,
  isPublished,
  title,
  topic: topLevelTopic,
  topics: lowerLevelTopics,
  description: overview,
  documentType,
  price: _isNumber(price) ? price : null,
  bulletPoints: salesBulletPoints,
  documentQa: resourceQuestions,
  images,
  seoUrl,
  type,
  videoUrl,
  availableFiles: availableFiles.map(file => ({
    ...file,
    label: file.fileType === 'DOC' ? 'Word' : file.fileType
  }))
})

export const toDocument = ({ data }) => documentMap(data)

export const toBundle = ({
  data: {
    uuid,
    name,
    overview,
    seoUrl,
    price,
    saving,
    questionAnswers,
    bulletPoints,
    isPublished,
    isFinalised,
    topLevelTopic,
    resources,
    howTo,
    videoUrl
  }
}) => ({
  id: uuid,
  title: name,
  description: overview,
  bundleDocuments: resources,
  bundleQa: questionAnswers,
  topic: topLevelTopic,
  price,
  saving,
  seoUrl,
  isFinalised,
  isPublished,
  bulletPoints,
  howTo,
  videoUrl
})

export const toSlimBundle = ({
  uuid,
  name,
  overview,
  price,
  resources,
  topLevelTopic
}) => ({
  id: uuid,
  title: name,
  description: overview,
  price,
  bundleDocuments: resources,
  topic: topLevelTopic
})

export const toSlimBundleList = ({ data }) => data.map(toSlimBundle)

export const toDocumentsList = json => {
  const { data } = json
  return data.map(documentMap)
}

export const toDocumentInvalid = json => {
  // TODO: this needs to change asap so we're not matching on long, arbitrary
  // strings; backend needs to return something like "code" field.
  const getMessage = json => {
    const message = _get(json, 'data', null)
    if (!message) {
      return null
    }
    switch (_lowerCase(message)) {
      case 'your subscription does not provide access to this document':
        return 'This document is not available for NatWest Mentor users. For further information contact <a href=\'mailto:info@sparqa.com\'>info@sparqa.com</a>.'
      default:
        return message
    }
  }
  return {
    statusCode: json.statusCode,
    status: json.status,
    message: getMessage(json)
  }
}

const toSearchTitle = title => toHtml(`<search-result-question>${title}</search-result-question>`)

const toSearchDescription = description => toHtml(`<search-result-answer>${description}</search-result-answer>`)

export const toSearchResults = (json, resultTransform) => {
  const { data } = json
  return {
    resultCount: data.totalNumberResults || 0,
    results: _map(data.results || [], resultTransform),
    facets: _map(data.facets || [], toFacet)
  }
}

export const toDocumentSearchResult = result => ({
  ...documentMap(result),
  title: toSearchTitle(result.title),
  description: toSearchDescription(result.overview)
})

export const toBundleSearchResult = result => ({
  ...toSlimBundle(result),
  title: toSearchTitle(result.name),
  description: toSearchDescription(result.overview)
})

export const toSavedDocuments = json => {
  const {
    totalNumberResults,
    results,
    facets
  } = json.data
  const modifiedResults = results.map(result => {
    const {
      uuid,
      documentUuid,
      documentTopLevelTopic,
      documentType,
      name,
      documentTitle,
      createdDate,
      modifiedDate,
      notes
    } = result
    return {
      id: uuid,
      documentId: documentUuid,
      topic: documentTopLevelTopic,
      type: documentType,
      savedName: name,
      documentName: documentTitle,
      created: createdDate,
      updated: modifiedDate,
      notes
    }
  })
  return {
    resultCount: totalNumberResults,
    results: modifiedResults,
    facets
  }
}

export const toPurchasedDocuments = json => json

export const toRelatedDocuments = ({ data }) => {
  return data.map(({ document, confidenceScore }) => (
    Object.assign({}, toDocument({ data: document }), { relationScore: confidenceScore })
  ))
}
