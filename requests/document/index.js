import { RequestDefinition } from 'core/api/ApiService'
import {
  URL_DOCUMENTS,
  URL_DOCUMENT,
  URL_TOPIC_DOCUMENTS,
  URL_DOCUMENT_INTERVIEW,
  URL_DOCUMENT_SEARCH,
  URL_SAVED_DOCUMENTS,
  URL_SAVED_DOCUMENT_INTERVIEW,
  URL_SAVED_DOCUMENT_INTERVIEW_RAW_TEXT,
  URL_PURCHASED_DOCUMENTS,
  URL_PURCHASED_DOCUMENT_IDS,
  URL_CHECK_DOCUMENT_PURCHASED,
  URL_RELATED_DOCUMENTS,
  URL_BUNDLE,
  URL_BUNDLES,
  URL_DOCUMENT_RELATED_BUNDLES,
  URL_BUNDLE_RELATED_BUNDLES,
  URL_BUNDLE_SEARCH,
  URL_TOPIC_RELATED_BUNDLES
} from 'constants/urls'
import {
  DOCUMENTS,
  DOCUMENT,
  TOPIC_DOCUMENTS,
  DOCUMENT_INTERVIEW,
  DOCUMENT_SEARCH,
  SAVED_DOCUMENTS,
  SAVED_DOCUMENT_INTERVIEW,
  SAVED_DOCUMENT_INTERVIEW_RAW_TEXT,
  PURCHASED_DOCUMENTS,
  PURCHASED_DOCUMENTS_IDS,
  DOCUMENT_PURCHASED,
  RELATED_DOCUMENTS,
  BUNDLE,
  BUNDLES,
  DOCUMENT_RELATED_BUNDLES,
  BUNDLE_RELATED_BUNDLES,
  BUNDLE_SEARCH,
  TOPIC_RELATED_BUNDLES
} from 'constants/requests'
import {
  toDocumentInterview,
  toDocument,
  toDocumentsList,
  toTopicDocumentsList,
  toSavedDocuments,
  toSavedDocumentInterview,
  toSavedDocumentInterviewRawText,
  toPurchasedDocuments,
  toRelatedDocuments,
  toInvalidResponse,
  toDocumentInvalid,
  toBundle,
  toSlimBundleList,
  toSearchResults,
  toDocumentSearchResult,
  toBundleSearchResult
} from 'requests/document/transform'

export const Documents = new RequestDefinition({
  name: DOCUMENTS,
  url: URL_DOCUMENTS,
  transform: toDocumentsList,
  transformInvalid: toInvalidResponse
})

export const Document = new RequestDefinition({
  name: DOCUMENT,
  url: URL_DOCUMENT,
  transform: toDocument,
  transformInvalid: toDocumentInvalid
})

export const TopicDocuments = new RequestDefinition({
  name: TOPIC_DOCUMENTS,
  url: URL_TOPIC_DOCUMENTS,
  transform: toTopicDocumentsList,
  transformInvalid: toInvalidResponse
})

export const DocumentInterview = new RequestDefinition({
  name: DOCUMENT_INTERVIEW,
  url: URL_DOCUMENT_INTERVIEW,
  transform: toDocumentInterview,
  transformInvalid: toInvalidResponse
})

export const SavedDocumentInterview = new RequestDefinition({
  name: SAVED_DOCUMENT_INTERVIEW,
  url: URL_SAVED_DOCUMENT_INTERVIEW,
  transform: toSavedDocumentInterview,
  transformInvalid: toInvalidResponse
})

export const SavedDocumentInterviewRawText = new RequestDefinition({
  name: SAVED_DOCUMENT_INTERVIEW_RAW_TEXT,
  url: URL_SAVED_DOCUMENT_INTERVIEW_RAW_TEXT,
  transform: toSavedDocumentInterviewRawText,
  transformInvalid: toInvalidResponse
})

export const DocumentSearch = new RequestDefinition({
  name: DOCUMENT_SEARCH,
  url: URL_DOCUMENT_SEARCH,
  transform: response => toSearchResults(response, toDocumentSearchResult),
  transformInvalid: toInvalidResponse
})

export const BundleSearch = new RequestDefinition({
  name: BUNDLE_SEARCH,
  url: URL_BUNDLE_SEARCH,
  transform: response => toSearchResults(response, toBundleSearchResult),
  transformInvalid: toInvalidResponse
})

export const PurchasedDocuments = new RequestDefinition({
  name: PURCHASED_DOCUMENTS,
  url: URL_PURCHASED_DOCUMENTS,
  transform: toDocumentsList,
  transformInvalid: toInvalidResponse
})

export const PurchasedDocumentsIDs = new RequestDefinition({
  name: PURCHASED_DOCUMENTS_IDS,
  url: URL_PURCHASED_DOCUMENT_IDS,
  transform: toPurchasedDocuments,
  transformInvalid: toInvalidResponse
})

export const SavedDocuments = new RequestDefinition({
  name: SAVED_DOCUMENTS,
  url: URL_SAVED_DOCUMENTS,
  transform: toSavedDocuments,
  transformInvalid: toInvalidResponse
})

export const DocumentPurchased = new RequestDefinition({
  name: DOCUMENT_PURCHASED,
  url: URL_CHECK_DOCUMENT_PURCHASED,
  transform: toPurchasedDocuments,
  transformInvalid: toInvalidResponse
})

export const RelatedDocuments = new RequestDefinition({
  name: RELATED_DOCUMENTS,
  url: URL_RELATED_DOCUMENTS,
  transform: toRelatedDocuments,
  transformInvalid: toInvalidResponse
})

export const Bundle = new RequestDefinition({
  name: BUNDLE,
  url: URL_BUNDLE,
  transform: toBundle,
  transformInvalid: toInvalidResponse
})

export const Bundles = new RequestDefinition({
  name: BUNDLES,
  url: URL_BUNDLES,
  transform: toSlimBundleList,
  transformInvalid: toInvalidResponse
})

export const DocumentRelatedBundles = new RequestDefinition({
  name: DOCUMENT_RELATED_BUNDLES,
  url: URL_DOCUMENT_RELATED_BUNDLES,
  transform: toSlimBundleList,
  transformInvalid: toInvalidResponse
})

export const BundleRelatedBundles = new RequestDefinition({
  name: BUNDLE_RELATED_BUNDLES,
  url: URL_BUNDLE_RELATED_BUNDLES,
  transform: toSlimBundleList,
  transformInvalid: toInvalidResponse
})

export const TopicRelatedBundles = new RequestDefinition({
  name: TOPIC_RELATED_BUNDLES,
  url: URL_TOPIC_RELATED_BUNDLES,
  transform: toSlimBundleList,
  transformInvalid: toInvalidResponse
})
