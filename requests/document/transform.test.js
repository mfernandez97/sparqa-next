/* eslint-env jest */
import { RESPONSE_FORBIDDEN } from 'front-end-core/constants/responses'
import { mockDocumentsFromEndpoint, mockDocumentFromEndpoint, mockDocument, mockDocuments } from '__testdata__'
import { toDocumentsList, toDocument, toDocumentInvalid } from './transform'

const mockInvalidResponse = {
  data: 'Your subscription does not provide access to this document',
  status: 'Forbidden',
  statusCode: RESPONSE_FORBIDDEN
}

describe('toDocumentsList', () => {
  it('transforms documents response json to the correct format', () => {
    expect(toDocumentsList(mockDocumentsFromEndpoint)).toStrictEqual(mockDocuments)
  })
})

describe('toDocument', () => {
  it('transforms document response json to the correct format', () => {
    expect(toDocument(mockDocumentFromEndpoint)).toStrictEqual(mockDocument)
  })
})
// I'm a little uncomfortable with assuming an invalid request is due to natwest but thats what we do right now
describe('toDocumentInvalid', () => {
  it('transforms an invalid response to the correct format', () => {
    expect(toDocumentInvalid(mockInvalidResponse)).toStrictEqual({
      statusCode: RESPONSE_FORBIDDEN,
      status: 'Forbidden',
      message: 'This document is not available for NatWest Mentor users. For further information contact <a href=\'mailto:info@sparqa.com\'>info@sparqa.com</a>.'
    })
  })
})
