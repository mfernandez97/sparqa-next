import { RequestDefinition } from 'core/api/ApiService'
import { PAYMENT_PLANS, STRIPE_ID, COMPANY_SIZE_OPTIONS, INDUSTRY_OPTIONS, USER_CURRENT_SUBSCRIPTION, USER_BILLING, CREDIT } from 'constants/requests'
import { toPaymentPlans, toStripeId, toCompanySizeOptions, toIndustryOptions, toCurrentSubscription, toBillingInformation, toCredit, toInvalidResponse } from './transform'
import { URL_BILLING_DETAILS, URL_CREDIT } from 'constants/urls'

export const PaymentPlans = new RequestDefinition({
  name: PAYMENT_PLANS,
  url: '/api/payments/products',
  transform: toPaymentPlans,
  transformInvalid: toInvalidResponse
})

export const StripeId = new RequestDefinition({
  name: STRIPE_ID,
  url: '/api/stripe/key',
  transform: toStripeId,
  transformInvalid: toInvalidResponse
})

export const CompanySizeOptions = new RequestDefinition({
  name: COMPANY_SIZE_OPTIONS,
  url: '/user-api/utils/company-sizes',
  transform: toCompanySizeOptions,
  transformInvalid: toInvalidResponse
})

export const IndustryOptions = new RequestDefinition({
  name: INDUSTRY_OPTIONS,
  url: '/user-api/utils/industries',
  transform: toIndustryOptions,
  transformInvalid: toInvalidResponse
})

export const UserCurrentSubscription = new RequestDefinition({
  name: USER_CURRENT_SUBSCRIPTION,
  url: '/secure/api/payments/subscriptions',
  transform: toCurrentSubscription,
  transformInvalid: toInvalidResponse
})

export const UserBilling = new RequestDefinition({
  name: USER_BILLING,
  url: URL_BILLING_DETAILS,
  transform: toBillingInformation,
  transformInvalid: toInvalidResponse
})

export const Credit = new RequestDefinition({
  name: CREDIT,
  url: URL_CREDIT,
  transform: toCredit,
  transformInvalid: toInvalidResponse
})
