import { _omit, _keys } from 'libraries/lodash'

export const toInvalidResponse = json => json

export const toPaymentPlans = json => {
  const { data } = json
  const monthlyBenefits = [
    'Full, unlimited access to all our Legal Guidance',
    '300+ auto-documents and templates',
    'Up to 5 team logins'
  ]
  const annualBenefits = [
    'All the benefits of our monthly plan',
    'PLUS 20% off the usual monthly price!',
    'Only one simple payment'
  ]
  const monthlyDisclaimer = '12-month contract paid monthly'
  const annualDisclaimer = '​12-month contract, one upfront payment'

  return data[0].plans.map(({ stripeId, name, price, recommended }) => ({
    stripeId,
    name,
    price: typeof price === 'number' ? price / 100 : null,
    recommended,
    benefits: recommended ? annualBenefits : monthlyBenefits,
    disclaimer: recommended ? annualDisclaimer : monthlyDisclaimer
  }))
}

export const toStripeId = json => {
  const { data } = json
  return data
}

export const toCompanySizeOptions = json => {
  const data = _keys(_omit(json, 'statusCode')).map(key => json[key])
  return data.map(({ id, size }) => ({
    value: typeof id === 'number' ? id.toString() : id,
    label: size
  }))
}

export const toIndustryOptions = json => {
  const data = _keys(_omit(json, 'statusCode')).map(key => json[key])
  return data.map(({ id, name }) => ({
    value: typeof id === 'number' ? id.toString() : id,
    label: name
  }))
}

export const toCurrentSubscription = json => {
  const {
    name,
    price,
    cancelling,
    expiryDate,
    status,
    discount,
    monthsRemaining
  } = json.data

  return {
    name,
    price,
    cancelling,
    expiryDate,
    status,
    discount,
    monthsRemaining
  }
}

export const toBillingInformation = json => {
  const {
    name,
    postcode,
    country,
    line1,
    line2,
    last4,
    expMonth,
    expYear
  } = json.data

  return {
    name,
    line1,
    line2,
    postcode,
    country,
    last4,
    expiryDate: `${expMonth}/${expYear}`
  }
}

export const toCredit = json => {
  const { received: purportedReceived, used } = json.data
  const received = purportedReceived >= 0 ? purportedReceived : 0
  const remaining = received - used
  return {
    received,
    used,
    remaining
  }
}
