/* eslint-env jest */

import { combineLatest, of } from 'rxjs'
import { map, switchMap } from 'rxjs/operators'
import { identity, omit, has } from 'lodash'
import { RESPONSE_OK, RESPONSE_BADRESPONSE } from 'front-end-core/constants/responses'
import { mapResponse, isValidResponse, flatMapResponse, mergeResponses } from 'core/api/dataflow'

const mockObserveRequest = response => of(true).pipe(switchMap(() => of(response)))
const mockTransform = response => ({ ...response, hello: 'world' })

describe('mapResponse', () => {
  it('maps a valid response', done => {
    const response = { statusCode: RESPONSE_OK, status: 'OK', data: {} }
    mockObserveRequest(response)
      .pipe(map(mapResponse(mockTransform)))
      .subscribe(result => {
        expect(response).toMatchObject(omit(result, 'hello'))
        expect(result.hello).toBe('world')
        done()
      })
  })

  it('maps an invalid response', done => {
    const response = { statusCode: RESPONSE_BADRESPONSE, status: 'Bad Response', data: {} }
    mockObserveRequest(response)
      .pipe(map(mapResponse(identity, mockTransform)))
      .subscribe(result => {
        expect(response).toMatchObject(omit(result, 'hello'))
        expect(result.hello).toBe('world')
        done()
      })
  })
})

describe('isValidResponse', () => {
  it('returns true for valid response', () => {
    const result = isValidResponse({ statusCode: RESPONSE_OK })
    expect(result).toBe(true)
  })

  it('returns false for an invalid response', () => {
    const result = isValidResponse({ statusCode: RESPONSE_BADRESPONSE })
    expect(result).toBe(false)
  })
})

describe('flatMapResponse', () => {
  it('maps an invalid response to an observable', done => {
    const response = { statusCode: RESPONSE_BADRESPONSE, status: 'Bad Response', data: {} }
    mockObserveRequest(response)
      .pipe(map(flatMapResponse(response => of(response), mockTransform)))
      .subscribe(result => {
        expect(result.subscribe).toBeInstanceOf(Function)
        done()
      })
  })
})

describe('mergeResponses', () => {
  it('merges responses into a single object', done => {
    const response1 = { a: true }
    const response2 = { b: true }
    combineLatest(
      mockObserveRequest(response1),
      mockObserveRequest(response2),
      mergeResponses
    ).subscribe(response => {
      expect(has(response, 'a')).toBe(true)
      expect(has(response, 'b')).toBe(true)
      done()
    })
  })
})
