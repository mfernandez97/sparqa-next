/* eslint-disable quote-props */

export default {
  'doc-424': {
    'image': `Sparqa-books.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-425': {
    'image': `sparqa-different-pieces.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#232A3A'
  },
  'doc-426': {
    'image': `Sparqa-magnifying glass.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-427': {
    'image': `Sparqa-Insurance Form.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-428': {
    'image': `Sparqa-pawns.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-429': {
    'image': `Sparqa-wood block faces.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-430': {
    'image': `Sparqa-Box.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-431': {
    'image': `Sparqa-door.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-432': {
    'image': `Sparqa-scales.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#3855A2'
  },
  'doc-433': {
    'image': `Sparqa-coin jar 2.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-434': {
    'image': `Sparqa-pregnancy test.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-435': {
    'image': `Sparqa-Pram.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-436': {
    'image': `Sparqa-suitcase.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-437': {
    'image': `Sparqa-Stethoscope.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-438': {
    'image': `Sparqa-calendar.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-439': {
    'image': `Sparqa-clock.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-440': {
    'image': `Sparqa-house.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-441': {
    'image': `Sparqa-Upload blocks.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-442': {
    'image': `Sparqa-party hat.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-443': {
    'image': `Sparqa-pawns.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-444': {
    'image': `Sparqa-Card machine.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-445': {
    'image': `Sparqa-microphone.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-446': {
    'image': `Sparqa-chess-piece.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-448': {
    'image': `Sparqa-headset.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-449': {
    'image': `Sparqa-Megaphone.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-450': {
    'image': `Sparqa-billboard.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-451': {
    'image': `Sparqa-Padlock.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#232A3A'
  },
  'doc-452': {
    'image': `Sparqa-credit card.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-456': {
    'image': `Sparqa-keys.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-457': {
    'image': `Sparqa-Trademark.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-458': {
    'image': `sparqa-copyright.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-459': {
    'image': `sparqa-copyright.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-460': {
    'image': `Sparqa-hard-hat-2.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-461': {
    'image': `Sparqa-lightbulb.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-462': {
    'image': `Sparqa-boxing-gloves.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#232A3A'
  },
  'doc-463': {
    'image': `Sparqa-European flag.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-464': {
    'image': `Sparqa-filling cabinet.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-465': {
    'image': `Sparqa-File.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-466': {
    'image': `Sparqa-Padlock.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#232A3A'
  },
  'doc-468': {
    'image': `Sparqa-CCTV.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#3855A2'
  },
  'doc-469': {
    'image': `Sparqa-Folders.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-470': {
    'image': `Sparqa-people lock.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-471': {
    'image': `Sparqa-Padlock.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#232A3A'
  },
  'doc-472': {
    'image': `Sparqa-rocket.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-473': {
    'image': `sparqa-abacus.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-474': {
    'image': `sparqa-puzzle.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#232A3A'
  },
  'doc-475': {
    'image': `Sparqa-Calculator.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-476': {
    'image': `Sparqa-name tag.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-477': {
    'image': `Sparqa-Org Chart.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-478': {
    'image': `Sparqa-name tag.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-479': {
    'image': `sparqa-shares.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-480': {
    'image': `Sparqa-green house.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-481': {
    'image': `sparqa-lock.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-482': {
    'image': `Sparqa-whistle.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-483': {
    'image': `Sparqa-protest.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-484': {
    'image': `Sparqa-Letter.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-485': {
    'image': `Sparqa-people circle.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-486': {
    'image': `Sparqa-Green person.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-488': {
    'image': `Sparqa-Red person.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-489': {
    'image': `Sparqa-Org Chart 2.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-490': {
    'image': `Sparqa-Thumb down.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-491': {
    'image': `Sparqa-Blue Person.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-492': {
    'image': `Sparqa-Pen.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-493': {
    'image': `Sparqa-paper stack.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-494': {
    'image': `Sparqa-books.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-495': {
    'image': `Sparqa-stationary.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-496': {
    'image': `Sparqa-name tag.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-497': {
    'image': `sparqa-puzzle.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-498': {
    'image': `Sparqa-Moving.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#3855A2'
  },
  'doc-499': {
    'image': `Sparqa-pawns.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-500': {
    'image': `Sparqa-Money bag.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-501': {
    'image': `sparqa-abacus.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-502': {
    'image': `sparqa-abacus.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#3855A2'
  },
  'doc-503': {
    'image': `sparqa-deflated.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-504': {
    'image': `sparqa-abacus.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-505': {
    'image': `Sparqa-presentation.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-506': {
    'image': `Sparqa-Money bag.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-507': {
    'image': `Sparqa-bar chart.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-508': {
    'image': `sparqa-slice.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#3855A2'
  },
  'doc-509': {
    'image': `Sparqa-piggy bank.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-510': {
    'image': `Sparqa-Bank.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-511': {
    'image': `Sparqa-Bank.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-512': {
    'image': `sparqa-money-jar.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-513': {
    'image': `Sparqa-house.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-514': {
    'image': `Sparqa-houses.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-515': {
    'image': `Sparqa-spanner.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-516': {
    'image': `Sparqa-Moving house.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-517': {
    'image': `sparqa-squeeze.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-518': {
    'image': `Sparqa-green house.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2C78BD'
  },
  'doc-519': {
    'image': `Sparqa-Moving.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#3855A2'
  },
  'doc-520': {
    'image': `Sparqa-Insurance Form.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-521': {
    'image': `Sparqa-hard hat.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#3855A2'
  },
  'doc-522': {
    'image': `Sparqa-Mask.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-523': {
    'image': `Sparqa-chair.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-524': {
    'image': `Sparqa-ladder.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-525': {
    'image': `Sparqa-Fire.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#3855A2'
  },
  'doc-526': {
    'image': `Sparqa-Wet floor.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-527': {
    'image': `Sparqa-Stethoscope.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-528': {
    'image': `Sparqa-magnifying glass.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#2CB7C3'
  },
  'doc-536': {
    'image': `Sparqa-hard hat.png`,
    'theme': 'LIGHT',
    'backgroundColor': '#3855A2'
  }
}
