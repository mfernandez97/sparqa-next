// Routes to known arbitrary CMS pages

export const TEAM = 'team'
export const BLOG = 'insights'
export const PARTNERS = 'partnerships'
export const PRESS = 'press'
export const AAL = 'ask-a-lawyer'
export const AAL_REGISTER = 'ask-a-lawyer-register'
export const COVID_19 = 'https://news.sparqa.com'
