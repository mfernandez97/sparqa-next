export const DOCUMENT_PRICE_OPTIONS = [
  { optionName: 'FREE', range: { min: 0, max: 0 } },
  { optionName: '£0.01 - £24.99', range: { min: 0.01, max: 24.99 } },
  { optionName: '£25 - £49.99', range: { min: 25, max: 49.99 } },
  { optionName: '£50 - £74.99', range: { min: 50, max: 74.99 } },
  { optionName: '£75 - £99.99', range: { min: 75, max: 99.99 } },
  { optionName: '100+', range: { min: 100 } }
]
