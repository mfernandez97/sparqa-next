import { TOPICS, DOCUMENTS, BUNDLES } from './categories'

export const TOPIC = {
  name: 'Guidance',
  category: TOPICS,
  colour: 'coral',
  hex: '#fa714f'
}

export const DOCUMENT = {
  name: 'Documents',
  category: DOCUMENTS,
  colour: 'steel-blue',
  hex: '#2579C5'
}

export const BUNDLE = {
  name: 'Toolkits',
  category: BUNDLES,
  colour: 'light-sea-green',
  hex: '#14bbc4'
}
