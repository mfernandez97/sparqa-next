export default [{
  name: 'David Lambert',
  position: 'CEO',
  company: 'Laundry Republic',
  date: '27th Dec 2019',
  assetUrl: 'assets/landing/landing-testimonial-photos/david-lambert.jpg',
  copy: '“Very useful, smart service for scaling businesses, full of exactly the kind of information we need on a day-to-day basis. And much more efficient than traditional approaches to legal services. Definitely recommend.”'
}, {
  name: 'Sean Nolan',
  position: 'CEO',
  company: 'Blink',
  assetUrl: 'assets/landing/landing-testimonial-photos/sean-nolan.jpg',
  copy: '“Sparqa Legal has been invaluable. High quality, joined up documentation and advice. We have used for all our employment, GDPR and website needs."'
}, {
  name: 'Sam Hussain',
  position: 'Founder',
  company: 'Log My Care',
  assetUrl: 'assets/landing/landing-testimonial-photos/sam-hussain.jpeg',
  copy: '“Really like the product and its super useful. I can already see the value and I know there’ll be more. I love it!”'
}, {
  name: 'Anais Fritz',
  position: 'Lawyer',
  assetUrl: 'assets/landing/landing-testimonial-photos/anais-fritz.jpeg',
  copy: '“As an in-house lawyer I’m constantly asked a diverse range of legal questions. Sparqa Legal is my secret weapon! The guidance and documents are as good as you’d get from the best City law firms.”'
}, {
  name: null,
  position: null,
  assetUrl: 'assets/landing/landing-testimonial-photos/google-icon.png',
  copy: '“As an in-house lawyer I’m constantly asked a diverse range of legal questions. Sparqa Legal is my secret weapon! The guidance and documents are as good as you’d get from the best City law firms.”'
}
]
