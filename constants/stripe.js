export const STRIPE_ID = 'pk_test_GbHQQx2jVcbrWAQcpqli6Xox'

export const COUNTRY_CODE_OPTIONS = [
  {label: 'United Kingdom', value: 'GB'},
  {label: 'Australia', value: 'AU'},
  {label: 'Austria', value: 'AT'},
  {label: 'Belgium', value: 'BE'},
  {label: 'Canada', value: 'CA'},
  {label: 'Denmark', value: 'DK'},
  {label: 'Finland', value: 'FI'},
  {label: 'France', value: 'FR'},
  {label: 'Germany', value: 'DE'},
  {label: 'Hong Kong', value: 'HK'},
  {label: 'Ireland', value: 'IE'},
  {label: 'Italy', value: 'IT'},
  {label: 'Japan', value: 'JP'},
  {label: 'Luxembourg', value: 'LU'},
  {label: 'Netherlands', value: 'NL'},
  {label: 'New Zealand', value: 'NZ'},
  {label: 'Norway', value: 'NO'},
  {label: 'Portugal', value: 'PT'},
  {label: 'Singapore', value: 'SG'},
  {label: 'Spain', value: 'ES'},
  {label: 'Sweden', value: 'SE'},
  {label: 'Switzerland', value: 'CH'},
  {label: 'United States', value: 'US'}
]
