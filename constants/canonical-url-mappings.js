export default {
  'doc-453': 'https://www.sparqa.com/insights/privacy-policy-template/', // Operating a website: Privacy and cookies
  'doc-464': 'https://www.sparqa.com/insights/gdpr-policy-template/', // Data protection and GDPR: Using personal data, policies and record-keeping
  'doc-424': 'https://www.sparqa.com/insights/employee-handbook-template/', // HR: Staff handbook and HR policies
  'doc-473': 'https://www.sparqa.com/insights/shareholders-agreement/', // Starting a company: Shareholders' agreement
  'doc-444': 'https://www.sparqa.com/insights/terms-and-conditions-template/', // Sales and commercial agreements: Terms and conditions of sale
  'doc-482': 'https://www.sparqa.com/insights/debt-collection-letter/', // Late payments and disputes: Chasing payments and enforcement,
  'doc-454': 'https://www.sparqa.com/insights/website-terms-and-conditions-template/' // Operating a website: Website content and terms of use
}
