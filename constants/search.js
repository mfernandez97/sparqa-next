import { DOCUMENTS } from './categories'

export const SNIPPET_LENGTH_MOBILE = 200
export const SNIPPET_LENGTH_DESKTOP = 400
export const TOPIC_FRAGMENT_COUNT_MOBILE = 2
export const TOPIC_FRAGMENT_COUNT_DESKTOP = 4

export const DEFAULT_CATEGORY = DOCUMENTS
