export const PARTNER_COOKIE = 'partner'
export const PARTNER_BANNER_DISMISSED_COOKIE = 'partner-banner-dismissed'
export const PARTNER_DISCOUNT_CODE_COOKIE = 'partner-discount-code'
export const COVID_19_BANNER_DISMISSED_COOKIE = 'covid-19-banner-dismissed'
