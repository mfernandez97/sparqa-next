/*

  Grid
  ----

  You can use the .grid(n) mixin to create a grid structure with n items per
  row. The gutters are controlled with paddings rather than margins to simplify
  the item widths (i.e. the items in grid with 4 items per row will have width
  25% rather than calc(25% - some value for margin)). Note that this means the
  grid items will normally be wrapping elements and any borders, background
  colours, box shadows, whatever, will usually be set on an inner element.

  The other gotcha is that unlike most of the tablet specific classes the tablet
  grid classes don't cascade down to mobile. This is because the nth child
  selectors can't be reset without setting the padding in the mixin rather than
  on the element, which would make the classes less flexible.

*/

.grid(@n) {
  @n-1: @n - 1;
  .flex;
  .flex-wrap;
  // Select any immediate child.
  > * {
    width: calc(100% / @n);
    // Item at start of row.
    &:nth-child(@{n}n - @{n-1}) {
      .pl0;
    }
    // Item at end of row.
    &:nth-child(@{n}n) {
      .pr0;
    }
    // Item in first row.
    &:nth-child(-n + @{n}) {
      .pt0;
    }
    // Item in last row.
    &:nth-last-child(-n + @{n}) {
      .pb0;
    }
  }
}

.grid-with-gutters(@n, @gw) {
  @n-1: @n - 1;
  .flex;
  .flex-wrap;
  // Select any immediate child.
  > * {
    // (100% / number of items) - (number of gutters * gutter width) / number of items
    width: calc((100% / @n) - ceil((((@n - 1) * @gw) / @n)));
    margin: @gw / 2;
    // Item at start of row.
    &:nth-child(@{n}n - @{n-1}) {
      .ml0;
    }
    // Item at end of row.
    &:nth-child(@{n}n) {
      .mr0;
    }
    // Item in first row.
    &:nth-child(-n + @{n}) {
      .mt0;
    }
    // Item in last row.
    &:nth-last-child(-n + @{n}) {
      .mb0;
    }
  }
}

.grid-1 { .grid(1); }
.grid-2 { .grid(2); }
.grid-3 { .grid(3); }
.grid-4 { .grid(4); }
.grid-5 { .grid(5); }

.grid-1-gutter-4 { .grid-with-gutters(1, @spacing-4); }
.grid-2-gutter-4 { .grid-with-gutters(2, @spacing-4); }
.grid-3-gutter-4 { .grid-with-gutters(3, @spacing-4); }
.grid-4-gutter-4 { .grid-with-gutters(4, @spacing-4); }
.grid-5-gutter-4 { .grid-with-gutters(5, @spacing-4); }

.grid-4-gutter-6 { .grid-with-gutters(4, @spacing-6); }

.grid-1-gutter-10 { .grid-with-gutters(1, @spacing-10); }
.grid-2-gutter-10 { .grid-with-gutters(2, @spacing-10); }
.grid-3-gutter-10 { .grid-with-gutters(3, @spacing-10); }
.grid-4-gutter-10 { .grid-with-gutters(4, @spacing-10); }
.grid-5-gutter-10 { .grid-with-gutters(5, @spacing-10); }


.desktop({
  .grid-3-d { .grid-3; }
  .grid-4-d { .grid-4; }
  .grid-5-d { .grid-5; }

  .grid-1-gutter-4-d { .grid-with-gutters(1, @spacing-4); }
  .grid-2-gutter-4-d { .grid-with-gutters(2, @spacing-4); }
  .grid-3-gutter-4-d { .grid-with-gutters(3, @spacing-4); }
  .grid-4-gutter-4-d { .grid-with-gutters(4, @spacing-4); }
  .grid-5-gutter-4-d { .grid-with-gutters(5, @spacing-4); }

  .grid-4-gutter-6-d { .grid-4-gutter-6; }

  .grid-1-gutter-10-d { .grid-with-gutters(1, @spacing-10); }
  .grid-2-gutter-10-d { .grid-with-gutters(2, @spacing-10); }
  .grid-3-gutter-10-d { .grid-with-gutters(3, @spacing-10); }
  .grid-4-gutter-10-d { .grid-with-gutters(4, @spacing-10); }
  .grid-5-gutter-10-d { .grid-with-gutters(5, @spacing-10); }
});

.tablet-only({
  .grid-1-t { .grid(1); }
  .grid-2-t { .grid-2; }
  .grid-3-t { .grid-3; }

  .grid-1-gutter-4-t { .grid-with-gutters(1, @spacing-4); }
  .grid-2-gutter-4-t { .grid-with-gutters(2, @spacing-4); }
  .grid-3-gutter-4-t { .grid-with-gutters(3, @spacing-4); }
  .grid-4-gutter-4-t { .grid-with-gutters(4, @spacing-4); }
  .grid-5-gutter-4-t { .grid-with-gutters(5, @spacing-4); }

  .grid-1-gutter-10-t { .grid-with-gutters(1, @spacing-10); }
  .grid-2-gutter-10-t { .grid-with-gutters(2, @spacing-10); }
  .grid-3-gutter-10-t { .grid-with-gutters(3, @spacing-10); }
  .grid-4-gutter-10-t { .grid-with-gutters(4, @spacing-10); }
  .grid-5-gutter-10-t { .grid-with-gutters(5, @spacing-10); }
});

.mobile({
  .grid-1-m { .grid-1; }
  .grid-2-m { .grid-2; }

  .grid-1-gutter-4-m { .grid-with-gutters(1, @spacing-4); }
  .grid-2-gutter-4-m { .grid-with-gutters(2, @spacing-4); }
  .grid-3-gutter-4-m { .grid-with-gutters(3, @spacing-4); }
  .grid-4-gutter-4-m { .grid-with-gutters(4, @spacing-4); }
  .grid-5-gutter-4-m { .grid-with-gutters(5, @spacing-4); }

  .grid-1-gutter-10-m { .grid-with-gutters(1, @spacing-10); }
  .grid-2-gutter-10-m { .grid-with-gutters(2, @spacing-10); }
  .grid-3-gutter-10-m { .grid-with-gutters(3, @spacing-10); }
  .grid-4-gutter-10-m { .grid-with-gutters(4, @spacing-10); }
  .grid-5-gutter-10-m { .grid-with-gutters(5, @spacing-10); }
});
