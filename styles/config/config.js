import { _includes, _mapValues } from 'libraries/lodash'
import styleConfig from 'styles/config/config.json'

const stripPx = val => Number(val.replace('px', ''))

export default _mapValues(
  styleConfig,
  val => (_includes(val, 'px') ? stripPx(val) : val)
)
