import { of } from 'rxjs'
import {
  RESPONSE_PENDING,
  RESPONSE_OFFLINE,
  RESPONSE_BADRESPONSE,
  RESPONSE_BADDATA,
  RESPONSE_NOTFOUND,
  RESPONSE_UNAUTHORIZED,
  RESPONSE_FORBIDDEN,
  PAYMENT_REQUIRED
} from 'front-end-core/constants/responses'
import { _find, _identity, _includes, _isUndefined } from 'libraries/lodash'

const invalidResponses = [
  RESPONSE_PENDING,
  RESPONSE_OFFLINE,
  RESPONSE_BADRESPONSE,
  RESPONSE_BADDATA,
  RESPONSE_NOTFOUND,
  RESPONSE_UNAUTHORIZED,
  RESPONSE_FORBIDDEN,
  PAYMENT_REQUIRED
]

const invalidData = response => {
  return _includes(invalidResponses, response.statusCode)
}

export const isValidResponse = response => {
  return !_includes(invalidResponses, response.statusCode)
}

export const mapResponse = (map, culpritMap = _identity) => {
  return (...args) => {
    const culprit = _find(args, invalidData)
    if (!_isUndefined(culprit)) {
      return culpritMap(culprit)
    }
    return map(...args)
  }
}

export const flatMapResponse = map => mapResponse(map, culprit => of(culprit))

export const mergeResponses = mapResponse(Object.assign.bind(Object, {}))
