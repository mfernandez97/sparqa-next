import { Observable, BehaviorSubject, Subject, of, forkJoin, never, merge } from 'rxjs'
import { tap, map, catchError, debounceTime, startWith, delay, takeUntil, filter, pluck, skip, first, distinctUntilChanged } from 'rxjs/operators'
import Route from 'route-parser'
import { _isEqual, _includes, _isString, _isFunction, _isArray, _split, _map, _identity, _has } from 'libraries/lodash'
import { parse } from 'qs'
import { toQueryString } from 'front-end-core/core/url'
import debug from 'front-end-core/core/debug'
import { RESPONSE_BADDATA, RESPONSE_OFFLINE, RESPONSE_PENDING } from 'front-end-core/constants/responses'
import { REQUEST_TIMEOUT } from 'front-end-core/constants/request'
import { API_HOST } from 'front-end-core/constants/urls'
import { mapResponse } from 'core/api/dataflow'

export class ApiRequest extends Subject {
  constructor (params, transform, transformInvalid, deps) {
    super()

    Object.assign(this, {
      deps,
      params,
      count: 0,
      Repeater: new Subject()
    })

    const Response = this.createRequest().pipe(
      tap(response => this.verifyResponse(response)),
      map(mapResponse(transform, transformInvalid)),
      catchError(e => of({ statusCode: RESPONSE_BADDATA }))
    )

    merge(
      Response,
      this.deps.Online
        .pipe(
          map(x => x ? { statusCode: RESPONSE_PENDING } : { statusCode: RESPONSE_OFFLINE })
        )
    )
      .pipe(debounceTime(5))
      .subscribe(response => this.next(response))
  }

  createRequest () {
    return this.deps.observeRequest(
      this.params,
      this.Repeater.pipe(startWith(true))
    )
  }

  verifyResponse (response) {
    // if the user is offline...
    if (response.statusCode === RESPONSE_OFFLINE) {
      // ... then wait a bit ...
      this.count = this.count + 1
      const timeDelay = Math.min(Math.pow(this.count, 2) * 100, 30000)
      of(true).pipe(delay(timeDelay)).subscribe(() => {
        // ... and trigger the repeater (which is the trigger for observeRequest's fetch)
        this.Repeater.next(true)
      })
      debug.warn(`Request to ${this.params.url} failed. Retry #${this.count} in ${timeDelay / 1000}s`)
    }
  }

  retry () {
    // Trigger for observeRequest
    this.Repeater.next(true)
  }

  get output () {
    return this.pipe(
      takeUntil(
        this.pipe(
          filter(response => {
            return !_includes([RESPONSE_PENDING, RESPONSE_OFFLINE], response.statusCode)
          }),
          delay(5)
        ),
        startWith({ statusCode: RESPONSE_PENDING })
      )
    )
  }
}

export class Api extends BehaviorSubject {
  constructor (routes, deps) {
    super({})
    this.routes = _isArray(routes)
      ? Object.assign({}, ...routes.map(r => ({ [r.name]: r.def })))
      : routes

    Object.assign(this, { deps, requests: {} })
  }

  mergeResponseValue (url, data) {
    const mergedValue = Object.assign(
      this.value,
      { [url]: data }
    )
    this.next(mergedValue)
  }

  getRequestProps (name, values, query, queryModifier = {}) {
    const [url, transform, transformInvalid] = this.routes[name]
    const route = new Route(url)
    const [path, queryString] = _split(route.reverse(values), '?')

    if (path === false) {
      debug.warn(`Incorrect route parameters for ${name} (def: ${url})`, values)
      return { params: false, transform, transformInvalid }
    }

    const basePath = API_HOST + path
    const defaultQuery = parse(queryString)

    return {
      params: {
        timeout: REQUEST_TIMEOUT,
        url: basePath + toQueryString({ ...defaultQuery, ...query, ...queryModifier }),
        storeUrl: basePath + toQueryString({ ...defaultQuery, ...query })
      },
      transform,
      transformInvalid
    }
  }

  refreshRoute = (name, values = {}, query = {}, queryModifier = {}) => {
    const { params, transform, transformInvalid } = this.getRequestProps(name, values, query, queryModifier)

    if (params !== false) {
      new ApiRequest(params, transform, transformInvalid, this.deps)
        .output
        .pipe(filter(response => response.statusCode !== RESPONSE_PENDING))
        .subscribe(data => this.mergeResponseValue(params.storeUrl, data))
    }

    return this.pipe(
      pluck(params.storeUrl),
      skip(1),
      first()
    )
  }

  refresh = (name, values = {}, query = {}, queryModifier = {}) => {
    if (values === '*') {
      // Get the route definition.
      const [definition] = this.routes[name]
      // Look for any existing cached routes that match the definition.
      const matches = _map(
        this.requests,
        (request, storeUrl) => new Route(definition).match(storeUrl)
      ).filter(values => values)
      // Run the refresh function on all of them and combine the results.
      return forkJoin(
        matches.map(values => {
          return this.refreshRoute(name, values, query, queryModifier)
        })
      )
    }
    // Run the refresh function on a single route.
    return this.refreshRoute(name, values, query, queryModifier)
  }

  fetch = (name, values = {}, query = {}, queryModifier = {}) => {
    const { params, transform, transformInvalid } = this.getRequestProps(name, values, query, queryModifier)

    if (params === false) {
      return never()
    }
    return Observable.create(observer => {
      // If the request hasn't already been made...
      if (!_has(this.requests, params.storeUrl)) {
        // ... then add it to requests store
        this.mergeResponseValue(params.storeUrl, { statusCode: RESPONSE_PENDING })
        this.requests[params.storeUrl] = new ApiRequest(params, transform, transformInvalid, this.deps)
        this.requests[params.storeUrl].output.subscribe(data => {
          return this.mergeResponseValue(params.storeUrl, data)
        })
      }
      this.pipe(
        pluck(params.storeUrl),
        distinctUntilChanged(_isEqual)
      ).subscribe(observer)
    })
  }

  observe = (name, values = {}, query = {}, memory = true) => {
    const { params, transform, transformInvalid } = this.getRequestProps(name, values, query)

    if (params === false) {
      return never()
    }

    if (!_has(this.value, params.url)) {
      this.mergeResponseValue(params.url, { statusCode: RESPONSE_PENDING })
    }

    return Observable.create(observer => {
      const request = this.deps.observeRequest(params).pipe(
        map(mapResponse(transform, transformInvalid)),
        catchError(e => {
          debug.warn(`Error processing data from ${params.url}: ${e.message}`)
          return of({ statusCode: RESPONSE_BADDATA })
        })
      )
      if (memory) {
        request.subscribe(data => {
          this.mergeResponseValue(params.url, data)
        })
        this.pipe(pluck(params.url)).subscribe(observer)
      } else {
        request.subscribe(observer)
      }
    })
  }
}

export class RequestDefinition {
  constructor ({ name = null, url = null, transform = null, transformInvalid = _identity }) {
    if (!_isString(name)) throw new Error('Incorrect request definition')
    if (!_isString(url)) throw new Error('Incorrect request definition')
    if (!_isFunction(transform)) throw new Error('Incorrect request definition')
    if (!_isFunction(transformInvalid)) throw new Error('Incorrect request definition')
    Object.assign(this, { name, url, transform, transformInvalid })
  }

  get def () {
    return [this.url, this.transform, this.transformInvalid]
  }
}
