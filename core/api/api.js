import 'whatwg-fetch'
import Cookies from 'js-cookie'
import { of, from } from 'rxjs'
import { delay, catchError, tap, switchMap } from 'rxjs/operators'
import { _has, _inRange } from 'libraries/lodash'
import {
  RESPONSE_BADRESPONSE,
  RESPONSE_OFFLINE,
  RESPONSE_NOTFOUND,
  RESPONSE_UNAUTHORIZED,
  RESPONSE_FORBIDDEN,
  RESPONSE_OK
} from 'front-end-core/constants/responses'
import debug from 'front-end-core/core/debug'
import { LOGOUT } from 'constants/controllers'
import getUrl from 'front-end-core/core/url'

const checkStatus = async (url, response) => {
  if (response.type === 'opaque' && response.status === 307) {
    const error = new Error('Not logged in.')
    const normalized = await normalizeResponse(response)
    const errorResponse = Object.assign({}, error, {
      ...normalized,
      statusCode: translateStatus(404) // override statusCode
    })
    if (process.env.NODE_ENV === 'production') {
      location.href = getUrl(LOGOUT)
    }
    errorLog('Cookies are out of date!')
    throw errorResponse
  } else if (response.ok) {
    return normalizeResponse(response)
  } else {
    const error = new Error(response.statusText)
    const normalized = await normalizeResponse(response)
    const errorResponse = { ...error, ...normalized }
    throw errorResponse
  }
}

const request = params => {
  const { url, method = 'get', headers: _headers = {}, credentials = 'same-origin' } = params
  const headers = method === 'get' ? { mode: 'no-cors', ..._headers } : _headers

  const requestObject = new Request(url, {
    credentials,
    mode: headers.mode,
    headers: new Headers(headers),
    ...params
  })

  return from(
    fetch(requestObject)
      .then(response => checkStatus(url, response))
  ).pipe(
    delay(10),
    catchError(error => of(error)),
    tap(() => {
      if (!loggedIn()) {
        location.href = getUrl(LOGOUT)
      }
    })
  )
}

const translateStatus = status => {
  const numericStatus = Number(status)
  if (_inRange(numericStatus, 200, 300)) { // Checks if n is between start and up to, but not including, end.
    return RESPONSE_OK
  }
  switch (numericStatus) {
    case 408: // Request Timeout
      return RESPONSE_OFFLINE
    case 404:
      return RESPONSE_NOTFOUND
    case 401:
      return RESPONSE_UNAUTHORIZED
    case 403:
      return RESPONSE_FORBIDDEN
    default:
      return RESPONSE_BADRESPONSE
  }
}

export const observeRequest = () => (params, trigger = of(true)) => {
  return trigger.pipe(switchMap(() => request(params)))
}

const responseIsJson = contentType => contentType && contentType.includes('application/json')

const normalizeResponse = async incomingResponse => {
  const contentType = incomingResponse.headers.get('content-type')
  let response = { statusCode: translateStatus(incomingResponse.status) }
  if (responseIsJson(contentType)) {
    const data = await incomingResponse.json()
    response = { ...data, ...response }
  } else {
    const data = await incomingResponse.text()
    response = { data, ...response }
  }
  return response
}

const loggedIn = () => !_has(Cookies.get(), 'fc-rememberMe')

// TODO: move to /core/debug?
const errorLog = message => debug.log(`%c${message}`, 'background: #F55641; color: #FFF; font: 20px/50px arial; padding: 10px 20px;')
