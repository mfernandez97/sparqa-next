/* eslint-env jest */

import Cookies from 'js-cookie'
import {
  RESPONSE_OK,
  RESPONSE_OFFLINE,
  RESPONSE_NOTFOUND,
  RESPONSE_UNAUTHORIZED,
  RESPONSE_FORBIDDEN
} from 'front-end-core/constants/responses'
import { observeRequest as createObserveRequest } from 'core/api/api'

jest.mock('front-end-core/core/url')

const observeRequest = createObserveRequest()
const createMockResponse = (status, json = {}) => {
  return new Response(JSON.stringify(json), {
    headers: new Headers({ 'content-type': 'application/json' }),
    status
  })
}
const createMockTextResponse = (status, text) => {
  return new Response(text, {
    headers: new Headers({ 'content-type': 'text/plain;charset=utf-8' }),
    status
  })
}

describe('observeRequest', () => {
  const mockFetch = (response, force = {}) => {
    window.fetch = jest.fn().mockImplementation(
      () => Promise.resolve(response)
        .then(response => {
          // Can only test 307 response if we force response.type to 'opaque'
          Object.keys(force).forEach(key => {
            response[key] = force[key]
          })
          return response
        })
    )
  }

  const mockData200 = {
    status: 'OK',
    data: {
      id: '-1',
      title: 'Chapters',
      parentId: '',
      bookmarkId: '-1',
      active: true,
      lastUpdate: '201809281426',
      description: null,
      overview: null
    }
  }

  const mockData403 = {
    status: 'Forbidden',
    data: {
      message: 'You don\'t have access to view this section of Q&A',
      code: 'SectionNotAllowed',
      type: 'UnauthorisedError'
    }
  }

  const mockResponse200 = createMockResponse(200, mockData200)
  const mockResponse201 = createMockResponse(201, mockData200)
  const mockResponse307 = createMockResponse(307)
  const mockResponse408 = createMockResponse(408, { status: 'Offline' })
  const mockResponse404 = createMockResponse(404, { status: 'Not Found' })
  const mockResponse401 = createMockResponse(401)
  const mockResponse403 = createMockResponse(403, mockData403)
  const mockResponseText = createMockTextResponse(200, 'text')

  // to clean test output
  const { location } = window
  beforeAll(() => {
    delete window.location
    window.location = { reload: jest.fn() }
  })
  afterAll(() => {
    window.location = location
  })

  beforeEach(() => {
    Cookies.get = jest.fn().mockImplementation(
      () => ({ 'fc-rememberMe': true })
    )
    // Stop console getting really noisy
    console.error = () => null
  })

  it('handles 200 responses', done => {
    mockFetch(mockResponse200)
    observeRequest({})
      .subscribe(response => {
        expect(response.statusCode).toBe(RESPONSE_OK)
        expect(response.status).toBe('OK')
        expect(response.data).toMatchObject(mockData200.data)
        done()
      })
  })

  it('handles 201 responses', done => {
    mockFetch(mockResponse201)
    observeRequest({})
      .subscribe(response => {
        expect(response.statusCode).toBe(RESPONSE_OK)
        expect(response.status).toBe('OK')
        expect(response.data).toMatchObject(mockData200.data)
        done()
      })
  })

  it('handles 307 responses', done => {
    mockFetch(mockResponse307, { type: 'opaque' })
    observeRequest({})
      .subscribe(response => {
        expect(response.statusCode).toBe(RESPONSE_NOTFOUND)
        done()
      })
  })

  it('handles 408 responses', done => {
    mockFetch(mockResponse408)
    observeRequest({})
      .subscribe(response => {
        expect(response.statusCode).toBe(RESPONSE_OFFLINE)
        expect(response.status).toBe('Offline')
        done()
      })
  })

  it('handles 404 responses', done => {
    mockFetch(mockResponse404)
    observeRequest({})
      .subscribe(response => {
        expect(response.statusCode).toBe(RESPONSE_NOTFOUND)
        expect(response.status).toBe('Not Found')
        done()
      })
  })

  it('handles 401 responses', done => {
    mockFetch(mockResponse401)
    observeRequest({})
      .subscribe(response => {
        expect(response.statusCode).toBe(RESPONSE_UNAUTHORIZED)
        done()
      })
  })

  it('handles 403 responses', done => {
    mockFetch(mockResponse403)
    observeRequest({})
      .subscribe(response => {
        expect(response.statusCode).toBe(RESPONSE_FORBIDDEN)
        expect(response.status).toBe('Forbidden')
        expect(response.data).toMatchObject(mockData403.data)
        done()
      })
  })

  it('handles responses with content-type of text', done => {
    mockFetch(mockResponseText)
    observeRequest({})
      .subscribe(response => {
        expect(response.statusCode).toBe(RESPONSE_OK)
        expect(response.data).toBe('text')
        done()
      })
  })
})
