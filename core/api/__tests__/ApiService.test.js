/* eslint-env jest */

import { BehaviorSubject, of } from 'rxjs'
import { delay, switchMap } from 'rxjs/operators'
import { RESPONSE_OK, RESPONSE_OFFLINE, RESPONSE_PENDING } from 'front-end-core/constants/responses'
import { ApiRequest, Api, RequestDefinition } from 'core/api/ApiService'

/**
 * Test helpers
 */
const mockResponseValid = { statusCode: RESPONSE_OK, data: 'success' }
const mockResponseOffline = { statusCode: RESPONSE_OFFLINE }
const mockParams = { timeout: 15000, url: '/api/mock', storeUrl: '/api/mock' }

const mockTransform = response => ({ ...response, mock: 'valid' })
const mockTransformInvalid = response => ({ ...response, mock: 'invalid' })
const mockOnline = new BehaviorSubject(true)

const mockRoutes = [
  new RequestDefinition({
    name: 'Mock',
    url: '/mock',
    transform: mockTransform,
    transformInvalid: mockTransformInvalid
  })
]

const getMockObserveRequest = response => (params, trigger = of(true)) => {
  return trigger.pipe(switchMap(() => of(response).pipe(delay(10))))
}
const getMockDeps = response => ({
  observeRequest: getMockObserveRequest(response),
  Online: mockOnline
})

/**
 * Tests
 */
describe('ApiRequest', () => {
  it('can be initialised successfully', () => {
    const apiRequest = new ApiRequest(
      mockParams,
      mockTransform,
      mockTransformInvalid,
      getMockDeps(mockResponseValid)
    )
    expect(apiRequest.params.timeout).toBe(mockParams.timeout)
    expect(apiRequest.params.url).toBe(mockParams.url)
    expect(apiRequest.params.storeUrl).toBe(mockParams.storeUrl)
    expect(apiRequest.deps.observeRequest).toBeInstanceOf(Function)
    expect(apiRequest.deps.Online).toBeInstanceOf(BehaviorSubject)
  })

  describe('createRequest', () => {
    const apiRequest = new ApiRequest(
      mockParams,
      mockTransform,
      mockTransformInvalid,
      getMockDeps(mockResponseValid)
    )
    it('successfully creates a request', done => {
      apiRequest.createRequest().subscribe(response => {
        expect(response.statusCode).toBe(RESPONSE_OK)
        done()
      })
    })
  })

  describe('verifyResponse', () => {
    // NOTE: verifyResponse is called in the constructor.
    it('does nothing if the response status is not offline', () => {
      const apiRequest = new ApiRequest(
        mockParams,
        mockTransform,
        mockTransformInvalid,
        getMockDeps(mockResponseValid)
      )
      expect(apiRequest.count).toBe(0)
    })

    it('retries a request with an offline response after a timeout', done => {
      const apiRequest = new ApiRequest(
        mockParams,
        mockTransform,
        mockTransformInvalid,
        getMockDeps(mockResponseOffline)
      )
      apiRequest.Repeater.subscribe(() => {
        done()
      })
    })
  })

  describe('retry', () => {
    it('retries a request', done => {
      const apiRequest = new ApiRequest(
        mockParams,
        mockTransform,
        mockTransformInvalid,
        getMockDeps(mockResponseValid)
      )
      let count = 0
      apiRequest.subscribe(() => {
        count = count + 1
        if (count < 2) {
          apiRequest.retry()
        } else {
          done()
        }
      })
    })
  })

  describe('output', () => {
    it('starts with status code of response pending', done => {
      const apiRequest = new ApiRequest(
        mockParams,
        mockTransform,
        mockTransformInvalid,
        getMockDeps(mockResponseValid)
      )
      let count = 0
      apiRequest.output.subscribe(({ statusCode }) => {
        count = count + 1
        if (count === 1) {
          expect(statusCode).toBe(RESPONSE_PENDING)
          done()
        }
      })
    })

    it('keeps being triggered until a status code of something other than pending or offline is returned', done => {
      const apiRequest = new ApiRequest(
        mockParams,
        mockTransform,
        mockTransformInvalid,
        getMockDeps(mockResponseValid)
      )
      apiRequest.output.subscribe(({ statusCode }) => {
        if (statusCode !== RESPONSE_PENDING && statusCode !== RESPONSE_OFFLINE) {
          done()
        }
      })
    })
  })
})

describe('Api', () => {
  it('can be initialised successfully', () => {
    const api = new Api(mockRoutes, {
      Online: mockOnline,
      observeRequest: getMockObserveRequest(mockResponseValid)
    })
    const mockRoute = mockRoutes[0]
    expect(api.routes[mockRoute.name]).toEqual([
      mockRoute.url,
      mockRoute.transform,
      mockRoute.transformInvalid
    ])
    expect(api.deps.observeRequest).toBeInstanceOf(Function)
    expect(api.deps.Online).toBeInstanceOf(BehaviorSubject)
    expect(api.requests).toMatchObject({})
  })
})

describe('RequestDefinition', () => {
  it('can be initialised successfully', () => {
    const requestDefinition = new RequestDefinition({
      name: 'Test',
      url: '/test',
      transform: mockTransform,
      transformInvalid: mockTransformInvalid
    })
    const [url, transform, transformInvalid] = requestDefinition.def
    expect(url).toBe('/test')
    expect(transform).toBeInstanceOf(Function)
    expect(transformInvalid).toBeInstanceOf(Function)
  })

  it('throws an error if incorrectly initialised', () => {
    expect(() => {
      RequestDefinition({ name: 5 })
    }).toThrow()
  })
})
