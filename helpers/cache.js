
export const tempCache = (getValue, cacheTimeout = 200) => {
  let cachedValue
  let timeout
  return () => {
    if (cachedValue === undefined) {
      cachedValue = getValue()
    }
    clearTimeout(timeout)
    timeout = setTimeout(() => {
      cachedValue = undefined
    }, cacheTimeout)
    return cachedValue
  }
}
