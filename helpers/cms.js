import { _has, _get } from 'libraries/lodash'

export const getBlogPostTitle = data => {
  if (_has(data, 'title1')) {
    return _get(data.title1[0], 'text', null)
  }
  return null
}
