import React from 'react'
import { pushUrl } from 'front-end-core/core/history'
import { Error } from 'components/StatusBox'

// Check compares an object value to a schema, which can be:
const checkAgainstSchema = (obj, schema) => {
  if (!obj) return !schema

  if (Array.isArray(schema)) {
    // Either an array of keys that must all return truthey values against the object
    return schema.every(k => !!obj[k])
  } else if (typeof schema === 'function') {
    // A custom function that takes the object as the argument
    return schema(obj)
  } else {
    // Another schema, so we can check keys deeper in the object.
    return Object.keys(schema).every(k => checkAgainstSchema(obj[k], schema[k]))
  }
}

function checkPropsExist (requiredProps, location) {
  return Component => {
    const component = (props) => {
      const propsExist = checkAgainstSchema(
        props,
        typeof requiredProps === 'function'
          ? requiredProps(props)
          : requiredProps
      )

      if (propsExist) {
        return <Component {...props} />
      } else if (location) {
        const url = (typeof location === 'function') ? location(props) : location
        pushUrl(url)
        return null
      } else {
        return <Error title='Error: Required props not found' />
      }
    }

    return component
  }
}

export default checkPropsExist
