import getUrl from 'front-end-core/core/url'
import { replaceUrl as replace, pushUrl as push } from 'front-end-core/core/history'
export * from 'constants/controllers'

export const replaceUrl = function (...args) {
  const url = getUrl(...args)
  return replace(url)
}

export const pushUrl = function (...args) {
  const url = getUrl(...args)
  return push(url)
}
