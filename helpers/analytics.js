import { _includes, _replace } from 'libraries/lodash'

export const labelElement = (answer) => {
  const regex = /<\s*(span|a)[^>]*>(.*?)<\s*\/\s*(span|a)>/g
  const labelled = _replace(answer, regex, (found) => {
    if (_includes(found, 'fc-glossary') || _includes(found, 'reference')) {
      const fragment = document.createRange().createContextualFragment(found)
      const glossary = fragment.querySelector('.fc-glossary')
      const element = fragment.querySelector('.fc-glossary') || fragment.querySelector('.fc-cross-reference') || fragment.querySelector('.fc-document-reference')
      const label = element && (glossary ? element.innerHTML : `${_replace(element.innerHTML, '&amp;', '&')} [${element.href}]`)
      element && element.setAttribute('data-track-label', label)
      return label ? element.outerHTML : found
    }
    return found
  })
  return labelled
}
