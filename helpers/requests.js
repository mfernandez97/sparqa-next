import EasySax from 'easysax'
import { _map, _result, _set, _isObject, _includes, _zipObject, _replace, _isArray } from 'libraries/lodash'
import debug from 'front-end-core/core/debug'
import HTML_TAGS from 'front-end-core/constants/html-tags'
import { labelElement } from './analytics'

const getCrossRefUrl = ({ rootId, questionId = null }) => questionId ? `topics/${rootId}/${questionId}` : `topics/${rootId}`

const getDocRefUrl = ({ documentUuid }) => `document/${documentUuid}/overview`

export const parseJson = json => {
  return eval('(' + json + ')') // eslint-disable-line
}

export const touch = (obj, path, value) => {
  if (!_result(obj, path)) {
    _set(obj, path, value)
  }
  return _result(obj, path)
}

export const deepPush = (obj, path, value) => {
  touch(obj, path, [])
  const target = _result(obj, path)
  if (target.indexOf(value) === -1) {
    target.push(value)
  } else if (console && console.error) {
    console.error(`Duplicate value "${value}" in ${_isArray(path) ? path.join('.') : path}`)
  }
}

const encodeApostrophes = str => {
  return _replace(str, /'/g, '&#39;')
}

const handleParserError = (stack, err) => {
  debug.warn(
    `XML parse error: ${err}, path: ${stack
      .map(s => s.name)
      .join(' > ')}, stack: `,
    stack
  )
}

const toAttributes = attr => !_isObject(attr)
  ? ''
  : _map(attr, (value, key) => `data-${key}='${value}'`).join(' ')

const wordJoiner = str => {
  return str.replace(
    /[)(.:,\];]/gi,
    (match, pos, str) =>
      str.substr(pos - 4, 5) !== '&amp;' && str.substr(pos - 5, 6) !== '&quot;' // this seems like a really bad idea but I'm out of ideas because
        ? `<span class='wj'>&nbsp;</span>${match}` // I'm not really sure what this function is meant to be doing
        : match
  )
}

const htmlTags = _zipObject(HTML_TAGS, _map(HTML_TAGS, () => JSON.parse(JSON.stringify(true))))

export const toHtml = (
  xmlStr,
  tagName = 'span',
  convertAllTags = false,
  ignore = []
) => {
  let output = ''
  const parser = new EasySax()
  const stack = []
  let pageWidth
  parser.on('error', err => handleParserError(stack, err))
  parser.on('startNode', (elem, attr, uq, str, tag) => {
    stack.push(elem)
    if (ignore.indexOf(elem) > -1) {
      // do nothing
    } else if (elem === 'glossary') {
      const popoverContent = encodeApostrophes(attr().definition)
      output += `<${tagName}
        class='fc-${elem}'
        data-track-click
        data-track-category='Glossary'
        data-popover-content='${popoverContent}'>`
    } else if (elem === 'cross-reference') {
      const { rootId, focusedId = null } = attr()
      const href = getCrossRefUrl({ rootId, questionId: focusedId })
      output += `<a
        class='fc-${elem}'
        href='${href}'
        data-track-click
        data-track-category='Cross Reference'
        data-track-label='${href}'
      >`
    } else if (elem === 'document-reference') {
      const { documentUuid } = attr()
      const href = getDocRefUrl({ documentUuid })
      output += `<a
        class='fc-${elem}'
        href='${href}'
        data-track-click
        data-track-category='Cross Reference'
        data-track-label='${href}'
      >`
    } else if (elem === 'colgroup') {
      output += '<colgroup>'
      pageWidth = +attr().pageWidth
    } else if (elem === 'col') {
      const width = +attr().width
      output += `<col width='${(100 * width / pageWidth).toFixed(2)}%'>`
    } else if (!convertAllTags && htmlTags.hasOwnProperty(elem)) { // eslint-disable-line
      output += tag()
    } else {
      output += `<${tagName} class='fc-${elem}' ${toAttributes(attr())}>`
    }
  })
  parser.on('textNode', text => {
    if (_includes(stack, 'cross-reference') || _includes(stack, 'document-reference')) {
      output += text
    } else {
      output += wordJoiner(text)
    }
  })
  parser.on('endNode', (elem, uq, str, tag) => {
    stack.pop()
    if (elem === 'col') return
    if (ignore.indexOf(elem) > -1) {
      // do nothing
    } else if (elem === 'cross-reference') {
      output += '</a>'
    } else if (elem === 'document-reference') {
      output += '</a>'
    } else if (!convertAllTags && htmlTags.hasOwnProperty(elem)) { // eslint-disable-line
      output += tag()
    } else {
      output += `</${tagName}>`
    }
  })
  parser.parse(xmlStr)
  const stripped = _replace(output, /<[^/>][^>]*><\/[^>]+>/g, '')
  return stripped && _includes(output, 'fc-answer') ? labelElement(stripped) : stripped
}

export const toFacet = facet => {
  const selectedValues = {}
  const selected = _map(facet.selected || [], ({ name, count }) => {
    selectedValues[name] = 1
    return { selected: true, name, count, value: name, parentId: 0 }
  })
  const available = _map(facet.available || [], ({ name, count }) => ({
    selected: false,
    name,
    count,
    value: name,
    parentId: 0
  })).filter(facet => !selectedValues[facet.value])
  return {
    name: facet.name,
    collapsed: selected.length > 0,
    options: Array.of(...selected, ...available).filter(v => !!v.value),
    parameterName: facet.parameterName
  }
}
