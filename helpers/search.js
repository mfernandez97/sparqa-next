import getUrl from 'front-end-core/core/url'
import { pushUrl } from 'front-end-core/core/history'
import { TOPICS } from 'constants/categories'
// refactored this out so that its hopefully easier to maintain if we ever change how searching works
const search = (category, query) => pushUrl(getUrl('search', { category: category || TOPICS }, { q: query }))

export default search
