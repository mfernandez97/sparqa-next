const host = process.env.PROXY || window.location.host // eslint-disable-line
export default `https://image-${host}/unsafe(/:dimensions)/:url`
