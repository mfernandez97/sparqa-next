import { pluck, map, distinctUntilChanged } from 'rxjs/operators'
import connect from 'helpers/connect'
import { _isObject } from 'libraries/lodash'
import { observePageState, updatePageState, CurrentState } from 'front-end-core/services/PageStateService'

const getPropStream = (propName, defaultValue, persist) => {
  return persist
    ? CurrentState.pipe(pluck(propName), map(v => v || defaultValue), distinctUntilChanged())
    : observePageState(propName)
}

export default (initialState, persist = false) => {
  if (!_isObject(initialState)) {
    throw new Error('pageState decorator expects its first argument to be an object')
  }

  const setPageState = mergeIn => updatePageState((state) => ({ ...state, ...mergeIn }))

  return connect(
    () =>
      Object.assign(
        {},
        ...Object.keys(initialState).map(propName => ({ [propName]: getPropStream(propName, initialState[propName], persist) }))
      ),
    { ...initialState, setPageState }
  )
}
