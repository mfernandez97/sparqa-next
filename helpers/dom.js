import { _isElement } from 'libraries/lodash'
import { getParentElement } from 'front-end-core/helpers/document'

export const emptyNode = node => {
  while ((node || {}).firstChild) { // same as node && node.firstChild except standardJS stops whining
    node.removeChild(node.firstChild)
  }
  return node
}

export const createNode = (nodeType, options = {}) => {
  const node = document.createElement(nodeType)
  Object.assign(node, options)
  return node
}

export const getNode = condition => {
  const inner = node => {
    if (!node || !_isElement(node)) {
      return false
    }
    if (condition(node)) {
      return node
    }
    return inner(getParentElement(node))
  }
  return inner
}
