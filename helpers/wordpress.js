import { WORDPRESS_ENDPOINT } from 'constants/wordpress'

export const postsUrl = `${WORDPRESS_ENDPOINT}/wp/v2/posts?per_page=:limit`

export const mediaUrl = `${WORDPRESS_ENDPOINT}/wp/v2/media/:id`
