import connect from 'helpers/connect'
import { map, pluck, distinctUntilChanged } from 'rxjs/operators'
import { _isObject, _isFunction } from 'libraries/lodash'
import { updateState, State as CurrentState } from 'front-end-core/services/StateService'
import deepMerge from 'deepmerge'

export default (mapSessionStateToProps, defaultSessionState = {}) => {
  if (!_isFunction(mapSessionStateToProps)) {
    throw new Error('sessionState decorator expects its first argument to be a function')
  } else if (!_isObject(defaultSessionState)) {
    throw new Error('sessionState decorator expects its second argument to be an object')
  }

  const setSessionState = newState => {
    const finalStateToMergeIn = _isFunction(newState)
      ? newState(CurrentState.value)
      : newState

    updateState(finalStateToMergeIn)
  }

  // Deep merge currentState on top of defaultSessionState and set state
  const sessionStateWithDefaults = deepMerge.all([
    {},
    defaultSessionState,
    CurrentState.value
  ])

  // Set session state to contain defaults
  updateState(sessionStateWithDefaults)

  // Observe the entire state
  // Map with mapSessionStateToProps before we output the result to connect
  const $propsFromSessionState = CurrentState.pipe(map(mapSessionStateToProps))

  return connect(
    () => Object.assign(
      {},
      ...Object.keys(mapSessionStateToProps(sessionStateWithDefaults)).map(propName => ({
        [propName]: $propsFromSessionState.pipe(pluck(propName), distinctUntilChanged())
      }))
    ),
    { setSessionState }
  )
}
