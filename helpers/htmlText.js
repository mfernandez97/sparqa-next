
/* extract and return pure text string from html (stolen from FC) */
export const htmlText = (html) => {
  const cleanExtract = (new DOMParser()).parseFromString(html, 'text/html').documentElement.textContent.trim()
  return cleanExtract
    .replace(/\u00A0/g, '') // changing nbsp to ' ' causes issues so trimming that specially
    .replace(/\s/g, ' ')
}
