import cx from 'classnames'
import { _keys, _pick, _values, _isEmpty, _startsWith } from 'libraries/lodash'
import Cookies from 'js-cookie'
import { PARTNER_COOKIE } from 'constants/cookies'
import { getBaseUrl } from 'front-end-core/core/history'
import getUrl from 'front-end-core/core/url'

const percent = (value, percentValue) => value * (percentValue / 100)

export const percentOff = (value, percentValue) => {
  return value - percent(value, percentValue || 0)
}

export const formatCurrency = (value, inPence = false) => {
  const amount = inPence ? Number(value) / 100 : value
  return amount % 1 === 0 ? amount.toString() : amount.toFixed(2).toString()
}

export const copyToClipboard = text => {
  return navigator.clipboard.writeText(text).then(
    () => true,
    () => false
  )
}

export const renameKeys = (object, keysMap) => (
  _keys(object).reduce((acc, currentValue) => ({
    ...acc,
    ...{ [keysMap[currentValue] || currentValue]: object[currentValue] }
  }), {})
)

export const isExternalLink = link => link && !_isEmpty(link.match(/https?:\/\//))

export const getClassName = (defaults, props) => {
  const overrides = _pick(props, _keys(defaults))
  const result = { ...defaults, ...overrides }
  return cx(props.className || '', _values(result))
}

export const getPartnerCode = () => Cookies.get(PARTNER_COOKIE) || undefined

export const formatFiltersAsQuery = filters => {
  return filters.reduce((result, { type, name }) => {
    if (result[type]) {
      result[type] = result[type].concat([name])
    } else {
      result[type] = [name]
    }
    return result
  }, {})
}

export const getUrlWithQuery = ({ controller, params, query }) => {
  const path = _startsWith(location.href, getBaseUrl())
    ? location.href.substr(getBaseUrl().length)
    : location.pathname
  return getUrl(controller, { path, ...params }, query)
}

export const stripTags = html => {
  const { body } = new DOMParser().parseFromString(html, 'text/html') // eslint-disable-line
  return body.innerText
}

export const currentYear = new Date().getFullYear()
