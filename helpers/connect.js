import { Component, createElement } from 'react'
import { Subject, merge } from 'rxjs'
import { map, startWith, scan, distinctUntilChanged, takeUntil } from 'rxjs/operators'
import { _isEqual } from 'libraries/lodash'
import debug from 'front-end-core/core/debug'

export default (propsToObservables, defaultProps = {}) => (WrappedComponent) => {
  if (typeof propsToObservables !== 'function') {
    throw new Error('connect decorator expects first argument to be a function')
  }

  class Connect extends Component {
    state = defaultProps
    componentIsMounted = false

    constructor (props) {
      super(...arguments)
      this.$state = new Subject()
      this.$update = new Subject()
      this.subscription = this.$state.pipe(
        startWith(defaultProps),
        scan((acc, curr) => ({ ...acc, ...curr })),
        distinctUntilChanged(_isEqual)
      ).subscribe(this.mergeStreamIntoState)
      this.subscribe(props)
    }

    componentDidMount () {
      this.componentIsMounted = true
    }

    componentWillUnmount () {
      this.$update.next(true)
      this.subscription.unsubscribe()
    }

    subscribe (props) {
      this.toStream(propsToObservables(props)).pipe(
        takeUntil(this.$update)
      ).subscribe(any => this.$state.next(any))
    }

    shouldComponentUpdate (props, state) {
      return !_isEqual(props, this.props) || !_isEqual(state, this.state)
    }

    componentDidUpdate (prevProps) {
      if (!_isEqual(prevProps, this.props)) {
        this.$update.next(true)
        this.subscribe(this.props)
      }
    }

    toStream = (obj) => {
      return merge(
        ...Object.keys(obj).map(key => obj[key].pipe(map(value => ({ [key]: value }))))
      )
    }

    mergeStreamIntoState = (state) => {
      if (this.componentIsMounted) {
        this.setState(state)
      } else {
        this.state = { ...this.state, ...state }
      }
    }

    componentDidCatch (error, info) {
      debug.error(error, info)
    }

    render () {
      const props = { ...this.props, ...this.state }
      if (Component.isPrototypeOf(WrappedComponent)) { // eslint-disable-line
        return createElement(WrappedComponent, props)
      } else {
        return WrappedComponent(props)
      }
    }
  }

  Connect.displayName = `RxConnect(${getDisplayName(WrappedComponent)})`

  return Connect
}

function getDisplayName (WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component'
}
