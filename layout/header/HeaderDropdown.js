import React, { useState } from 'react'
import cx from 'classnames'
import connect from 'helpers/connect'
import { $toggle } from 'services/WindowService'
import Link from 'components/Link'
import Icon from 'components/Icon'

const FeaturesDropdown = connect(({ toggleName }) => ({
  showFeaturesDropDown: $toggle({
    selectNode: node => node.hasAttribute('data-toggle') && node.getAttribute('data-toggle') === toggleName,
    excludeNode: node => node.hasAttribute('data-avoid-toggle') && node.getAttribute('data-avoid-toggle') === toggleName
  })
}))(({ items, displayName, toggleName, showFeaturesDropDown }) => (
  <div className='relative pa0 ma0'>
    <div
      className={cx('pointer bg-transparent fs mh3 dark-slate-blue hover-dark-slate-grey nowrap flex')}
      data-toggle={toggleName}
    >
      {displayName} <Icon name='chevron-down' />
    </div>
    {showFeaturesDropDown
      ? (
        <div className='features-menu pa0 ma0 features-menu-top-arrow absolute left-0 z115'>
          <div className='ma0 flex flex-column bg-white br2 items-stretch overflow-hidden shadow-3-black-fade-20 min-w-image-5'>
            {items.map((item, index) => (
              <Item key={index} {...{ ...item, toggleName }} />
            ))}
          </div>
        </div>
      )
      : null}
  </div>
))

export default FeaturesDropdown

const Item = ({ index, controller, params, text, depth, childItems = [], toggleName }) => {
  const [open, setOpen] = useState(false)
  const baseClassName = cx('w-100 fs pr6 pv2 dark-slate-grey pointer lh-copy hover-white hover-bg-dark-slate-blue', {
    pl6: depth === 0,
    'pl8 bg-moon-grey': depth > 0
  })

  if (childItems.length > 0) {
    return (
      <>
        <button
          onClick={() => setOpen(!open)}
          className={cx(baseClassName, 'tl flex items-center justify-start')}
          data-avoid-toggle={toggleName}
        >
          {text} <Icon name={open ? 'chevron-right' : 'chevron-down'} />
        </button>
        <div className={cx('flex-column', { dn: !open, flex: open })}>
          {childItems.map((item, index) => <Item key={index} {...item} />)}
        </div>
      </>
    )
  }

  return (
    <Link
      key={index}
      controller={controller}
      params={params}
      className={cx(baseClassName, 'no-underline w-100')}
    >
      {text}
    </Link>
  )
}
