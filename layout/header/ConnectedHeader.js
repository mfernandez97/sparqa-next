import React from 'react'
import { map } from 'rxjs/operators'
import connect from 'helpers/connect'
import { Media, $mobileMenuVisible, $mobileSearchMode } from 'services/WindowService'
import { $isLogged, $user } from 'services/UserService'
import Header from './Header'

export default connect(() => ({
  mobile: Media.Mobile,
  tablet: Media.Tablet,
  user: $user,
  isLogged: $isLogged.pipe(map(({ isLogged }) => isLogged)),
  mobileMenuVisible: $mobileMenuVisible,
  mobileSearch: $mobileSearchMode
}))((props) => <Header {...props} />)
