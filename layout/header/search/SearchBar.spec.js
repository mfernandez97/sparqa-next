import React from 'react'
import { mount } from 'enzyme'
import SearchBar from './SearchBar'
import SearchMenuDropdown from './SearchMenuDropdown'

jest.mock('./SearchMenuDropdown')

describe('SearchBar component', () => {
  let Component
  const onSubmit = jest.fn()
  beforeAll(() => {
    Component = mount(<SearchBar {...{ onSubmit }} />)
  })
  afterAll(() => {
    Component.unmount()
  })
  it('should render the SearchMenuDropdown component', () => {
    expect(Component.find(SearchMenuDropdown)).toHaveLength(1)
  })
  it('should render a form element', () => {
    expect(Component.find('form')).toHaveLength(1)
  })
})
