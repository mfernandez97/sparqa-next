import React, { useEffect, useRef, useState } from 'react'
import cx from 'classnames'
import Button from '@material-ui/core/Button'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import { isEmpty } from 'lodash'

export const MENU_ITEMS = [
  { title: 'Documents', category: 'documents' },
  { title: 'Toolkits', category: 'toolkits' },
  { title: 'Guidance', category: 'topics' }
]

const SearchMenuDropdown = ({
  category,
  searchCategory,
  setSearchCategory,
  searchStyle
}) => {
  const [anchorEl, setAnchorEl] = useState(null)
  const buttonRef = useRef()

  const handleClick = event => setAnchorEl(event.currentTarget)
  const handleClose = () => setAnchorEl(null)
  const handleOnFocus = () => isEmpty(anchorEl) && buttonRef.current.blur()

  useEffect(() => setAnchorEl(null), [category])

  return (
    <div style={{ height: '2.5rem' }}>
      <Button
        ref={buttonRef}
        onFocus={handleOnFocus}
        aria-controls='customized-menu'
        aria-haspopup='true'
        variant='contained'
        color='primary'
        style={{ paddingLeft: 8, paddingRight: 4 }}
        className='relative'
        classes={{ root: cx('h-100 outline-0 pv2', searchStyle), contained: 'shadow-none' }}
        onClick={handleClick}
      >
        <ButtonImage {...{ searchCategory }} />
        <ArrowDropDownIcon className='white' />
      </Button>
      <Menu
        classes={{ list: 'flex flex-column' }}
        PaperProps={{ square: true }}
        elevation={4}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {MENU_ITEMS.map(({ title, category }) => (
          <MenuItem
            key={title}
            onClick={() => {
              setSearchCategory(category)
              handleClose()
            }}
            classes={{ gutters: 'ph6 pv3' }}
          >
            <ListItemIcon
              classes={{ root: 'black min-w-icon-7' }}
              style={{ minWidth: '2.5rem' }}
            >
              <ListItemImage {...{ category }} />
            </ListItemIcon>
            <ListItemText primary={title} />
          </MenuItem>
        ))}
      </Menu>
    </div>
  )
}

const ListItemImage = ({ category }) => {
  switch (category) {
    case 'documents':
      return <img src='assets/grid-icons/png/templates-alt-2.png' className='h-icon-7 h-icon-5-m w-icon-7 w-icon-5-m' />
    case 'toolkits':
      return <img src='assets/grid-icons/png/setting-up-alt-2.png' className='h-icon-7 h-icon-5-m w-icon-7 w-icon-5-m' />
    case 'topics':
      return <img src='assets/grid-icons/png/qna.png' className='h-icon-7 h-icon-5-m w-icon-7 w-icon-5-m' />
    default :
      return null
  }
}

const ButtonImage = ({ searchCategory }) => {
  switch (searchCategory) {
    case 'documents':
      return <img src='assets/grid-icons/png/templates-alt.png' className='white w-icon-7 w-icon-5-m' />
    case 'toolkits':
      return <img src='assets/grid-icons/png/setting-up-alt.png' className='white w-icon-7 w-icon-5-m' />
    case 'topics':
      return <img src='assets/grid-icons/png/qna-alt.png' className='white w-icon-7 w-icon-5-m' />
    default:
      return null
  }
}

export default SearchMenuDropdown
