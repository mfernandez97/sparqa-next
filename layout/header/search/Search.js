import React, { createRef, useContext, useEffect, useState } from 'react'
import cx from 'classnames'
import getUrl from 'front-end-core/core/url'
import { pushUrl } from 'front-end-core/core/history'
import { _trim } from 'libraries/lodash'
import { LayoutContext } from '../../Main'
import * as categories from 'constants/categories'
import { DEFAULT_CATEGORY } from 'constants/search'

import SearchBar from './SearchBar'

const Search = ({ setPageState, searchQuery, mobile = false, tablet }) => {
  const { activeCategory, activeSubCategory } = useContext(LayoutContext)
  const handleEdit = e => setPageState({ searchQuery: e.target.value })
  const inputRef = createRef()
  const handleSubmit = category => e => {
    e.preventDefault()
    const q = _trim(searchQuery)
    if (q) {
      pushUrl(getUrl('search', { category: category || categories.TOPICS }, { q }))
    } else {
      inputRef.current.focus()
    }
  }
  const desktop = !(mobile || tablet)
  const [searchCategory, setSearchCategory] = useState(activeCategory || DEFAULT_CATEGORY)

  useEffect(() => {
    setSearchCategory(activeCategory || DEFAULT_CATEGORY)
  }, [activeCategory, setSearchCategory])

  useEffect(() => {
    if (activeSubCategory) {
      setSearchCategory(activeSubCategory)
    }
  }, [activeSubCategory, setSearchCategory])

  return (
    <div
      className={cx(
        'items-center flex grow-1 h2-m w-100 mh4'
      )}
    >
      <SearchBar
        {...{ desktop, tablet, mobile, searchQuery, inputRef, searchCategory, setSearchCategory }}
        handleSubmit={handleSubmit(searchCategory)}
        onEdit={handleEdit}
        category={activeCategory}
      />
    </div>
  )
}

export default Search
