import React from 'react'
import pageState from 'helpers/pageState'
import Search from './Search'

export default pageState({ searchQuery: '' }, true)(props => <Search {...props} />)
