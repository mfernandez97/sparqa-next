import React from 'react'

const getPlaceHolderFromSearchCategory = (searchCategory) => {
  switch (searchCategory) {
    case 'documents':
      return 'Search Documents'
    case 'toolkits':
      return 'Search Toolkits'
    case 'topics':
      return 'Search Guidance'
    default:
  }
}

const SearchBox = ({ inputClassname, inputRef, searchCategory, searchQuery, onEdit }) => {
  const placeholder = getPlaceHolderFromSearchCategory(searchCategory)
  return (
    <input
      className={inputClassname}
      ref={inputRef}
      type='text'
      placeholder={placeholder}
      value={searchQuery}
      onChange={onEdit}
    />
  )
}

export default SearchBox
