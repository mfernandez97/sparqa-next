import React from 'react'
import cx from 'classnames'
import { _trim } from 'libraries/lodash'
import Button from '@material-ui/core/Button'
import SearchIcon from '@material-ui/icons/Search'
import SearchBox from './SearchBox'
import SearchMenuDropdown from './SearchMenuDropdown'

const SearchBar = ({
  handleSubmit,
  onEdit,
  searchQuery,
  inputRef,
  desktop,
  tablet,
  category,
  searchCategory,
  setSearchCategory
}) => {
  const dataAttributes = desktop ? {} : { 'data-mobile-search': true }
  const inputClassname = cx(
    'w-100 bn fs fw5 pv4 ml4 pr6 lh-copy b-t fm-t fs-m bg-transparent',
    'dark-slate-grey placeholder-mid-grey-fade-60',
    { 'outline-0': desktop || tablet },
    { 'search-bar-pl': false }
  )
  return (
    <div className='w-100 flex items-center search-box' style={{ height: '2.5rem' }}>
      <SearchMenuDropdown {...{ setSearchCategory, searchCategory, category }} />
      <form
        className='ba b--light-silver w-100'
        style={{ height: '2.5rem' }}
        onSubmit={handleSubmit}
        data-track-submit
        data-track-category='Search'
        data-track-label={_trim(searchQuery)}
        {...dataAttributes}
      >
        <SearchBox {...{ inputClassname, inputRef, searchCategory, searchQuery, onEdit }} />
      </form>
      <Button
        className='white w9 ph0 w6-m pointer z10'
        style={{ height: '2.5rem' }}
        classes={{ root: 'br0' }}
        variant='outlined'
        onClick={handleSubmit}
      >
        <SearchIcon className='w-icon-7 w-icon-5-m' />
      </Button>
    </div>
  )
}

export default SearchBar
