import React from 'react'
import { mount } from 'enzyme'
import Header from './Header'
import Covid19Banner from 'components/Covid19Banner'
import CreditBanner from 'components/CreditBanner'
import FestiveBanner from 'components/FestiveBanner'
import PartnershipBanner from 'components/CMS/PartnershipBanner'
import Search from './search/Search'
import { ButtonContainer } from './Buttons'
import Hamburger from 'components/Hamburger'
import MobileMenu from 'components/MobileMenu'
import ResponseConsumer from 'components/ResponseConsumer'

jest.mock('./AccountMenu')
jest.mock('components/Button')
jest.mock('components/Covid19Banner')
jest.mock('components/CreditBanner')
jest.mock('./FeaturesDropdown')
jest.mock('components/FestiveBanner')
jest.mock('components/Hamburger')
jest.mock('./HeaderDropdown')
jest.mock('components/Link')
jest.mock('components/Logo')
jest.mock('components/MobileMenu')
jest.mock('components/CMS/PartnershipBanner')
jest.mock('./search/Search')
jest.mock('./Buttons')
jest.mock('components/Hamburger')
jest.mock('components/ResponseConsumer')

describe('Header component', () => {
  let Component
  beforeAll(() => {
    Component = mount(<Header headerVisible />)
  })
  afterAll(() => {
    Component.unmount()
  })

  it('should render the banners', () => {
    const banners = [
      Covid19Banner,
      CreditBanner,
      FestiveBanner,
      PartnershipBanner
    ]
    banners.forEach(banner => {
      expect(Component.find(banner)).toHaveLength(1)
    })
  })

  describe('On desktop', () => {
    beforeAll(() => {
      Component = mount(<Header {...{ headerVisible: true, mobile: false, tablet: false }} />)
    })
    afterAll(() => {
      Component.unmount()
    })
    it('should render the search component', () => {
      expect(Component.find(Search)).toHaveLength(1)
    })
    it('should not render the hamburger menu', () => {
      expect(Component.find(Hamburger)).toHaveLength(0)
      expect(Component.find(MobileMenu)).toHaveLength(0)
    })
    // The buttons used in the Header are rendered by ResponseConsumer so I'm
    // checking that it's called and that it's called with the correct props.
    it('should render ResponseConsumer with the correct props', () => {
      const rc = Component.find(ResponseConsumer)
      expect(rc).toHaveLength(1)
      expect(rc.prop('notfoundComponent')).toBe(ButtonContainer)
      expect(rc.prop('unauthorizedComponent')).toBe(ButtonContainer)
    })
    it('should render correctly on desktop', () => {
      expect(Component).toMatchSnapshot()
    })
  })

  describe('On tablet', () => {
    beforeAll(() => {
      Component = mount(<Header {...{ headerVisible: true, tablet: true }} />)
    })
    afterAll(() => {
      Component.unmount()
    })
    it('should render the search component', () => {
      expect(Component.find(Search)).toHaveLength(1)
    })
    it('should render the hamburger menu', () => {
      expect(Component.find(Hamburger)).toHaveLength(1)
      expect(Component.find(MobileMenu)).toHaveLength(1)
    })
    it('should not render the register, login or account buttons', () => {
      expect(Component.find(ResponseConsumer)).toHaveLength(0)
    })
    it('should render correctly on tablet', () => {
      expect(Component).toMatchSnapshot()
    })
  })
})
