import React from 'react'
import Button, { themes, themeModifiers } from 'components/Button'
import { REGISTER, LOGIN } from 'constants/controllers'
import cx from 'classnames'

const RegisterButton = () => (
  <Button
    link
    size='small'
    theme={themes.DARK_SLATE_GREY}
    themeModifier={themeModifiers.INVERT}
    display='di'
    margin='mr2'
    padding='ph7 pv4'
    br=''
    shadow=''
    controller={REGISTER}
    data-track-click
    data-track-category='Session'
    data-track-action='Register'
    data-track-label='Navbar'
  >
      Register
  </Button>
)

const LoginButton = () => (
  <Button
    link
    size='small'
    theme={themes.LIGHT_SEA_GREEN}
    themeModifier={themeModifiers.INVERT}
    color='dark-slate-grey'
    margin='ml2'
    padding='ph7 pv4'
    display='di'
    br=''
    shadow=''
    controller={LOGIN}
    params={{ path: location.pathname.substr(1) }}
  >
      Log in
  </Button>
)

export const AccountButton = ({ toggleDesktopMenuVisible = null, proxy = false }) => (
  <button
    className={cx(
      { 'hidden': proxy },
      'pointer bg-royal-blue-3 fw6 alice-blue fs no-underline pv5 ph6 nowrap hover-white-fade-40'
    )}
    onClick={() => toggleDesktopMenuVisible()}
  >
    My Account
  </button>
)

export const ButtonContainer = () => (
  <div className='landing-heading-buttons'>
    <RegisterButton />
    <LoginButton />
  </div>
)
