import React from 'react'

export const RegisterButton = jest.fn().mockImplementation(() => {
  return <div>REGISTER BUTTON</div>
})

export const AccountButton = jest.fn().mockImplementation(() => {
  return <div>ACCOUNT BUTTON</div>
})

export const LoginButton = jest.fn().mockImplementation(() => {
  return <div>LOGIN BUTTON</div>
})
