import React, { useEffect, useState } from 'react'
import cx from 'classnames'
import { TOPICS, ASK_A_LAWYER } from 'constants/categories'
import Hidden from '@material-ui/core/Hidden'
import Covid19Banner from 'components/Covid19Banner'
import CreditBanner from 'components/CreditBanner'
import FestiveBanner from 'components/FestiveBanner'
import Hamburger from 'components/Hamburger'
import Link, { REGISTER, TOPICSMENU, DOCUMENTSMENU, NOTFOUND, ZIRCON, ABOUT } from 'components/Link'
import Logo from 'components/Logo'
import MobileMenu from 'components/MobileMenu'
import PartnershipBanner from 'components/CMS/PartnershipBanner'
import AccountMenu from './AccountMenu'
import { AccountButton, ButtonContainer } from './Buttons'
import FeaturesDropdown from './FeaturesDropdown'
import HeaderDropdown from './HeaderDropdown'
import Search from './search'
import ResponseConsumer from 'components/ResponseConsumer'

export default (props) => {
  const {
    mobile, tablet, isLogged, mobileMenuVisible, headerBackground, mobileSearch, setIsPartnerBannerVisible,
    headerVisible, ExtendedHeader, pageController, pageSection, activeCategory, user
  } = props
  const isDesktop = !(mobile || tablet)
  const [deskTopMenuVisible, setDeskTopMenuVisible] = useState(false)
  const [isMobileMenuActive, setIsMobileMenuActive] = useState(false)

  const toggleDesktopMenuVisible = () => setDeskTopMenuVisible(!deskTopMenuVisible)

  useEffect(() => {
    setIsMobileMenuActive(mobileMenuVisible)
  }, [mobileMenuVisible, setIsMobileMenuActive])

  return !headerVisible ? null : (
    <>
      {/* This catches any click not in the header and automatically closes the account menu */}
      {deskTopMenuVisible && <div className='absolute top0 left0 vh-100 vw-100' onClick={toggleDesktopMenuVisible} />}
      <div className='relative shadow-3-black-fade-10 z115'>
        <header
          className={cx(
            'relative z120-t',
            { 'shadow-3-black-fade-10': isMobileMenuActive },
            headerBackground,
            { 'bg-white': !headerBackground }
          )}
          onClick={() => deskTopMenuVisible && toggleDesktopMenuVisible()}
          // handles clicks on the header while the account menu is open and closes it
        >
          <Covid19Banner />
          <FestiveBanner />
          <PartnershipBanner {...{ isLogged, setIsPartnerBannerVisible }} />
          <CreditBanner />
          <div
            className={cx(
              'layout-width-constraint ph4-m flex items-center justify-between pv4',
              { pv6: isDesktop }
            )}
          >
            <Logo className='hover-filter-o-40 mr6 mr0-m w-image-6 w-image-2-t w-auto-m shrink-0' />
            {isDesktop ? (
              <>
                <Search {...{ mobile }} />
                <Hidden smDown>
                  {isLogged ? (
                    <FeatureLinks {...{ activeCategory }} />
                  ) : (
                    <StaticLinks />
                  )}
                </Hidden>
                <Hidden mdUp>
                  <StaticLinks {...{ isLogged }} />
                </Hidden>
                <ResponseConsumer
                  response={user}
                  content={() => <AccountButton {...{ toggleDesktopMenuVisible }} />}
                  notfoundComponent={ButtonContainer}
                  unauthorizedComponent={ButtonContainer}
                  pendingComponent={() => <AccountButton proxy />}
                />
              </>
            ) : (
              <div className='flex items-center w-100'>
                <Search {...{ mobile }} />
                <Hamburger {...{ isMobileMenuActive }} />
              </div>
            )}
          </div>
        </header>
        {isDesktop && (
          <AccountMenu
            {...{
              visible: deskTopMenuVisible,
              desktop: isDesktop,
              user,
              mobile,
              tablet,
              pageSection,
              pageController,
              handleCloseMenu: toggleDesktopMenuVisible
              // The account menu closes itself when a button is pressed (I don't think we did this before but it feels more natural)
            }}
          />)}
        {!isDesktop && <MobileMenu visible={isMobileMenuActive} searchMode={mobileSearch} mobile={mobile} {...{ mobile, user, isLogged }} />}
        {ExtendedHeader}
      </div>
    </>
  )
}

const FeatureLinks = ({ activeCategory }) => {
  const links = [
    { text: 'Guidance', type: 'link', controller: TOPICSMENU, category: TOPICS },
    {
      text: 'Documents',
      type: 'dropdown',
      items: [{
        text: 'Documents',
        depth: 0,
        controller: DOCUMENTSMENU
      }, {
        text: 'Toolkits',
        depth: 0,
        controller: DOCUMENTSMENU,
        params: { category: 'toolkits' }
      }],
      toggleName: 'document-menu'
    },
    { text: 'Ask a lawyer', type: 'link', controller: NOTFOUND, category: ASK_A_LAWYER, params: { path: ZIRCON } }
  ]

  const active = activeCategory
  const baseClassName = 'no-underline fw6 fs mh6'

  return (
    <div className='flex items-center grow-1 shrink-1 justify-start' style={{ minWidth: '19.5rem' }}>
      {links.map(item => {
        if (item.type === 'dropdown') {
          return <HeaderDropdown displayName={item.text} {...item} key='dropdown' />
        }
        const { text, controller, category, params } = item
        return (
          <Link
            key={controller}
            className={cx(
              baseClassName,
              {
                'feature-link': active === category,
                'hover-slide-underline b dark-slate-blue hover-dark-slate-grey': active !== category
              }
            )}
            {...{ controller, params }}
          >
            {text}
          </Link>
        )
      })}
    </div>
  )
}

export const StaticLinks = ({ isLogged }) => {
  const baseClassName = 'dark-slate-blue hover-dark-slate-grey no-underline fs mh6 hover-slide-underline pointer header-static-links'
  const links = [
    { text: 'About', controller: ABOUT },
    { text: 'Pricing', controller: REGISTER }
  ]

  return (
    <div className='flex basis-auto shrink-0 items-center justify-start'>
      <FeaturesDropdown />
      {!isLogged && links.map(({ text, controller = null, href = null }) => controller ? (
        <Link key={controller} className={baseClassName} controller={controller}>
          {text}
        </Link>
      ) : (
        <a key={href} className={baseClassName} href={href}>
          {text}
        </a>
      ))}
    </div>
  )
}
