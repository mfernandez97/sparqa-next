import React from 'react'
import { BLOG } from 'constants/cms-pages'
import { TOPICSMENU, DOCUMENTSMENU, NOTFOUND, ZIRCON } from 'components/Link'
import HeaderDropdown from './HeaderDropdown'

const items = [{
  text: 'Guidance',
  depth: 0,
  controller: TOPICSMENU
}, {
  text: 'Documents',
  depth: 0,
  controller: DOCUMENTSMENU
},
{
  text: 'Toolkits',
  depth: 0,
  controller: DOCUMENTSMENU,
  params: { category: 'toolkits' }
},
{
  text: 'Ask a lawyer',
  depth: 0,
  controller: NOTFOUND,
  params: { path: ZIRCON }
},
{
  text: 'Insights',
  depth: 0,
  controller: NOTFOUND,
  params: { path: BLOG }
}]

const FeaturesDropdown = () => (
  <HeaderDropdown displayName='Services' {...{ items }} toggleName='features-menu' />
)

export default FeaturesDropdown
