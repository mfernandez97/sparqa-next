import cx from 'classnames'
import Avatar from 'components/Avatar'
import { _first } from 'libraries/lodash'
import ResponseConsumer from 'components/ResponseConsumer'
import PropTypes from 'prop-types'
import React from 'react'

const WrappedUser = ({ desktop, user }) => {
  const { name, firstName, lastName } = user

  const renderContent = () => (
    <div className={cx('flex items-center', {
      'pa6 bb b--light-grey': desktop,
      'pt6 layout-width-constraint': !desktop
    })}
    >
      <Avatar
        firstLetter={_first(firstName)}
        secondLetter={_first(lastName)}
        className={cx({ 'bg-royal-blue-3': desktop, 'bg-midnight-blue-2-lighten-5': !desktop })}
        large={!desktop}
      />
      <h3 className={cx('ma0', {
        'ml4 dark-grey fw6 fs': desktop,
        'ml5 dark-slate-grey fw8 fl': !desktop
      })}
      >
        {name}
      </h3>
    </div>
  )

  return (
    <ResponseConsumer response={user} content={renderContent} />
  )
}

WrappedUser.propTypes = {
  user: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.shape({
      email: PropTypes.string,
      firstName: PropTypes.string,
      lastName: PropTypes.string,
      name: PropTypes.string,
      passwordStatus: PropTypes.string,
      pin: PropTypes.string,
      roles: PropTypes.arrayOf(PropTypes.string),
      uri: PropTypes.string,
      userId: PropTypes.string
    })
  ])
}

export default WrappedUser
