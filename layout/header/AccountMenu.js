import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import { URL_FAQS } from 'constants/links'
import { DOCUMENTS } from 'constants/categories'
import { SUBSCRIPTION } from 'constants/account-pages'
import Icon from 'components/Icon'
import Link, { ACCOUNT, CONTACT, LOGOUT, ABOUT, SAVEDDOCUMENTS, PURCHASES } from 'components/Link'
import WrappedUser from 'layout/header/WrappedUser'

const manageLinks = [
  { name: 'Account', controller: ACCOUNT },
  { name: 'Subscription', controller: ACCOUNT, params: { section: SUBSCRIPTION } }
]

const helpLinks = [
  { name: 'FAQs', externalLink: URL_FAQS },
  { name: 'About Us', controller: ABOUT },
  { name: 'Contact', controller: CONTACT }
]

const documentLinks = showPurchases => ([
  { name: 'Documents in Progress', controller: SAVEDDOCUMENTS },
  ...showPurchases ? [{ name: 'Purchases', controller: PURCHASES, params: { category: DOCUMENTS } }] : []
])

const AccountMenu = ({ visible, user, pageSection, pageController, desktop, handleCloseMenu }) => visible ? (
  <div className={cx({
    'account-menu shadow-3-royal-blue-3-fade-40 z105 absolute br2 bg-white': desktop,
    'pb11 grow-1 relative': !desktop
  })}
  >
    <WrappedUser {...{ user, desktop }} />
    <Section {...{ title: 'MANAGE', links: manageLinks, desktop: desktop, pageSection, pageController, handleCloseMenu }} />
    <Section {...{ title: 'MY DOCUMENTS', links: documentLinks(user.isFreemium), desktop: desktop, pageSection, pageController, handleCloseMenu }} />
    <Section {...{ title: 'HELP', links: helpLinks, desktop: desktop, pageSection, pageController, handleCloseMenu }} />
    <Link
      controller={LOGOUT}
      className={cx('flex items-center no-underline fw6 fm', {
        'justify-center mid-grey pa6 b--light-grey hover-filter-o-40': desktop,
        'justify-start dark-slate-grey bg-white pt6 pb9 layout-width-constraint absolute left0 bottom0 w-100': !desktop
      })}
      onClick={handleCloseMenu}
    >
      <Icon className='w-icon-8 h-icon-8 ma0 mr2' name='power' /><span>Sign Out</span>
    </Link>
  </div>
) : null

AccountMenu.propTypes = {
  visible: PropTypes.bool
}

AccountMenu.defaultProps = {
  visible: false
}

const Section = ({ title, links, desktop, pageSection, pageController, handleCloseMenu }) => (
  <div
    className={cx(
      'bb', {
        'pa6 b--light-grey': desktop,
        'pv7 layout-width-constraint b--midnight-blue-2-lighten-5 last-of-type-bn': !desktop
      }
    )}
  >
    {title && (
      <h3 className={cx('ma0 fs tracked lh-solid fw6 mb6', {
        'royal-blue-3': desktop,
        'dark-slate-grey o-40': !desktop
      })}
      >
        {title}
      </h3>
    )}
    <ul className='list-reset pa0 ma0'>
      {links.map(link => <SectionItem key={link.name} {...{ desktop, pageSection, pageController, handleCloseMenu }} {...link} />)}
    </ul>
  </div>
)

Section.propTypes = {
  title: PropTypes.string,
  links: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      controller: PropTypes.string,
      params: PropTypes.object
    })
  )
}

Section.defaultProps = {
  title: null,
  links: []
}

class SectionItem extends Component {
  get className () {
    const { desktop } = this.props
    return cx({
      'last-child-mb0 mb4 lh-solid fxs': desktop,
      'mb7 last-child-mb0': !desktop
    })
  }

  get linkClassName () {
    const { desktop } = this.props
    return cx('no-underline', {
      fw5: desktop,
      'black hover-black-fade-40': desktop && !this.isDisabled,
      'silver no-pointer-event default-cursor': desktop && this.isDisabled,
      'dark-slate-grey b fl w-100 db': !desktop
    })
  }

  get isDisabled () {
    const { controller, pageController, pageSection, params } = this.props
    if (controller !== pageController) {
      return false
    }
    if (pageController === ACCOUNT && !params) {
      return pageSection !== SUBSCRIPTION
    }
    if (pageController === ACCOUNT && params) {
      return (params.section === pageSection)
    }
    return true
  }

  renderLink () {
    const { name, externalLink, controller, href, params, query, handleCloseMenu } = this.props

    if (externalLink) {
      return (
        <a className={this.linkClassName} href={externalLink} rel='noopener noreferrer' target='_blank'>
          {name}
        </a>
      )
    }
    if (href) {
      return (
        <a className={this.linkClassName} href={href} onClick={handleCloseMenu}>
          {name}
        </a>
      )
    }
    return (
      <Link data-toggle-menu={false} className={this.linkClassName} onClick={handleCloseMenu} {...{ controller, params, query }}>
        {name}
      </Link>
    )
  }

  render () {
    return <li className={this.className}>{this.renderLink()}</li>
  }
}

SectionItem.propTypes = {
  desktop: PropTypes.bool,
  externalLink: PropTypes.string,
  controller: PropTypes.string,
  params: PropTypes.object,
  name: PropTypes.string.isRequired
}

SectionItem.defaultProps = {
  desktop: false,
  externalLink: null,
  controller: null,
  params: null
}

export default AccountMenu
