import React, { Component, createContext } from 'react'
import ReactDOM from 'react-dom'
import { _get } from 'libraries/lodash'
import { of } from 'rxjs'
import { delay, startWith, debounceTime, map, switchMap } from 'rxjs/operators'
import cx from 'classnames'
import { getPageUrl } from 'front-end-core/core/history'
import { CONSENT } from 'constants/controllers'
import customMetaPages from 'constants/custom-meta-data'
import { ScrollMain } from 'services/WindowService'
import { SearchProvider } from 'context'
import ScrollingContainer, { STATE_KEYS } from 'components/ScrollingContainer'
import Metadata from 'components/Metadata'
import ConnectedHeader from './header/ConnectedHeader'
import Footer from './Footer'

export const LayoutContext = createContext({
  activeCategory: null,
  activeSubCategory: null,
  setActiveSubCategory: () => {},
  isLogged: false,
  mobileMenuOpen: false
})

const metaTitle = 'Sparqa Legal | The online legal platform for business'
const metaDescription = 'Get expert legal guidance & autogenerate documents for your business. Our online platform helps businesses manage their legal, compliance & regulatory needs for an affordable price.'

export default class Layout extends Component {
  state = {
    components: {},
    pageData: {},
    layoutData: {},
    activeSubCategory: null,
    setActiveSubCategory: (activeSubCategory) => this.setState({ activeSubCategory }),
    isPartnerBannerVisible: false
  }

  subscription = null
  mounted = false

  constructor (props) {
    super(props)
    this.subscription = props.state.pipe(
      debounceTime(5),
      map(s =>
        Object.assign(s, {
          pageId: _get(s, 'layoutData.pageId', getPageUrl(location.href)),
          searchType: _get(
            s,
            'layoutData.searchType',
            this.state.searchType
          ),
          url: _get(s, 'layoutData.url', getPageUrl(location.href))
        })
      ),
      switchMap(
        state =>
          this.state.pageId !== state.pageId
            ? of(state).pipe(
              delay(50),
              startWith({ pageId: null })
            )
            : of(state)
      )
    ).subscribe(this.updateState)
  }

  componentDidMount () {
    this.mounted = true
    this.emitScroll(ReactDOM.findDOMNode(this.scrollMain))
  }

  setIsPartnerBannerVisible= isPartnerBannerVisible => this.setState({ isPartnerBannerVisible })

  updateState = (state) => {
    if (this.mounted) {
      this.setState(state)
    } else {
      this.state = state
    }
  }

  componentWillUnmount () {
    if (typeof this.subscription.dispose === 'function') {
      this.subscription.dispose()
    } else if (typeof this.subscription.unsubscribe === 'function') {
      this.subscription.unsubscribe()
    }
  }

  emitScroll = scrollingContainer => {
    ScrollMain.next({
      scrollMain: scrollingContainer.scrollTop,
      scrollHeight: scrollingContainer.scrollHeight,
      viewHeight: ReactDOM.findDOMNode(this).getBoundingClientRect().height,
      footerOffset: ReactDOM.findDOMNode(this.footer).offsetTop,
      pageId: this.state.layoutData.pageId
    })
  }

  handleScrollMain = ({ currentTarget }) => {
    this.emitScroll(currentTarget)
  }

  setScrollMain = ref => {
    this.scrollMain = ref
  }

  setFooter = ref => {
    this.footer = ref
  }

  render () {
    const {
      pageData: { controller, pageClassName, headerBackground, section },
      isPartnerBannerVisible, activeSubCategory, setActiveSubCategory,
      layoutData: { activeCategory, stickyHeader = true, isLogged }
    } = this.state
    const pageId = _get(this.state, 'layoutData.pageId', location.href)
    const headerVisible = controller !== CONSENT
    const useDefaultMetadata = !customMetaPages.includes(controller)

    const headerProps = {
      pageId,
      headerVisible,
      activeCategory,
      activeSubCategory,
      headerBackground,
      ExtendedHeader: this.renderComponent('components.ExtendedHeader'),
      setIsPartnerBannerVisible: this.setIsPartnerBannerVisible,
      pageController: controller,
      pageSection: section
    }

    return (
      <SearchProvider value={_get(this.state, 'pageData.searchContextValue', {})}>
        <LayoutContext.Provider
          value={{ activeCategory, activeSubCategory, setActiveSubCategory }}
        >
          {/* Need this here because we if don't overide meta tags with data-react-helmet in index.html then those tags will be deleted */}
          {useDefaultMetadata && <Metadata pageTitle={metaTitle} pageDescription={metaDescription} />}
          <div className={cx('layout-wrapper', pageClassName, { 'partner-banner': isPartnerBannerVisible })}>
            <div id='portal-destination' />
            {stickyHeader && <ConnectedHeader {...headerProps} />}
            <ScrollingContainer
              className={cx(
                'scrolling-container-main',
                {
                  'vh-100-minus-nav': stickyHeader && headerVisible,
                  'vh-100': !stickyHeader || !headerVisible
                }
              )}
              stateKey={STATE_KEYS.MAIN}
              onScroll={this.handleScrollMain}
              pageId={pageId}
              ref={this.setScrollMain}
            >
              {!stickyHeader && <ConnectedHeader {...headerProps} />}
              <PageMinHeight headerVisible={headerVisible}>
                {this.renderComponent('components.Main')}
              </PageMinHeight>
              <Footer ref={this.setFooter} isLogged={isLogged} />
            </ScrollingContainer>
          </div>
        </LayoutContext.Provider>
      </SearchProvider>
    )
  }

  renderComponent (path) {
    const Component = _get(this.state, path)
    const data = _get(this.state, 'pageData', {})
    const layoutData = _get(this.state, 'layoutData', {})
    const key = `${_get(this.state, 'layoutData.pageId', location.href)}-${path}`

    if (Component) {
      return <Component key={key} layoutData={layoutData} {...data} />
    }
    return undefined
  }
}

const PageMinHeight = ({ headerVisible, children }) => {
  return <div className={cx({ 'min-vh-100-minus-nav': headerVisible, 'min-vh-100': !headerVisible })}>{children}</div>
}
