import React, { Component } from 'react'
import cx from 'classnames'
import { map } from 'rxjs/operators'
import { URL_TERMS_OF_USE, URL_PRIVACY_POLICY, URL_FAQS, URL_REFERRAL_SERVICE_TERMS } from 'constants/links'
import { currentYear } from 'helpers/misc'
import { $isLogged } from 'services/UserService'
import connect from 'helpers/connect'
import { TEAM, BLOG, PARTNERS, PRESS } from 'constants/cms-pages'
import Link, { CONTACT, REGISTER, ABOUT } from 'components/Link'
import SocialIcon from 'components/SocialIcon'

export default @connect(() => ({
  isLogged: $isLogged.pipe(map(({ isLogged }) => isLogged))
}))
class Footer extends Component {
  render () {
    const { isLogged } = this.props
    return (
      <footer className='footer bg-midnight-blue-2 pv8 ph7-t pt7-t pb8-t'>
        <div className='layout-width-constraint flex flex-wrap justify-between'>
          <div className='flex justify-start flex-wrap mb9 mt2 mb6-t mt0-t'>
            <div className='mb6 mr9 w-100-t'>
              <img className='w-icon-10' src='assets/logo-sm.svg' alt='Sparqa Legal logo' />
            </div>
            <Links title='Find your way' className='mr10 mr8-t'>
              <Link className='no-underline white fm fw6 hover-white-fade-40' controller={ABOUT}>About</Link>
              <a className='no-underline white fm fw6 hover-white-fade-40' href={'/' + TEAM}>Team</a>
              <Link className='no-underline white fm fw6 hover-white-fade-40' controller={CONTACT}>Contact</Link>
              <a className='no-underline white fm fw6 hover-white-fade-40' href={'/' + BLOG}>Insights</a>
              {!isLogged && <Link className='no-underline white fm fw6 hover-white-fade-40' controller={REGISTER}>Pricing</Link>}
              <a className='no-underline white fm fw6 hover-white-fade-40' href={'/' + PARTNERS}>Partners</a>
              <a className='no-underline white fm fw6 hover-white-fade-40' href={'/' + PRESS}>Press</a>
            </Links>
            <Links title='All the legal stuff'>
              <a className='no-underline white fm fw6 hover-white-fade-40' href={URL_REFERRAL_SERVICE_TERMS} rel='noopener noreferrer' target='_blank'>Referral Service T&Cs</a>
              <a className='no-underline white fm fw6 hover-white-fade-40' href={URL_PRIVACY_POLICY} rel='noopener noreferrer' target='_blank'>Privacy Policy</a>
              <a className='no-underline white fm fw6 hover-white-fade-40' href={URL_TERMS_OF_USE} rel='noopener noreferrer' target='_blank'>T&Cs</a>
              <a className='no-underline white fm fw6 hover-white-fade-40' href={URL_FAQS} rel='noopener noreferrer' target='_blank'>FAQs</a>
            </Links>
          </div>
          <Links title='Follow us here' className='mb10 mt2 mt0-t w-100-t' listClassName='flex'>
            <SocialIcon className='mr6' iconClassName='svg-icon hover-filter-o-40' type='facebook' />
            <SocialIcon className='mr6' iconClassName='svg-icon hover-filter-o-40' type='twitter' />
            <SocialIcon iconClassName='svg-icon hover-filter-o-40' type='linkedin' />
          </Links>
          <span className='w-100 blue-grey fs tc tl-t fxs-t'>
            Copyright &copy; {currentYear} Sparqa. All rights reserved.
          </span>
        </div>
      </footer>
    )
  }
}

const Links = ({ title, className, listClassName, children }) => (
  <div className={cx(className)}>
    <span className='blue-grey fw6 fs'>{title}</span>
    <ul className={cx('list-reset pa0 ma0', listClassName)}>
      {React.Children.map(
        children, child => (
          <li className='mv4'>{child}</li>
        )
      )}
    </ul>
  </div>
)
