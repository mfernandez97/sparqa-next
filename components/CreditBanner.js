import React from 'react'
import { map } from 'rxjs/operators'
import Banner from 'components/Banner'
import { formatCurrency } from 'helpers/misc'
import connect from 'helpers/connect'
import { $isLogged, $user } from 'services/UserService'
import { $credit } from 'services/PaymentsService'
import ResponseConsumer from 'components/ResponseConsumer'

const CreditBanner = ({ user, credit: { received, remaining } }) => {
  if (remaining <= 1) {
    return null
  }
  return user.isFreemium ? (
    <Banner className='bg-crimson light-grey pv2'>
      <div className='layout-width-constraint tc'>
        Of the £{formatCurrency(received, true)} credit you've received, you have £{formatCurrency(remaining, true)} remaining.
      </div>
    </Banner>
  ) : null
}

const ConnectedBanner = connect(() => ({
  isLogged: $isLogged.pipe(map(({ isLogged }) => isLogged)),
  credit: $credit(),
  user: $user
}))(props => props.isLogged ? (
  <ResponseConsumer
    responses={[props.credit, props.user]}
    content={() => <CreditBanner {...props} />}
    pendingComponent={() => null}
    unauthorizedComponent={() => null}
    notfoundComponent={() => null}
    baddataComponent={() => null}
    badresponseComponent={() => null}
    offlineComponent={() => null}
    forbiddenComponent={() => null}
  />
) : null)

export default ConnectedBanner
