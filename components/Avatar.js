import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'

const Avatar = ({ firstLetter = null, secondLetter = null, large = false, className = null}) => (
  <div className={cx(className, 'br-100 relative', { 'w-icon-10 h-icon-10': !large, 'w-icon-15 h-icon-15': large })}>
    <span className='absolute center fw7 fl white'>
      {firstLetter && firstLetter.toUpperCase()}
      {secondLetter && secondLetter.toUpperCase()}
    </span>
  </div>
)

Avatar.propTypes = {
  firstLetter: PropTypes.string,
  secondLetter: PropTypes.string,
  large: PropTypes.bool,
  className: PropTypes.string
}

export default Avatar
