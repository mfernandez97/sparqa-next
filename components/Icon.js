import React from 'react'
import PropTypes from 'prop-types'
import SVG, { preloadSVGs } from 'components/SVG'
import cx from 'classnames'

const Icon = ({ name, className }) => (
  <SVG className={cx(className, 'icon-base')} url={getIconUrl(name)} />
)

const getIconUrl = name => `assets/icons/${name}.svg`

// If an icon doesn't initially appear but appears in response to e.g. a user
// interaction then it might be useful to preload it in componentDidMount or
// something to avoid showing the nullIcon.
export const preloadIcons = names => preloadSVGs(names.map(name => getIconUrl(name)))

Icon.propTypes = {
  name: PropTypes.string.isRequired
}

export default Icon
