import React, { cloneElement } from 'react'
import cx from 'classnames'
import { _isString, _compact, _join } from 'libraries/lodash'
import CarouselContainer, { Carousel, Slide, CarouselButton, CarouselDots } from 'components/Carousel'
import Icon from 'components/Icon'

const personDetails = (name, position, company) => {
  return _join(_compact([name, position, company]), ', ') || ''
}

const Photo = ({ assetUrl }) => (
  <div className='w-image-4 h-image-4 mhauto tc br-50 absolute top0 left-50 x--50-y--50 overflow-hidden pa2 testimonial-border'>
    <div className='testimonial-image' style={{ backgroundImage: `url(${assetUrl})` }} />
  </div>
)

const Testimonial = ({ copy, name, position = null, company = null, assetUrl }) => (
  <div className='bg-near-white w-80 w-90-m br2-1 mhauto tc pt10 relative testimonial-body shadow-3-black-fade-20 mt10'>
    <Photo assetUrl={assetUrl} />
    <p className='i pt6 mt0 testimonial-name ph4'>{personDetails(name, position, company)}</p>
    {_isString(copy) ? (
      <p className='w-80 mhauto lh-title fxxl testimonial-text fm-m mt6'>
        {copy}
      </p>
    ) : cloneElement(copy, { className: cx(copy.props.className, 'w-80 mhauto lh-title fxxl testimonial-text fm-m mt6') })}
  </div>
)

const Testimonials = ({ className, testimonials, title = 'What our customers say' }) => (
  <section className={cx(className, 'relative')}>
    <h2 className='tc dark-slate-grey fxxxl fw8 landing-headings mb8 mt0 layout-width-constraint'>{title}</h2>
    <div className='relative mb6 max-w-measure-6 mhauto'>
      <CarouselContainer>
        <div className='ph8-m ph0-xs-m'>
          <Carousel>
            {testimonials.map((testimonial, index) => (
              <Slide key={index}><Testimonial {...testimonial} /></Slide>
            ))}
          </Carousel>
        </div>
        <CarouselButton
          position='left'
          enabledClassName='dark-slate-grey'
          disabledClassName='transparent'
          data-track-click
          data-track-category='Marketing: Testimonial'
        >
          <Icon className='h-icon-8 w-icon-8' name='arrow-left' />
        </CarouselButton>
        <CarouselButton
          position='right'
          enabledClassName='dark-slate-grey'
          disabledClassName='transparent'
          data-track-click
          data-track-category='Marketing: Testimonial'
        >
          <Icon className='h-icon-8 w-icon-8' name='arrow-right' />
        </CarouselButton>
        <CarouselDots
          className='mhauto max-w-image-4 mt8'
          activeDotClassName='bg-dark-slate-grey'
          inactiveDotsClassName='bg-white'
          makeDotProps={() => ({
            'data-track-click': true,
            'data-track-category': 'Marketing: Testimonial'
          })}
        />
      </CarouselContainer>
    </div>
  </section>
)

export default Testimonials
