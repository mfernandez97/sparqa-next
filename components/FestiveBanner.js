import React from 'react'
import Banner from 'components/Banner'

const ON = false

const FestiveBanner = () => ON ? (
  <Banner className='bg-crimson white pv2'>
    <div className='layout-width-constraint tc fs'>
      Ask a Lawyer’ is away for the festive break, returning on 4 January 2022. Submit your query and we’ll respond as soon as possible when we return.
    </div>
  </Banner>
) : null

export default FestiveBanner
