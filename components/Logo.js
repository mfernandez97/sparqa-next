import React from 'react'
import cx from 'classnames'
import Link, { LANDING } from 'components/Link'
import connect from 'helpers/connect'
import { $logoUrl } from 'services/UserService'

const Logo = ({ className = '', src }) => (
  <Link className={cx('logo flex items-center', className)} controller={LANDING}>
    <img
      className='white bicubic w-image-2-t w-image-6 w-icon-9-m max-h-icon-9'
      alt='Logo'
      src={src} />
  </Link>
)

export default connect(() => ({
  src: $logoUrl
}))(Logo)
