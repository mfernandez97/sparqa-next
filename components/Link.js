import React from 'react'
import { _omit } from 'libraries/lodash'
import PropTypes from 'prop-types'
import getUrl from 'front-end-core/core/url'
import { getBaseUrl } from 'front-end-core/core/history'
import { WORDPRESSURLS } from 'constants/controllers'

export * from 'constants/controllers'

const Link = props => {
  const linkProps = {
    ..._omit(props, ['controller', 'params', 'query', 'children']),
    rel: props.target === '_blank' ? 'noopener noreferrer' : ''
  }
  const { controller, params = {}, query = {} } = props
  const url = getUrl(controller, params, query)
  const formattedUrl = (WORDPRESSURLS.includes(controller) || WORDPRESSURLS.includes(url))
    ? '/' + (url === 'false' ? '' : url)
    : url

  return (
    <a {...linkProps} href={formattedUrl === 'false' ? getBaseUrl() : formattedUrl || getBaseUrl()}>
      {props.children}
    </a>
  )
}

Link.propTypes = {
  controller: PropTypes.string.isRequired
}

export default Link
