import React, {useEffect, useState} from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import { URL_FACEBOOK, URL_TWITTER, URL_LINKEDIN } from 'constants/links'

const SocialIcon = ({type, className, iconClassName = null, excludeLink = false, mobile = false}) => {
  const linkMap = new Map([['facebook', URL_FACEBOOK], ['twitter', URL_TWITTER], ['linkedin', URL_LINKEDIN]])
  const [svgName, setSvgName] = useState(`${type}-circled`)
  const [link, setLink] = useState(null)

  useEffect(() => {
    setSvgName(`${type}-circled`)
    setLink(linkMap.get(type) || null)
  }, [type])

  const renderIcon = (className) => (
    <img
      className={cx(className)}
      src={`assets/icons/${svgName}${mobile ? '-black' : ''}.svg`}
      alt={`Follow us on ${type}`}
    />
  )

  return (excludeLink || !link) ? renderIcon(cx(className, iconClassName)) : (
    <a
      {...{className}}
      href={link}
      target='_blank'
      rel='noopener noreferrer'
    >
      {renderIcon(iconClassName)}
    </a>
  )
}

SocialIcon.propTypes = {
  type: PropTypes.oneOf(['facebook', 'twitter', 'linkedin']).isRequired,
  excludeLink: PropTypes.bool,
  iconClassName: PropTypes.string
}

export default SocialIcon