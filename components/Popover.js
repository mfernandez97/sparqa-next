import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import { getBoundingClientRect, getPathTo } from 'front-end-core/helpers/document'
import { getNode } from 'helpers/dom'
import { $toggle } from 'services/WindowService'
import connect from 'helpers/connect'

@connect(() => ({
  popover: $toggle({
    selectNode: node => node.hasAttribute('data-popover-content'),
    showMapping: e => {
      const node = e.target
      const selectNode = getNode(node => node.hasAttribute('data-popover-content'))
      const nodeWithContent = node.hasAttribute('data-popover-content') ? node : selectNode(node)
      const clientRects = node.getClientRects()
      return {
        popoverKey: getPathTo(node),
        dimensions: clientRects[clientRects.length - 1],
        content: nodeWithContent.dataset.popoverContent
      }
    },
    hideMapping: () => ({ popoverKey: null, dimensions: null, content: null }),
    initialValue: { popoverKey: null, dimensions: null, content: null }
  })
}))
class Container extends Component {
  renderItem = () => {
    const { popoverKey, content, dimensions } = this.props.popover
    return (
      <PopoverItem
        key={popoverKey}
        invertColors={this.props.invertColors}
        {...{ dimensions, content, container: getBoundingClientRect(this.refs.container) }}
      />
    )
  }

  render () {
    const { popoverKey } = this.props.popover
    return (
      <div ref='container' className='relative h0 z5'>
        {popoverKey && this.renderItem()}
      </div>
    )
  }
}

class PopoverItem extends Component {
  state = {
    top: 0,
    left: 0,
    arrow: 0
  }

  componentDidMount () {
    const { dimensions, container } = this.props
    const { width } = getBoundingClientRect(this.refs.popover)
    const top = dimensions.bottom - container.top
    const left = Math.min(
      Math.max(
        0,
        dimensions.left + dimensions.width / 2 - width / 2 - container.left
      ),
      container.width - width
    )
    const arrow = dimensions.left + dimensions.width / 2 - container.left - left
    this.setState({ top, left, arrow })
  }

  render () {
    const { invertColors = false } = this.props
    const { top, left, arrow } = this.state
    return (
      <div
        style={{ top, left }}
        className={cx('popover__item absolute mt4 z50 db top-100 default-cursor max-w-measure-3 pa6 br1 fs', {
          'bg-dark-grey white': !invertColors,
          'bg-white dark-grey': invertColors
        })}
        ref='popover'
      >
        <div
          style={{ left: arrow }}
          className={cx('absolute w0 h0 left-50', { popover__arrow: !invertColors, 'popover__arrow--light': invertColors })}
        />
        <div dangerouslySetInnerHTML={{ __html: this.props.content }} />
      </div>
    )
  }
}

PopoverItem.propTypes = {
  dimensions: PropTypes.shape({
    bottom: PropTypes.number,
    height: PropTypes.number,
    left: PropTypes.number,
    right: PropTypes.number,
    top: PropTypes.number,
    width: PropTypes.number
  }).isRequired,
  container: PropTypes.shape({
    bottom: PropTypes.number,
    height: PropTypes.number,
    left: PropTypes.number,
    right: PropTypes.number,
    top: PropTypes.number,
    width: PropTypes.number
  }).isRequired,
  content: PropTypes.string.isRequired
}

export default Container
