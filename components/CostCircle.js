import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'

const CostCircle = ({ cost }) => {
  const numChars = cost.length
  const className = cx('cost-circle absolute right8 right6-m top8 bg-steel-blue-lighten-25 br-100 w-icon-16 h-icon-16 tc white fw6', {
    'fxl': numChars <= 2,
    'fl': numChars === 3,
    'fm': numChars >= 4
  })
  return (
    <div className={className}>
      <span className='absolute'>
        <sup className=' absolute fxxs'>
          £
        </sup>
        {cost}
      </span>
      {Number(cost) > 0 && <sub className=' absolute fxxs'>+vat</sub>}
    </div>
  )
}

CostCircle.propTypes = {
  cost: PropTypes.string
}

export default CostCircle
