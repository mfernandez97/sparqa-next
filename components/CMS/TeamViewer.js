import React, { Component } from 'react'
import { $teamMembers } from 'services/CMSService'
import connect from 'helpers/connect'
import ResponseConsumer from 'components/ResponseConsumer'
import TeamViewerComponent from 'components/TeamViewer'

@connect(props => ({
  team: $teamMembers({ ids: props.teamMembers.map(({ id }) => id), pageSize: 100 })
}))
export default class TeamViewer extends Component {
  renderContent = () => {
    return <TeamViewerComponent team={this.props.team} />
  }

  render () {
    return (
      <ResponseConsumer response={this.props.team} content={this.renderContent} />
    )
  }
}
