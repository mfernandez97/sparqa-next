import React, { Component, createRef, useState } from 'react'
import cx from 'classnames'
import { isValidResponse } from 'core/api/dataflow'
import { PARTNER_BANNER_DISMISSED_COOKIE } from 'constants/cookies'
import { getPartnerCode } from 'helpers/misc'
import { $partnershipBanner } from 'services/CMSService'
import Cookies from 'js-cookie'
import connect from 'helpers/connect'
import RichText from 'components/CMS/RichText'
import Modal, { ModalBox } from 'components/Modal'
import Icon from 'components/Icon'
import Banner from 'components/Banner'
import ResponseConsumer from 'components/ResponseConsumer'

// NOTE: informing the parent component whether it's visible or not rather than
// having the logic to determine whether its visible in the parent and informing
// the child feels like an antipattern, but the layout files are messy atm so
// I'll fix it when I refactor them. See ticket: FAL-1227
// NOTE: Turns out it wasn't just an antipattern but actually broke the login
// redirect!
const PartnershipBannerContainer = ({ response, setIsPartnerBannerVisible, isLogged }) => {
  if (isLogged) {
    return null
  }
  if (isValidResponse(response)) {
    const {
      copy: richText,
      align,
      modal,
      link_url: linkUrl,
      link_text: linkText,
      text_colour: textHexCode,
      partner_name: title,
      background_colour: bgHexCode,
      title_text_colour: titleHexCode,
      title_background_colour: titleBgHexCode
    } = response.data || {}
    return modal === 'yes' ? (
      <PartnershipBannerWithModal
        title={title}
        align={align}
        linkUrl={linkUrl}
        linkText={linkText}
        richText={richText}
        bgHexCode={bgHexCode}
        textHexCode={textHexCode}
        setIsVisible={setIsPartnerBannerVisible}
        titleHexCode={titleHexCode}
        titleBgHexCode={titleBgHexCode}
      />
    ) : (
      <LinkedPartnershipBanner
        title={title}
        align={align}
        linkUrl={linkUrl}
        linkText={linkText}
        richText={richText}
        bgHexCode={bgHexCode}
        textHexCode={textHexCode}
        setIsVisible={setIsPartnerBannerVisible}
        titleHexCode={titleHexCode}
        titleBgHexCode={titleBgHexCode}
      />
    )
  }
  return null
}

class PartnershipBannerWithModal extends Component {
  state = { dismissed: false, modalVisible: false }

  container = createRef()

  setDismissed = dismissed => {
    this.setState({ dismissed })
  }

  setModalVisible = modalVisible => this.setState({ modalVisible })

  render () {
    const { title, align, richText, linkUrl, linkText, textHexCode, bgHexCode, titleHexCode, titleBgHexCode } = this.props
    if (this.state.dismissed) return null
    return (
      <>
        <div style={{ backgroundColor: bgHexCode }} className='partner-banner-truncated dark-grey pointer' onClick={() => this.setModalVisible(true)}>
          <PartnershipBannerInner
            align={align}
            title={title}
            richText={richText}
            textHexCode={textHexCode}
            setDismissed={this.setDismissed}
            titleHexCode={titleHexCode}
            titleBgHexCode={titleBgHexCode}
          />
        </div>
        <PartnershipModal
          title={title}
          visible={this.state.modalVisible}
          linkUrl={linkUrl}
          linkText={linkText}
          richText={richText}
          bgHexCode={bgHexCode}
          closeModal={() => this.setModalVisible(false)}
          textHexCode={textHexCode}
          titleHexCode={titleHexCode}
          titleBgHexCode={titleBgHexCode}
        />
      </>
    )
  }
}

const LinkedPartnershipBanner = ({
  title,
  align,
  linkUrl,
  richText,
  bgHexCode,
  textHexCode,
  setIsVisible,
  titleHexCode,
  titleBgHexCode
}) => {
  const [dismissed, setDismissed] = useState(false)
  if (dismissed) return null
  return linkUrl.url ? (
    <Banner
      href={linkUrl.url}
      target={linkUrl.target}
      inlineStyle={{ backgroundColor: bgHexCode }}
      className='dark-grey pointer'
    >
      <PartnershipBannerInner
        title={title}
        align={align}
        richText={richText}
        textHexCode={textHexCode}
        setDismissed={dismissed => {
          setDismissed(dismissed)
        }}
        titleHexCode={titleHexCode}
        titleBgHexCode={titleBgHexCode}
      />
    </Banner>
  ) : null
}

const PartnershipBannerInner = ({
  title,
  align,
  richText,
  truncated = false,
  textHexCode,
  setDismissed,
  titleHexCode,
  titleBgHexCode
}) => {
  const titleStyleObj = { color: titleHexCode, backgroundColor: titleBgHexCode }
  const richTextStyleObj = { color: textHexCode }
  return (
    <div className={cx('layout-width-constraint pv4 flex items-center relative', { 'justify-start': align === 'left', 'justify-center': align === 'center' })}>
      <h4 style={titleStyleObj} className={cx('b mv0 fs ttu nowrap mr4 lh-solid ph3 pv2 br2', { 'lh-title': !truncated })}>{title}</h4>
      {richText && (
        <RichText style={richTextStyleObj} className={cx('dark-grey fs overflow-hidden partner-banner-text', { 'lh-solid': truncated, 'lh-title': !truncated })}>
          {richText}
        </RichText>
      )}
      <CloseButton
        onClick={e => {
          e.preventDefault()
          e.stopPropagation()
          const partner = getPartnerCode() || ''
          Cookies.set(PARTNER_BANNER_DISMISSED_COOKIE, partner)
          setDismissed(true)
        }}
      />
    </div>
  )
}

const CloseButton = ({ onClick }) => (
  <button className='absolute top-50 right2 pa2 y--50 pointer bg-transparent flex items-center hover-bg-white-fade-20 br2' onClick={onClick}>
    <Icon className='w-icon-5 h-icon-5 white' name='close' />
  </button>
)

const PartnershipModal = ({ title, visible, linkUrl, linkText, richText, textHexCode, bgHexCode, titleBgHexCode, closeModal, titleHexCode }) => (
  <Modal visible={visible} onMaskClick={closeModal}>
    <ModalBox title={title} closeModal={closeModal} titleStyle={{ color: textHexCode, backgroundColor: bgHexCode }}>
      <div className='pa8 flex flex-column justify-center'>
        <RichText className='dark-grey fm'>{richText}</RichText>
        {linkUrl.url && linkText && (
          <a
            href={linkUrl.url}
            style={{ color: titleHexCode, backgroundColor: titleBgHexCode }}
            target={linkUrl.target}
            rel={linkUrl.target === '_blank' ? 'noopener noreferrer' : ''}
            className='br-pill outline-0 transition-all pointer no-underline pv6 ph8 lh-solid b fs tc mt8 hover-filter-brighter-115'
          >
            {linkText}
          </a>
        )}
      </div>
    </ModalBox>
  </Modal>
)

const ConnectedPartnershipBanner = connect(({ cookie }) => ({
  response: $partnershipBanner({ uid: cookie })
}))(props => {
  return (
    <ResponseConsumer
      content={() => <PartnershipBannerContainer {...props} />}
      response={props.response}
      pendingComponent={() => null}
      unauthorizedComponent={() => null}
      notfoundComponent={() => null}
      baddataComponent={() => null}
      badresponseComponent={() => null}
      offlineComponent={() => null}
      forbiddenComponent={() => null}
    />
  )
})

const PartnershipBannerWithCookie = props => {
  const cookie = getPartnerCode() || ''
  const bannerDismissed = cookie === Cookies.get(PARTNER_BANNER_DISMISSED_COOKIE)
  return !bannerDismissed ? <ConnectedPartnershipBanner {...props} cookie={cookie} /> : null
}

export default PartnershipBannerWithCookie
