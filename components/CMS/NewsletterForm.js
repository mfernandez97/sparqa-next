import React, { Component, Fragment, useState } from 'react'
import cx from 'classnames'
import insist from 'components/Form/esv-react-form/insist'
import { isEmailValid } from 'components/Form/customRules'
import Form, { Field, FormConsumer, ErrorMessages, getTextFieldClassNames } from 'components/Form'
import Icon from 'components/Icon'

const initialValues = { email: '' }
const schema = {
  email: insist().required().custom(isEmailValid.rule, isEmailValid.options)
}

export default class NewsletterForm extends Component {
  submit = email => {
    return Promise.resolve(`New newsletter subscriber ${email}`)
  }

  render () {
    return (
      <div className='br2 flex-column items-center bg-white shadow-3-black-fade-10 pa6 mb7 ba b--royal-blue-3'>
        <h2 className='mt0 mb4 cyan b fl tl'>Sign up for our newsletter</h2>
        <p className='cyan fs mb6'>Nam eu egestas magna. Suspendisse rhoncus id augue at feugiat.</p>
        <NewsletterInput />
      </div>
    )
  }
}

const NewsletterInput = () => {
  const [ showErrors, setShowErrors ] = useState(false)
  const [ errorMessages, setErrorMessages ] = useState([])
  const [ successMessage, setSuccessMessage ] = useState(null)
  return (
    <Fragment>
      <Form
        schema={schema}
        className='flex'
        initialValues={initialValues}
        onSubmit={(e, { values, onFinishSubmit }) => {
          setTimeout(() => {
            setSuccessMessage('Success!')
            onFinishSubmit()
          }, 500)
        }}
      >
        <Field name='email'>
          {props => {
            // Can I do this ...?
            setShowErrors(props.showErrors)
            setErrorMessages(props.errorMessages)
            return <EmailInput {...props} />
          }}
        </Field>
        <FormConsumer>
          {({ state: { isSubmitting } }) => <EmailSubmit isSubmitting={isSubmitting} />}
        </FormConsumer>
      </Form>
      {showErrors && <ErrorMessages className='mt2' messages={errorMessages} />}
      {successMessage && (
        <p className='mt2 mb0 medium-sea-green fxs'>{successMessage}</p>
      )}
    </Fragment>
  )
}

const EmailInput = ({
  value,
  onBlur,
  onChange,
  completedWithErrors,
  completedWithoutErrors
}) => {
  const { color, borderColor } = getTextFieldClassNames({
    completedWithErrors,
    completedWithoutErrors
  })
  return (
    <input
      type='text'
      value={value}
      onBlur={onBlur}
      onChange={e => onChange(e.currentTarget.value)}
      className={cx(
        color,
        borderColor,
        'fxs ph6 pv5 w-100 db br-pill mv0 ml0 mr4 focus-outline-0 ba grow-1 shrink-0 basis0 transition-all'
      )}
      placeholder='Email'
    />
  )
}

const EmailSubmit = ({ isSubmitting }) => {
  const className = cx(
    'transition-all pointer',
    'royal-blue-3 bg-white',
    'hover-bg-royal-blue-3 hover-white',
    'ba b--royal-blue-3 br-pill',
    'relative',
    'w-icon-10 h-icon-10'
  )
  return (
    <button
      type='submit'
      disabled={isSubmitting}
      className={className}
    >
      <Icon className='w-icon-4 h-icon-4 left-50 top-50 x--50-y--50 absolute' name='arrow-right' />
    </button>
  )
}
