import React, { Component } from 'react'
import { map } from 'rxjs/operators'
import { isValidResponse } from 'core/api/dataflow'
import { BLOGAUDIENCE } from 'constants/controllers'
import { $audiences } from 'services/CMSService'
import connect from 'helpers/connect'
import CardList, { Card } from 'components/Card'
import ResponseConsumer from 'components/ResponseConsumer'

@connect(() => ({
  audiences: $audiences().pipe(
    map(response => {
      if (isValidResponse(response)) {
        return response.results
          .filter(result => !!result.uid)
          .map(result => ({
            uid: result.uid,
            icon: result.data.icon,
            name: result.data.name,
            description: result.data.description
          }))
      }
      return response
    })
  )
}))
class AudienceCardList extends Component {
  renderContent = () => {
    const { audiences, items_per_row: itemsPerRow, image_aspect_ratio: imageAspectRatio } = this.props
    return (
      <CardList itemsPerRow={itemsPerRow}>
        {audiences.map(audience => (
          <Card
            key={audience.uid}
            descriptionRichText
            title={audience.name}
            titleClassName='fl'
            imgUrl={audience.icon.url}
            description={audience.description}
            imageAspectRatio={imageAspectRatio}
            linkParams={{ controller: BLOGAUDIENCE, params: { uid: audience.uid } }}
          />
        ))}
      </CardList>
    )
  }

  render () {
    return (
      <ResponseConsumer response={this.props.audiences} content={this.renderContent} />
    )
  }
}

export default AudienceCardList
