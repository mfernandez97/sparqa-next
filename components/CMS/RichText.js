import React from 'react'
import cx from 'classnames'

import { RichText as PrismicRichText } from 'prismic-reactjs'

const RichText = ({ children, ...props }) => {
  const { text_align: textAlign } = props
  const className = cx(props.className, 'cms-rich-text', {
    tl: textAlign === 'left',
    tc: textAlign === 'center',
    tr: textAlign === 'right'
  })
  return (
    <div style={props.style} className={className}>
      {PrismicRichText.render(children, () => null)}
    </div>
  )
}

export const plainText = text => {
  return PrismicRichText.asText(text)
}

export default RichText
