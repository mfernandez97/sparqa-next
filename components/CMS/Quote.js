import React, { Component } from 'react'
import cx from 'classnames'

export default class Quote extends Component {
  render () {
    const {
      quote,
      name_of_the_author: nameOfTheAuthor = [],
      portrait_author: portraitAuthor = {}
    } = this.props
    return (
      <div className='mt8 mb6 flex items-center'>
        {portraitAuthor.url && (
          <div className='w-30 max-w-image-13 mr6 dn-m w-40-t'>
            <img className='w-100' src={portraitAuthor.url} alt={nameOfTheAuthor[0].text} />
          </div>
        )}
        <div>
          <FancyText>{quote[0].text}</FancyText>
          {nameOfTheAuthor.length > 0 && <Text>- {nameOfTheAuthor[0].text}</Text>}
        </div>
      </div>
    )
  }
}

const FancyText = ({ className, children }) => (
  <h3 className={cx('navy fxxxl i fw2 lh-copy mt0 mb6', className)}>{children}</h3>
)

const Text = ({ className, children }) => (
  <p className={cx('navy fs mb6', className)}>{children}</p>
)
