import React from 'react'
import cx from 'classnames'
import { _head, _split } from 'libraries/lodash'
import { isExternalLink } from 'helpers/misc'
import RichText from 'components/CMS/RichText'
import FeatureBlock from 'components/FeatureBlock'

const Button = ({ href, text, bgColor = 'white', textColor = 'dark-grey', pageTitle }) => (
  <a
    href={href}
    {...(isExternalLink(href)) && { target: '_blank', rel: 'noopener noreferrer' }}
    className={cx(
      'dib tc no-underline br-pill pv6 ph10 transition-all mt8 db-t w-80-t mhauto fm',
      `bg-${bgColor} ${textColor} hover-bg-${textColor} hover-${bgColor}`
    )}
    data-track-click
    data-track-category='Call To Action'
    data-track-action='Click'
    data-track-label={`[${text}][${pageTitle}][${window.location.href}]`}
  >
    {text}
  </a>
)

const Boxed = props => {
  const {
    title,
    subtitle,
    pageTitle,
    button_text: buttonText,
    button_link: buttonLink,
    image: { url: assetUrl }
  } = props
  const copyLeft = props.orientation !== 'content right / image left'
  const bgColor = _head(_split(props.background_color, ' '))
  const textColor = _head(_split(props.text_color, ' '))
  const buttonBgColor = _head(_split(props.button_background_color, ' '))
  const buttonTextColor = _head(_split(props.button_text_color, ' '))
  const button = buttonText && buttonLink ? (
    <Button href={buttonLink} text={buttonText} bgColor={buttonBgColor} textColor={buttonTextColor} pageTitle={pageTitle} />
  ) : null
  return (
    <section className='flex flex-wrap justify-center pv11 pv9-t'>
      <FeatureBlock
        className='layout-width-constraint ma0'
        top={false}
        button={button}
        copy={<RichText>{props.main_body}</RichText>}
        {...{ title, subtitle, assetUrl, copyLeft, bgColor, textColor }}
      />
    </section>
  )
}

export default Boxed
