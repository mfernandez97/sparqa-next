import React from 'react'
import { _zip } from 'libraries/lodash'
import RichText from 'components/CMS/RichText'
import TestimonialsComponent from 'components/Testimonials'

const Testimonials = props => {
  const { title1: title, headshot, author, main_body: mainBody } = props

  const testimonials = _zip(headshot, author, mainBody).map((
    [headshot, author, mainBody]
  ) => ({
    name: author,
    assetUrl: headshot.url,
    assetAlt: headshot.alt,
    copy: <RichText>{mainBody}</RichText>
  }))

  return (
    <TestimonialsComponent className='pv11 pv9-t' title={title} testimonials={testimonials} />
  )
}

export default Testimonials
