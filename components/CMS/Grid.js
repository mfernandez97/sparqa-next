import React from 'react'
import cx from 'classnames'
import { _zip, _head, _split } from 'libraries/lodash'
import GridComponent, { GridItem } from 'components/Grid'

const Grid = props => {
  const items = _zip(props.image, props.image_link, props.item_title, props.item_subtitle)
  const textColor = _head(_split(props.text_color, ' '))
  const bgColor = `bg-${_head(_split(props.background_color, ' '))}`
  return (
    <GridComponent
      title={props.title1}
      className={cx(props.className, bgColor, textColor, 'pv9')}
      itemsPerRow={props.items_per_row}
      itemsPerRowMobile={props.items_per_row_mobile}
      gutterWidth='10'
      widthConstraint
    >
      {items.map((
        [image, imageLink, title, subtitle], index
      ) => (
        <GridItem
          key={index}
          image={image}
          title={title}
          link={imageLink}
          description={subtitle}
          imageAspectRatio={props.image_aspect_ratio}
        />)
      )}
    </GridComponent>
  )
}

export default Grid
