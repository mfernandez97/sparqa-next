import React from 'react'

const Image = ({ image = {} }) => {
  return image.url ? (
    <div className='w-100 mv8'>
      <img className='w-100' src={image.url} alt={image.alt} />
    </div>
  ) : null
}

export default Image
