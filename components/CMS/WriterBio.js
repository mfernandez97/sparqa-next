import React from 'react'
import RichText from 'components/CMS/RichText'

const WriterBio = ({ title, subtitle, description, image }) => {
  return (
    <div className='br2 bg-white ph6-t pb6-t pt9-t shadow-3-black-fade-10 mt10 mhauto flex relative-t'>
      <div
        className='br2 br--left w-image-1-t h-image-1-t w-image-12 br-100-t absolute-t top0 left-50 x--50-y--50-t shadow-3-black-fade-10-t'
        style={{
          backgroundSize: 'cover',
          backgroundPositionX: 'center',
          backgroundImage: `url(${image.url})`
        }}
      />
      <div className='pa6 pa0-t basis0 grow-1 shrink-1 min-h-image-12'>
        <h2 className='mv0 fxl'>{title}</h2>
        <h3 className='mt0 mb4 fm'>{subtitle}</h3>
        <RichText className='fm'>{description}</RichText>
      </div>
    </div>
  )
}

export default WriterBio
