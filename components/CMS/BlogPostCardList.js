import React, { Component, createContext } from 'react'
import { map } from 'rxjs/operators'
import { isValidResponse } from 'core/api/dataflow'
import { BLOGPOST } from 'constants/controllers'
import { $blogPosts } from 'services/CMSService'
import { getBlogPostTitle } from 'helpers/cms'
import connect from 'helpers/connect'
import CardList, { Card } from 'components/Card'
import RichText from 'components/CMS/RichText'
import ResponseConsumer from 'components/ResponseConsumer'
import Pagination from 'components/Results/Pagination'

const PaginationContext = createContext({})

@connect(({ p, limit = 20, documentLinks = [] }) => ({
  blogPosts: $blogPosts({ page: p, pageSize: limit, documentLinks }).pipe(
    map(response => {
      if (isValidResponse(response)) {
        return {
          ...response,
          results: response.results
            .filter(result => !!result.uid)
            .map(result => ({
              uid: result.uid,
              title: getBlogPostTitle(result.data),
              authors: result.data.authors,
              description: result.data.description,
              thumbnailImage: result.data.thumbnail_image
            }))
        }
      }
      return response
    })
  )
}))
class BlogPostCardList extends Component {
  renderContent = () => {
    const {
      blogPosts,
      controller,
      items_per_row: itemsPerRow,
      mobile_card_type: mobileCardType,
      desktop_card_type: desktopCardType,
      image_aspect_ratio: imageAspectRatio,
      card_title_font_size: cardTitleFontSize,
      card_title_alignment: cardTitleAlignment
    } = this.props

    const cardProps = {
      desktopCardType,
      mobileCardType,
      titleFontSize: cardTitleFontSize,
      titleAlignment: cardTitleAlignment
    }

    return blogPosts.results.length ? (
      <PaginationContext.Provider
        value={{
          pageNumber: blogPosts.page,
          pageLimit: blogPosts.results_per_page,
          category: null,
          defaultQuery: { p: 1 }
        }}
      >
        <div className='mb6'>
          <CardList itemsPerRow={itemsPerRow} desktopCardType={desktopCardType} mobileCardType={mobileCardType}>
            {blogPosts.results.map(({ uid, title, description, thumbnailImage }) => (
              <Card
                key={uid}
                title={title}
                {...cardProps}
                image={thumbnailImage}
                description={description}
                imageAspectRatio={imageAspectRatio}
                linkParams={{ controller: BLOGPOST, params: { uid } }}
                descriptionComponent={<RichText className='fs fw4 mv0'>{description}</RichText>}
              />
            ))}
          </CardList>
        </div>
        {blogPosts.total_pages > 1 && (
          <PaginationContext.Consumer>
            {context => (
              <Pagination
                context={context}
                controller={controller}
                resultCount={blogPosts.total_results_size}
              />
            )}
          </PaginationContext.Consumer>
        )}
      </PaginationContext.Provider>
    ) : <p className='i'>No posts found.</p>
  }

  render () {
    return (
      <ResponseConsumer response={this.props.blogPosts} content={this.renderContent} />
    )
  }
}

export default BlogPostCardList
