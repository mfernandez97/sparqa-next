import React from 'react'
import RichText from 'components/CMS/RichText'
import StartNowForm from 'components/Account/StartNowForm'

const RegisterCTA = props => {
  const title = props.heading || 'Register Now!'
  const description = props.description || null
  const buttonText = props.button_text || 'Start Now'
  return (
    <div className='br2 flex-column items-center bg-white shadow-3-black-fade-10 pa6 gradient-blue-green-2'>
      <h2 className='mt0 mb4 white tracked b fxxl tc'>{title}</h2>
      {description && <RichText className='white fs mb4'>{description}</RichText>}
      <StartNowForm buttonText={buttonText} placeholder='Email' textColor='dark-slate-grey' />
    </div>
  )
}

export default RegisterCTA
