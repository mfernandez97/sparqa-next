import React, { Component } from 'react'
import cx from 'classnames'

export default class Typeform extends Component {
  componentDidMount () {
    initTypeform()
  }

  render () {
    const { form_url: formUrl } = this.props
    const fullHeight = this.props.full_height === 'yes'
    return formUrl ? (
      <div
        className={cx('typeform-widget w-100', {
          'vh-100-minus-nav': fullHeight,
          'h-measure-3': !fullHeight
        })}
        data-url={formUrl}
      />
    ) : null
  }
}

const initTypeform = () => {
  let js = document
  let q = document
  let d = document
  const gi = d.getElementById
  const ce = d.createElement
  const gt = d.getElementsByTagName
  const id = 'typef_orm'
  const b = 'https://embed.typeform.com/'
  if (!gi.call(d, id)) {
    js = ce.call(d, 'script')
    js.id = id
    js.src = b + 'embed.js'
    q = gt.call(d, 'script')[0]
    q.parentNode.insertBefore(js, q)
  }
}
