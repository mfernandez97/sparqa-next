import React from 'react'
import cx from 'classnames'
import { _split, _first } from 'libraries/lodash'
import { isExternalLink } from 'helpers/misc'
import RichText from 'components/CMS/RichText'

const FullWidth = props => {
  const {
    title,
    subtitle,
    pageTitle,
    main_body: mainBody,
    button_text: buttonText,
    button_link: buttonLink
  } = props

  const fullHeight = props.full_height === 'yes'

  const textColor = _first(_split(props.text_color, ' '))
  const backgroundColor = `bg-${_first(_split(props.background_color, ' '))}`
  const buttonTextColor = _first(_split(props.button_text_color, ' '))
  const buttonBackgroundColor = _first(_split(props.button_background_color, ' '))

  const mainBodyExists = !!mainBody.reduce((acc, b) => acc + b.text, '')
  const contentExists = title || subtitle || mainBodyExists || buttonText

  const backgroundImage = props.background_image.url ? {
    backgroundImage: `url(${props.background_image.url})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center center'
  } : {}

  return (
    <div
      className={cx('w-100 flex flex-column justify-center items-center', backgroundColor, {
        'pb-30 pb-56-t pb-100-m max-vh-100-minus-nav': !contentExists,
        'vh-100-minus-nav': fullHeight
      })}
      style={backgroundImage}
    >
      {contentExists && (
        <div className='layout-width-constraint max-w-measure-4 mhauto tc pv10'>
          {title && <h2 className={cx('f-graphic-32 fw5 mv0 lh-solid', textColor)}>{title}</h2>}
          {subtitle && <h3 className={cx('mb0 fxxl fw7', textColor, { mt2: title, mt0: !title })}>{subtitle}</h3>}
          {mainBodyExists && <RichText className={cx(textColor, { mt6: title || subtitle })}>{mainBody}</RichText>}
          {(buttonText && buttonLink) && (
            <a
              className={cx(
                'dib tc no-underline br-pill pv6 ph10 transition-all mt8 db-t w-80-t mhauto',
                `hover-bg-${buttonTextColor} hover-${buttonBackgroundColor}`,
                buttonTextColor,
                `bg-${buttonBackgroundColor}`,
                { mt8: mainBodyExists || title || subtitle }
              )}
              href={buttonLink}
              {...(isExternalLink(buttonLink)) && { target: '_blank', rel: 'noopener noreferrer' }}
              data-track-click
              data-track-category='Call To Action'
              data-track-action='Click'
              data-track-label={`[${buttonText}][${pageTitle}][${window.location.href}]`}
            >
              {buttonText}
            </a>
          )}
        </div>
      )}
    </div>
  )
}

export default FullWidth
