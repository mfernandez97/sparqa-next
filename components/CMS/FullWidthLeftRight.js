import React from 'react'
import cx from 'classnames'
import { _head, _split } from 'libraries/lodash'
import { isExternalLink } from 'helpers/misc'
import RichText from 'components/CMS/RichText'

const FullWidthLeftRight = props => {
  const {
    title1: title,
    subtitle,
    pageTitle,
    main_body: mainBody,
    button_text: buttonText,
    button_link: buttonLink,
    image: { url: assetUrl, alt: assetAlt }
  } = props

  const copyLeft = props.orientation !== 'content right / image left'
  const bgColor = `bg-${_head(_split(props.background_color, ' '))}`
  const textColor = _head(_split(props.text_color, ' '))
  const buttonBgColor = _head(_split(props.button_background_color, ' '))
  const buttonTextColor = _head(_split(props.button_text_color, ' '))

  const mainBodyExists = !!mainBody.reduce((acc, b) => acc + b.text, '')
  const contentExists = title || subtitle || mainBodyExists || buttonText
  const imageExists = !!assetUrl

  return (
    <div className={cx(bgColor, 'h-100')}>
      <div className={
        cx('layout-width-constraint mhauto tl pv9 flex flex-column-t flex-column-reverse-t justify-center align-center', {
          'flex-row-reverse': !copyLeft
        })
      }
      >
        {contentExists && (
          <div className={cx('grow-1 shrink-1 flex flex-column justify-center max-w-50 max-w-none-t items-center-t', {
            'items-end': !copyLeft,
            'basis-auto': imageExists,
            basis0: !imageExists
          })}
          >
            <div className='max-w-measure-3'>
              {title && <h2 className={cx('fxxxl fw7 mv0 lh-solid', textColor)}>{title}</h2>}
              {subtitle && <h3 className={cx('mb0 fxxl fw4', textColor, { mt2: title, mt0: !title })}>{subtitle}</h3>}
              {mainBodyExists && <RichText className={cx(textColor, { mt6: title || subtitle })}>{mainBody}</RichText>}
              {(buttonText && buttonLink) && (
                <a
                  className={cx(
                    'dib tc no-underline br-pill pv6 ph10 transition-all mt8 db-t w-80-t mhauto fm',
                    `hover-bg-${buttonTextColor} hover-${buttonBgColor}`,
                    buttonTextColor,
                    `bg-${buttonBgColor}`,
                    { mt8: mainBodyExists || title || subtitle }
                  )}
                  href={buttonLink}
                  {...(isExternalLink(buttonLink)) && { target: '_blank', rel: 'noopener noreferrer' }}
                  data-track-click
                  data-track-category='Call To Action'
                  data-track-action='Click'
                  data-track-label={`[${buttonText}][${pageTitle}][${window.location.href}]`}
                >
                  {buttonText}
                </a>
              )}
            </div>
          </div>
        )}
        {imageExists && (
          <div className={cx('flex justify-center align-center basis0 grow-1 shrink-1 max-w-50 max-w-none-t db-t tc', {
            'mb8-t mb6-m': contentExists
          })}
          >
            <img className='w-100 h-100 max-w-measure-3-t' src={assetUrl} alt={assetAlt} />
          </div>
        )}
      </div>
    </div>
  )
}

export default FullWidthLeftRight
