import React from 'react'
import { _includes } from 'libraries/lodash'
import Button, { themes, themeModifiers } from 'components/Button'

const CMSButton = ({ type, text, size, link, ...props }) => {
  const theme = _includes(themes, props.theme) ? props.theme : themes.LIGHT_SEA_GREEN
  const themeModifier = _includes(themeModifiers, props.theme_modifier) ? props.theme_modifier : null
  const alignment = getAlignment(props.alignment)
  const rel = link.target === '_blank' ? 'noreferrer noopener' : ''
  return (
    <div className={`pv6 flex ${alignment}`}>
      <Button anchor margin='' href={link.url} target={link.target} rel={rel} size={size} theme={theme} themeModifier={themeModifier}>{text}</Button>
    </div>
  )
}

const getAlignment = alignment => {
  switch (alignment) {
    case 'left':
      return 'justify-start'
    case 'right':
      return 'justify-end'
    default:
      return 'justify-center'
  }
}

export default CMSButton
