import React, { Component, Fragment } from 'react'
import { _omit, _snakeCase, _has } from 'libraries/lodash'
import { Media } from 'services/WindowService'
import { AUDIENCE, BLOG_POST, TOPIC } from 'services/CMSService'
import connect from 'helpers/connect'
import Image from 'components/CMS/Image'
import Quote from 'components/CMS/Quote'
import RichText from 'components/CMS/RichText'
import RegisterCTA from 'components/CMS/RegisterCTA'
import TopicCardList from 'components/CMS/TopicCardList'
import NewsletterForm from 'components/CMS/NewsletterForm'
import AudienceCardList from 'components/CMS/AudienceCardList'
import BlogPostCardList from 'components/CMS/BlogPostCardList'
import Typeform from 'components/CMS/Typeform'
import FullWidth from 'components/CMS/FullWidth'
import Boxed from 'components/CMS/Boxed'
import FullWidthLeftRight from 'components/CMS/FullWidthLeftRight'
import Testimonials from 'components/CMS/Testimonials'
import Grid from 'components/CMS/Grid'
import TeamViewer from 'components/CMS/TeamViewer'
import FullWidthLeftRightText from 'components/CMS/FullWidthLeftRightText'
import Button from 'components/CMS/Button'

@connect(() => ({
  tablet: Media.Tablet
}))
class Slice extends Component {
  renderChildren = () => {
    const childrenWithProps = React.Children.map(this.props.children, child =>
      React.cloneElement(child, { ..._omit(this.props, 'tablet'), ...child.props })
    )
    return childrenWithProps
  }

  render () {
    const { hide_on_mobile: hideOnMobile, tablet } = this.props
    const hidden = hideOnMobile && tablet
    return hidden ? null : this.renderChildren()
  }
}

const SliceContainer = ({ slice, ...props }) => {
  // NOTE: I can't remember why I had to do this. I think the json just wasn't
  // that friendly and it made it simpler to rejig it.
  const repeatedItems = slice.items.reduce((acc, item) => {
    for (const key in item) {
      if (_has(acc, key)) {
        acc[key].push(item[key])
      } else {
        acc[key] = [item[key]]
      }
    }
    return acc
  }, {})
  return (
    <Slice
      {...props}
      {...repeatedItems}
      {...slice.primary}
      type={slice.slice_type}
    >
      {sliceResolver(slice)}
    </Slice>
  )
}

const sliceResolver = (slice, props) => {
  switch (slice.slice_type) {
    case 'cards':
      return cardListResolver(slice)
    case 'text':
      return <RichText>{slice.primary.text}</RichText>
    case 'image':
      return <Image />
    case 'quote':
      return <Quote />
    case 'free_trial_form':
      return <RegisterCTA />
    case 'newsletter_form':
      return <NewsletterForm />
    case 'team_grid':
      return <TeamViewer teamMembers={slice.items.map(({ team_members: teamMembers }) => teamMembers)} />
    case 'full_width':
      return <FullWidth />
    case 'boxed':
      return <Boxed />
    case 'typeform':
      return <Typeform />
    case 'full_width_left_right':
      return <FullWidthLeftRight />
    case 'testimonials':
      return <Testimonials />
    case 'grid':
      return <Grid />
    case 'full_width_left_right_text':
      return <FullWidthLeftRightText />
    case 'button':
      return <Button />
    default:
      return <Fragment key={JSON.stringify(slice.primary)} />
  }
}

const cardListResolver = (slice, props) => {
  switch (_snakeCase(slice.primary.custom_type)) {
    case AUDIENCE:
      return <AudienceCardList {...props} />
    case BLOG_POST:
      return <BlogPostCardList {...props} />
    case TOPIC:
      return <TopicCardList {...props} />
    default:
      return <></>
  }
}

export default SliceContainer
