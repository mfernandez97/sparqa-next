import React from 'react'
import cx from 'classnames'
import { _split, _camelCase } from 'libraries/lodash'
import { renameKeys, isExternalLink } from 'helpers/misc'
import RichText from 'components/CMS/RichText'

const getColour = colour => _split(colour, ' ')[0]

const getAlignment = alignment => alignment && `t${alignment[0]}`

const FullWidthLeftRightText = props => {
  const newKeys = Object.keys(props).reduce((acc, currentValue) => ({ ...acc, ...{ [currentValue]: _camelCase(currentValue) } }), {})
  const modifiedProps = renameKeys(props, newKeys)

  const {
    leftTitle,
    leftTitleAlignment,
    leftSubtitle,
    leftSubtitleAlignment,
    leftBody,
    leftBodyAlignment,
    leftTextColor,
    leftBackgroundColor,
    leftButtonText,
    leftButtonLink,
    leftButtonTextColor,
    leftButtonBackgroundColor,
    leftButtonAlignment,
    rightTitle,
    rightTitleAlignment,
    rightSubtitle,
    rightSubtitleAlignment,
    rightBody,
    rightBodyAlignment,
    rightTextColor,
    rightBackgroundColor,
    rightButtonText,
    rightButtonLink,
    rightButtonTextColor,
    rightButtonBackgroundColor,
    rightButtonAlignment
  } = modifiedProps

  const hasLeft = leftTitle || leftSubtitle || leftBody
  const hasRight = rightTitle || rightSubtitle || rightBody
  const hasLeftButton = leftButtonText && leftButtonLink
  const hasRightButton = rightButtonText && rightButtonLink

  const backgroundContainerClassName = 'w-50 w-100-t flex pv9'
  const contentContainerClassName = 'basis0 grow-1 shrink-1 flex flex-column'
  const titleClassName = 'fxxxl fw7 mv0 lh-solid w-100'
  const subtitleClassName = 'fxxl fw4 mb0 w-100'
  const buttonClassName = 'dib db-t w-80-t tc fm no-underline br-pill mt8 pv6 ph10 transition-all pointer'

  return (
    <div className='flex flex-column-t'>
      <div className={cx(backgroundContainerClassName, 'justify-end', `bg-${getColour(leftBackgroundColor)}`)}>
        <div className='full-width-left-right-text flex'>
          {hasLeft && (
            <div className={cx(contentContainerClassName, 'items-start pr9 pr0-t')}>
              {leftTitle && (
                <h2 className={cx(titleClassName, leftTextColor, getAlignment(leftTitleAlignment) || 'tl')}>
                  {leftTitle}
                </h2>
              )}
              {leftSubtitle && (
                <h3 className={cx(subtitleClassName, leftTextColor, getAlignment(leftSubtitleAlignment) || 'tl',
                  {
                    mt2: leftTitle, mt0: !leftTitle
                  })}
                >
                  {leftSubtitle}
                </h3>
              )}
              {leftBody && (
                <RichText className={cx(leftTextColor, getAlignment(leftBodyAlignment) || 'tl', 'w-100',
                  {
                    mt6: leftTitle || leftSubtitle
                  })}
                >
                  {leftBody}
                </RichText>
              )}
              {hasLeft && hasLeftButton && (
                <a
                  className={cx(buttonClassName,
                    leftButtonTextColor, `hover-${leftButtonBackgroundColor}`,
                    `bg-${leftButtonBackgroundColor}`, `hover-bg-${leftButtonTextColor}`,
                    {
                      mlauto: leftButtonAlignment === 'right',
                      mrauto: leftButtonAlignment === 'left',
                      mhauto: leftButtonAlignment === 'center' || !leftButtonAlignment
                    }
                  )}
                  href={leftButtonLink}
                  {...isExternalLink(leftButtonLink) && { target: '_blank', rel: 'noopener noreferrer' }}
                >
                  {leftButtonText}
                </a>
              )}
            </div>
          )}
        </div>
      </div>
      <div className={cx(backgroundContainerClassName, 'justify-start', `bg-${getColour(rightBackgroundColor)}`)}>
        <div className='full-width-left-right-text flex'>
          {hasRight && (
            <div className={cx(contentContainerClassName, 'items-end pl9 pl0-t')}>
              {rightTitle && (
                <h2 className={cx(titleClassName, rightTextColor, getAlignment(rightTitleAlignment) || 'tr')}>
                  {rightTitle}
                </h2>
              )}
              {rightSubtitle && (
                <h3 className={cx(subtitleClassName, rightTextColor, getAlignment(rightSubtitleAlignment) || 'tr',
                  {
                    mt2: rightTitle, mt0: !rightTitle
                  })}
                >
                  {rightSubtitle}
                </h3>
              )}
              {rightBody && (
                <RichText className={cx(rightTextColor, getAlignment(rightBodyAlignment) || 'tr', 'w-100',
                  {
                    mt6: rightTitle || rightSubtitle
                  })}
                >
                  {rightBody}
                </RichText>
              )}
              {hasRight && hasRightButton && (
                <a
                  className={cx(buttonClassName,
                    rightButtonTextColor, `hover-${rightButtonBackgroundColor}`,
                    `bg-${rightButtonBackgroundColor}`, `hover-bg-${rightButtonTextColor}`,
                    {
                      mlauto: rightButtonAlignment === 'right',
                      mrauto: rightButtonAlignment === 'left',
                      mhauto: rightButtonAlignment === 'center' || !rightButtonAlignment
                    }
                  )}
                  href={rightButtonLink}
                  {...isExternalLink(rightButtonLink) && { target: '_blank', rel: 'noopener noreferrer' }}
                >
                  {rightButtonText}
                </a>
              )}
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

export default FullWidthLeftRightText
