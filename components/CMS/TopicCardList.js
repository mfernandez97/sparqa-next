import React, { Component } from 'react'
import { map } from 'rxjs/operators'
import { isValidResponse } from 'core/api/dataflow'
import { BLOGTOPIC, BLOGTOPICS } from 'constants/controllers'
import { $topics } from 'services/CMSService'
import connect from 'helpers/connect'
import CardList, { Card } from 'components/Card'
import Link from 'components/Link'
import ResponseConsumer from 'components/ResponseConsumer'

@connect(({ limit = 20 }) => ({
  topics: $topics({ pageSize: limit }).pipe(
    map(response => {
      if (isValidResponse(response)) {
        const moreToShow = response.total_results_size > response.results_per_page * response.page
        return {
          moreToShow,
          results: response.results
            .filter(result => !!result.uid)
            .map(result => ({
              uid: result.uid,
              icon: result.data.icon,
              name: result.data.name,
              description: result.data.description
            }))
        }
      }
      return response
    })
  )
}))
class TopicCardList extends Component {
  renderContent = () => {
    const {
      topics,
      items_per_row: itemsPerRow,
      tablet_card_type: tabletCardType,
      mobile_card_type: mobileCardType,
      desktop_card_type: desktopCardType,
      image_aspect_ratio: imageAspectRatio,
      card_title_font_size: cardTitleFontSize,
      card_title_alignment: cardTitleAlignment
    } = this.props

    const cardProps = {
      desktopCardType,
      tabletCardType,
      mobileCardType,
      titleFontSize: cardTitleFontSize,
      titleAlignment: cardTitleAlignment
    }

    return (
      <>
        <CardList itemsPerRow={itemsPerRow} desktopCardType={desktopCardType} tabletCardType={tabletCardType} mobileCardType={mobileCardType}>
          {topics.results.map(topic => (
            <Card
              {...cardProps}
              key={topic.uid}
              title={topic.name}
              image={topic.icon}
              imageAspectRatio={imageAspectRatio}
              linkParams={{ controller: BLOGTOPIC, params: { uid: topic.uid } }}
            />
          ))}
        </CardList>
        {topics.moreToShow && <Link className='fm fxl-t fw5 navy hover-navy-fade-80 hover-navy-t bt-t b--navy pv0-t mt4 dib no-underline hover-underline' controller={BLOGTOPICS}>see all topics</Link>}
      </>
    )
  }

  render () {
    return (
      <ResponseConsumer response={this.props.topics} content={this.renderContent} />
    )
  }
}

export default TopicCardList
