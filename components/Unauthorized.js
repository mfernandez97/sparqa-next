import { Component } from 'react'
import { replaceUrl, getBaseUrl } from 'front-end-core/core/history'
import getUrl from 'front-end-core/core/url'
import { LOGIN } from 'constants/controllers'
import { _startsWith } from 'libraries/lodash'

export default class Unauthorized extends Component {
  componentDidMount () {
    const path = _startsWith(location.href, getBaseUrl())
      ? location.href.substr(getBaseUrl().length)
      : location.pathname
    const redirectBase = getUrl(LOGIN)
    const redirectTo = getUrl(LOGIN, { path: path })
    !_startsWith(path, redirectBase) && setTimeout(() => replaceUrl(redirectTo), 0) // this is to wait for the end of lifecycle
  }

  render () {
    return null
  }
}
