import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { _groupBy, _includes, _isUndefined, _isNumber, _head, _compact } from 'libraries/lodash'
import { Loading, Error, Offline } from 'components/StatusBox'
import Unauthorized from 'components/Unauthorized'
import Forbidden from 'components/Forbidden'
import {
  RESPONSE_PENDING,
  RESPONSE_OFFLINE,
  RESPONSE_BADRESPONSE,
  RESPONSE_BADDATA,
  RESPONSE_NOTFOUND,
  RESPONSE_UNAUTHORIZED,
  RESPONSE_FORBIDDEN
} from 'front-end-core/constants/responses'

export default class ResponseConsumer extends Component {
  renderStatuses (responses) {
    const {
      pendingComponent,
      baddataComponent,
      badresponseComponent,
      notfoundComponent,
      offlineComponent,
      unauthorizedComponent,
      forbiddenComponent,
      baddataProps,
      badresponseProps,
      notfoundProps,
      offlineProps,
      unauthorizedProps,
      forbiddenProps
    } = this.props

    const normalizedResponses = normalizeResponses(responses)
    const grouped = _groupBy(normalizedResponses, ({ statusCode }) => statusCode)

    if (grouped.hasOwnProperty(RESPONSE_PENDING)) { // eslint-disable-line
      return <Render component={pendingComponent} />
    }

    if (grouped.hasOwnProperty(RESPONSE_BADDATA)) { // eslint-disable-line
      return (
        <Render
          component={baddataComponent}
          props={baddataProps}
          message={getMessage(grouped[RESPONSE_BADDATA]) || null}
          status={getStatus(grouped[RESPONSE_BADDATA]) || baddataProps.title}
        />
      )
    }

    if (grouped.hasOwnProperty(RESPONSE_BADRESPONSE)) { // eslint-disable-line
      return (
        <Render
          component={badresponseComponent}
          props={badresponseProps}
          message={getMessage(grouped[RESPONSE_BADRESPONSE]) || null}
          status={getStatus(grouped[RESPONSE_BADRESPONSE]) || badresponseProps.title}
        />
      )
    }

    if (grouped.hasOwnProperty(RESPONSE_NOTFOUND)) { // eslint-disable-line
      return (
        <Render
          component={notfoundComponent}
          props={notfoundProps}
          message={getMessage(grouped[RESPONSE_NOTFOUND]) || null}
          status={getStatus(grouped[RESPONSE_NOTFOUND]) || notfoundProps.title}
        />
      )
    }

    if (grouped.hasOwnProperty(RESPONSE_OFFLINE)) { // eslint-disable-line
      return (
        <Render
          component={offlineComponent}
          props={offlineProps}
          message={getMessage(grouped[RESPONSE_OFFLINE]) || null}
          status={getStatus(grouped[RESPONSE_OFFLINE]) || offlineProps.title}
        />
      )
    }

    if (grouped.hasOwnProperty(RESPONSE_UNAUTHORIZED)) { // eslint-disable-line
      return (
        <Render
          component={unauthorizedComponent}
          props={unauthorizedProps}
          message={getMessage(grouped[RESPONSE_UNAUTHORIZED]) || null}
          status={getStatus(grouped[RESPONSE_UNAUTHORIZED]) || unauthorizedProps.title}
        />
      )
    }

    if (grouped.hasOwnProperty(RESPONSE_FORBIDDEN)) { // eslint-disable-line
      return (
        <Render
          component={forbiddenComponent}
          props={forbiddenProps}
          message={getMessage(grouped[RESPONSE_FORBIDDEN]) || null}
          status={getStatus(grouped[RESPONSE_FORBIDDEN]) || forbiddenProps.title}
        />
      )
    }

    return null
  }

  renderError (e) {
    console.error(e)
    return <Render component={Error} />
  }

  render () {
    try {
      const { content, responses, response } = this.props
      const allResponses = _compact(responses.concat(
        _isUndefined(response) ? [] : [response]
      ))

      const notOk = allResponses.reduce((accumulator, response) => {
        if (!responseOk(response.statusCode)) {
          return [...accumulator, response]
        }
        return accumulator
      }, [])

      return notOk.length ? this.renderStatuses(notOk) : content()
    } catch (e) {
      this.renderError(e)
    }
  }
}

const Render = ({ component: Component, props, ...rest }) =>
  !Component ? null : <Component {...props} {...rest} />

const responseOk = status => {
  return !_includes(
    [
      RESPONSE_PENDING,
      RESPONSE_OFFLINE,
      RESPONSE_BADRESPONSE,
      RESPONSE_BADDATA,
      RESPONSE_NOTFOUND,
      RESPONSE_UNAUTHORIZED,
      RESPONSE_FORBIDDEN
    ],
    status
  )
}

const normalizeResponses = responses => {
  return responses.map(
    response => _isNumber(response) ? { statusCode: response, message: null } : response
  )
}

const getThing = what => responses => _head(responses)[what]
const getMessage = getThing('message')
const getStatus = getThing('status')

ResponseConsumer.propTypes = {
  pendingComponent: PropTypes.func,
  baddataProps: PropTypes.shape({
    title: PropTypes.string
  }),
  baddataComponent: PropTypes.func,
  badresponseProps: PropTypes.shape({
    title: PropTypes.string
  }),
  badresponseComponent: PropTypes.func,
  notfoundProps: PropTypes.shape({
    title: PropTypes.string
  }),
  notfoundComponent: PropTypes.func,
  offlineProps: PropTypes.shape({
    title: PropTypes.string
  }),
  offlineComponent: PropTypes.func,
  unauthorizedProps: PropTypes.shape({
    title: PropTypes.string
  }),
  unauthorizedComponent: PropTypes.func,
  forbiddenProps: PropTypes.shape({}),
  forbiddenComponent: PropTypes.func,
  response: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
    PropTypes.bool
  ]),
  responses: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object,
      PropTypes.number,
      PropTypes.array,
      PropTypes.bool
    ])
  ),
  content: PropTypes.func
}

ResponseConsumer.defaultProps = {
  pendingComponent: Loading,
  baddataProps: { title: 'Data loading error' },
  baddataComponent: Error,
  badresponseProps: { title: 'Data loading error' },
  badresponseComponent: Error,
  notfoundProps: { title: 'Not found' },
  notfoundComponent: Error,
  offlineProps: { title: 'Not available offline' },
  offlineComponent: Offline,
  unauthorizedProps: { title: 'Not logged in' },
  unauthorizedComponent: Unauthorized,
  forbiddenProps: {},
  forbiddenComponent: Forbidden,
  responses: [],
  content: () => null
}
