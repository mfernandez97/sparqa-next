import React from 'react'
import cx from 'classnames'
import Icon from 'components/Icon'

const BackButton = ({ children, onBack, className }) => (
  <button
    className={cx(className, 'white fm b pv7 ph8 bg-steel-blue w-100 lh-copy relative pointer')}
    onClick={onBack}
  >
    <Icon className='w-icon-4 mr3 absolute center-v left-layout-padding' name='chevron-left-alt' />
    {children}
  </button>
)

export default BackButton
