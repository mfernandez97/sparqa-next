import React from 'react'
import Icon from 'components/Icon'
import AccountMenu from 'layout/header/AccountMenu'

const MobileAccountMenu = ({ exitAccount, user }) => (
  <div className='flex grow-1 flex-column'>
    <button
      className='shrink-0 grow-0 layout-width-constraint tl pt6 pb0 bg-transparent ma0 w-50 pointer'
      onClick={exitAccount}
    >
      <Icon className='black w-icon-6' name='arrow-left' />
    </button>
    <AccountMenu visible {...{ user }} />
  </div>
)

export default MobileAccountMenu
