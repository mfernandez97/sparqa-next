import React from 'react'
import Link, { DOCUMENTSMENU } from 'components/Link'
import BackButton from 'components/MobileMenu/BackButton'

const DocumentSubcategories = ({ onBack }) => {
  const linkClassName = 'b fl dark-slate-grey w-100 bg-transparent pv6 layout-width-constraint no-underline tl bb b--midnight-blue-2-lighten-5 db lh-copy'
  return (
    <div className='list-reset pa0 ma0'>
      <BackButton onBack={onBack}>Documents</BackButton>
      <Link controller={DOCUMENTSMENU} className={linkClassName}>
        Documents
      </Link>
      <Link controller={DOCUMENTSMENU} params={{ category: 'toolkits' }} className={linkClassName}>
        Toolkits
      </Link>
    </div>
  )
}

export default DocumentSubcategories
