import React, { Component } from 'react'
import { _last, _filter, _map, _sortBy } from 'libraries/lodash'
import connect from 'helpers/connect'
import { $sectionIdToUrlMap, $topicQuestionIdToUrlMap } from 'services/SeoService'
import { $questions, $rootTopics, $flattenedTopics, getParentTopicId } from 'services/TopicService'
import ResponseConsumer from 'components/ResponseConsumer'
import Link, { TOPICSCONTENT } from 'components/Link'
import BackButton from 'components/MobileMenu/BackButton'

export default class TopicsBrowser extends Component {
  state = { path: [] }

  handleClickTopic = (topicId, title, children) => e => {
    const { path } = this.state
    this.setState({ path: [...path, { topicId, title, children }] })
  }

  handlePreviousTopic = e => {
    const { path } = this.state
    this.setState({ path: path.slice(0, path.length - 1) })
  }

  render () {
    const { path } = this.state
    const { exitBrowser } = this.props
    const currentTopic = _last(path)
    return !path.length ? (
      <RootTopics onClick={this.handleClickTopic} onBack={exitBrowser} />
    ) : (
      <Topics
        topicId={currentTopic.topicId}
        title={currentTopic.title}
        children={currentTopic.children}
        onClick={this.handleClickTopic}
        onBack={this.handlePreviousTopic}
      />
    )
  }
}

@connect(() => ({
  rootTopics: $rootTopics()
}))
class RootTopics extends Component {
  renderItem = topic => (
    <TopicItem key={topic.id} {...{ ...topic, onClick: this.props.onClick }} />
  )

  renderContent = () => {
    const { rootTopics, onBack } = this.props
    return (
      <ul className='list-reset pa0 ma0'>
        <li><BackButton onBack={onBack}>Guidance</BackButton></li>
        {rootTopics.map(this.renderItem)}
      </ul>
    )
  }

  render () {
    const { rootTopics } = this.props
    return <ResponseConsumer response={rootTopics} content={this.renderContent} />
  }
}

@connect(({ topicId }) => ({
  allTopics: $flattenedTopics({ topicId })
}))
class Topics extends Component {
  renderItem = topic => (
    <ConnectedTopicItem key={topic.id} {...{ ...topic, topics: this.props.allTopics, onClick: this.props.onClick }} />
  )

  renderContent = () => {
    const { title, onBack, children } = this.props
    return (
      <ul className='list-reset pa0 ma0'>
        <li key='back'><BackButton onBack={onBack}>{title}</BackButton></li>
        {_map(children, this.renderItem)}
      </ul>
    )
  }

  render () {
    const { allTopics } = this.props
    return <ResponseConsumer response={allTopics} content={this.renderContent} />
  }
}

const TopicItem = ({ id, path, title, children, topics, questions, onClick, sectionIdToUrl, questionIdToUrl }) => {
  const className = 'b fl dark-slate-grey w-100 bg-transparent pv6 layout-width-constraint no-underline tl bb b--midnight-blue-2-lighten-5 db lh-copy pointer'
  if (children.length <= 0) {
    const topicQuestions = _filter(questions, ({ topicId }) => topicId === id)
    const orderedTopicQuestions = _sortBy(topicQuestions, ({ questionNumber }) => Number(questionNumber))
    const firstQuestion = orderedTopicQuestions[0]
    if (!firstQuestion) {
      return null
    }
    const section = sectionIdToUrl[path[0]]
    const topicName = sectionIdToUrl[getParentTopicId({ topics, questions, questionId: firstQuestion.tocId })]
    const questionName = questionIdToUrl[firstQuestion.tocId]
    return (
      <li>
        <Link
          data-toggle-menu={false}
          className={className}
          controller={TOPICSCONTENT}
          params={{ section, topicName, questionName }}
        >
          {title}
        </Link>
      </li>
    )
  }
  return (
    <li>
      <button className={className} onClick={onClick(id, title, children)}>
        {title}
      </button>
    </li>
  )
}

const ConnectedTopicItem = connect(({ parentId }) => ({
  questions: $questions({ topicId: parentId }),
  sectionIdToUrl: $sectionIdToUrlMap,
  questionIdToUrl: $topicQuestionIdToUrlMap
}))(TopicItem)
