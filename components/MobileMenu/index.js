import React, { Component, useEffect, useState } from 'react'
import cx from 'classnames'
import { BLOG, AAL } from 'constants/cms-pages'
import Link, { LOGIN, ABOUT, REGISTER, NOTFOUND, DOCUMENTSMENU } from 'components/Link'
import { URL_TERMS_OF_USE, URL_PRIVACY_POLICY, URL_FAQS, URL_REFERRAL_SERVICE_TERMS } from 'constants/links'
import Icon from 'components/Icon'
import SocialIcon from 'components/SocialIcon'
import ScrollingContainer, { STATE_KEYS } from 'components/ScrollingContainer'
import TopicsBrowser from 'components/MobileMenu/TopicsBrowser'
import AccountMenu from 'components/MobileMenu/AccountMenu'
import DocumentSubcategories from 'components/MobileMenu/DocumentSubcategories'

const MobileMenu = ({ visible, searchMode, mobile, user, isLogged }) => {
  const [browserMode, setBrowserMode] = useState(false)
  const [accountMode, setAccountMode] = useState(false)
  const [subcategoriesMode, setSubcategoriesMode] = useState(false)

  useEffect(() => {
    if (visible) {
      setBrowserMode(false)
      setAccountMode(false)
    }
  }, [visible, setBrowserMode, setAccountMode])

  const baseClassName = 'pa0 w-100 slide fixed z105'

  return (
    <nav className={cx('mobile-menu bg-white',
      baseClassName,
      {
        'x-vw-100': !visible,
        x0: visible
      })}
    >
      <ScrollingContainer
        className={cx('vh-100-minus-nav', { flex: accountMode })}
        stateKey={STATE_KEYS.MOBILE_MENU}
      >
        <MobileMenuContent
          {...{
            searchMode,
            mobile,
            browserMode,
            accountMode,
            subcategoriesMode,
            setBrowserMode,
            setSubcategoriesMode,
            setAccountMode,
            user,
            isLogged
          }}
        />
      </ScrollingContainer>
    </nav>
  )
}

export default MobileMenu

const MobileMenuContent = ({
  browserMode,
  accountMode,
  subcategoriesMode,
  setBrowserMode,
  setSubcategoriesMode,
  setAccountMode,
  user,
  isLogged
}) => {
  const handleToggleBrowser = bool => e => setBrowserMode(bool)
  const handleToggleDocumentSubcategoriesMode = bool => e => setSubcategoriesMode(bool)
  const handleToggleAccount = bool => e => setAccountMode(bool)

  if (browserMode) {
    return <TopicsBrowser exitBrowser={handleToggleBrowser(false)} />
  }
  if (accountMode) {
    return <AccountMenu exitAccount={handleToggleAccount(false)} {...{ user }} />
  }
  if (subcategoriesMode) {
    return <DocumentSubcategories onBack={handleToggleDocumentSubcategoriesMode(false)} />
  }

  return (
    <div className='dark-slate-grey'>
      <AccountLinks onEnterAccount={handleToggleAccount(true)} {...{ isLogged }} />
      <FeatureLinks
        onEnterBrowser={handleToggleBrowser(true)}
        onEnterDocumentSubcategories={handleToggleDocumentSubcategoriesMode(true)}
      />
      <ul className='list-reset ma0 layout-width-constraint ml7'>
        <StaticLinks {...{ isLogged }} />
        <ExternalLinks />
      </ul>
      <SocialLinks />
    </div>
  )
}

class AccountLinks extends Component {
  get className () {
    return 'dark-slate-grey bb b--midnight-blue-2-lighten-10 bg-white fm b layout-width-constraint pv7 no-underline flex items-center justify-between w-100 pointer'
  }

  renderLoginLink () {
    return (
      <Link
        data-toggle-menu={false}
        className={this.className}
        controller={LOGIN}
        params={{ path: location.pathname.substr(1) }}
      >
        <span className='ml7'>Register / Login</span>
        <Icon className='w-icon-5' name='user' />
      </Link>
    )
  }

  renderAccountButton () {
    return (
      <button className={this.className} onClick={this.props.onEnterAccount}>
        <span className='ml7'>My account</span>
        <Icon className='icon-fill-black w-icon-5 mr5' name='user' />
      </button>
    )
  }

  render () {
    return this.props.isLogged ? this.renderAccountButton() : this.renderLoginLink()
  }
}

const FeatureLinks = () => {
  const baseClassName = cx(
    'db dark-slate-grey b fm layout-width-constraint pv7 bb b--midnight-blue-2-lighten-10 w-100 tl bg-transparent',
    'no-underline flex items-center justify-between pointer'
  )

  return (
    <>
      <Link data-toggle-menu={false} controller={NOTFOUND} params={{ path: 'topics' }} className={baseClassName}>
        <span className='ml7'>Guidance</span>
        <Icon className='w-icon-4 mr5 black' name='chevron-right-alt' />
      </Link>
      <Link data-toggle-menu={false} controller={DOCUMENTSMENU} className={baseClassName}>
        <span className='ml7'>Documents</span>
        <Icon className='w-icon-4 mr5 black' name='chevron-right-alt' />
      </Link>
      <Link data-toggle-menu={false} controller={DOCUMENTSMENU} params={{ category: 'toolkits' }} className={baseClassName}>
        <span className='ml7'>Toolkits</span>
        <Icon className='w-icon-4 mr5 black' name='chevron-right-alt' />
      </Link>
      <Link data-toggle-menu={false} controller={NOTFOUND} params={{ path: AAL }} className={baseClassName}>
        <span className='ml7'>Ask a Lawyer</span>
        <Icon className='w-icon-4 mr3 black' name='chevron-right-alt' />
      </Link>
      <Link data-toggle-menu={false} controller={NOTFOUND} params={{ path: BLOG }} className={baseClassName}>
        <span className='ml7'>Insights</span>
        <Icon className='w-icon-4 h-icon-4 mr5 black' name='blogger' />
      </Link>
    </>
  )
}

const StaticLinks = ({ isLogged }) => {
  const className = 'db black fm fw6 no-underline pv6'
  const links = [
    { text: 'About', controller: ABOUT },
    ...isLogged ? [] : [{ text: 'Pricing', controller: REGISTER }]
  ]

  return (
    links.map(({ text, controller = null, href = null }) => (
      <li key={controller}>
        {controller ? (
          <Link data-toggle-menu={false} {...{ className, controller }}>
            {text}
          </Link>
        ) : (
          <a data-toggle-menu={false} {...{ className, controller }}>
            {text}
          </a>
        )}
      </li>
    ))
  )
}

const externalLinks = [
  { text: 'Referral Service T&Cs', href: URL_REFERRAL_SERVICE_TERMS },
  { text: 'Privacy Policy', href: URL_PRIVACY_POLICY },
  { text: 'T&Cs', href: URL_TERMS_OF_USE },
  { text: 'FAQs', href: URL_FAQS }
]

const ExternalLinks = () => externalLinks.map(({ text, href }) => (
  <li key={href}>
    <a className='db black fm fw6 no-underline pv6' href={href}>
      {text}
    </a>
  </li>
))

const socialMediaVendors = ['facebook', 'twitter', 'linkedin']

const SocialLinks = () => (
  <div className='flex justify-start items-center pv8 layout-width-constraint'>
    {socialMediaVendors.map(type => (
      <SocialIcon
        key={type}
        className='mr6'
        {...{ type }}
        mobile
      />
    ))}
  </div>
)
