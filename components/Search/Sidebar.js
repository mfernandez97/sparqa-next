import React, { Component } from 'react'
import getUrl from 'front-end-core/core/url'
import { replaceUrl } from 'front-end-core/core/history'
import { _get } from 'libraries/lodash'
import * as categories from 'constants/categories'
import { SEARCH, DOCUMENTSMENU } from 'constants/controllers'
import { updateQueryParams } from 'services/SearchService'
import { $topicResults } from 'services/TopicService'
import { $documentResults, $bundleResults } from 'services/DocumentService'
import connect from 'helpers/connect'
import Sidebar from 'components/Filters/Sidebar'
import Link from 'components/Link'

const $items = (category, params) => {
  switch (category) {
    case categories.DOCUMENTS:
      return $documentResults(params)
    case categories.BUNDLES:
      return $bundleResults(params)
    default:
      return $topicResults(params)
  }
}

export default @connect(({ searchPhrase, pageNumber, pageLimit, query, category }) => ({
  items: $items(category, { searchPhrase, pageNumber, pageLimit, query })
}))
class Container extends Component {
  onChangeFacetOption = (parameterName, value, currentlySelected) => {
    const turningOn = !currentlySelected

    const { query, category, defaultQuery, controller } = this.props
    const newQuery = updateQueryParams(query, parameterName, value, { removeParam: !turningOn })

    // TODO: check "turningOn"
    const params = controller === SEARCH || (controller === DOCUMENTSMENU && category === categories.BUNDLES)
      ? { category }
      : {}

    const url = getUrl(controller, params, { ...defaultQuery, ...newQuery, p: turningOn ? defaultQuery.p : '1' })
    replaceUrl(url)
  }

  render () {
    const { items, category, className, searchPhrase, controller } = this.props
    return (
      <div className={className}>
        <Link
          controller={controller}
          params={{ category }}
          query={{ q: searchPhrase }}
          className='no-underline bg-transparent fs grey b pv3 ph6 mt6 dn-d'
        >
          Reset filters
        </Link>
        <Sidebar
          facets={_get(items, 'facets', [])}
          responses={[items]}
          resultCount={_get(items, 'resultCount', null)}
          handleChangeOption={this.onChangeFacetOption}
        />
      </div>
    )
  }
}
