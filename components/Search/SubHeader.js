import React from 'react'
import { Media } from 'services/WindowService'
import { $documentResults, $bundleResults } from 'services/DocumentService'
import { $topicResults } from 'services/TopicService'
import connect from 'helpers/connect'
import { SearchConsumer } from 'context'
import { SubHeader } from 'components/Filters'
import CategoryFilters from './CategoryFilters'
import Sidebar from './Sidebar'

const ConnectedSubHeader = connect(({ searchPhrase, pageNumber, pageLimit, query }) => ({
  topicResults: $topicResults({ searchPhrase, pageNumber, pageLimit, query }),
  bundleResults: $bundleResults({ searchPhrase, pageNumber, pageLimit, query }),
  documentResults: $documentResults({ searchPhrase, pageNumber, pageLimit, query }),
  tablet: Media.Tablet
}))(props => {
  const { topicResults, bundleResults, documentResults } = props
  return (
    <SubHeader
      TabFiltersComponent={CategoryFilters}
      SidebarFiltersComponent={Sidebar}
      responses={[topicResults, bundleResults, documentResults]}
      {...props}
    />
  )
})

const SubHeaderWithSearchContext = props => (
  <SearchConsumer>
    {context => <ConnectedSubHeader {...context} {...props} />}
  </SearchConsumer>
)

export default SubHeaderWithSearchContext
