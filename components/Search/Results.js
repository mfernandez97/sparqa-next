import React, { Component } from 'react'
import * as categories from 'constants/categories'
import connect from 'helpers/connect'
import { $isSubscribedUser } from 'services/UserService'
import { $flattenedTopics, $topicResults } from 'services/TopicService'
import { $documentResults, $bundleResults, $purchasedDocumentsIds } from 'services/DocumentService'
import { $bundleSeoIdToUrl, $documentSeoIdToUrl, $sectionIdToUrlMap, $topicQuestionIdToUrlMap } from 'services/SeoService'
import { Media } from 'services/WindowService'
import { SearchConsumer } from 'context'
import ResponseConsumer from 'components/ResponseConsumer'
import { DocumentResults, BundleResults, TopicResults, Pagination } from 'components/Results'

@connect(({ includeTopicResults, searchPhrase, pageNumber, pageLimit, query }) => ({
  ...(includeTopicResults && {
    allTopics: $flattenedTopics({ topicId: null }),
    topics: $topicResults({ searchPhrase, pageNumber, pageLimit, query }),
    questionIdUrlMap: $topicQuestionIdToUrlMap
  }),
  sectionIdUrlMap: $sectionIdToUrlMap,
  bundles: $bundleResults({ searchPhrase, pageNumber, pageLimit, query }),
  bundleIdUrlMap: $bundleSeoIdToUrl,
  documents: $documentResults({ searchPhrase, pageNumber, pageLimit, query }),
  documentIdUrlMap: $documentSeoIdToUrl,
  mobile: Media.Mobile,
  purchasedProductIds: $purchasedDocumentsIds(),
  isSubscribed: $isSubscribedUser
}))
class Container extends Component {
  get results () {
    const { category, topics, bundles, documents } = this.props
    switch (category) {
      case categories.DOCUMENTS:
        return documents.results
      case categories.BUNDLES:
        return bundles.results
      default:
        return topics.results
    }
  }

  get resultCount () {
    const { category, topics, bundles, documents } = this.props
    switch (category) {
      case categories.DOCUMENTS:
        return documents.resultCount
      case categories.BUNDLES:
        return bundles.resultCount
      default:
        return topics.resultCount
    }
  }

  renderContent = () => (
    <div className={this.props.className}>
      <Results results={this.results} {...this.props} />
      {!!this.resultCount && (
        <SearchConsumer>
          {context => <Pagination context={context} resultCount={this.resultCount} controller={this.props.controller} />}
        </SearchConsumer>
      )}
    </div>
  )

  render () {
    const {
      topics,
      bundles,
      category,
      documents,
      allTopics,
      className,
      bundleIdUrlMap,
      sectionIdUrlMap,
      questionIdUrlMap,
      documentIdUrlMap,
      purchasedProductIds,
      isSubscribed
    } = this.props
    return (
      <ResponseConsumer
        responses={[
          topics,
          bundles,
          documents,
          allTopics,
          bundleIdUrlMap,
          sectionIdUrlMap,
          questionIdUrlMap,
          documentIdUrlMap,
          purchasedProductIds,
          isSubscribed
        ]}
        content={this.renderContent}
        unauthorizedComponent={() => this.renderContent()}
        pendingComponent={() => (
          <div className={className}>
            <Results pending category={category} />
          </div>
        )}
      />
    )
  }
}

const Results = props => {
  const { category } = props
  switch (category) {
    case categories.DOCUMENTS:
      return <DocumentResults {...props} />
    case categories.BUNDLES:
      return <BundleResults {...props} />
    default:
      return <TopicResults {...props} />
  }
}

export default Container
