import React, { Component } from 'react'
import PropTypes from 'prop-types'
import pageState from 'helpers/pageState'
import Sidebar from './Sidebar'
import Results from './Results'

@pageState({ topicsQuery: {}, documentsQuery: {}, bundlesQuery: {} }, true)
class Main extends Component {
  render () {
    return (
      <>
        {this.props.metaDataComponent}
        <div className='bg-search-main'>
          <div className='layout-width-constraint flex pb12 pb6-t pt6-t'>
            <Sidebar
              className='bg-alice-blue basis-auto min-vh-100 dn-t'
              {...this.props}
              defaultQuery={{
                q: this.props.searchPhrase,
                p: 1,
                l: this.props.pageLimit
              }}
            />
            <Results className='grow-1 basis0 pl7 pt9 pl0-t pt0-t' {...this.props} />
          </div>
        </div>
      </>
    )
  }
}

Main.propTypes = {
  searchPhrase: PropTypes.string,
  pageLimit: PropTypes.number,
  pageNumber: PropTypes.number,
  category: PropTypes.string.isRequired,
  query: PropTypes.object
}

Main.defaultProps = {
  pageLimit: 20,
  pageNumber: 1,
  query: {}
}

export default Main
