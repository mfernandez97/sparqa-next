import React from 'react'
import { _get } from 'libraries/lodash'
import * as categories from 'constants/categories'
import { TOPIC, DOCUMENT, BUNDLE } from 'constants/category-filters'
import Link, { SEARCH, DOCUMENTSMENU } from 'components/Link'
import Tabs, { Tab, TabLabel } from 'components/Filters/Tabs'

const CategoryFilters = ({
  category,
  defaultQuery,
  topicResults,
  bundleResults,
  documentResults,
  includeTopicResults,
  ...rest
}) => {
  const getCategoryQuery = category => rest[`${category}Query`] || {}
  const categoryFilters = []
  if (includeTopicResults) {
    categoryFilters.push({
      ...TOPIC,
      isActive: category === categories.TOPICS,
      resultCount: _get(topicResults, 'resultCount', null)
    })
  }
  categoryFilters.push({
    ...DOCUMENT,
    isActive: category === categories.DOCUMENTS,
    resultCount: _get(documentResults, 'resultCount', null)
  }, {
    ...BUNDLE,
    isActive: category === categories.BUNDLES,
    resultCount: _get(bundleResults, 'resultCount', null)
  })

  return (
    <div className='layout-width-constraint-d'>
      <div className='ph0 search-results-category-filters-ml'>
        <Tabs>
          {categoryFilters.map((tab, index, tabs) => {
            return (
              <Tab
                key={tab.name}
                first={index === 0}
                last={index === tabs.length - 1}
                label={
                  <Filter
                    {...tab}
                    defaultQuery={defaultQuery}
                    getCategoryQuery={getCategoryQuery}
                    showNewTag={false}
                    controller={rest.controller}
                  />
                }
                {...tab}
              />
            )
          })}
        </Tabs>
      </div>
    </div>
  )
}

const Filter = ({ name, category, isActive, resultCount, colour, showNewTag, defaultQuery, getCategoryQuery, controller }) => {
  const query = getCategoryQuery(category)
  const params = controller === SEARCH || (controller === DOCUMENTSMENU && category === categories.BUNDLES)
    ? { category }
    : {}

  return (
    <Link
      {...{ controller, params }}
      query={{ ...defaultQuery, ...query }}
      className='no-underline'
    >
      <TabLabel name={name} category={category} isActive={isActive} resultCount={resultCount} colour={colour} showNewTag={showNewTag} />
    </Link>
  )
}

export default CategoryFilters
