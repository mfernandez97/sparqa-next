import React from 'react'
import cx from 'classnames'
import { _isUndefined } from 'libraries/lodash'
import { ButtonContainer } from './esv-react-carousel'
import Icon from 'components/Icon'

const Button = ({
  position = 'left',
  positionClassName = 'top-50 y--50',
  children,
  enabledClassName,
  disabledClassName,
  leftClassName,
  rightClassName,
  ...props
}) => (
  <ButtonContainer position={position}>
    {({ disabled, eventHandlers }) => (
      <button
        type='button'
        {...eventHandlers}
        {...props}
        className={cx('absolute dn-xs-m bg-transparent',
          positionClassName, {
            [`${enabledClassName} pointer`]: !disabled,
            [disabledClassName]: disabled,
            left0: position === 'left' && _isUndefined(leftClassName),
            right0: position !== 'left' && _isUndefined(rightClassName),
            [leftClassName]: position === 'left' && !_isUndefined(leftClassName),
            [rightClassName]: position !== 'left' && !_isUndefined(rightClassName)
          })}
      >
        {React.Children.count(children) > 0 ? (
          children
        ) : (
          <Icon
            name={position === 'left' ? 'chevron-left' : 'chevron-right'}
            className='w-icon-10 h-icon-10'
          />
        )}
      </button>
    )}
  </ButtonContainer>
)

export default Button
