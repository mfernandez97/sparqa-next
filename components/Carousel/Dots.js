import React from 'react'
import cx from 'classnames'
import { DotsContainer } from './esv-react-carousel'

const Dot = ({ active, onClick, activeClassName, inactiveClassName }) => (
  <button
    type='button'
    onClick={onClick}
    className={cx('pa3 br-100 pointer', {
      [activeClassName]: active,
      [inactiveClassName]: !active
    })}
  />
)

const Dots = ({
  className,
  makeDotProps,
  activeDotClassName,
  inactiveDotsClassName
}) => (
  <div className={cx(className, 'flex justify-between items-center')}>
    <DotsContainer>
      {({ dotsProps }) => dotsProps.map((props, i) => (
        <Dot
          key={i}
          {...props}
          {...(makeDotProps ? makeDotProps(i) : {})}
          activeClassName={activeDotClassName}
          inactiveClassName={inactiveDotsClassName}
        />
      ))}
    </DotsContainer>
  </div>
)

export default Dots
