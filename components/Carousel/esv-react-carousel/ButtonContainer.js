import React, { createContext } from 'react'
import { CarouselConsumer } from './Container'

const { Provider, Consumer: ButtonConsumer } = createContext()

const ConnectedButton = combine => ({ children }) => (
  <CarouselConsumer>
    {({ state: { numSlides, currentIndex }, actions: { setCurrentIndex } }) => {
      const nextIndex = combine(currentIndex, 1)
      const disabled = nextIndex < 0 || nextIndex > numSlides - 1
      const onClick = e => {
        const nextIndex = combine(currentIndex, 1)
        if (disabled) {
          return
        }
        setCurrentIndex(nextIndex)
      }
      const onMouseUp = e => e.stopPropagation()
      const onTouchEnd = e => e.stopPropagation()
      const eventHandlers = { onMouseUp, onTouchEnd, onClick }
      return (
        <Provider value={{ disabled, eventHandlers }}>
          <ButtonConsumer>
            {children}
          </ButtonConsumer>
        </Provider>
      )
    }}
  </CarouselConsumer>
)

const ButtonSwitch = ({ position = 'left', children }) => {
  const Container = position === 'left'
    ? ConnectedButton((a, b) => a - b)
    : ConnectedButton((a, b) => a + b)
  return <Container>{children}</Container>
}

export default ButtonSwitch
