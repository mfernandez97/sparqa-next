import React, { Component, createRef } from 'react'
import cx from 'classnames'
import { CarouselConsumer, CarouselContext } from './Container'

class Carousel extends Component {
  // Refs
  swiper = createRef()
  container = createRef()

  // FPS limit
  startTime = null
  fpsInterval = 1000 / 60

  // Drag and drop
  left = 0
  dragging = false
  dragStartX = 0
  upperBound = 0
  overallDragStartX = 0

  get lowerBound () {
    const { numSlides } = this.props
    return (numSlides - 1) * this.slideWidth * -1
  }

  componentDidMount () {
    window.addEventListener('mouseup', this.onDragEndMouse)
    window.addEventListener('touchend', this.onDragEndTouch)
    window.addEventListener('resize', () => {
      // Recalculate widths on resize.
      this.setSlideWidth()
      this.setFinalPosition()
      this.forceUpdatePosition()
    })

    this.props.setNumSlides(React.Children.count(this.props.children))

    // Set initial position.
    this.setSlideWidth()
    this.setFinalPosition()
    this.forceUpdatePosition()

    // setTimeout hack to prevent the transition class getting added too soon.
    setTimeout(this.enableTransitionEffect, 0)
  }

  componentWillUnmount () {
    window.removeEventListener('mouseup', this.onDragEndMouse)
    window.removeEventListener('touchend', this.onDragEndTouch)
    window.removeEventListener('resize', this.onDragEndTouch)
    window.removeEventListener('mousemove', this.onMouseMove)
    window.removeEventListener('touchmove', this.onTouchMove)
    this.subscription && this.subscription.unsubscribe()
  }

  componentDidUpdate (prevProps) {
    const { currentIndex } = this.props
    // If the current slide index has changed...
    if (prevProps.currentIndex !== currentIndex) {
      // Then update the carousel position accordingly.
      this.setFinalPosition()
      this.forceUpdatePosition()
      this.overallDragStartX = this.left
    }
  }

  enableTransitionEffect = () => {
    this.swiper.current.classList.add('transition-all')
  }

  disableTransitionEffect = () => {
    this.swiper.current.classList.remove('transition-all')
  }

  setSlideWidth = () => {
    // The slides are the same width as the outer container. Need this for
    // calculating carousel final position.
    if (this.container.current) {
      this.slideWidth = this.container.current.offsetWidth
    }
  }

  setFinalPosition = () => {
    // The carousel must always come to rest on the current slide.
    this.left = this.props.currentIndex * this.slideWidth * -1
  }

  updatePosition = () => {
    // If the user is still dragging...
    if (this.dragging) {
      // Then keep calling this function!
      window.requestAnimationFrame(this.updatePosition)
    }

    const now = Date.now()
    // Work out how long it's been since the last carousel position update.
    const elapsed = now - this.startTime

    // Update the carousel position, but don't go crazy and update literally
    // every time this function is called. Make sure a bit of time has elapsed
    // since the last update.
    if (this.dragging && elapsed > this.fpsInterval) {
      this.swiper.current.style.transform = `translateX(${this.left}px)`
      this.startTime = Date.now()
    }
  }

  forceUpdatePosition = () => {
    // Just update the position with none of that time elapsed or
    // requestAnimationFrame nonsense.
    if (this.swiper.current) {
      this.swiper.current.style.transform = `translateX(${this.left}px)`
    }
  }

  onSwiped = () => {
    // Calls onSwipe function passed as a prop.
    if (this.props.onSwipe) {
      this.props.onSwipe()
    }
  }

  onDragEnd = (dragEndX = 0) => {
    if (this.props.disableDrag) {
      return
    }
    // The user has stopped dragging the carousel.
    this.dragging = false
    // Add the transition effect back in so the carousel will glide to its final
    // position.
    this.enableTransitionEffect()
    const { numSlides, currentIndex } = this.props
    const threshold = 0.01
    // Work out the overall (horizontal) difference between where the user
    // started dragging and where they stopped.
    const difference = dragEndX - this.overallDragStartX
    // If this difference passes a (somewhat arbitrary!) threshold...
    if (Math.abs(difference) > Math.abs(this.swiper.current.offsetWidth * threshold)) {
      // Then update the current slide index. (If the difference is negative then
      // they swiped left; otherwise they swiped right.)
      const nextIndex = difference < 0 ? Math.min(currentIndex + 1, numSlides - 1) : Math.max(currentIndex - 1, 0)
      this.props.setCurrentIndex(nextIndex)
      // Call onSwiped method, which just calls any onSwipe function passed as a
      // prop.
      this.onSwiped()
    }
    // Reset drag start.
    this.overallDragStartX = 0
    // Snap to a slide.
    this.setFinalPosition()
    this.forceUpdatePosition()
  }

  onDragEndMouse = e => {
    if (this.props.disableDrag) {
      return
    }
    // Stop listening to move events when the user stops dragging.
    window.removeEventListener('mousemove', this.onMouseMove)
    // Record where they stopped dragging and call function with shared drag
    // end logic.
    this.onDragEnd(e.clientX)
  }

  onDragEndTouch = e => {
    if (this.props.disableDrag) {
      return
    }
    // Stop listening to move events when the user stops dragging.
    window.removeEventListener('touchmove', this.onTouchMove)
    const touch = e.changedTouches[0]
    // Record where they stopped dragging and call function with shared drag
    // end logic.
    this.onDragEnd(touch.clientX)
  }

  onMouseMove = e => {
    if (this.props.disableDrag) {
      return
    }
    // Work out how far they've dragged since the last event.
    const difference = Math.round(e.clientX - this.dragStartX)
    // New carousel position should be its current position plus this distance.
    const left = this.left + difference

    // Update the carousel position, but within boundaries so they can't drag it
    // off the page or anything.
    if (left < this.lowerBound) {
      this.left = this.lowerBound
    } else if (left > this.upperBound) {
      this.left = this.upperBound
    } else {
      this.left = left
    }

    // Update start position ready for the next move event.
    this.dragStartX = e.clientX
  }

  onTouchMove = e => {
    if (this.props.disableDrag) {
      return
    }
    const touch = e.targetTouches[0]
    // Work out how far they've dragged since the last event.
    const difference = Math.round(touch.clientX - this.dragStartX)
    // New carousel position should be its current position plus this distance.
    const left = this.left + difference

    // Update the carousel position, but within boundaries so they can't drag it
    // off the page or anything.
    if (left < this.lowerBound) {
      this.left = this.lowerBound
    } else if (left > this.upperBound) {
      this.left = this.upperBound
    } else {
      this.left = left
    }

    // Update start position ready for the next move event.
    this.dragStartX = touch.clientX
  }

  onDragStart = clientX => {
    if (this.props.disableDrag) {
      return
    }
    // The user is now dragging the carousel.
    this.dragging = true
    // Record where they started dragging.
    this.dragStartX = clientX // This will be updated in move events.
    this.overallDragStartX = clientX // This will be saved for comparison in dragEnd event.
    // Disable the transition effect while dragging so that the carousel doesn't
    // lag behind their finger.
    this.disableTransitionEffect()
    // Start updating the carousel's position.
    window.requestAnimationFrame(this.updatePosition)
  }

  handleOnDragStartMouse = e => {
    if (this.props.disableDrag) {
      return
    }
    // Call function with shared drag logic.
    this.onDragStart(e.clientX)
    // Listen for move events.
    window.addEventListener('mousemove', this.onMouseMove)
  }

  handleOnDragStartTouch = e => {
    if (this.props.disableDrag) {
      return
    }
    const touch = e.targetTouches[0]
    // Call function with shared drag logic.
    this.onDragStart(touch.clientX)
    // Listen for move events.
    window.addEventListener('touchmove', this.onTouchMove)
  }

  render () {
    const { children, numSlides, className } = this.props
    return (
      <div
        ref={this.container}
        className={cx('w-100 overflow-hidden', className)}
        onMouseDown={this.handleOnDragStartMouse}
        onTouchStart={this.handleOnDragStartTouch}
      >
        <div
          ref={this.swiper}
          className='flex justify-between'
          style={{ width: `${numSlides * 100}%` }}
        >
          {children}
        </div>
      </div>
    )
  }
}

const ConnectedCarousel = props => (
  <CarouselConsumer>
    {({ state, actions }) => (
      <Carousel {...props} {...state} {...actions} />
    )}
  </CarouselConsumer>
)

export { ConnectedCarousel as default, CarouselConsumer, CarouselContext }
