import React, { Component, createContext } from 'react'

const CarouselContext = createContext(null)
const { Provider: CarouselProvider, Consumer: CarouselConsumer } = CarouselContext

// Any carousel actions or state we want to make available to other components
// (e.g. buttons) goes into the Container component so it can be passed via
// context.

class Container extends Component {
  constructor (props) {
    super(props)
    this.state = {
      numSlides: 0,
      currentIndex: props.initialIndex || 0
    }
  }

  setCurrentIndex = currentIndex => {
    this.setState({ currentIndex })
  }

  setNumSlides = numSlides => {
    this.setState({ numSlides })
  }

  render () {
    return (
      <CarouselProvider
        value={{
          state: this.state,
          actions: {
            setNumSlides: this.setNumSlides,
            setCurrentIndex: this.setCurrentIndex
          }
        }}
      >
        {this.props.children}
      </CarouselProvider>
    )
  }
}

export { Container as default, CarouselConsumer, CarouselContext }
