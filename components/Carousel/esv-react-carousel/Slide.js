import React from 'react'

const Slide = ({ children }) => (
  <div className='grow-1 shrink-1 basis0'>{children}</div>
)

export default Slide
