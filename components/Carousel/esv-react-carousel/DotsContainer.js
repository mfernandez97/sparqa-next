import React, { createContext } from 'react'
import { CarouselConsumer } from './Container'

const { Provider, Consumer: DotsConsumer } = createContext()

const ConnectedDots = ({ children }) => (
  <CarouselConsumer>
    {({ state: { numSlides, currentIndex }, actions: { setCurrentIndex } }) => {
      const dotsProps = makeProps({ numDots: numSlides, currentIndex, setCurrentIndex })
      return (
        <Provider value={{ dotsProps }}>
          <DotsConsumer>
            {children}
          </DotsConsumer>
        </Provider>
      )
    }}
  </CarouselConsumer>
)

const makeProps = ({ numDots, currentIndex, setCurrentIndex }) => {
  let props = []
  for (let i = 0; i <= numDots - 1; i++) {
    props.push({
      active: currentIndex === i,
      onClick: () => setCurrentIndex(i),
      onMouseUp: e => e.stopPropagation(),
      onTouchEnd: e => e.stopPropagation()
    })
  }
  return props
}

export default ConnectedDots
