import React, { Component } from 'react' // eslint-disable-line
import ReactDOM from 'react-dom'
import { _split } from 'libraries/lodash'

export default class Portal extends Component {
  constructor (props) {
    super(props)
    const classes = props.className ? _split(props.className, ' ') : []
    this.el = document.createElement('div')
    classes.length && this.el.classList.add(...classes)
  }

  componentDidMount () {
    this.props.root.appendChild(this.el)
  }

  componentWillUnmount () {
    this.props.root.removeChild(this.el)
  }

  render () {
    return ReactDOM.createPortal(this.props.children, this.el)
  }
}
