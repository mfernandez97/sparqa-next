import React, { Component, Children, cloneElement } from 'react' // eslint-disable-line
import PropTypes from 'prop-types'
import { filter } from 'rxjs/operators'
import { _isFunction, _isEqual } from 'libraries/lodash'
import { getBoundingClientRect } from 'front-end-core/helpers/document'
import connect from 'helpers/connect'
import { $scroll } from 'components/ScrollingContainer'

@connect(({ stateKey }) => ({
  scrollingContainer: $scroll.pipe(filter(s => s.stateKey === stateKey))
}))
class InScrollViewSensor extends Component {
  timeout = null
  el = null
  position = {}

  state = { inView: false }

  componentDidMount () {
    this.setPosition()
    this.checkInView()
  }

  componentWillUnmount () {
    clearTimeout(this.timeout)
  }

  componentDidUpdate (prevProps) {
    const { scrollingContainer } = this.props
    if (!_isEqual(prevProps.scrollingContainer, scrollingContainer)) {
      this.setPosition()
      this.checkInView()
    }
  }

  get children () {
    return Children.map(
      this.props.children,
      child => cloneElement(child, {
        ref: ref => {
          this.setElement(ref)
          _isFunction(child.ref) && child.ref(ref)
        }
      })
    )
  }

  checkInView = () => {
    const { inView } = this.state
    const {
      scrollingContainer: {
        position: { top: scTop = 0, bottom: scBottom = 0 } = {}
      } = {},
      onEnterView,
      onExitView
    } = this.props
    const { top, bottom } = this.position
    const topInView = top > scBottom && !(top < scTop)
    const bottomInView = bottom < scTop && !(bottom > scBottom)
    if (topInView || bottomInView) {
      inView && this.setState({ inView: false }, () => {
        _isFunction(onExitView) && onExitView(this.el)
      })
    } else {
      !inView && this.setState({ inView: true }, () => {
        _isFunction(onEnterView) && onEnterView(this.el)
      })
    }
  }

  setPosition = el => {
    this.position = getBoundingClientRect(this.el)
  }

  setElement = ref => { this.el = ref }

  render () {
    return this.children
  }
}

export default InScrollViewSensor

InScrollViewSensor.propTypes = {
  children: PropTypes.element.isRequired,
  onEnterView: PropTypes.func,
  onExitView: PropTypes.func
}
