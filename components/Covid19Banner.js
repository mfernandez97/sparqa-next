import React, { useState } from 'react'
import Cookies from 'js-cookie'
import { COVID_19 } from 'constants/cms-pages'
import { COVID_19_BANNER_DISMISSED_COOKIE } from 'constants/cookies'
import Icon from 'components/Icon'
import Banner from 'components/Banner'

const ON = false

const Covid19Banner = () => {
  const alreadyDismissed = Cookies.get(COVID_19_BANNER_DISMISSED_COOKIE)
  const [dismissed, setDismissed] = useState(alreadyDismissed)

  return ON && !dismissed ? (
    <Banner className='bg-coral white pv2'>
      <a className='white no-underline' href={COVID_19}>
        <div className='layout-width-constraint relative'>
          <div className='tc fs ph2-m'>
            COVID-19 Alert: Get the latest guidance on what the Coronavirus Pandemic means for your business here.
            <button className='br2 ml4 pointer bg-light-sea-green white fw6 pv2 ph4'>
              View
            </button>
          </div>
          <CloseButton
            onClick={e => {
              e.preventDefault()
              e.stopPropagation()
              Cookies.set(COVID_19_BANNER_DISMISSED_COOKIE, true)
              setDismissed(true)
            }}
          />
        </div>
      </a>
    </Banner>
  ) : null
}

const CloseButton = ({ onClick }) => (
  <button className='absolute top-50 right2 pa2 y--50 pointer bg-transparent flex items-center hover-bg-white-fade-20 br2' onClick={onClick}>
    <Icon className='w-icon-5 h-icon-5 white' name='close' />
  </button>
)

export default Covid19Banner
