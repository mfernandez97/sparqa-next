import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import Icon from 'components/Icon'

export default class Checkbox extends Component {
  state = { focused: false }

  handleFocus = e => this.setState({ focused: true })
  handleBlur = e => this.setState({ focused: false })

  render () {
    const {
      name,
      circle,
      checked,
      readOnly,
      displayName,
      handleChange,
      highlightColor,
      iconHighlightColor
    } = this.props
    const { focused } = this.state

    return (
      <div className={cx('flex items-center justify-start', { [highlightColor]: checked })}>
        <span className={cx('mr4 ba w-icon-5 h-icon-5 db relative pointer', {
          'grey b--grey': !checked,
          [`b--${highlightColor || iconHighlightColor} color-inherit bg-${highlightColor}`]: checked,
          'input-focus': focused,
          'br-100': circle,
          br1: !circle
        })}
        >
          <input
            name={name}
            className='o-0 pointer w-icon-5 h-icon-5'
            type='checkbox'
            checked={checked}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            onChange={(e) => {
              if (!readOnly) {
                handleChange(e)
              }
            }}
            // FIXME: This is a bit hacky. I would've thought if readOnly attribute
            // was applied, the onChange wouldn't trigger, but thats not how it
            // seems to work in practice, didn't spend much time looking into it.
            readOnly={readOnly}
          />
          {checked && (
            <span className={cx('absolute center no-pointer-events flex', { [iconHighlightColor]: iconHighlightColor && checked })}>
              <Icon className='color-inherit' name='check' />
            </span>
          )}
        </span>
        <span className='fs color-inherit tl fw4'>{displayName || name}</span>
      </div>
    )
  }
}

Checkbox.propTypes = {
  checked: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,
  displayName: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  handleChange: PropTypes.func,
  readOnly: PropTypes.bool,
  circle: PropTypes.bool,
  highlightColor: PropTypes.oneOf(['navy', 'dark-grey'])
}

Checkbox.defaultProps = {
  displayName: null,
  readOnly: false,
  circle: true,
  highlightColor: null
}
