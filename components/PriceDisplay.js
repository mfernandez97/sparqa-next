import React from 'react'
import { map } from 'rxjs/operators'
import { _isObject } from 'libraries/lodash'
import { getClassName, formatCurrency, percentOff } from 'helpers/misc'
import { $credit } from 'services/PaymentsService'
import connect from 'helpers/connect'
import ResponseConsumer from 'components/ResponseConsumer'

const Price = ({ price, remainingCredit = 0, discount = 0, pill = false, priceTextProps = {}, ...props }) => {
  const isFree = price === 0
  const priceAfterDiscount = percentOff(price, Number(discount))
  const priceAfterCredit = remainingCredit >= priceAfterDiscount ? 0 : priceAfterDiscount - remainingCredit

  const { containerDefaults, priceDefaults, originalPriceDefaults } = getDefaults({ pill, isFree })
  const containerClassName = getClassName(containerDefaults, props)
  const priceClassName = getClassName(priceDefaults, priceTextProps)
  const originalPriceClassName = getClassName(originalPriceDefaults, {})
  return (
    <div className={containerClassName}>
      <span className={priceClassName}>
        {remainingCredit > 0 && <sup className={originalPriceClassName}>£{formatCurrency(priceAfterDiscount, true)}</sup>}
        {isFree ? 'Free' : `£${formatCurrency(priceAfterCredit, true)}`}
      </span>
    </div>
  )
}

const ConnectedPrice = connect(props => ({
  remainingCredit: $credit().pipe(map(credit => _isObject(credit) ? credit.remaining : credit))
}))(props => (
  <ResponseConsumer
    responses={[props.price, props.remainingCredit]}
    content={() => <Price {...props} />}
    unauthorizedComponent={() => <Price {...props} remainingCredit={0} />}
    notfoundComponent={() => <Price {...props} remainingCredit={0} />}
  />
))

const getDefaults = ({ pill, isFree }) => {
  const containerDefaults = {
    br: pill ? 'br-pill' : '',
    bg: !isFree && pill ? 'bg-coral' : '',
    width: pill ? 'w-auto' : 'w-100',
    display: pill ? 'dib' : '',
    margin: 'mb0 mt4',
    padding: pill ? 'ph6 pv4' : '',
    textAlign: 'tc'
  }

  const priceDefaults = {
    color: isFree ? 'light-sea-green' : pill ? 'white' : 'coral',
    position: 'relative',
    fontSize: 'fxxl',
    textAlign: 'tc',
    fontWeight: 'fw5'
  }

  const originalPriceDefaults = {
    color: pill ? 'light-grey' : 'grey',
    margin: pill ? 'mr2' : '',
    fontSize: 'fs',
    position: pill ? '' : 'absolute',
    transform: 'x--50',
    lineHeight: 'lh-solid',
    textTransform: 'strikethrough'
  }

  return { containerDefaults, priceDefaults, originalPriceDefaults }
}

export default ConnectedPrice
