import React from 'react'
import { PLAN_NAMES } from 'components/PaymentPlans/helpers'
import ConnectedPlan from 'components/PaymentPlans/ConnectedPlan'
import ConnectedPlans from 'components/PaymentPlans/ConnectedPlans'

export { PLAN_NAMES }

export const FreemiumPlan = props => (
  <ConnectedPlan {...props} planName={PLAN_NAMES.FREEMIUM} />
)
export const AnnualPlan = props => (
  <ConnectedPlan {...props} planName={PLAN_NAMES.ANNUAL} />
)
export const MonthlyPlan = props => (
  <ConnectedPlan {...props} planName={PLAN_NAMES.ANNUAL} />
)

export default ConnectedPlans
