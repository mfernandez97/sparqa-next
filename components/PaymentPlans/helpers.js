import { _find, _toLower } from 'libraries/lodash'
import { percentOff, formatCurrency } from 'helpers/misc'

export const PLAN_NAMES = {
  ANNUAL: 'annual',
  MONTHLY: 'monthly',
  FREEMIUM: 'free'
}

export const getPrice = (planName, price, discount) => {
  if (_toLower(planName) === PLAN_NAMES.FREEMIUM) {
    return 'FREE'
  }
  return formatCurrency(percentOff(price, Number(discount)))
}

export const formatPlan = (plan, discount) => {
  const { name, price, stripeId } = plan
  const isFreemium = _toLower(name) === PLAN_NAMES.FREEMIUM
  const title = isFreemium ? 'Forever' : name
  const value = stripeId
  const formattedPrice = getPrice(name, price, discount)
  const buttonText = isFreemium ? 'Start Now' : 'Select Plan'
  const showVat = !isFreemium
  const showCurrency = !isFreemium
  return {
    ...plan,
    title,
    value,
    price: formattedPrice,
    buttonText,
    showVat,
    showCurrency
  }
}

export const getPlanByName = (plans, planName) => _find(
  plans,
  ({ name }) => _toLower(name) === _toLower(planName)
)
