import React from 'react'
import { map } from 'rxjs/operators'
import { _isArray, _isObject } from 'libraries/lodash'
import { $paymentPlans } from 'services/PaymentsService'
import connect from 'helpers/connect'
import { formatPlan, getPlanByName } from 'components/PaymentPlans/helpers'
import Plan from 'components/PaymentPlans/Plan'
import ResponseConsumer from 'components/ResponseConsumer'

const ConnectedPlan = connect(({ planName, discount = 0 }) => ({
  plan: $paymentPlans().pipe(
    map(plans => _isArray(plans) ? getPlanByName(plans, planName) : plans),
    map(plan => _isObject(plan) ? formatPlan(plan, discount) : plan)
  )
}))(({ plan, ...props }) => {
  return (
    <ResponseConsumer
      response={plan}
      content={() => <Plan {...plan} {...props} />}
    />
  )
})

export default ConnectedPlan
