import React from 'react'
import cx from 'classnames'
import { map } from 'rxjs/operators'
import { _isArray, _isFunction, _sortBy } from 'libraries/lodash'
import getUrl from 'front-end-core/core/url'
import { pushUrl } from 'front-end-core/core/history'
import { REGISTER } from 'constants/controllers'
import { $paymentPlans } from 'services/PaymentsService'
import sessionState from 'helpers/sessionState'
import connect from 'helpers/connect'
import { formatPlan } from 'components/PaymentPlans/helpers'
import Plan from 'components/PaymentPlans/Plan'
import ResponseConsumer from 'components/ResponseConsumer'
import useSwiper from 'hooks/useSwiper'
import SwiperControls from 'pages/landing/swiper/SwiperControls'
import SwiperWrapper from 'pages/landing/swiper/SwiperWrapper'

const Plans = ({ plans, className, selectedPlan, getOnChange }) => (
  <div className={cx('w-100 min-w-measure-7-d tl pb6 flex flex-column-t justify-center items-center-t', className)}>
    {plans.map(plan => (
      <Plan
        {...plan}
        clickable
        key={plan.stripeId}
        selected={selectedPlan === plan.stripeId}
        onChange={getOnChange(plan.stripeId)}
      />
    ))}
  </div>
)

const PlanCarousel = ({ plans, selectedPlan, getOnChange, carousel }) => {
  const recommendedPlanIndex = plans.findIndex(plan => plan.recommended)
  const selectedPlanIndex = plans.findIndex(plan => plan.stripeId === selectedPlan)
  const initialSlide = selectedPlanIndex >= 0 ? selectedPlanIndex : recommendedPlanIndex
  const { swiperRef, onSlideChange, isEnd, isBeginning } = useSwiper()
  const pagination = {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable: true
  }
  const breakpoints = {
    0: {
      slidesPerView: 1,
      spaceBetween: 10
    }
  }

  return (
    <div className='relative pricing-plan-swiper-pagination'>
      <SwiperControls
        {...{ swiperRef, isEnd, isBeginning, marginTop: -64 }}
        paddingCx=''
        buttonClassName='royal-blue-3'
      />
      <SwiperWrapper
        {...{
          swiperRef,
          onSlideChange,
          pagination,
          initialSlide,
          breakpoints
        }}
      >
        {plans.map(plan => (
          <div key={plan.stripeId} className='ph4-t'>
            <Plan
              {...{ ...plan, carousel }}
              clickable
              key={plan.stripeId}
              selected={selectedPlan === plan.stripeId}
              onChange={getOnChange(plan.stripeId)}
            />
          </div>
        ))}
      </SwiperWrapper>
    </div>
  )
}

const CarouselSwitch = ({ carousel, ...props }) => carousel ? (
  <PlanCarousel {...{ ...props, carousel }} />
) : (
  <Plans {...props} />
)

const ConnectedPlans = connect(({ discount = 0, filter = plan => true }) => ({
  plans: $paymentPlans().pipe(
    map(plans => {
      if (!_isArray(plans)) {
        return plans
      }
      return _sortBy(
        plans.map(plan => formatPlan(plan, discount)).filter(filter),
        plan => plan.price === 'FREE' ? 0 : Number(plan.price)
      )
    })
  )
}))(({ plans, ...props }) => {
  const getOnChange = stripeId => e => {
    if (_isFunction(props.onChange)) {
      props.onChange(e)
    } else if (props.savePlanToSession) {
      e.preventDefault()
      const { setSessionState } = props
      const { price } = plans.find(plan => plan.stripeId === stripeId)
      setSessionState(sessionState => ({
        register: {
          ...sessionState.register,
          plan: stripeId,
          selectedPlanCost: price
        }
      }))
    }
    if (props.linkToRegister) {
      pushUrl(getUrl(REGISTER))
    }
  }
  return (
    <ResponseConsumer
      response={plans}
      content={() => <CarouselSwitch plans={plans} getOnChange={getOnChange} {...props} />}
    />
  )
})

const LinkedPlans = sessionState(
  ({ register: { plan } }) => ({ register: { plan } }),
  { register: {} }
)(ConnectedPlans)

const LinkedSwitch = props => props.savePlanToSession ? (
  <LinkedPlans {...props} />
) : (
  <ConnectedPlans {...props} />
)

export default LinkedSwitch
