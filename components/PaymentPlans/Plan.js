import React, { createRef } from 'react'
import cx from 'classnames'
import { getClassName } from 'helpers/misc'
import Icon from 'components/Icon'
import Button, { themes } from 'components/Button'

const PlanSwitch = ({ clickable, ...props }) => clickable ? (
  <ClickablePlan {...props} />
) : (
  <NonClickablePlan {...props} />
)

const ClickablePlan = ({
  value,
  checked,
  onChange,
  buttonText = 'Select Plan',
  ...props
}) => {
  const ref = createRef()

  const onClick = () => {
    const input = ref.current
    input.click()
  }

  const defaults = getDefaults({
    selected: props.selected,
    recommended: props.recommended,
    carousel: props.carousel
  })

  const className = getClassName(defaults, props)

  return (
    <Plan
      className={className}
      onClick={onClick}
      InputComponent={() => (
        <SelectButton selected={props.selected}>
          <HiddenInput
            ref={ref}
            value={value}
            checked={checked}
            onChange={onChange}
          />
          {props.selected ? 'Selected' : buttonText}
        </SelectButton>
      )}
      {...props}
    />
  )
}

const NonClickablePlan = props => {
  const defaults = getDefaults(props)
  const className = getClassName(defaults, props)
  return (
    <Plan
      className={className}
      onClick={undefined}
      InputComponent={() => (
        <HiddenInput
          value={props.value}
          checked={props.checked}
          onChange={undefined}
        />
      )}
      {...props}
    />
  )
}

const Plan = ({
  title,
  price,
  showVat = true,
  onClick,
  benefits = [],
  selected,
  className,
  recommended,
  showCurrency = true,
  InputComponent,
  disclaimer
}) => {
  return (
    <div className={className} onClick={onClick} id={`payment-plan-${price}`}>
      {recommended && <RecommendedTag />}
      {selected && <SelectedOverlay />}
      <h3 className='fxxxl fw8 tracked tc mt0 mb2'>{title}</h3>
      <Price price={price} showCurrency={showCurrency} showVat={showVat} />
      {disclaimer && <span className='tc fm mb6 b'>{disclaimer}</span>}
      {benefits.length > 0 && <BenefitsList benefits={benefits} />}
      <InputComponent />
    </div>
  )
}

const RecommendedTag = () => (
  <div className='absolute left4 right4 top6 fxs fw6 tc'>
    <Icon className='w-icon-4 h-icon-4 mr2 recommended-star-translate' name='star' />
    BEST VALUE
  </div>
)

const SelectedOverlay = () => (
  <div className='absolute left0 top0 w-100 h-100 medium-sea-green bg-white-fade-50 z10'>
    <Icon name='check' className='absolute center w-icon-16 h-icon-16' />
  </div>
)

const Price = ({ price, showCurrency, showVat }) => (
  <div
    className={cx('fw7 mb8 lh-solid w-100 flex justify-center', {
      'f-graphic-64': price.length <= 4,
      'f-graphic-50': price.length > 4
    })}
  >
    {showCurrency && <sup className='lh-copy tracked mlauto fxxl grow-1 shrink-1 basis0 tr mt5 self-start'>£/</sup>}
    <span>{price}</span>
    {showVat && <span className='fw8 fm tracked-tight flex items-end mrauto grow-1 shrink-1 basis0 tl mb4'>+VAT</span>}
  </div>
)

const BenefitsList = ({ benefits }) => (
  <ul className='ma0 pa0 list-reset'>
    {benefits.map((benefit, i) => (
      <li key={i} className='ma0 mb6 pv0 ph8 relative flex items-center fs fw6 lh-title'>
        <Icon className='w-icon-6 h-icon-6 br-100 ba pa1 shrink-0 mr4' name='check' />
        {benefit}
      </li>
    ))}
  </ul>
)

const SelectButton = ({ children, selected }) => (
  <Button
    width='w-60'
    theme={themes.ROYAL_BLUE_3}
    color={selected ? 'medium-sea-green' : 'steel-blue'}
    hoverBg={null}
    padding='pa6'
    className='absolute z15 left-50 bottom0 x--50-y-50 plan-option-label'
    hoverColor={selected ? 'medium-sea-green' : 'steel-blue'}
    borderColor={selected ? 'b--medium-sea-green' : 'b--royal-blue-3'}
    CustomComponent={props => <label {...props} />}
  >
    {children}
  </Button>
)

const HiddenInput = React.forwardRef((props, ref) => (
  <input
    ref={ref}
    type='radio'
    className='remove-tick dib'
    value={props.value}
    checked={props.selected}
    onChange={props.onChange}
  />
))

const getDefaults = ({ recommended, carousel, selected }) => {
  const bg = recommended ? 'gradient-blue-green-2' : 'bg-white'
  const misc = selected ? null : 'not-selected-plan-option'
  const color = recommended ? 'white' : 'cyan'
  const margin = 'mv8 mh5 mb8-m'
  const shadow = null
  const pointer = selected ? null : 'pointer'
  const hoverShadow = selected ? null : 'hover-shadow-6-royal-blue-3'
  const width = carousel ? 'w-auto' : 'plan-option-width'

  return {
    bg,
    br: 'br2-1',
    misc,
    grow: 'grow-0',
    color,
    width,
    shrink: 'shrink-0',
    border: 'ba',
    shadow,
    margin,
    display: 'flex',
    padding: 'pt9 pb8 ph0',
    pointer,
    position: 'relative',
    transition: 'transition-all',
    borderColor: 'b--royal-blue-3',
    hoverShadow,
    flexDirection: 'flex-column'
  }
}

export default PlanSwitch
