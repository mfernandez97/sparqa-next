import React from 'react'
import { mount } from 'enzyme'
import { Helmet } from 'react-helmet'
import generateProductFaqMetadata from './generateProductFaqMetadata'
import Metadata from './index.js'

jest.mock('react-helmet')
jest.mock('./generateProductFaqMetadata')
/**
 * Note to future testers,
 * I don't recommend trying to test the proptypes validation. Doing so pollutes
 * the test logs with console errors, and the community as a whole seems against
 * the idea. I guess its an case of testing a lib rather than you're own code,
 * as much as I personally would quite like a check on correct input validation.
 */
describe('Metadata Component', () => {
  describe('when provided a valid title and describe but no FAQ', () => {
    const title = 'Foobar'
    const description = 'Hello World, this is my description'
    let Component

    beforeAll(() => {
      Component = mount(<Metadata pageDescription={description} pageTitle={title} />)
    })

    it('should call Helmet with defer set to false', () => {
      expect(Helmet).toHaveBeenCalledWith(
        expect.objectContaining({
          defer: false
        }),
        expect.anything()
      )
    })

    it('should render the page title and description', () => {
      expect(Component.find(`title[content="${title}"]`)).toBeTruthy()
      expect(Component.find(`meta[itemProp="name"][content="${title}"]`)).toBeTruthy()

      expect(Component.find(`meta[name="description"][content="${description}"]`)).toBeTruthy()
      expect(Component.find(`meta[itemProp="description"][content="${description}"]`)).toBeTruthy()
    })

    it('should render the opengraph metadata', () => {
      expect(Component.find(`meta[property="og:title"][content="${title}"]`)).toBeTruthy()
      expect(Component.find(`meta[property="og:description"][content="${description}"]`)).toBeTruthy()
    })

    it('should render the twitter metadata', () => {
      expect(Component.find(`meta[name="twitter:title"][content="${title}"]`)).toBeTruthy()
      expect(Component.find(`meta[name="twitter:description"][content="${description}"]`)).toBeTruthy()
    })

    it('should not render the faq metadata component', () => {
      expect(generateProductFaqMetadata).not.toHaveBeenCalled()
    })

    afterAll(() => {
      Component.unmount()
      jest.clearAllMocks()
    })
  })
  describe('when provided a title description and FAQ', () => {
    const title = 'Foobar'
    const description = 'Hello World, this is my description'
    const faqs = [
      {
        question: 'q1',
        answer: 'a1'
      },
      {
        question: 'q2',
        answer: 'a2'
      },
      {
        question: 'q3',
        answer: 'a3'
      },
      {
        question: 'q4',
        answer: 'a4'
      }
    ]
    let Component

    beforeAll(() => {
      Component = mount(<Metadata pageDescription={description} pageTitle={title} faqs={faqs} />)
    })

    it('should call Helmet with defer set to false', () => {
      expect(Helmet).toHaveBeenCalledWith(
        expect.objectContaining({
          defer: false
        }),
        expect.anything()
      )
    })

    it('should render the page title and description', () => {
      expect(Component.find(`title[content="${title}"]`)).toBeTruthy()
      expect(Component.find(`meta[itemProp="name"][content="${title}"]`)).toBeTruthy()

      expect(Component.find(`meta[name="description"][content="${description}"]`)).toBeTruthy()
      expect(Component.find(`meta[itemProp="description"][content="${description}"]`)).toBeTruthy()
    })

    it('should render the opengraph metadata', () => {
      expect(Component.find(`meta[property="og:title"][content="${title}"]`)).toBeTruthy()
      expect(Component.find(`meta[property="og:description"][content="${description}"]`)).toBeTruthy()
    })

    it('should render the twitter metadata', () => {
      expect(Component.find(`meta[name="twitter:title"][content="${title}"]`)).toBeTruthy()
      expect(Component.find(`meta[name="twitter:description"][content="${description}"]`)).toBeTruthy()
    })

    it('should create a script component of type ld+json', () => {
      expect(Component.find('script[type="application/ld+json"]')).toBeTruthy()
    })

    it('should pass the provided FAQs to the ProductFaq component', () => {
      expect(generateProductFaqMetadata).toHaveBeenCalledWith(faqs)
    })

    afterAll(() => {
      Component.unmount()
      jest.clearAllMocks()
    })
  })
})
