import React from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import generateProductFaqMetadata from './generateProductFaqMetadata'

const Metadata = props => {
  const { pageTitle, pageDescription, faqs } = props

  return (
    <Helmet defer={false}>
      <title data-react-helmet='true'>{pageTitle}</title>
      <meta name='description' content={pageDescription} data-react-helmet='true' />

      <meta itemProp='name' content={pageTitle} data-react-helmet='true' />
      <meta itemProp='description' content={pageDescription} data-react-helmet='true' />

      <meta property='og:title' title={pageTitle} data-react-helmet='true' />
      <meta property='og:description' content={pageDescription} data-react-helmet='true' />

      <meta name='twitter:title' content={pageTitle} data-react-helmet='true' />
      <meta name='twitter:description' content={pageDescription} data-react-helmet='true' />

      {faqs && faqs.length && (
        <script type='application/ld+json' data-react-helmet='true'>
          {generateProductFaqMetadata(faqs)}
        </script>
      )}

    </Helmet>
  )
}

Metadata.propTypes = {
  pageTitle: PropTypes.string.isRequired,
  pageDescription: PropTypes.string.isRequired,
  faqs: PropTypes.arrayOf(PropTypes.exact({
    question: PropTypes.string.isRequired,
    answer: PropTypes.string.isRequired
  }))
}

export default Metadata
