/**
 * FAQ shape
 * {
 *  question,
 *  answer
 * }
 */
const ProductFaqMetadata = (faqs) => `
  {
    "@context": "http://schema.org",
    "@type": "FAQPage",
    "mainEntity": [
      ${faqs.map(({ question, answer }) => `
        {
          "@type": "Question",
          "name": "${question}",
          "acceptedAnswer": {
            "@type": "Answer",
            "text": "${answer}"
          }
        }`
      )}
    ]
  }
`

export default ProductFaqMetadata
