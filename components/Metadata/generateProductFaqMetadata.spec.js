import generateProductFaqMetadata from './generateProductFaqMetadata'
// this is going to be minimally tested as most of the validation is done by the Metadata component
describe('generateProductFaqMetadata function', () => {
  describe('should generate the correct data format for a FAQ schema', () => {
    const faqs = [
      {
        question: 'q1',
        answer: 'a1'
      },
      {
        question: 'q2',
        answer: 'a2'
      }
    ]
    let metadataJSON
    beforeAll(() => {
      // took me way too long to realise that the function just creates valid json
      metadataJSON = JSON.parse(generateProductFaqMetadata(faqs))
    })

    it('should have the context schema.org', () => {
      expect(metadataJSON['@context']).toBe('http://schema.org')
    })
    it('should have the type faq page', () => {
      expect(metadataJSON['@type']).toBe('FAQPage')
    })
    it('should have question q1 with answer a1 in the body of the metadata', () => {
      const firstBlock = metadataJSON.mainEntity[0]
      expect(firstBlock['@type']).toBe('Question')
      expect(firstBlock.name).toBe('q1')

      const answerBlock = firstBlock.acceptedAnswer
      expect(answerBlock['@type']).toBe('Answer')
      expect(answerBlock.text).toBe('a1')
    })
    it('should have question q2 with answer a2 in the body of the metadata', () => {
      const secondBlock = metadataJSON.mainEntity[1]
      expect(secondBlock['@type']).toBe('Question')
      expect(secondBlock.name).toBe('q2')

      const answerBlock = secondBlock.acceptedAnswer
      expect(answerBlock['@type']).toBe('Answer')
      expect(answerBlock.text).toBe('a2')
    })
  })
})
