import React from 'react'
import Grid, { GridItem } from 'components/Grid'

export const Card = props => (
  <GridItem
    {...props}
    imageBackgroundSize='cover'
    titleAlignment='left'
    descriptionAlignment='left'
    className='shadow-3-black-fade-10 hover-shadow-3-black-fade-30 bg-white dark-grey'
  />
)

export default Grid
