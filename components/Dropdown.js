import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import Checkbox from 'components/Checkbox'
import Icon from 'components/Icon'
import { Media } from 'services/WindowService'
import connect from 'helpers/connect'

@connect(() => ({
  mobile: Media.Mobile,
  tablet: Media.Tablet
}))
export default class Dropdown extends Component {
  render () {
    const { title, options, className, isOpen, mobile, tablet, onSelectOption, multi = true, optionListClassName, optionSpacing, size = 'L' } = this.props
    const numSelected = options.reduce((total, { selected }) => selected ? total + 1 : total, 0)
    const active = isOpen || numSelected > 0
    return (
      <div className={cx('relative pointer static-m', className)}>
        <p className={cx('relative fw5 hover-navy ma0', {
          'navy hover-navy-m': active,
          'silver hover-silver-m': !active,
          'fxl': size === 'L',
          'fm': size === 'S'
        })}>
          {title} {(numSelected > 0 && (tablet || mobile)) && `(${numSelected})`}
          <Icon className={cx({
            'w-icon-3 h-icon-3 ml2': size === 'L',
            'w-icon-2 h-icon2 ml1': size === 'S'
          })} name='menu-down-alt' />
        </p>
        <div className={cx(optionListClassName, 'absolute bg-white shadow-3-black-fade-10 left-50 x--50 transition-all',
          {
            'no-pointer-events o-0': !isOpen,
            'o-100': isOpen
          }
        )}>
          {options.map(({ name, value, selected }, index) =>
            <div
              onClick={onSelectOption(value)}
              key={value}
              className={cx({ [optionSpacing]: index !== options.length - 1 })}
              value={value}
            >
              { multi ? (
                <Checkbox readOnly highlightColor='navy' circle={false} name={name} checked={selected} />
              ) : (
                <p className='tc'>{name}</p>
              )}
            </div>
          )}
        </div>
      </div>
    )
  }
}

Dropdown.propTypes = {
  options: PropTypes.array,
  title: PropTypes.string,
  className: PropTypes.string,
  left: PropTypes.bool,
  right: PropTypes.bool,
  size: PropTypes.string
}

Dropdown.defaultProps = {
  options: [],
  title: '',
  className: '',
  left: false,
  right: false
}
