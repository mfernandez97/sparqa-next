import React from 'react'

export default () => (
  <div className='w-100 pv8 ph0 tc flex items-center justify-center'>
    <img className='dib w-icon-16 h-icon-16 mr6' src='/assets/icons/broken-link.png' />
    <span>Not Found</span>
  </div>
)
