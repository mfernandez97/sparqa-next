import React from 'react'

const ServiceDown = () => (
  <div className='maauto mv10 min-w-measure-3 max-w-measure-4 min-w-auto-m max-w-none-m br2 pa8 ph5-m bg-white relative'>
    Our registration service is currently unavailable, please try to register again later.  We apologise for the inconvenience.
  </div>
)

export default ServiceDown
