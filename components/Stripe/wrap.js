import React, { Children, cloneElement, useCallback, useEffect, useState } from 'react'
// import { StripeProvider, Elements, injectStripe } from 'react-stripe-elements'
import { RESPONSE_PENDING } from 'front-end-core/constants/responses'
import ResponseConsumer from 'components/ResponseConsumer'
import { $stripeId } from 'services/PaymentsService'
import connect from 'helpers/connect'

import { Elements, ElementsConsumer } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js'

const ElementsProvider = ({ stripeId, children }) => {
  const [stripePromise, setStripePromise] = useState(null)
  
  useEffect(() => {
    setStripePromise(loadStripe(stripeId))
  }, [])

  return stripePromise && (
    <Elements stripe={stripePromise} options={{ fonts: [ {src: 'https://fonts.googleapis.com/css?family=Nunito:400' }] }} >
        <ElementsConsumer>
          {({stripe, elements}) => (
            Children.map(children, child => cloneElement(child, { stripe, elements }))
          )}
        </ElementsConsumer>
    </Elements>
  )
}

const StripeWrapper = connect(() => ({ stripeId: $stripeId() }))(props => (
  <ResponseConsumer response={props.stripeId} content={() => <ElementsProvider {...props} />} />
))

export default StripeWrapper
