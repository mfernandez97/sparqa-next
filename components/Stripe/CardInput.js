import React from 'react'
import { CardElement } from '@stripe/react-stripe-js';

const CardInput = ({ disabled, baseClassName, setErrorMessage }) => (
  <CardElement
    options={{
      disabled,
      hidePostalCode: true,
      classes: {
        base: baseClassName,
        focus: 'form__stripe-card-input--focus',
        complete: 'form__stripe-card-input--complete',
        invalid: 'form__stripe-card-input--error'
      },
      style: {
        base: {
          fontFamily: `'Nunito Sans', sans-serif`,
          fontSize: '14px',
          fontSmoothing: 'antialiased',
          color: '#14bbc4'
        },
        complete: {
          color: '#14bbc4'
        },
        invalid: {
          color: '#404040'
        }
      }
    }}
    onChange={e => {
      setErrorMessage(e.error ? e.error.message : null)
    }}
  />
)

export default CardInput
