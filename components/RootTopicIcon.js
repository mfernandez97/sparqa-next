import React, {memo, useCallback} from 'react'
import cx from 'classnames'
import SVG from 'components/SVG'

const iconMap = new Map([
  ['203', `human-resources.svg`],
  ['225', `sales-and-commercial.svg`],
  ['227', `operating-website.svg`],
  ['229', `intellectual-property.svg`],
  ['231', `data-protection-and-gdpr.svg`],
  ['233', `starting-a-company.svg`],
  ['235', `late-payments-and-disputes.svg`],
  ['237', `directors-appointment.svg`],
  ['239', `company-admin.svg`],
  ['241', `financing-a-business.svg`],
  ['243', `occupying-business-premises.svg`],
  ['245', `health-and-safety.svg`],
  ['247', `selling-a-business.svg`]
])

const iconColorMap = new Map([
  ['203', { light: 'coral', dark: 'coral' }],
  ['225', { light: 'steel-blue', dark: 'steel-blue' }],
  ['227', { light: 'light-sea-green', dark: 'light-sea-green' }],
  ['229', { light: 'dark-slate-grey', dark: 'white' }],
  ['231', { light: 'coral', dark: 'coral' }],
  ['233', { light: 'navy', dark: 'white' }],
  ['235', { light: 'coral', dark: 'coral' }],
  ['237', { light: 'navy', dark: 'white' }],
  ['239', { light: 'light-sea-green', dark: 'light-sea-green' }],
  ['241', { light: 'navy', dark: 'white' }],
  ['243', { light: 'steel-blue', dark: 'steel-blue' }],
  ['245', { light: 'light-sea-green', dark: 'light-sea-green' }],
  ['247', { light: 'navy', dark: 'white' }]
])

const RootTopicIcon = memo(({ bookmarkId, backgroundType, className }) => {
  const getRootIcon = useCallback(bookmarkId => {
    const PATH = `assets/grid-icons/svg/`
    return PATH + iconMap.get(bookmarkId)
  }, [bookmarkId])

  const getRootIconColor = useCallback(({ bookmarkId, backgroundType = 'light' }) => {
    const iconColors = iconColorMap.get(bookmarkId)
    return iconColors ? iconColors[backgroundType] : null
  },[bookmarkId, backgroundType])

  return (
    <SVG
      url={getRootIcon(bookmarkId)}
      className={cx(className, getRootIconColor({ bookmarkId, backgroundType }))}
    />
  )
})

export default RootTopicIcon
