import React from 'react'
import cx from 'classnames'
import IconButton from '@material-ui/core/IconButton'
import CloseMenuIcon from '@material-ui/icons/Close'
import MenuIcon from '@material-ui/icons/Menu'
import Zoom from '@material-ui/core/Zoom'
import Grow from '@material-ui/core/Grow'

const Hamburger = ({isMobileMenuActive}) => (
  <IconButton data-toggle-menu={!isMobileMenuActive} className={cx('ml4')} size='small'>
    {!isMobileMenuActive ? (
      <Zoom in={true} timeout={300}>
        <MenuIcon className='dark-slate-grey'/>
      </Zoom>
    ): (
      <Grow in={true} timeout={500}>
        <CloseMenuIcon className='dark-slate-grey'/>
      </Grow>
    )}
  </IconButton>
)

export default Hamburger
