import React from 'react'
import PropTypes from 'prop-types'
import { getClassName } from 'helpers/misc'
import Icon from 'components/Icon'

const StatusBox = ({ title, iconName, ...props }) => {
  const defaults = getDefaults()
  const className = getClassName(defaults, props)
  return (
    <div className={className}>
      <Icon className='w-icon-13 h-icon-13 grey mb6' name={iconName} />
      {title && <p>{title}</p>}
    </div>
  )
}

const getDefaults = () => ({
  width: 'w-100',
  items: 'items-center',
  display: 'flex',
  justify: 'justify-center',
  padding: 'pv8 ph0',
  textAlign: 'tc',
  flexDirection: 'flex-column'
})

StatusBox.propTypes = {
  title: PropTypes.string,
  iconName: PropTypes.string
}

StatusBox.defaultProps = {
  title: null,
  iconName: 'clock'
}

export const Loading = () => <StatusBox />
export const Error = ({ title }) => <StatusBox title={title} iconName='alert' />
export const Offline = ({ title }) => <StatusBox title={title} iconName='wifi' />

Error.propTypes = { title: PropTypes.string }
Error.defaultProps = { title: 'Error occured' }
Offline.propTypes = { title: PropTypes.string.isRequired }
Offline.defaultProps = { title: 'Not available offline' }

export default StatusBox
