import React, { Component } from 'react'
import { Subject } from 'rxjs'
import { first, distinctUntilChanged, filter } from 'rxjs/operators'
import { _debounce, _isFunction, _isElement } from 'libraries/lodash'
import cx from 'classnames'
import { observePageState, updatePageState } from 'front-end-core/services/PageStateService'
import { getBoundingClientRect } from 'front-end-core/helpers/document'
import connect from 'helpers/connect'

export const $scrollToElement = new Subject()
export const $scroll = new Subject()

export const STATE_KEYS = {
  MOBILE_MENU: 'mobile-menu',
  MAIN: 'main',
  DOCUMENT_MOBILE_FILTERS: 'documents-mobile-filters',
  SEARCH_FACETS: 'search-facets',
  ACCORDION: 'accordion',
  QUESTIONS: 'questions',
  SAVED_DOCUMENT_EDIT: 'saved-document-edit'
}

@connect(props => ({
  scrollTo: observePageState(`scroll-${props.stateKey}`).pipe(first())
}), { scrollTo: 0 })
class ScrollingContainer extends Component {
  mounted = false
  timeout = null
  element = null

  constructor (props) {
    super()
    // Subscribe to the stream of elements to scroll to. When the stream emits
    // a new element, scroll to it if the component has mounted.
    this.subscription = $scrollToElement.pipe(
      distinctUntilChanged(),
      filter(({ stateKey }) => stateKey === props.stateKey)
    ).subscribe(({ el }) => { this.setElement(el, this.mounted) })
  }

  get className () {
    const { noScroll } = this.props
    return cx(
      {
        'overflow-y-auto': !noScroll,
        'overflow-y-hidden': noScroll
      },
      'scrolling-container',
      this.props.className
    )
  }

  componentDidMount () {
    // When the component mounts scroll to the correct position.
    // NOTE: The setTimeout is a hack because the element dimensions are wrong
    // when the component first mounts.
    this.mounted = true
    if (_isElement(this.element)) {
      this.timeout = setTimeout(this.scrollToPosition, 240)
    } else {
      this.scrollToPosition()
    }
  }

  componentDidUpdate (prevProps, prevState) {
    // componentDidMount isn't called for the main scrolling container when
    // the user navigates to a new page, so have to check manually.
    const { stateKey, pageId } = this.props
    if (stateKey === STATE_KEYS.MAIN && prevProps.pageId !== pageId) {
      this.scrollToPosition()
    }
  }

  componentWillUnmount () {
    this.subscription.unsubscribe()
    if (this.timeout) {
      clearTimeout(this.timeout)
    }
  }

  setScrollbar = ref => { this.scrollbar = ref }

  setElement = (el, updatePosition = false) => {
    this.element = el
    if (updatePosition) {
      this.scrollToPosition()
    }
  }

  handleScroll = e => {
    e.stopPropagation()
    _isFunction(this.props.onScroll) && this.props.onScroll(e)
    this.handleScrollInternal()
  }

  handleScrollInternal = _debounce(() => {
    // Record the scroll position.
    if (this.scrollbar) {
      const { scrollTop } = this.scrollbar
      const { stateKey, adjustScrollTop } = this.props
      const position = getBoundingClientRect(this.scrollbar)
      const top = _isFunction(adjustScrollTop) ? adjustScrollTop(position.top, false) : position.top
      $scroll.next({
        stateKey,
        scrollTop,
        position: { ...position, top }
      })
      updatePageState(state => ({ ...state, [`scroll-${stateKey}`]: scrollTop }))
    }
  }, 150)

  scrollToPosition = () => {
    if (_isElement(this.element)) {
      this.scrollToElement()
    } else {
      this.scrollbar.scrollTop = this.props.scrollTo
      this.handleScrollInternal()
    }
  }

  scrollToElement = () => {
    const { adjustScrollTop } = this.props
    const scrollTo = top(this.element) + this.scrollbar.scrollTop - top(this.scrollbar)
    const position = _isFunction(adjustScrollTop) ? adjustScrollTop(scrollTo) : scrollTo
    this.scrollbar.scrollTop = position + 1
  }

  render () {
    return (
      <div className={this.className} ref={this.setScrollbar} onScroll={this.handleScroll}>
        {this.props.children}
      </div>
    )
  }
}

const top = element => element.getBoundingClientRect().top

export default ScrollingContainer
