import React, { Component } from 'react'
import cx from 'classnames'
import ResponseConsumer from 'components/ResponseConsumer'
import Filters from 'components/Filters'

export default class Sidebar extends Component {
  renderContent = () => {
    const {
      facets,
      resultCount,
      sortOptions,
      showSortOptions,
      handleChangeOption,
      handleChangeSortOption
    } = this.props
    return (
      <Filters
        facets={facets}
        resultCount={resultCount}
        sortOptions={sortOptions}
        showSortOptions={showSortOptions}
        handleChangeOption={handleChangeOption}
        handleChangeSortOption={handleChangeSortOption}
      />
    )
  }

  render () {
    const { responses, className } = this.props
    return (
      <div className={cx('search-page-left search-results-filters-width pt7 pt0-t pr6 layout-width-constraint', className)}>
        {responses ? (
          <ResponseConsumer
            responses={responses}
            content={this.renderContent}
          />
        ) : this.renderContent()}
      </div>
    )
  }
}
