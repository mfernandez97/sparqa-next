import React from 'react'
import cx from 'classnames'
import Checkbox from 'components/Checkbox'
import Icon from 'components/Icon'
import CollapsibleBox from 'components/CollapsibleBox'

export { default as Sidebar } from './Sidebar'
export { default as SubHeader } from './SubHeader'

export const FilterBar = ({
  className,
  iconColor = 'silver',
  onClickSort = null,
  onClickFilter,
  sortIconColor = null,
  filterIconColor = null,
  showSortOptions = false
}) => {
  return (
    <div
      className={cx(
        'layout-width-constraint flex items-center pv4', {
          'justify-end': !showSortOptions,
          'justify-between': showSortOptions
        },
        className
      )}
    >
      {showSortOptions && (
        <button className={cx('bg-transparent flex items-center fs ph0 ml8', sortIconColor || iconColor)} onClick={onClickSort}>
          <Icon className={cx('w-icon-7', sortIconColor || iconColor)} name='swap-vertical' />
          Sort by
        </button>
      )}
      <button className={cx('bg-transparent flex items-center fs ph0 mr8', filterIconColor || iconColor)} onClick={onClickFilter}>
        <Icon className={cx('w-icon-7 mr2', filterIconColor || iconColor)} name='filter-variant' />
        Filter by
      </button>
    </div>
  )
}

const Filters = ({
  facets = [],
  sortOptions = [],
  resultCount = 0,
  showSortOptions = false,
  handleChangeOption,
  handleChangeSortOption
}) => {
  if (showSortOptions) {
    return (
      <div>
        <ul className='list-reset pa0'>
          {sortOptions.map(option => (
            <FacetOption
              key={option.value}
              name={option.name}
              value={option.value}
              selected={option.selected}
              handleChange={handleChangeSortOption}
              parameterName={option.parameterName}
            />
          ))}
        </ul>
      </div>
    )
  }
  return (
    <div>
      <span className='navy dn'>Showing ({resultCount} results)</span>
      <ul className='list-reset pa0'>
        {facets.map(facet => (
          <li key={facet.name}>
            <CollapsibleBox
              title={facet.name}
              expandable
              startExpanded
              titleClassName='navy fw6 fxl mt0 mb4 pt3'
              chevronClassName='navy'
              titleContainerClassName='ph0'
              scrollingContainerClassName=''
            >
              <Facet
                handleChangeOption={handleChangeOption}
                {...facet}
              />
            </CollapsibleBox>
          </li>
        ))}
      </ul>
    </div>
  )
}

const Facet = ({ name, options, parameterName, handleChangeOption }) => (
  <ul className='list-reset pa0 pv6'>
    {options.map(option => (
      <FacetOption
        key={option.value}
        handleChange={handleChangeOption}
        parameterName={parameterName}
        {...option}
      />
    ))}
  </ul>
)
const FacetOption = ({
  name,
  value,
  count = null,
  selected,
  handleChange = () => null,
  parameterName
}) => (
  <li className='mb6'>
    <Checkbox
      name={`${name}${count ? ' (' + count + ')' : ''}`}
      circle={false}
      checked={selected}
      handleChange={() => handleChange(parameterName, value, selected)}
      highlightColor='navy'
      iconHighlightColor='white'
    />
  </li>
)

export default Filters
