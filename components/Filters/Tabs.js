import React from 'react'
import cx from 'classnames'
import { _omit, _isNumber } from 'libraries/lodash'
import { withStyles } from '@material-ui/core/styles'
import Tabs from '@material-ui/core/Tabs'
import MuiTab from '@material-ui/core/Tab'
import pageState from 'helpers/pageState'

const tabStyles = (props = {}) => theme => ({
  root: {
    opacity: 1,
    overflow: 'visible',
    textTransform: 'none',
    paddingRight: props.last ? '20px' : '1rem',
    paddingLeft: props.first ? '20px' : 0
  }
})

export const Tab = props => {
  const Component = withStyles(tabStyles(props))(props => (
    <MuiTab {..._omit(props, ['first', 'last', 'isActive', 'resultCount'])} disableRipple component='div' />
  ))
  return <Component {...props} />
}

const ScrollableTabs = pageState({ tabValue: 0 }, true)(({
  tabValue,
  children,
  setPageState,
  scrollable = true
}) => {
  const handleChange = (event, newValue) => {
    setPageState({ tabValue: newValue })
  }
  return (
    <Tabs
      value={tabValue}
      variant={scrollable ? 'scrollable' : 'standard'}
      onChange={handleChange}
      aria-label='tabs'
      scrollButtons='auto'
      TabIndicatorProps={{ style: { display: 'none' } }}
    >
      {children}
    </Tabs>
  )
})

export const TabLabel = ({ name, colour, showNewTag, isActive, resultCount, showResultCount = true }) => (
  <div
    className={cx('flex justify-center items-center br2 db pv4 ph4 w-100 min-w-image-9 fm fm-t no-underline nowrap w-100-t mt2-t tc ba lh-solid', {
      [`white bg-${colour} relative b--${colour}`]: isActive,
      'silver b--silver fw5': !isActive,
      relative: showNewTag
    })}
  >
    {showNewTag && <span className='bg-light-sea-green white br2 ph2 pv2 absolute right0 dn-t x-100_s4 fxs'>NEW</span>}
    <span className={cx({ 'fw6 fw5-t': isActive })}>
      {name}
    </span>
    &nbsp;
    {showResultCount && (_isNumber(resultCount) ? `(${resultCount})` : '(...)')}
  </div>
)

export default ScrollableTabs
