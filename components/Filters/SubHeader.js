import React from 'react'
import cx from 'classnames'
import pageState from 'helpers/pageState'
import ResponseConsumer from 'components/ResponseConsumer'
import { FilterBar } from 'components/Filters'

const Container = (props) => {
  const isMobileMenuOpen = false

  return (
    <>
      <div className='ie-filters-top-spacing dn' />
      <div className='search-page-top subheader-min-height bg-white sticky ie-sticky-fix ie-search-header-top w-100 top0 relative z5'>
        <div
          className={cx(
            'pt4 pt4-t',
            { 'shadow-3-black-fade-10': isMobileMenuOpen }
          )}
        >
          <ResponseConsumer
            content={() => <SubHeader {...props} />}
            responses={props.responses}
            pendingComponent={() => <SubHeader pending includeTopicResults={props.includeTopicResults} />}
          />
        </div>
      </div>
    </>
  )
}

export default Container

// TODO: check if need different name for docs and search to avoid name clash (maybe prefix with window.location or something?)
const SubHeader = pageState(
  { filtersOpen: false, sortOptionsOpen: false },
  true
)((props) => {
  const {
    setPageState,
    pending,
    topicResults,
    documentResults,
    bundleResults,
    searchPhrase,
    filtersOpen,
    sortOptionsOpen,
    showSortOptions = false,
    SidebarFiltersComponent = null,
    TabFiltersComponent = null,
    includeTopicResults = true,
    pageLimit
  } = props

  const setFiltersOpen = filtersOpen => setPageState({ filtersOpen })
  const setSortOptionsOpen = sortOptionsOpen => setPageState({ sortOptionsOpen })

  return (
    <div className={cx({ 'vh-100-minus-nav-t flex-t flex-column-t': filtersOpen || sortOptionsOpen })}>
      <div className={cx({ pv4: !includeTopicResults })}>
        {includeTopicResults && (
          <ResultsMessage {...{ pending, topicResults, documentResults, bundleResults, searchPhrase }} />
        )}
        {TabFiltersComponent && <TabFiltersComponent {...props} />}
        <FilterBar
          className='dn-d'
          sortIconColor={sortOptionsOpen ? 'navy' : 'silver'}
          filterIconColor={filtersOpen ? 'navy' : 'silver'}
          showSortOptions={showSortOptions}
          onClickSort={_ => {
            const update = !sortOptionsOpen
            setSortOptionsOpen(update)
            filtersOpen && setFiltersOpen(false)
          }}
          onClickFilter={_ => {
            const update = !filtersOpen
            setFiltersOpen(update)
            sortOptionsOpen && setSortOptionsOpen(false)
          }}
        />
      </div>
      {SidebarFiltersComponent && (
        <SidebarFiltersComponent
          className={cx('bg-white basis-auto overflow-y-auto dn-d transition-all grow-1 pl8', {
            'x--100 absolute': !filtersOpen && !sortOptionsOpen,
            x0: filtersOpen || sortOptionsOpen
          })}
          {...props}
          defaultQuery={{
            q: searchPhrase,
            p: 1,
            l: pageLimit
          }}
          showSortOptions={sortOptionsOpen}
        />
      )}
    </div>
  )
})

const ResultsMessage = ({ pending, topicResults, documentResults, bundleResults, searchPhrase }) => (
  <div className='layout-width-constraint'>
    <h3 className='navy fm fs-m fw2 fw6-m mt0 mh0 mb4'>
      <span>
        <Message {...{ pending, topicResults, documentResults, bundleResults, searchPhrase }} />
      </span>
    </h3>
  </div>
)

const Message = ({ pending, topicResults, documentResults, bundleResults, searchPhrase }) => {

  if (pending) {
    return <PendingMessage />
  }

  const resultCount = topicResults.resultCount + documentResults.resultCount + bundleResults.resultCount
  const displaySearchPhrase = <span className='fw6'>&quot;{searchPhrase}&quot;</span>

  return resultCount === 0
    ? <NoResultsFoundMessage {...{displaySearchPhrase}}  />
    : <ResultsFoundMessage {...{displaySearchPhrase, resultCount}} />
}

const PendingMessage = () => (
  <>Search results for <span className='fw6'>&quot;...&quot;</span> (...) results</>
)

const NoResultsFoundMessage = ({ displaySearchPhrase }) => (
  <>No results found for {displaySearchPhrase}</>
)

const ResultsFoundMessage = ({ displaySearchPhrase, resultCount }) => {
  const displayResultCount = `(${resultCount || '...'} ${resultCount === 1 ? 'result' : 'results'})`

  return (
    <>Search results for {displaySearchPhrase} {displayResultCount}</>
  )
}
