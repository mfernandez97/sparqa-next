import React, { Component, Fragment } from 'react'
import { $paymentPlans, getPlanPrice } from 'services/PaymentsService'
import ResubscribeModal from 'components/ResubscribeModal'
import connect from 'helpers/connect'
import ResponseConsumer from 'components/ResponseConsumer'
import MarketingModal from './MarketingModal'
import Button, { themes, themeModifiers } from 'components/Button'

@connect(() => ({ paymentPlans: $paymentPlans() }))
export default class Expired extends Component {
  state = {
    showResubscribeModal: false,
    showConfirmFreemiumModal: false
  }

  closeResubscribeModal = () => this.setState({ showResubscribeModal: false })

  closeConfirmFreemiumModal = () => this.setState({ showConfirmFreemiumModal: false })

  renderContent = () => {
    const { paymentPlans } = this.props
    const monthlyPrice = getPlanPrice({ name: 'monthly', paymentPlans })
    const annualPrice = getPlanPrice({ name: 'annual', paymentPlans })
    return (
      <Fragment>
        <div className='w-100 pv8 tc flex-column items-center justify-center self-start mb12-t mb13-m ph6 max-w-measure-4 mhauto'>
          <h2 className='mt0 fxl b dark-slate-grey'>Subscription Expired</h2>
          <p className='tl'>
            To view this content please select from one of the following options:
          </p>
          <ul className='tl'>
            <li>To continue accessing Sparqa Legal's extensive library of Q&A guidance you can register to our FREE plan.</li>
            <li>To unlock and access any previously saved documents and benefit from +300 contracts and templates please re-subscribe to either our monthly or annual payment plans - from only £{monthlyPrice} a month or £{annualPrice} for the year.</li>
          </ul>
          <div className='flex justify-evenly flex-column-reverse-t items-center-t tc no-underline mt8'>
            <Button
              theme={themes.DARK_SLATE_GREY}
              onClick={e => this.setState({ showConfirmFreemiumModal: true })}
              margin='mv0 mr4 mr0-t mb6-6'
            >
              Switch to FREE Plan
            </Button>
            <Button
              theme={themes.DARK_SLATE_GREY}
              themeModifier={themeModifiers.INVERT}
              onClick={e => this.setState({ showResubscribeModal: true })}
              margin='mv0 ml4 ml0-t'
            >
              Renew subscription
            </Button>
          </div>
        </div>
        <ResubscribeModal visible={this.state.showResubscribeModal} closeModal={this.closeResubscribeModal} />
        <MarketingModal visible={this.state.showConfirmFreemiumModal} closeModal={this.closeConfirmFreemiumModal} />
      </Fragment>
    )
  }

  render () {
    return <ResponseConsumer content={this.renderContent} response={this.props.paymentPlans} />
  }
}
