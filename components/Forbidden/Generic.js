import React from 'react'
import Modal, { ModalBox } from 'components/Modal'
import Icon from 'components/Icon'
import Button, { themeModifiers } from 'components/Button'
import { DEFAULTLOGGED } from 'constants/controllers'

const Generic = ({ title, message = null }) => (
  <Modal visible>
    <ModalBox title={title}>
      <div className='pa8 pa6-m'>
        <div className='tc mb6'>
          <Icon className='w-icon-10 grey' name='alert' />
        </div>
        {message && <p className='mb6' dangerouslySetInnerHTML={{ __html: message }} />}
        <Button
          link
          controller={DEFAULTLOGGED}
          themeModifier={themeModifiers.SOLID}
        >
          Home
        </Button>
      </div>
    </ModalBox>
  </Modal>
)
export default Generic
