import React from 'react'
import Route from 'route-parser'
import { switchMap } from 'rxjs/operators'
import { route as routeString } from 'pages/documentinterview'
import { $document } from 'services/DocumentService'
import { $documentSeoUrlToId } from 'services/SeoService'
import connect from 'helpers/connect'
import Modal, { ModalBox } from 'components/Modal'
import Icon from 'components/Icon'
import Button, { themeModifiers } from 'components/Button'
import ResponseConsumer from 'components/ResponseConsumer'

const route = new Route(routeString)

const Freemium = ({ document }) => {
  const href = window.location.href.replace(/\/interview$/, '/overview')
  const copy = document.price > 0
    ? 'Follow the link below to the overview page where you will be able to buy this document.'
    : 'You can access this document from the overview page. Follow the link below!'
  return (
    <Modal visible>
      <ModalBox title='Access Denied'>
        <div className='pa8 pa6-m'>
          <div className='tc mb6'>
            <Icon className='w-icon-10 grey' name='alert' />
          </div>
          <p className='mb6'>{copy}</p>
          <Button
            anchor
            href={href}
            themeModifier={themeModifiers.SOLID}
          >
            Go to Overview
          </Button>
        </div>
      </ModalBox>
    </Modal>
  )
}

const ConnectedFreemium = connect(() => ({
  document: $documentSeoUrlToId.pipe(switchMap(documentSeoUrlToId => {
    const path = window.location.pathname.slice(1)
    const { documentName } = route.match(path)
    const documentId = documentSeoUrlToId[documentName]
    return $document({ documentId })
  }))
}))(props => (
  <ResponseConsumer
    response={props.document}
    content={() => <Freemium {...props} document={props.document} />}
  />
))

export default ConnectedFreemium
