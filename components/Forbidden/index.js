import React from 'react'
import { $user } from 'services/UserService'
import connect from 'helpers/connect'
import ResponseConsumer from 'components/ResponseConsumer'
import StatusBox from 'components/StatusBox'
import Expired from './Expired'
import Freemium from './Freemium'
import Generic from './Generic'

// NOTE: We currently only show Forbidden pages to Freemium or expired users on
// document interview pages.
const Forbidden = ({ user, status = null, message = null }) => {
  if (status) {
    return (
      <Generic
        title={status}
        message={message}
      />
    )
  }

  if (!user.isFreemium && !user.isSubscribed) {
    return <Expired />
  }

  if (user.isFreemium) {
    return <Freemium />
  }

  return (
    <StatusBox
      title={'You don\'t have permission to see this. Please check your subscription status.'}
      iconName='alert'
      className='vh-100-minus-nav'
    />
  )
}

const ForbiddenConsumer = props => (
  <ResponseConsumer response={props.user} content={() => <Forbidden {...props} />} />
)

const ConnectedForbidden = connect(() => ({
  user: $user
}))(ForbiddenConsumer)

export default ConnectedForbidden
