import React, { Component } from 'react'
import { USER } from 'constants/requests'
import { refresh } from 'services/ApiService'
import { updateMarketingConsent } from 'services/UserService'
import Modal, { ModalBox } from 'components/Modal'
import { FreemiumPlan } from 'components/PaymentPlans'
import insist from 'components/Form/esv-react-form/insist'
import Svg from 'components/SVG'
import Form, { Field as FormField, FormSubmit, ErrorMessages } from 'components/Form'

const schema = {
  marketingConsent: insist().isTrue({ message: 'To be eligible for our free tier, you must agree to receive updates on the laws and relevant promotions that affect your business.' })
}

const initialValues = {
  marketingConsent: false
}

export default class MarketingModal extends Component {
  state = {
    success: null,
    error: null
  }

  reload = () => window.location.reload()

  handleSubmit = (e, { onFinishSubmit }) => {
    updateMarketingConsent({ marketingConsent: true })
      .toPromise()
      .then(({ marketingConsent }) => {
        marketingConsent && this.setState({ success: true }, onFinishSubmit)
      })
      .then(() => refresh(USER))
      .catch(() => {
        this.setState({ error: 'Unable to update your account' }, () => {
          throw new Error(this.state.error)
        })
      })
  }

  success = () => (
    <>
      <Svg
        url='assets/smile.svg'
        alt='Smiling face'
        className='light-sea-green db mhauto mt0 mb7 w-image-5 h-image-5'
      />
      <div className='tc b fxxl tracked light-sea-green lh-title mb7 mhauto mt0'>
        You have succesfully switched to a free account.
      </div>
    </>
  )

  confirm = () => (
    <Form
      schema={schema}
      initialValues={initialValues}
      onSubmit={this.handleSubmit}
    >
      <FreemiumPlan margin='mhauto' selected clickable={false} />
      <Field
        label='I would like to receive updates on the laws and relevant promotions that affect my business.'
        type='checkbox'
        name='marketingConsent'
      />
      <FormSubmit>Confirm</FormSubmit>
      {this.state.error && <ErrorMessages message={this.state.error} />}
    </Form>
  )

  render () {
    const { success } = this.state
    return (
      <Modal visible={this.props.visible} onMaskClick={success ? this.reload : this.props.closeModal} className={this.props.className}>
        <ModalBox title='Free Plan' closeModal={success ? this.reload : this.props.closeModal}>
          <div className='pa7'>
            {!success ? this.confirm() : this.success()}
          </div>
        </ModalBox>
      </Modal>
    )
  }
}

const Field = props => <div className='mv8'><FormField {...props} /></div>
