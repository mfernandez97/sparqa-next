import 'polyfills/prepend'
import React, { Component } from 'react'
import propTypes from 'prop-types'
import ScrollingContainer from 'components/ScrollingContainer'
import { _isFunction, _minBy } from 'libraries/lodash'
import { tempCache } from 'helpers/cache'
import { emptyNode, createNode } from 'helpers/dom'
import cx from 'classnames'

const subtractMinValue = (list, propName) => {
  const min = _minBy(list, propName)[propName]
  return list.map(e => ({ ...e, [propName]: e[propName] - min }))
}

export default class StickyScrollingContainer extends Component {
  static propTypes = {
    stickyQuery: propTypes.string.isRequired,
    depthPropName: propTypes.string.isRequired
  }

  static defaultProps = {
    depthPropName: 'depth'
  }

  state = {
    stackedElements: [],
    fixedElements: [],
    fixedTo: null
  }

  fixedHost = null

  getScrollingContainer = tempCache(() => {
    const { container } = this.refs

    return container
      ? container.querySelector('.scrolling-container')
      : null
  })

  get scrollTop () {
    const scrollingContainer = this.getScrollingContainer()
    return scrollingContainer
      ? scrollingContainer.scrollTop
      : 0
  }

  getStickyList = tempCache(() => {
    const { stickyQuery } = this.props
    const { container } = this.refs
    const scrollingContainer = this.getScrollingContainer()

    if (!container || !scrollingContainer) {
      return []
    }

    const containerTop = scrollingContainer.getBoundingClientRect().top - scrollingContainer.scrollTop
    const nodes = [...container.querySelectorAll(stickyQuery)]
    return subtractMinValue(
      nodes.map(
        (el, index) => {
          const { top } = el.getBoundingClientRect()
          const height = el.offsetHeight
          return {
            el,
            top: Math.floor(top - containerTop),
            bottom: Math.floor(top + height - containerTop),
            height,
            depth: +el.dataset[this.props.depthPropName],
            id: index
          }
        }),
      'depth'
    )
  })

  // can be improved
  getStackedList = (fromTop) => {
    const stackedHeight = (stack, depth) => stack.slice(0, depth).reduce((a, s) => a + s.height, 0)

    return this.getStickyList().reduce(
      ({ stackedElements, fixedElements, fixedTo }, currentElement, index, list) => {
        const nextElement = list[index + 1] || null

        if (currentElement.top < fromTop + stackedHeight(stackedElements, currentElement.depth)) {
          stackedElements = [...stackedElements.slice(0, currentElement.depth), currentElement]
        }

        if (
          nextElement &&
          nextElement.top < fromTop + stackedHeight(stackedElements) &&
          nextElement.top >= fromTop + stackedHeight(stackedElements, nextElement.depth)
        ) {
          fixedElements = stackedElements.slice(nextElement.depth)
          fixedTo = nextElement.el
          stackedElements = [...stackedElements.slice(0, nextElement.depth)]
        }

        return { stackedElements, fixedElements, fixedTo }
      },
      { stackedElements: [], fixedElements: [], fixedTo: null }
    )
  }

  handleScroll = e => {
    const { stackedElements, fixedElements, fixedTo } = this.getStackedList(this.scrollTop)
    const toStr = s => s.map(a => a.id).join(',')
    if (toStr(this.state.stackedElements) !== toStr(stackedElements)) {
      this.setState({ stackedElements, fixedElements, fixedTo })
    }

    if (_isFunction(this.props.onScroll)) {
      this.props.onScroll({
        ...e,
        adjustedScrollTop: this.scrollTop + stackedElements.reduce((a, e) => a + e.height, 0)
      })
    }
  }

  emptyFixedList () {
    this.fixedHost && this.fixedHost.parentNode.removeChild(this.fixedHost)
    this.fixedHost = null
  }

  fixNodes = (fixTo, elements) => {
    this.fixedHost = createNode('div', { className: 'sticky-scrolling-container__fixed' })
    fixTo.prepend(this.fixedHost)
    fixTo.style.position = 'relative'

    elements.forEach(el => this.fixedHost.appendChild(el.cloneNode(true)))
  }

  stackNode = (container) => ({ el }) => {
    container.appendChild(el.cloneNode(true))
  }

  componentDidMount () {
    this.componentDidUpdate()
  }

  componentDidUpdate () {
    if (this.refs.overlay) {
      const container = emptyNode(this.refs.overlay)
      this.state.stackedElements.forEach(this.stackNode(container))
      this.emptyFixedList()
      if (this.state.fixedTo && this.state.fixedTo.parentNode) {
        this.fixNodes(
          this.state.fixedTo.parentNode,
          this.state.fixedElements.map(({ el }) => el)
        )
      }
    }
  }

  adjustScrollTop = (scrollTo, subtract = true) => {
    const stickyElementsHeight = this.getStickyList()
      .filter(el => el.top < scrollTo)
      .reduce((a, e) => [...a.slice(0, e.depth), e], [])
      .reduce((a, e) => a + e.height, 0)
    if (!subtract) {
      return scrollTo + stickyElementsHeight
    }
    return scrollTo - stickyElementsHeight
  }

  render () {
    return (
      <div ref='container' className={cx('sticky-scrolling-container', this.props.containerClassName)}>
        <div ref='overlay' className={cx('sticky-scrolling-container__overlay', this.props.overlayClassName)} />
        <ScrollingContainer
          {...this.props}
          adjustScrollTop={this.adjustScrollTop}
          onScroll={this.handleScroll}
        />
      </div>
    )
  }
}
