import React from 'react'
import { _includes, _keys, _omit } from 'libraries/lodash'
import { getClassName } from 'helpers/misc'
import Link from 'components/Link'

const LIGHT_SEA_GREEN = 'light-sea-green'
const DARK_SLATE_GREY = 'dark-slate-grey'
const ROYAL_BLUE_3 = 'royal-blue-3'
const CORAL = 'coral'

const SOLID = 'solid'
const INVERT = 'invert'
const INVERT_BORDER_COLOR = 'invert-border-color'

export const themes = { LIGHT_SEA_GREEN, DARK_SLATE_GREY, ROYAL_BLUE_3, CORAL }
export const themeModifiers = { SOLID, INVERT, INVERT_BORDER_COLOR }

const Button = ({
  anchor = false,
  link = false,
  size = 'large',
  theme = LIGHT_SEA_GREEN,
  themeModifier = null,
  CustomComponent = null,
  disabled = false,
  ...props
}) => {
  if (theme && !_includes(themes, theme)) {
    throw new Error('Unrecognised button theme')
  }
  if (themeModifier && !_includes(themeModifiers, themeModifier)) {
    throw new Error('Unrecognised button theme modifer')
  }
  const options = { size, theme, themeModifier, disabled }
  const defaults = getDefaults(options)
  const className = getClassName(defaults, props)
  const rest = _omit(props, ['className', ..._keys(defaults)])
  if (CustomComponent) {
    return <CustomComponent className={className} {...rest} />
  }
  if (link) {
    return <Link className={className} {...rest} />
  }
  if (anchor) {
    const rel = rest.target === '_blank' ? 'noopener noreferrer' : ''
    return <a className={className} rel={rel} {...rest} />
  }
  return <button className={className} {...rest} />
}

const getDefaults = ({ size, theme, themeModifier, disabled }) => {
  const maxWidth = getMaxWidth(size)
  const bg = getBackgroundColor({ theme, themeModifier })
  const color = getColor({ theme, themeModifier })
  const hoverColor = getHoverColor({ theme, themeModifier })
  const hoverBg = getHoverBackgroundColor({ theme, themeModifier })
  const borderColor = getBorderColor({ theme, themeModifier })
  const opacity = getOpacity({ disabled })
  const cursor = getCursor({ disabled })
  return {
    bg,
    br: 'br-pill',
    fw: 'b',
    color,
    width: 'w-100',
    items: 'items-center',
    border: 'ba',
    margin: 'mv0 mhauto',
    shadow: 'shadow-3-black-fade-20',
    display: 'flex',
    hoverBg,
    justify: 'justify-center',
    outline: 'outline-0',
    padding: 'pa5',
    fontSize: 'fs',
    maxWidth,
    textWrap: 'nowrap',
    textAlign: 'tc',
    underline: 'no-underline',
    lineHeight: 'lh-solid',
    transition: 'transition-all',
    hoverColor,
    hoverShadow: 'hover-shadow-3-black-fade-30',
    borderColor,
    cursor,
    opacity
  }
}

const getMaxWidth = size => {
  switch (size) {
    case 'small':
      return 'max-w-image-6'
    case 'medium':
      return 'max-w-measure-1'
    default:
      return 'max-w-measure-3'
  }
}

const getBackgroundColor = ({ theme, themeModifier }) => {
  if (themeModifier === INVERT || themeModifier === SOLID) {
    return `bg-${theme}`
  }
  return 'bg-white'
}

const getHoverBackgroundColor = ({ theme, themeModifier }) => {
  switch (themeModifier) {
    case SOLID:
      return getSolidHoverBackgroundColor({ theme })
    case INVERT:
      return 'hover-bg-white'
    default:
      return `hover-bg-${theme}`
  }
}

const getColor = ({ theme, themeModifier }) => {
  if (themeModifier === INVERT || themeModifier === SOLID) {
    return 'white'
  }
  return theme
}

const getHoverColor = ({ theme, themeModifier }) => {
  if (themeModifier === INVERT) {
    return `hover-${theme}`
  }
  return 'hover-white'
}

const getBorderColor = ({ theme, themeModifier }) => {
  switch (themeModifier) {
    case SOLID:
      return 'b--transparent'
    case INVERT:
    case INVERT_BORDER_COLOR:
      return `b--${theme}`
    default:
      return 'b--white'
  }
}

const getSolidHoverBackgroundColor = ({ theme }) => {
  switch (theme) {
    case DARK_SLATE_GREY:
      return 'hover-bg-dark-slate-grey-lighten-5-saturate-15'
    case ROYAL_BLUE_3:
      return 'hover-bg-royal-blue-3-saturate-35'
    case CORAL:
      return 'hover-bg-coral-saturate-125'
    default:
      return 'hover-bg-light-sea-green-saturate-15'
  }
}

const getOpacity = ({ disabled }) => disabled && 'o-30'

const getCursor = ({ disabled }) => disabled ? 'not-allowed' : 'pointer'

export default Button
