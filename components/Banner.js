import React from 'react'
import cx from 'classnames'

const Banner = ({ children, className: classes, inlineStyle: style, href, target }) => {
  const baseClassName = 'w-100 no-underline db'
  const className = cx(baseClassName, classes)
  const rel = target === '_blank' ? 'noopener noreferrer' : ''
  return href ? (
    <a {...{ className, style, href, target, rel }}>
      {children}
    </a>
  ) : (
    <div className={className}>
      {children}
    </div>
  )
}

export default Banner
