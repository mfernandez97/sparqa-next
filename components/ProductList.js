import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import { _get, _isArray, _includes } from 'libraries/lodash'
import connect from 'helpers/connect'
import { formatCurrency } from 'helpers/misc'
import { getDocumentIcon, $purchasedDocumentsIds } from 'services/DocumentService'
import { $sectionIdToUrlMap, $documentSeoIdToUrl, $bundleSeoIdToUrl } from 'services/SeoService'
import { $isSubscribedUser } from 'services/UserService'
import ResponseConsumer from 'components/ResponseConsumer'
import Link, { DOCUMENTSMENU, DOCUMENTOVERVIEW, BUNDLEOVERVIEW } from 'components/Link'
import FloatingBox from 'components/FloatingBox'
import RootTopicIcon from 'components/RootTopicIcon'
import Svg from 'components/SVG'
import Skeleton from '@material-ui/lab/Skeleton'

const ProductListSwitch = ({ productType, pending, ...props }) => {
  const isBundle = productType === 'bundle'

  return pending
    ? <ProductList links={[...Array(4)]} pending {...{ isBundle, ...props }} />
    : <ConnectedProductList {...{ ...props, productType, isBundle, ...props }} />
}

const ConnectedProductList = connect(({ isBundle }) => ({
  seoIdToUrl: isBundle ? $bundleSeoIdToUrl : $documentSeoIdToUrl,
  sectionIdToUrl: $sectionIdToUrlMap,
  isSubscribed: $isSubscribedUser,
  purchasedProductIds: $purchasedDocumentsIds()
}))(({
  seoIdToUrl,
  sectionIdToUrl,
  isBundle,
  isSubscribed,
  list,
  productType,
  title,
  purchasedProductIds,
  startExpanded
}) => {
  const controller = isBundle ? BUNDLEOVERVIEW : DOCUMENTOVERVIEW
  const links = list.map(product => {
    const baseClassName = 'w-icon-6 h-icon-6 shrink-0'
    return Object.assign({}, product, {
      controller,
      free: product.price === 0,
      params: {
        section: sectionIdToUrl[product.topic.bookmarkId],
        [`${productType}Name`]: seoIdToUrl[product.id]
      },
      purchased: purchasedProductIds && _includes(purchasedProductIds, product.id),
      renderIcon: () => isBundle ? (
        <RootTopicIcon bookmarkId={product.topic.bookmarkId} className={cx(baseClassName, 'ml2')} />
      ) : (
        <Svg
          className={cx(baseClassName)}
          url={getDocumentIcon(_get(product, 'documentType.name'))}
        />
      )
    })
  })

  const renderContent = () => (
    <ProductList
      {...{
        isBundle,
        title,
        links,
        showPrice: !isSubscribed,
        startExpanded
      }}
    />
  )

  return (
    <ResponseConsumer
      response={[seoIdToUrl, sectionIdToUrl, isSubscribed, purchasedProductIds]}
      content={renderContent}
      pendingComponent={() => <ProductListSwitch pending {...{ productType, startExpanded, title }} />}
    />
  )
})

ConnectedProductList.propTypes = {
  productType: PropTypes.oneOf(['document', 'bundle']).isRequired
}

const ProductLink = ({
  controller,
  params,
  title,
  price,
  purchased,
  free,
  renderIcon,
  showPrice,
  isBundle
}) => (
  <Link
    className='flex hover-underline no-underline fm items-center grey ml2'
    {...{ controller, params }}
  >
    <span className='flex flex-column items-center fxs mr8'>
      {renderIcon()}
      {showPrice && (
        <p className={cx('mv0', {
          ml1: isBundle,
          coral: !purchased,
          'steel-blue': purchased,
          'light-sea-green': !purchased && free
        })}
        >
          {purchased ? 'Purchased' : free ? 'Free' : `£${formatCurrency(price, true)}`}
        </p>
      )}
    </span>
    <span className='grey'>{title}</span>
  </Link>
)

const ProductList = ({
  isBundle,
  title,
  links,
  showPrice,
  startExpanded,
  pending
}) => links.length ? (
  <FloatingBox
    expandable
    title={title}
    titleClassName='ml5'
    startExpanded={_isArray(startExpanded) ? _includes(startExpanded, 1) : startExpanded}
    className={cx('mt0-t mb6-m mb8 last-child-mb0 grow-1 relative static-t shadow-1-silver-fade-40')}
    titleContainerClassName='relative pl10 pr6'
    icon={
      <img
        src={isBundle ? 'assets/toolkit.svg' : 'assets/grid-icons/png/templates.png'}
        alt={`${isBundle ? 'Bundles' : 'Documents'} icon`}
        className='w-icon-9 h-icon-9 mr6 absolute left6'
      />
    }
  >
    <ul className='h-100 list-reset ma0 pa0'>
      {links.map((link, index) => (
        <li key={index} className='pl6 pr7 pb6 mb6 bb b--light-silver'>
          {pending
            ? (
              <div className='flex'>
                <div className='flex flex-column ml2 mr6'>
                  <Skeleton variant='circle' height={24} width={24} />
                  <Skeleton width={25} height={18} />
                </div>
                <Skeleton className='grow-1' />
              </div>
            )
            : (
              <ProductLink {...{ ...link, showPrice, isBundle }} />
            )}
        </li>
      ))}
      <li className='pb6'>
        <Link
          className='flex hover-underline no-underline fm tc items-center grey'
          controller={DOCUMENTSMENU}
        >
          <span className='basis0 grow-1 shrink-1'>
              See all {isBundle ? 'toolkits' : 'documents'}
            <img className='w-icon-6 ml4 vertical-align-t-bottom grey' src='assets/icons/arrow-right.svg' />
          </span>
        </Link>
      </li>
    </ul>
  </FloatingBox>
) : null

export default ProductListSwitch
