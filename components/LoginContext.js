import React, { Component, createContext } from 'react'
import Cookies from 'js-cookie'
import { merge, forkJoin } from 'rxjs'
import { filter, first } from 'rxjs/operators'
import { login } from 'requests/dao/user'
import { replaceUrl, getBaseUrl } from 'front-end-core/core/history'
import { isValidResponse } from 'core/api/dataflow'
import { pushEvent } from 'services/AnalyticsService'
import { refresh, observe } from 'services/ApiService'
import {
  USER,
  USER_BILLING,
  DOCUMENT_INTERVIEW,
  USER_CURRENT_SUBSCRIPTION,
  SAVED_DOCUMENTS,
  SAVED_DOCUMENT_INTERVIEW,
  SAVED_DOCUMENT_INTERVIEW_RAW_TEXT,
  PURCHASED_DOCUMENTS,
  DOCUMENT_PURCHASED,
  CREDIT,
  TOPICS,
  TOPIC_CONTENT,
  CUSTOMER_DETAILS
} from 'constants/requests'
import { PARTNER_COOKIE, PARTNER_BANNER_DISMISSED_COOKIE, PARTNER_DISCOUNT_CODE_COOKIE } from 'constants/cookies'

let LoginContext
const { Provider, Consumer: LoginConsumer } = LoginContext = createContext({ responseError: null })

const refreshRequests = () => merge(
  refresh(TOPICS),
  refresh(TOPIC_CONTENT),
  refresh(USER_BILLING),
  refresh(DOCUMENT_INTERVIEW, '*'),
  refresh(USER_CURRENT_SUBSCRIPTION),
  refresh(SAVED_DOCUMENTS),
  refresh(SAVED_DOCUMENT_INTERVIEW, '*'),
  refresh(SAVED_DOCUMENT_INTERVIEW_RAW_TEXT, '*'),
  refresh(PURCHASED_DOCUMENTS),
  refresh(DOCUMENT_PURCHASED, '*'),
  refresh(CREDIT),
  refresh(CUSTOMER_DETAILS)
)

class LoginProvider extends Component {
  state = { responseError: null }

  handleSubmit = (values = null, trackLogin = false) => {
    this.setState({ responseError: null })
    return login(values).then(response => this.handleLoginResponse(Object.assign(response, { trackLogin })))
  }

  handleLoginResponse = ({ ok, trackLogin }) => ok ? this.handleAuthorized(trackLogin) : this.handleUnauthorized()

  handleUnauthorized = () => {
    this.setState({ responseError: 'Incorrect email address or password' })
  }

  handleAuthorized = trackLogin => {
    trackLogin && pushEvent({ category: 'Session', action: 'Login' })
    const path = this.props.path ? this.props.path : 'topics'
    if (!this.props.noRedirect) {
      replaceUrl(getBaseUrl() + path)
    }
    // The partner cookie is deleted by the backend, but in the case of CMS
    // previews the cookie is set on the frontend so might as well delete it on
    // the frontend just in case.
    Cookies.remove(PARTNER_COOKIE)
    Cookies.remove(PARTNER_BANNER_DISMISSED_COOKIE)
    Cookies.remove(PARTNER_DISCOUNT_CODE_COOKIE)

    return forkJoin(
      observe(USER).pipe(
        filter(isValidResponse),
        first()
      ), refreshRequests())
      .toPromise()
      .then(([user]) => user)
  }

  render () {
    return (
      <Provider
        value={{
          responseError: this.state.responseError,
          handleSubmit: this.handleSubmit,
          handleLoginResponse: this.handleLoginResponse,
          handleUnauthorized: this.handleUnauthorized,
          handleAuthorized: this.handleAuthorized
        }}
      >
        {this.props.children}
      </Provider>
    )
  }
}

export { LoginProvider, LoginConsumer, LoginContext }
