import React from 'react'
import cx from 'classnames'
import ReactHTMLEllipsis from 'react-lines-ellipsis/lib/html'
import responsiveHOC from 'react-lines-ellipsis/lib/responsiveHOC'
import { _get, _slice, _join, _isNil } from 'libraries/lodash'
import { getDocumentIcon } from 'services/DocumentService'
import { Media } from 'services/WindowService'
import connect from 'helpers/connect'
import Link, { TOPICSCONTENT, DOCUMENTOVERVIEW, BUNDLEOVERVIEW } from 'components/Link'
import ResultPath from './ResultPath'

const RESOURCE_MAX_TITLE_LENGTH = 83
const RESOURCE_MAX_TITLE_LENGTH_MOBILE = 83

const HTMLEllipsis = responsiveHOC()(ReactHTMLEllipsis)

export const DocumentResultItem = ({
  id,
  price,
  title,
  mobile,
  pending,
  purchased,
  description,
  documentType,
  isSubscribed,
  topic,
  sectionIdUrlMap,
  documentIdUrlMap
}) => {
  const typeName = pending ? 'Forms' : _get(documentType, 'name')
  const bookmarkId = pending ? null : topic.bookmarkId
  return (
    <ResultItem
      text={description}
      price={price}
      title={title}
      mobile={mobile}
      pending={pending}
      iconUrl={getDocumentIcon(typeName, { alt: true })}
      purchased={purchased}
      linkParams={pending ? {} : {
        documentName: documentIdUrlMap[id],
        section: sectionIdUrlMap[bookmarkId]
      }}
      isSubscribed={isSubscribed}
      linkController={DOCUMENTOVERVIEW}
    />
  )
}

export const TopicResultItem = ({
  path,
  path: hierarchy,
  tocId,
  s2,
  answer,
  mobile,
  pending,
  question,
  questionNumber,
  sectionIdUrlMap,
  questionIdUrlMap
}) => (
  <ResultItem
    text={answer}
    title={question}
    mobile={mobile}
    pending={pending}
    iconUrl='assets/question.svg'
    resultPath={path}
    linkParams={pending ? {} : {
      section: sectionIdUrlMap[hierarchy[0].id],
      topicName: sectionIdUrlMap[s2],
      questionName: questionIdUrlMap[tocId]
    }}
    questionNumber={questionNumber}
    linkController={TOPICSCONTENT}
    showResultPath
  />
)

export const BundleResultItem = ({
  id,
  title,
  price,
  mobile,
  pending,
  purchased,
  description,
  isSubscribed,
  bundleIdUrlMap,
  sectionIdUrlMap,
  bundleDocuments,
  topic
}) => {
  const sectionId = _get(topic, 'bookmarkId', null)

  const linkParams = {
    section: _get(sectionIdUrlMap, sectionId, null),
    bundleName: _get(bundleIdUrlMap, id, null)
  }

  return (
    <ResultItem
      text={description}
      title={title}
      price={price}
      mobile={mobile}
      pending={pending}
      iconUrl='assets/toolkit.svg'
      purchased={purchased}
      resources={bundleDocuments}
      linkParams={pending ? {} : linkParams}
      isSubscribed={isSubscribed}
      showResources
      linkController={BUNDLEOVERVIEW}
    />
  )
}

export const RestrictedResultsItem = ({ question: title, mobile, questionNumber }) => {
  const iconUrl = 'assets/question.svg'
  return (
    <div className='search-result flex flex-row hover-shadow-3-black-fade-10 shadow-4-black-fade-5 transition-all br2 relative mb4-t'>
      <div className='absolute bg-white-fade-70 top0 bottom0 left0 right0 z0 flex justify-center items-center'>
        <p className='pv3 ph4 br2'>This section is not available for NatWest Mentor users. For further information contact <a href='mailto:info@sparqa.com'>info@sparqa.com</a>.</p>
      </div>
      {iconUrl && !mobile && <img className='w-icon-10 h-icon-10 shrink-0 mr7' src={iconUrl} alt='' />}
      <BlurredItemContent title={title} questionNumber={questionNumber} />
    </div>
  )
}

const ResultItem = ({
  text,
  title,
  price,
  mobile,
  iconUrl,
  pending,
  purchased,
  resources = [],
  resultPath = null,
  linkParams = {},
  isSubscribed = false,
  linkController,
  questionNumber = null,
  showResources = false,
  showResultPath = false
}) => {
  const maxResourceTitleLength = mobile ? RESOURCE_MAX_TITLE_LENGTH_MOBILE : RESOURCE_MAX_TITLE_LENGTH
  const showNumResources = mobile ? 2 : 4
  const showPrice = !_isNil(price) && !purchased && !isSubscribed
  const showPurchased = purchased && !isSubscribed

  return (
    <div className='search-result hover-shadow-3-black-fade-10 shadow-4-black-fade-5 transition-all-0_6 br2 mb4'>
      {pending ? (
        <div className='pa6 pa4-t flex flex-row'>
          {iconUrl && !mobile && <img className='w-icon-13 h-icon-13 shrink-0 mr6' src={iconUrl} alt='' />}
          <BlurredItemContent showResultPath={showResultPath} />
        </div>
      ) : (
        <Link
          className='no-underline pa6 pa5-t flex flex-row'
          controller={linkController}
          params={linkParams}
        >
          <div className={cx('flex flex-column mr6', { 'self-center': !showPrice && !showPurchased })}>
            {iconUrl && <img className='w-icon-13 h-icon-13 shrink-0' src={iconUrl} alt='' />}
            {showPrice && <span className='mt2 tc fs coral'>{Number(price) === 0 ? 'FREE' : `£${price / 100}`}</span>}
            {showPurchased && <span className='mt2 tc fs steel-blue'>Purchased</span>}
          </div>
          <div className='pt6 w-100'>
            <Title title={title} questionNumber={questionNumber} />
            {resultPath && showResultPath && <ResultPath path={resultPath} />}
            <div className='fs grey fw5'>
              <HTMLEllipsis unsafeHTML={text} maxLine={2} />
            </div>
            {showResources && (
              <div className='flex flex-wrap mt4 flex-column-t'>
                {_slice(resources, 0, showNumResources).map(({ uuid, name }) => {
                  const truncatedName = name.length <= maxResourceTitleLength ? name : `${_join(_slice(name, 0, maxResourceTitleLength - 3), '')}...`
                  return (
                    <div key={uuid} className='flex w-50 w-100-t justify-start items-center mt4 pr2'>
                      <img src={getDocumentIcon('Forms')} alt='' className='shrink-0 w-icon-6 mr4' />
                      <h5 className='dark-grey fw7 fs mv0'>{truncatedName}</h5>
                    </div>
                  )
                })}
              </div>
            )}
          </div>
        </Link>
      )}
    </div>
  )
}

const Title = connect(() => ({
  mobile: Media.Mobile,
  tablet: Media.Tablet
}))(({ title, className, questionNumber, ...media }) => {
  const mobile = media.mobile || media.tablet
  return (
    <h4 className={cx('search-result-title grey fm fw6 mt0 mb2 db', className)}>
      {mobile ? (
        <HTMLEllipsis
          maxLine={2}
          unsafeHTML={`${questionNumber ? 'Q' + questionNumber.toString() + ': ' : ''}${title}`}
        />
      ) : (
        <>
          {questionNumber && <span>Q{questionNumber}: </span>}
          <span dangerouslySetInnerHTML={{ __html: title }} />
        </>
      )}
    </h4>
  )
})

const BlurredItemContent = ({ title, questionNumber, showResultPath }) => (
  <div className='w-100 pr6 pt6'>
    {title ? (
      <Title title={title} className='o-50' questionNumber={questionNumber} />
    ) : (
      <span className='bg-grey-fade-20 h-icon-6 w-90 mr2 db' />
    )}
    {showResultPath && (
      <div className='flex flex-wrap items-center justify-start'>
        <span className='bg-mid-grey-fade-40 h-icon-2 w-image-8 mr2 w-auto-t grow-1-t' />
        <span className='mid-grey-fade-40'>/</span>
        <span className='mh2 bg-mid-grey-fade-40 h-icon-2 w-image-9 w-auto-t grow-1-t' />
        <span className='mid-grey-fade-40'>/</span>
        <span className='ml2 bg-mid-grey-fade-40 h-icon-2 w-measure-1 w-auto-t grow-1-t' />
      </div>
    )}
    <div className='w-100 fs bg-grey-fade-20 white fw5 pa2 mt2 mb1 h-icon-4' />
    <div className='w-100 fs bg-grey-fade-20 white fw5 pa2 mt2 h-icon-4' />
  </div>
)
