import React, { Component } from 'react'
import cx from 'classnames'
import PropTypes from 'prop-types'
import { _range } from 'libraries/lodash'
import { Media } from 'services/WindowService'
import connect from 'helpers/connect'
import Link, { SEARCH } from 'components/Link'
import Icon from 'components/Icon'

const PaginationLink = ({
  last,
  first,
  children,
  selected = false,
  controller = null,
  params = null,
  query = null
}) => {
  const baseClassName = cx('br bt bb w-icon-9 h-icon-9 fs flex justify-center items-center no-underline', {
    'ba br2 br--left': first,
    'br2 br--right': last
  })
  if (selected) {
    return <span className={cx(baseClassName, 'white bg-light-sea-green b--light-sea-green')}>{children}</span>
  }
  return (
    <Link
      className={cx(baseClassName, 'b--light-silver light-sea-green')}
      controller={controller}
      params={params}
      query={query}
    >
      {children}
    </Link>
  )
}

const JumpLink = ({
  query,
  params,
  children,
  controller
}) => {
  return (
    <Link
      query={query}
      params={params}
      className='light-sea-green no-underline fs flex items-center'
      controller={controller}
    >
      {children}
    </Link>
  )
}

export default @connect(() => ({
  mobile: Media.Mobile,
  tablet: Media.Tablet
}))
class Pagination extends Component {
  renderPage = (context, pageCount) => pageNumber => {
    const { category, defaultQuery, query } = context
    const { controller = SEARCH } = this.props
    const last = pageNumber === pageCount
    const first = pageNumber === 1

    return pageNumber === context.pageNumber
      ? <PaginationLink key={pageNumber} last={last} first={first} selected>{pageNumber}</PaginationLink>
      : (
        <PaginationLink
          key={pageNumber}
          last={last}
          first={first}
          controller={controller}
          params={{ category }}
          query={{ ...defaultQuery, ...query, p: pageNumber }}
        >
          {pageNumber}
        </PaginationLink>
      )
  }

  renderContent = () => {
    const { context, resultCount, controller = SEARCH, mobile, tablet } = this.props
    const { pageLimit, pageNumber, category, defaultQuery, query } = context
    const isDevice = mobile || tablet
    const maxLinksVisible = isDevice ? 4 : 10

    const resultsOnPage = pageLimit * pageNumber > resultCount ? resultCount % pageLimit : pageLimit
    const lowerLimit = pageLimit * (pageNumber - 1) + 1
    const upperLimit = lowerLimit + resultsOnPage - 1
    const pageCount = Math.ceil(resultCount / pageLimit)

    const lowerPageLimit = Math.max(1, pageNumber - 5)
    const upperPageLimit = Math.min(lowerPageLimit + maxLinksVisible, pageCount + 1)

    const links = []

    if (pageLimit < resultCount) {
      if (pageNumber > 1) {
        links.push(
          <div key='before' className='pr5 flex'>
            <JumpLink
              key='first'
              controller={controller}
              params={{ category }}
              query={{ ...defaultQuery, ...query, p: 1 }}
            >
              <Icon name='chevron-double-left' /> First
            </JumpLink>
            <JumpLink
              key='prev'
              controller={controller}
              params={{ category }}
              query={{ ...defaultQuery, ...query, p: pageNumber - 1 }}
            >
              <Icon name='chevron-left' /> Previous
            </JumpLink>
          </div>
        )
      }

      links.push(..._range(lowerPageLimit, upperPageLimit, 1).map(this.renderPage(context, pageCount)))

      if (pageNumber < pageCount) {
        links.push(
          <div key='after' className='pl5 flex'>
            <JumpLink
              key='next'
              controller={controller}
              params={{ category }}
              query={{ ...defaultQuery, ...query, p: pageNumber + 1 }}
            >
              Next <Icon name='chevron-right' />
            </JumpLink>
            <JumpLink
              key='last'
              controller={controller}
              params={{ category }}
              query={{ ...defaultQuery, ...query, p: pageCount }}
            >
              Last <Icon name='chevron-double-right' />
            </JumpLink>
          </div>
        )
      }
    }

    return (
      <section className='mt8 mb6-t'>
        <p className='dark-slate-grey fs mt0 mb3'>Showing {lowerLimit}-{upperLimit} of {resultCount}</p>
        <div className='flex'>
          {links}
        </div>
      </section>
    )
  }

  render () {
    return this.renderContent()
  }
}

Pagination.propTypes = {
  resultCount: PropTypes.number.isRequired
}
