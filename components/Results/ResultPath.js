import React from 'react'
import PropTypes from 'prop-types'
import { _filter, _sortBy } from 'libraries/lodash'
import connect from 'helpers/connect'
import { $questions, $flattenedTopics, getParentTopicId } from 'services/TopicService'
import { $sectionIdToUrlMap, $topicQuestionIdToUrlMap } from 'services/SeoService'
import ResponseConsumer from 'components/ResponseConsumer'
import Link, { TOPICSCONTENT, TOPICOVERVIEW } from 'components/Link'

const ResultPath = ({ path }) => (
  <div className='fs coral mb2 lh-title'>
    {path.map(({ id, parentId, title }, index) => index === 0 ? (
      <span key={id} className='coral fxs no-underline'>{title}</span>
    ) : <ConnectedResultPathItem key={id} {...{ id, parentId, title, index, path }} />)}
  </div>
)

const ResultPathItem = ({ id, title, path, index, allTopics, questions, questionIdUrlMap, sectionIdUrlMap }) => {
  const getLinkParams = hasChildTopics => {
    if (hasChildTopics) {
      return { section, topicName }
    }
    const questionName = questionIdUrlMap[firstQuestion.tocId]
    return { section, topicName, questionName }
  }
  const topic = allTopics[id]
  const hasChildTopics = topic.children.length > 0
  const topicQuestions = _filter(questions, ({ topicId }) => topicId === id)
  const orderedTopicQuestions = _sortBy(topicQuestions, ({ questionNumber }) => Number(questionNumber))
  const firstQuestion = orderedTopicQuestions[0]
  const section = sectionIdUrlMap[path[0].id]
  const topicName = topic.depth === 2 ? sectionIdUrlMap[topic.id] : sectionIdUrlMap[getParentTopicId({ topics: allTopics, questions, topicId: id })]
  const controller = hasChildTopics ? TOPICOVERVIEW : TOPICSCONTENT
  const linkParams = getLinkParams(hasChildTopics)
  return (
    <>
      {index === 1 && ' / '}
      {(
        <Link
          className='coral fxs no-underline hover-underline'
          controller={controller}
          params={linkParams}
        >
          {title}
        </Link>
      )}
      {index !== path.length - 1 && ' / '}
    </>
  )
}

const ConnectedResultPathItem = connect(({ parentId }) => ({
  questions: $questions({ topicId: parentId }),
  allTopics: $flattenedTopics({ topicId: null }),
  sectionIdUrlMap: $sectionIdToUrlMap,
  questionIdUrlMap: $topicQuestionIdToUrlMap
}))(({ questions, allTopics, sectionIdUrlMap, questionIdUrlMap, ...props }) => {
  return (
    <ResponseConsumer
      responses={[questions, allTopics, sectionIdUrlMap, questionIdUrlMap]}
      content={() => <ResultPathItem {...props} {...{ questions, allTopics, sectionIdUrlMap, questionIdUrlMap }} />}
    />
  )
})

ResultPath.propTypes = {
  path: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
    })
  ).isRequired
}

export default ResultPath
