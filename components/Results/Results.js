import React from 'react'
import { _includes } from 'libraries/lodash'
import { TopicResultItem, DocumentResultItem, BundleResultItem, RestrictedResultsItem } from './ResultItem'

export const TopicResults = ({
  topics = { results: [] },
  mobile,
  pending,
  allTopics,
  searchPhrase,
  sectionIdUrlMap,
  questionIdUrlMap,
  noResultsMessage,
  noResultsMessageClassName
}) => {
  const { results = [] } = topics

  if (pending) {
    return Array.apply(null, Array(5)).map((_, index) => (
      <TopicResultItem key={index} pending />
    ))
  }

  return results.length ? results.map(result => result.restricted ? (
    <RestrictedResultsItem {...result} mobile={mobile} />
  ) : (
    <TopicResultItem
      key={`${result.topicId}/${result.tocId}`}
      {...{ ...result, allTopics }}
      path={result.path}
      mobile={mobile}
      sectionIdUrlMap={sectionIdUrlMap}
      questionIdUrlMap={questionIdUrlMap}
    />
  )) : <NoResultsMessage category='Guidance' searchPhrase={searchPhrase} message={noResultsMessage} className={noResultsMessageClassName} />
}

export const DocumentResults = ({
  mobile,
  pending,
  results,
  isSubscribed,
  searchPhrase,
  onClickSeeMore,
  onClickSeeLess,
  sectionIdUrlMap,
  documentIdUrlMap,
  showSeeMoreButton,
  showSeeLessButton,
  purchasedProductIds,
  noResultsMessage,
  noResultsMessageClassName
}) => {
  if (pending) {
    return Array.apply(null, Array(5)).map((_, index) => (
      <DocumentResultItem key={index} pending />
    ))
  }
  return results.length ? (
    <>
      {results.map(result => (
        <DocumentResultItem
          key={result.title}
          {...result}
          mobile={mobile}
          purchased={_includes(purchasedProductIds, result.id)}
          isSubscribed={isSubscribed}
          documentIdUrlMap={documentIdUrlMap}
          sectionIdUrlMap={sectionIdUrlMap}
        />
      ))}
      {showSeeMoreButton && (
        <SeeMoreButton onClick={onClickSeeMore}>see more</SeeMoreButton>
      )}
      {showSeeLessButton && (
        <SeeMoreButton onClick={onClickSeeLess}>see less</SeeMoreButton>
      )}
    </>
  ) : <NoResultsMessage category='Documents' searchPhrase={searchPhrase} message={noResultsMessage} className={noResultsMessageClassName} />
}

export const BundleResults = ({
  mobile,
  pending,
  results,
  isSubscribed,
  searchPhrase,
  onClickSeeMore,
  onClickSeeLess,
  bundleIdUrlMap,
  sectionIdUrlMap,
  showSeeMoreButton,
  showSeeLessButton,
  purchasedProductIds,
  noResultsMessage,
  noResultsMessageClassName
}) => {
  if (pending) {
    return Array.apply(null, Array(5)).map((_, index) => (
      <BundleResultItem key={index} pending />
    ))
  }

  return results.length ? (
    <>
      {results.map(result => (
        <BundleResultItem
          key={result.title}
          {...result}
          mobile={mobile}
          purchased={_includes(purchasedProductIds, result.id)}
          isSubscribed={isSubscribed}
          bundleIdUrlMap={bundleIdUrlMap}
          sectionIdUrlMap={sectionIdUrlMap}
        />
      ))}
      {showSeeMoreButton && (
        <SeeMoreButton onClick={onClickSeeMore}>see more</SeeMoreButton>
      )}
      {showSeeLessButton && (
        <SeeMoreButton onClick={onClickSeeLess}>see less</SeeMoreButton>
      )}
    </>
  ) : <NoResultsMessage category='Toolkits' searchPhrase={searchPhrase} message={noResultsMessage} className={noResultsMessageClassName} />
}

const NoResultsMessage = ({ category, searchPhrase = null, message = 'No results found', className }) => searchPhrase ? (
  <p className={className}>No results found for <span className='b'>"{searchPhrase}"</span> in {category}.</p>
) : (
  <p className={className}>{message}</p>
)

const SeeMoreButton = ({ onClick, children }) => (
  <button
    onClick={onClick}
    className='bg-transparent fm fxl-t fw5 pointer navy hover-navy-fade-80 hover-navy-t w-100-t bt-t b--navy pv0-t mt4 mt4-t'
  >
    {children}
  </button>
)
