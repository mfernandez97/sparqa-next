import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'

class ProgressBar extends Component {
  renderBars (numberOfBars) {
    const output = []
    const classNames = 'pb2 ba b--light-sea-green grow-1'

    for (let numberOfSolidBars = numberOfBars; numberOfSolidBars; numberOfSolidBars--) {
      output.push(
        <span
          key={numberOfSolidBars}
          className={cx(classNames, 'bg-light-sea-green', { 'br2 br--top br--left': numberOfSolidBars === numberOfBars })}
        />
      )
    }
    for (let numberOfTransparentBars = 3 - numberOfBars; numberOfTransparentBars; numberOfTransparentBars--) {
      output.push(
        <span
          className={cx(classNames, { 'br2 br--top br--right': numberOfTransparentBars === 1 })}
          key={numberOfBars + numberOfTransparentBars}
        />
      )
    }

    return output
  }

  render () {
    const numberOfBars = Math.floor(this.props.percentage / 33)

    return (
      <div className='flex br2 br--top absolute left0 top0 w-100'>
        {this.renderBars(numberOfBars)}
      </div>
    )
  }
}

ProgressBar.propTypes = {
  percentage: PropTypes.number
}

export default ProgressBar
