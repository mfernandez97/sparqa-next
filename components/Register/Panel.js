import React, { Component } from 'react'
import cx from 'classnames'
import { percentOff, formatCurrency } from 'helpers/misc'
import sessionState from 'helpers/sessionState'
import CostCircle from 'components/CostCircle'
import { FormBox } from 'components/Form'
import ProgressBar from './ProgressBar'

class RegisterPanel extends Component {
  get cost () {
    const { register: { selectedPlanCost }, discount } = this.props
    return formatCurrency(percentOff(selectedPlanCost, discount))
  }

  render () {
    const {
      title,
      subHeading,
      showSelectionCost,
      children,
      containerClass,
      boxClass,
      progress
    } = this.props
    return (
      <>
        <h1 className='white fxxl tracked tc mt8 mb6 mhauto max-w-measure-4'>{title}</h1>
        <div className={cx('mhauto mb12 mb6-m relative', containerClass)}>
          <FormBox title={subHeading} className={boxClass}>
            {progress && <ProgressBar percentage={progress} />}
            {showSelectionCost && <CostCircle cost={this.cost} />}
            {children}
          </FormBox>
        </div>
      </>
    )
  }
}
export default sessionState(
  ({ register: { selectedPlanCost } }) => ({ register: { selectedPlanCost } })
)(RegisterPanel)
