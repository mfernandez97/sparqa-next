import React from 'react'
import cx from 'classnames'
import Icon from 'components/Icon'

const CloseModalButton = ({ closeModal, top = '6', right = '6' }) => (
  <button className={cx('absolute flex align-center transition-all pointer pa2 br2 bg-transparent hover-bg-white-fade-20', `top${top}`, `right${right}`)} onClick={closeModal}>
    <Icon className='w-icon-5 h-icon-5 white' name='close' />
  </button>
)

export default CloseModalButton
