import React from 'react'
import { _isFunction } from 'libraries/lodash'
import cx from 'classnames'
import CloseButton from './CloseButton'

const ModalBox = ({ title, closeModal, children, className, fullWidth, titleStyle = {} }) => (
  <div className={cx(className, 'bg-white w-100-t br2 overflow-hidden', {
    'w-measure-3-d': !fullWidth,
    'w-100': fullWidth
  })}
  >
    <h2 style={titleStyle} className='ma0 bg-light-sea-green w-100 white tc fm b pa6 pr9-m relative'>
      {title}
      {_isFunction(closeModal) && <CloseButton closeModal={closeModal} />}
    </h2>
    {children}
  </div>
)

export default ModalBox
