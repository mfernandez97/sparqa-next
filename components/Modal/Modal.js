import React, { useRef } from 'react'
import { _isFunction } from 'libraries/lodash'
import cx from 'classnames'
import { getClassName } from 'helpers/misc'
import Portal from 'components/Portal'
import { RemoveScroll } from 'react-remove-scroll'

const Modal = ({ children, onMaskClick, useFlex = false, forceFullWidth = false, ...props }) => {
  const modalContent = useRef(null)
  const onClick = e => {
    const clickedInner = modalContent.current.contains(e.target)
    !clickedInner && onMaskClick()
  }
  const defaults = getDefaults()
  const className = getClassName(defaults, props)

  return (
    <RemoveScroll>
      <div
        onClick={_isFunction(onMaskClick) ? onClick : () => null}
        className={className}
      >
        <div
          className={cx(
            'h-100 mhauto layout-width-constraint w-100-t',
            {
              table: !useFlex,
              'flex justify-center items-center w-100-m': useFlex
            }
          )}
        >
          <div
            ref={modalContent}
            className={cx({
              'table-cell v-mid w-100': !useFlex,
              'w-100-m': useFlex,
              'w-100': forceFullWidth
            })}
          >
            {children}
          </div>
        </div>
      </div>
    </RemoveScroll>
  )
}

// TODO: investigate why Portal doesn't work with Carousel
const PortalModal = ({ usePortal, visible, ...props }) => {
  const Component = usePortal ? (
    <Portal root={document.getElementById('portal-destination')}>
      <Modal {...props} />
    </Portal>
  ) : <Modal {...props} />
  return visible ? Component : null
}

const getDefaults = () => ({
  bg: 'bg-dark-slate-grey-fade-70',
  top: 'top0',
  left: 'left0',
  right: 'right0',
  bottom: 'bottom0',
  zIndex: 'z120',
  padding: 'pv8 pv4-m ph4-m',
  position: 'absolute',
  overflow: 'overflow-y-auto'
})

export default PortalModal
