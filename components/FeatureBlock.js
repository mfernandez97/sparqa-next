import React from 'react'
import cx from 'classnames'
import { _isString } from 'libraries/lodash'

const FeatureBlock = ({
  assetUrl = null,
  title = null,
  subtitle = null,
  copy = null,
  button = null,
  copyLeft = true,
  bgColor = 'dark-grey',
  textColor = 'white',
  className = ''
}) => {
  const hasImage = !!assetUrl
  const contentClassName = cx('w-100-t flex grow-1 items-end', { 'w-40 tl-t': hasImage, 'max-w-measure-3 tc mhauto w-100': !hasImage })
  return (
    <div className={cx('w-100 relative br1', className)}>
      {assetUrl && (
        <div
          style={{ backgroundImage: `url(${assetUrl})` }}
          className={cx(
            'shadow-3-black-fade-20 br2 h-measure-2 h-measure-1-m w-45 w-80-t absolute z5 bg-white bg-contain bg-center bg-no-repeat flex flex-column-reverse left-50-t x--50-t',
            { left10: !copyLeft, right10: copyLeft }
          )}
        />
      )}
      <div className={`ba br2 w-100 pv15 mt8 relative flex flex-column z0 min-h-measure-2 ${textColor} bg-${bgColor}`}>
        <div className={cx('flex flex-column grow-1 justify-start ph10 pv8 items-center-t ph4-t pt0-t pb8-t mh10-t ph0-m mh9-m',
          {
            'pv8-t': !hasImage,
            'mt-measure-2-t mt-measure-0-m': hasImage,
            'items-end items-center-t': !copyLeft && hasImage
          })}
        >
          <h2 className={cx('mt0 mb0 mr0-t mt6-m fxxxl fw7', contentClassName)}>{title}</h2>
          {subtitle && <h3 className={cx('fw4 fxxl mt2 mb6 mt0 mr0-t', contentClassName)}>{subtitle}</h3>}
          {copy && (
            _isString(copy) ? <p className={cx(contentClassName)}>{copy}</p> : <div className={cx(contentClassName)}>{copy}</div>
          )}
          {button && <div className={contentClassName}>{button}</div>}
        </div>
      </div>
    </div>
  )
}

export default FeatureBlock
