import React from 'react'
import cx from 'classnames'
import { _isEmpty } from 'libraries/lodash'
import FloatingBox from 'components/FloatingBox'

const SupplementaryProductInfo = ({ qa, resources, resourceComponent: Resources }) => {
  const hasQa = !_isEmpty(qa)
  const hasResources = !_isEmpty(resources)

  return !hasQa && !hasResources ? null : (
    <section className='relative'>
      <div className={cx('layout-width-constraint flex flex-column-t pt8 pb8 pb0-t', {
        'justify-around': !hasQa || !hasResources,
        'justify-between': hasQa && hasResources
      })}
      >
        {hasQa && <ProductQa {...{ qa, hasResources }} />}
        {hasResources && <Resources {...{ hasQa, resources }} />}
      </div>
    </section>
  )
}

const ProductQa = ({ qa, hasResources }) => (
  <ul className={cx('flex flex-column ma0 pa0 w-100-t list-reset', { 'w-90': !hasResources, 'w-65': hasResources })}>
    {qa.map(({ question, answer }, index) => {
      const isFirst = index === 0
      const isLast = index === (qa.length - 1)
      return (
        <li key={index}>
          <FloatingBox
            expandable
            title={question}
            scrollingStateKey={question}
            className={cx('bg-white shadow-3-black-fade-30', { mb4: isFirst, 'mt4 mb6-t': isLast, mv5: !isFirst && !isLast })}
            titleContainerClassName='relative pl10 pr6'
            icon={
              <img
                src='assets/grid-icons/png/qna.png' alt='Q&A icon'
                className='w-icon-9 h-icon-9 mr6 absolute left6'
              />
            }
            seoDump
          >
            <div className='document-qa' dangerouslySetInnerHTML={{ __html: answer }} />
          </FloatingBox>
        </li>
      )
    })}
  </ul>
)

export default SupplementaryProductInfo
