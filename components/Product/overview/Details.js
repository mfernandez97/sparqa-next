import React from 'react'
import Description from './Description'
import ProductMedia from './ProductMedia'

const Details = props => {
  const { product: { images, title, videoUrl } } = props

  return (
    <div className='flex mv10 mv8-t'>
      <Description {...props} />
      <ProductMedia {...{ images, title, videoUrl }} />
    </div>
  )
}

export default Details
