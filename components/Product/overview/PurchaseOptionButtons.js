import React from 'react'
import cx from 'classnames'
import { _isEmpty } from 'libraries/lodash'
import { ACCOUNT, REGISTER } from 'constants/controllers'
import { SUBSCRIPTION } from 'constants/account-pages'
import { SELECT_PLAN } from 'constants/register-pages'
import { HOTDOCS } from 'constants/service-availability'
import DocumentButton from './DocumentButton'
import SubscribeButton from './SubscribeButton'

const PurchaseOptionButtons = props => {
  const path = location.pathname.substr(1)
  const { hasAccess, isLogged, product: { bulletPoints } } = props
  const hasBulletPoints = !_isEmpty(bulletPoints)
  const showSubscribe = !hasAccess

  const subscribeController = isLogged ? ACCOUNT : REGISTER
  const subscribeParams = isLogged ? { section: SUBSCRIPTION } : { step: SELECT_PLAN, path }

  return (
    <div className={cx('flex flex-column mb6-t', {
      'mt8 mt0-t': !hasBulletPoints,
      mr8: hasBulletPoints && !hasAccess
    })}
    >
      <div className='mb6'>
        {HOTDOCS ? (
          <DocumentButton {...{ ...props, hasBulletPoints }} />
        ) : (
          <div className='crimson max-w-measure-4'>
            Unfortunately our document service is currently unavailable, please try again later.
            We apologise for the inconvenience.
          </div>
        )}
      </div>
      {showSubscribe && (
        <SubscribeButton
          controller={subscribeController}
          params={subscribeParams}
        />
      )}
    </div>
  )
}

export default PurchaseOptionButtons
