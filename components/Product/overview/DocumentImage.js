import React, { useState } from 'react'
import { _get } from 'libraries/lodash'
import Route from 'route-parser'
import cx from 'classnames'
import { URL_DOCUMENT_IMAGE } from 'constants/urls'
import Icon from 'components/Icon'

const DocumentImage = props => {
  const [error, setError] = useState(false)

  const image = _get(props, ['image'], {
    url: false,
    portrait: true
  })
  const { url, portrait: isPortrait } = image

  const src = url && new Route(URL_DOCUMENT_IMAGE).reverse({
    url: url,
    dimensions: isPortrait ? '273x388' : '388x273'
  })

  const { handleImageClick } = props

  return (
    <div className='flex align-center justify-center'>
      <span className='relative pointer midnight-blue-2 hover-royal-blue-2' onClick={() => handleImageClick(image)}>
        {!url || error ? (
          <img className='shadow-2-grey-fade-50' src='/assets/document-placeholder.png' />
        ) : (
          <img
            {...{ src }}
            alt='Document Image'
            className={cx('shadow-2-grey-fade-50', {
              'landscape-document-image': !isPortrait
            })}
            onError={() => setError(true)}
          />
        )}
        <div className='absolute right4 bottom6 bg-grey-fade-20 br3 flex items-center justify-center pa2'>
          <Icon name='eye' className='w-icon-8' />
        </div>
      </span>
    </div>
  )
}

export default DocumentImage
