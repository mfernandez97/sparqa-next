import React from 'react'
import Title from './Title'
import Details from './Details'

const Overview = props => (
  <section className='pt8 pb8 pb0-t'>
    <div className='layout-width-constraint'>
      <Title title={props.product.title} />
      <Details {...props} />
    </div>
  </section>
)

export default Overview
