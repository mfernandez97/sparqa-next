import React from 'react'
import { DOCUMENTSMENU } from 'constants/controllers'
import Button, { themeModifiers } from 'components/Button'
import Icon from 'components/Icon'

const NotAvailable = ({ copy }) => (
  <div className='mt5 pa3 navy fxs fw6 tc tl-t'>
    <p className='fm mb4'>{copy}</p>
    <Button
      width='w-image-12'
      padding='pa6'
      size='medium'
      margin='mh0'
      link
      themeModifier={themeModifiers.SOLID}
      controller={DOCUMENTSMENU}
    >
      <Icon name='arrow-left' /><span className='maauto'>Back to documents</span>
    </Button>
  </div>
)

export default NotAvailable
