import React from 'react'
import connect from 'helpers/connect'
import { Media } from 'services/WindowService'
import BuyNowButton from './BuyNowButton'
import NotAvailable from './NotAvailable'
import NotAvailableForPurchase from './NotAvailableForPurchase'
import TrackedButton from './TrackedButton'

const DocumentButton = ({
  hasAccess, isBundle, onClickBuyNow, getFreeProduct, section, isFree, isLogged,
  product: { id, price, title, isStatic = false, availableFiles }, documentName,
  availableForPurchase, mobile, tablet
}) => {
  if (hasAccess && isBundle) {
    return null
  }
  if (!availableForPurchase) {
    return <NotAvailableForPurchase {...{ isBundle }} />
  }
  if ((mobile || tablet) && !isStatic) {
    return <NotAvailable copy='This feature is not currently available on mobile! Please login on desktop to continue.' />
  }
  if (!hasAccess) {
    return (
      <BuyNowButton {...{
        isStatic,
        isBundle,
        price,
        onClickBuyNow,
        getFreeProduct,
        isFree,
        isLogged
      }}
      />
    )
  }

  return (
    <TrackedButton {...{
      isLogged,
      isStatic,
      title,
      documentId: id,
      documentName,
      section,
      availableFiles,
      inModal: false
    }}
    />
  )
}

const ConnectedDocumentButton = connect(() => ({
  mobile: Media.Mobile,
  tablet: Media.Tablet
}))(props => <DocumentButton {...props} />)

export default ConnectedDocumentButton
