import React, { useState } from 'react'
import Route from 'route-parser'
import cx from 'classnames'
import { URL_DOCUMENT_IMAGE } from 'constants/urls'
import Modal from 'components/Modal'
import CloseButton from 'components/Modal/CloseButton'

const DocumentImageModal = ({ visible, closeModal, image }) => {
  const [error, setError] = useState(false)
  const { url, portrait: isPortrait } = Object.assign({
    url: false,
    portrait: true
  }, image)
  const src = url && new Route(URL_DOCUMENT_IMAGE).reverse({
    url: url,
    dimensions: isPortrait ? '546x776' : '776x546'
  })

  return (
    <Modal useFlex usePortal {...{ visible, onMaskClick: closeModal }}>
      <div className='relative bottom9 left6'>
        <CloseButton {...{ closeModal }} />
      </div>
      {!url || error ? (
        <img className='shadow-2-grey-fade-50 max-vh-100' src='/assets/document-placeholder.png' />
      ) : (
        <img
          {...{
            src,
            alt: 'Document Image',
            className: cx('shadow-2-grey-fade-50 max-vh-100'),
            onError: () => setError(true)
          }}
        />
      )}
    </Modal>
  )
}

export default DocumentImageModal
