import React from 'react'
import Button, { themes, themeModifiers } from 'components/Button'
import Icon from 'components/Icon'

const SubscribeButton = ({ controller, params }) => (
  <Button
    link
    controller={controller}
    params={params}
    size='medium'
    width='w-image-12'
    padding='pv6'
    margin='mh0'
    theme={themes.ROYAL_BLUE_3}
    themeModifier={themeModifiers.SOLID}
  >
    <span className='mr4 no-underline'>Unlimited Access - £10</span><Icon name='arrow-right' />
  </Button>
)

export default SubscribeButton
