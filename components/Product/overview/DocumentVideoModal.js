import React, { useState } from 'react'
import Vimeo from '@u-wave/react-vimeo'
import Modal from 'components/Modal'
import CloseButton from 'components/Modal/CloseButton'
import Error from 'components/StatusBox'
import { pushEvent } from 'services/AnalyticsService'

const DocumentVideoModal = ({ visible, closeModal, videoUrl }) => {
  const [error, setError] = useState(false)
  const [showCloseButton, setShowCloseButton] = useState(false)
  const handleCloseModal = () => {
    setShowCloseButton(false)
    closeModal()
  }
  const baseEventObject = {
    category: 'Vimeo',
    label: videoUrl
  }

  return (
    <Modal {...{ visible, onMaskClick: handleCloseModal }}>
      {showCloseButton && (
        <div className='relative bottom9 left6'>
          <CloseButton closeModal={handleCloseModal} />
        </div>
      )}
      {error ? (
        <Error title='Unable to load video' />
      ) : (
        <Vimeo
          width={700}
          className='flex'
          video={videoUrl}
          showTitle={false}
          onError={() => setError(true)}
          onReady={() => setShowCloseButton(true)}
          onPlay={({ seconds }) => pushEvent({ ...baseEventObject, action: seconds === 0 ? 'Started video' : 'Resumed video' })}
          onPause={({ seconds, duration }) => {
            // onPause also fires when a video ends so we're checking if a video has ended
            // before the 'Paused video' event is fired
            if (seconds < duration) {
              pushEvent({ ...baseEventObject, action: 'Paused video' })
            }
          }}
          onEnd={() => pushEvent({ ...baseEventObject, action: 'Completed video' })}
          onSeeked={() => pushEvent({ ...baseEventObject, action: 'Skipped video forward or backward' })}
        />
      )}
    </Modal>
  )
}

export default DocumentVideoModal
