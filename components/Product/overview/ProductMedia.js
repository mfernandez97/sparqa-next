import React, { useState } from 'react'
import Button, { themeModifiers } from 'components/Button'
import Icon from 'components/Icon'
import ImageCarousel from './ImageCarousel'
import DocumentImage from './DocumentImage'
import DocumentImageModal from './DocumentImageModal'
import DocumentVideoModal from './DocumentVideoModal'
import useSwiper from 'hooks/useSwiper'

const ProductMedia = ({ images, title, videoUrl }) => {
  const [imageModalVisible, setImageModalVisible] = useState(false)
  const [videoModalVisible, setVideoModalVisible] = useState(false)
  const swiper = useSwiper()
  const hasMultipleImages = images.length > 1

  return (
    <>
      <div className='w-50 tc dn-t document-blob'>
        {hasMultipleImages ? (
          <ImageCarousel
            {...{
              images,
              handleImageClick: setImageModalVisible,
              swiper
            }}
          />
        ) : (
          <DocumentImage image={images[0]} handleImageClick={setImageModalVisible} />
        )}
        {videoUrl && (
          <Button
            width='w-auto'
            margin='mt6 mb0 mhauto'
            themeModifier={themeModifiers.SOLID}
            onClick={() => setVideoModalVisible(true)}
          >
            Play Video
            <Icon name='play-circle-outline' className='ml2 h-icon-5 w-icon-5' />
          </Button>
        )}
      </div>
      <DocumentVideoModal
        visible={videoModalVisible}
        closeModal={() => setVideoModalVisible(false)}
        title={`${title} - Instructional video`}
        {...{ videoUrl }}
      />
      <DocumentImageModal
        image={hasMultipleImages ? images[swiper.currentIndex] : images[0]}
        visible={imageModalVisible}
        closeModal={() => setImageModalVisible(false)}
      />
    </>
  )
}

export default ProductMedia
