import React from 'react'
import cx from 'classnames'
import DownloadButton from './DownloadButtons'
import GetStartedButton from './GetStartedButton'

const TrackedButton = ({
  isStatic,
  title,
  documentId,
  documentName,
  section,
  availableFiles,
  inModal
}) => {
  const trackingData = {
    'data-track-click': true,
    'data-track-category': 'Document',
    ...(!isStatic && { 'data-track-action': 'Get Started' }),
    'data-track-label': title
  }
  const margin = cx({ mhauto: inModal, mh0: !inModal, mb6: isStatic })

  return isStatic ? (
    <DownloadButton
      {...{ trackingData, availableFiles, documentId, margin }}
    />
  ) : (
    <GetStartedButton
      params={{ documentName, section }}
      {...{ trackingData, margin }}
    />
  )
}

export default TrackedButton
