import React from 'react'

const Title = ({ title }) => (
  <div className='mb5'>
    <h1 className='f-graphic-50 lighter dark-slate-grey mv0'>{title}</h1>
  </div>
)

export default Title
