import React from 'react'
import cx from 'classnames'
import { _isEmpty } from 'libraries/lodash'
import PurchaseOptionButtons from './PurchaseOptionButtons'
import SaleSection from './SaleSection'
import Price from '../Price'

const Description = props => {
  const { isFree, hasAccess, product: { bulletPoints, description, price } } = props
  const hasBulletPoints = !_isEmpty(bulletPoints)
  return (
    <div className='w-50 w-100-t mr6'>
      <span className='fc-answer' dangerouslySetInnerHTML={{ __html: description }} />
      <div className='flex flex-wrap-m'>
        <div className='flex flex-column'>
          {!hasAccess && (
            <div>
              <Price {...{
                price,
                isFree,
                className: cx({
                  'pr8-t': isFree,
                  'mr8 pr2': !isFree
                })
              }}
              />
            </div>
          )}
          {hasBulletPoints && !hasAccess && <PurchaseOptionButtons {...props} />}
        </div>
        <SaleSection {...{ ...props, isFree }} />
      </div>
    </div>
  )
}

export default Description
