import React from 'react'
import { DOCUMENTINTERVIEW } from 'constants/controllers'
import Button, { themeModifiers } from 'components/Button'
import Icon from 'components/Icon'

const GetStartedButton = ({ params, onClick, trackingData, margin }) => (
  <Button
    link
    params={params}
    controller={DOCUMENTINTERVIEW}
    size='small'
    themeModifier={themeModifiers.SOLID}
    onClick={onClick}
    {...{ ...trackingData, margin }}
  >
    <span className='mr4 no-underline'>Get Started</span><Icon name='arrow-right' />
  </Button>
)

export default GetStartedButton
