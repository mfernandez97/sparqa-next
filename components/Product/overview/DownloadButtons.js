import React from 'react'
import getUrl from 'front-end-core/core/url'
import { DOCUMENTCONTENT } from 'constants/controllers'
import Button, { themeModifiers } from 'components/Button'
import Icon from 'components/Icon'

const DownloadButtons = ({ availableFiles, ...props }) => availableFiles.map(({ fileType, label }) => (
  <DownloadButton key={label} {...{ ...props, fileType, label }} />
))

const DownloadButton = ({ trackingData, margin, fileType, documentId, label }) => (
  <Button
    anchor
    size='medium'
    themeModifier={themeModifiers.SOLID}
    target='_blank'
    href={getUrl(DOCUMENTCONTENT, { documentId, fileType })}
    data-track-action={`Download Static [${fileType}]`}
    {...{ ...trackingData, margin }}
  >
    <span className='mr4'>Download as {label}</span><Icon name='arrow-down' />
  </Button>
)

export default DownloadButtons
