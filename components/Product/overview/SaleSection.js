import React from 'react'
import cx from 'classnames'
import { _isEmpty } from 'libraries/lodash'
import { formatCurrency } from 'helpers/misc'
import Icon from 'components/Icon'
import PurchaseOptionButtons from './PurchaseOptionButtons'

const SaleSection = props => {
  const { product: { bulletPoints, price, saving = 0 }, hasAccess, isBundle, isFree, isLogged } = props
  const isExpensive = price.toString().length > 4
  const hasBulletPoints = !_isEmpty(bulletPoints)
  return (
    <div className={cx('w-100', {
      'pt8 pt4-m': hasBulletPoints,
      'mvauto h-100': !hasBulletPoints,
      'ml9 ml6-m': !hasAccess && !hasBulletPoints,
      'ml10 ml0-t': (!hasAccess && isExpensive) || (!hasAccess && hasBulletPoints && isFree)
    })}
    >
      {isBundle && !hasAccess && saving > 0 && (
        <p className='coral fm fw6 mb6'>
          <span className='ba br-100 ph3 mr4'>£</span>
          Save £{formatCurrency(saving, true)} than when bought separately
        </p>
      )}
      {hasBulletPoints && (
        <ul className='list-reset ma0 pa0 basis-100 basis-auto-m'>
          {bulletPoints.map((item, index) => {
            const isFirst = index === 0
            return (
              <li className={cx('flex items-start', { 'mt0 mb6': isFirst, mv6: !isFirst })} key={index}>
                <span className='inline-flex ba b--light-sea-green br-100 pa1 mr3 v-mid shrink-0'>
                  <Icon className='h-icon-4 w-icon-4 light-sea-green' name='check' />
                </span>
                <span>{item}</span>
              </li>
            )
          })}
        </ul>
      )}
      {(!hasBulletPoints || hasAccess) && (
        <PurchaseOptionButtons
          isFree={isFree}
          isLogged={isLogged}
          hasBulletPoints={hasBulletPoints}
          {...props}
        />
      )}
    </div>
  )
}

export default SaleSection
