import React from 'react'
import { SUBSCRIPTION } from 'constants/account-pages'
import {
  ACCOUNT,
  REGISTER
} from 'constants/controllers'
import Link from 'components/Link'
import NotAvailable from './NotAvailable'

const NotAvailableForPurchase = ({ isLogged, isBundle }) => {
  const linkProps = {
    controller: isLogged ? ACCOUNT : REGISTER,
    params: isLogged && { section: SUBSCRIPTION }
  }
  return (
    <NotAvailable copy={
      <>
        This {isBundle ? 'toolkit' : 'document'} is currently only available as part of our
        <Link {...linkProps}>subscription plans</Link>.
      </>
    }
    />
  )
}

export default NotAvailableForPurchase
