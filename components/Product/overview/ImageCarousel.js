import React from 'react'
import DocumentImage from './DocumentImage'
import SwiperControls from 'pages/landing/swiper/SwiperControls'
import SwiperWrapper from 'pages/landing/swiper/SwiperWrapper'

const ImageCarousel = ({ images, handleImageClick, swiper }) => {
  const { swiperRef, isEnd, isBeginning, onSlideChange, realIndexChange } = swiper
  const pagination = {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable: true
  }
  const breakpoints = {
    0: {
      slidesPerView: 1,
      spaceBetween: 10
    }
  }

  return (
    <div className='relative image-carousel-pagination'>
      <SwiperControls
        {...{ swiperRef, isEnd, isBeginning }}
        paddingCx=''
        buttonClassName='black'
      />
      <SwiperWrapper {...{ swiperRef, breakpoints, onSlideChange, realIndexChange, pagination}} >
        {images.map((image, index) => (
          <div key={index}>
            <DocumentImage {...{ image, handleImageClick }} />
          </div>
        ))}
      </SwiperWrapper>
    </div>
  )
}

export default ImageCarousel
