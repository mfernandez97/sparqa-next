import React from 'react'
import Button, { themeModifiers } from 'components/Button'
import Icon from 'components/Icon'

const BuyNowButton = ({ isLogged, isStatic, onClickBuyNow, getFreeProduct, isFree }) => {
  const onClick = (isLogged && isFree) ? getFreeProduct : onClickBuyNow
  return (
    <Button
      {...{ onClick }}
      width='w-image-12'
      padding='pv6'
      size='medium'
      margin='mh0'
      themeModifier={themeModifiers.SOLID}
    >
      <span className='mr4'>{!isFree ? 'Buy Now' : isStatic ? 'Download' : 'Get Started'}</span>
      <Icon
        name={!isFree || !isStatic ? 'arrow-right' : 'arrow-down'}
      />
    </Button>
  )
}

export default BuyNowButton
