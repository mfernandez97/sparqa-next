import React from 'react'
// this is quite a nice pattern for a mock, might think about creating a helper for it
const mockedPropAccessor = {}

export default jest.fn().mockImplementation((props) => {
  Object.assign(mockedPropAccessor, props)
  return <div>CONFIRM PURCHASE MODAL</div>
})

export { mockedPropAccessor }
