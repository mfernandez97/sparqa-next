import React, { createContext } from 'react'

/**
 * To use this mock for consumers, you will need to implement the MockedProvider behaviour
 * and then mount it above the test component
 *
 * CONTEXT STATE
 *
 * registerModalVisible: bool
 * confirmPurchaseModalVisible: bool
 * purchaseSuccessModalVisible: bool
 * registerModalVisited: bool
 * registerVisited: bool
 * billingVisited: bool
 * setRegisterModalTab: (registerModalTab: TABS) => undefined
 * setRegisterModalVisible: (registerModalVisible: bool) => undefined
 * setConfirmPurchaseModalVisible: (confirmPurchaseModalVisible: bool) => undefined
 * setPurchaseSuccessModalVisible: (purchaseSuccessModalVisible: bool) => undefined
 */

/**
 * These are _not_ going to abide by the above criteria to make sure that the correct value is being
 * passed along (by making each non fn value unique)
 * funcitons can remain basic as we can ask jest if that specific function was accessed
 */

const defaultState = {
  registerModalVisible: 'registerModalVisible',
  confirmPurchaseModalVisible: 'confirmPurchaseModalVisible',
  purchaseSuccessModalVisible: 'purchaseSuccessModalVisible',
  registerModalVisited: 'registerModalVisited',
  registerVisited: 'registerVisited',
  billingVisited: 'billingVisited',
  setRegisterModalTab: jest.fn(),
  setRegisterModalVisible: jest.fn(),
  setConfirmPurchaseModalVisible: jest.fn(),
  setPurchaseSuccessModalVisible: jest.fn()
}

const MockedContext = createContext(defaultState)

const { Provider: MockedProvider, Consumer: ModalConsumer } = MockedContext

const ModalProvider = jest.fn().mockImplementation(({ children }) => (
  <div>
    {children}
  </div>
))

export {
  MockedContext as default,
  ModalProvider,
  ModalConsumer,
  MockedProvider,
  defaultState
}
