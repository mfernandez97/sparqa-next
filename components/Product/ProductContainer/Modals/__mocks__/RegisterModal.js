import React from 'react'

const TABS = { BILLING: 'billing', ACCOUNT: 'account', LOADING: 'loading' }
const mockedPropAccessor = {}

export default jest.fn().mockImplementation((props) => {
  Object.assign(mockedPropAccessor, props)
  return <div>REGISTER MODAL</div>
})

export { mockedPropAccessor, TABS }
