import React, { Component, createContext } from 'react'
import { TABS as REGISTER_MODAL_TABS, TABS } from './RegisterModal'
import { onCheckout } from 'services/AnalyticsService'

const defaults = {
  registerModalVisible: false,
  confirmPurchaseModalVisible: false,
  purchaseSuccessModalVisible: false,
  registerModalVisited: false,
  registerVisited: false,
  billingVisited: false
}

const ModalContext = createContext(defaults)
const { Provider, Consumer: ModalConsumer } = ModalContext

class ModalProvider extends Component {
  constructor (props) {
    super(props)
    this.state = {
      ...defaults,
      registerModalTab: props.isLogged ? TABS.BILLING : TABS.ACCOUNT
    }
  }

  setRegisterModalTab = registerModalTab => {
    this.setState({ registerModalTab }, () => {
      if (this.state.registerModalVisible) {
        const {
          registerModalTab,
          registerVisited,
          billingVisited
        } = this.state

        if (!registerVisited && registerModalTab === REGISTER_MODAL_TABS.ACCOUNT) {
          onCheckout({ ...this.props.productData, step: 2 })
          this.setState({ registerVisited: true })
        }

        if (!billingVisited && registerModalTab === REGISTER_MODAL_TABS.BILLING) {
          onCheckout({ ...this.props.productData, step: 3 })
          this.setState({ billingVisited: true })
        }
      }
    })
  }

  setRegisterModalVisible = registerModalVisible => {
    registerModalVisible && this.setState({ registerModalVisited: true })
    this.setState({ registerModalVisible })
  }

  setConfirmPurchaseModalVisible = confirmPurchaseModalVisible => this.setState({ confirmPurchaseModalVisible })
  setPurchaseSuccessModalVisible = purchaseSuccessModalVisible => this.setState({ purchaseSuccessModalVisible })

  render () {
    const {
      setRegisterModalTab,
      setRegisterModalVisible,
      setConfirmPurchaseModalVisible,
      setPurchaseSuccessModalVisible
    } = this
    return (
      <Provider
        value={{
          ...this.state,
          setRegisterModalTab,
          setRegisterModalVisible,
          setConfirmPurchaseModalVisible,
          setPurchaseSuccessModalVisible
        }}
      >
        {this.props.children}
      </Provider>
    )
  }
}

export { ModalContext as default, ModalProvider, ModalConsumer }
