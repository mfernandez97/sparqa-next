import React from 'react'
import Modal, { ModalBox } from 'components/Modal'
import Form, { ErrorMessages, FormSubmit, FormConsumer } from 'components/Form'
import DiscountCodeContainer from 'components/Account/DiscountCodeContainer'
import DiscountCodeField from 'components/Account/DiscountCodeField'
import PriceDisplay from 'components/PriceDisplay'

const ConfirmPurchaseModal = ({
  visible,
  discount = 0,
  onSubmit,
  closeModal,
  documentName,
  documentPrice,
  responseError,
  appliedDiscountCode
}) => {
  const handleSubmit = onSubmit
  return (
    <Modal visible={visible} onMaskClick={closeModal}>
      <ModalBox title='Confirm Purchase' closeModal={closeModal}>
        <div className='tc pa8'>
          {documentPrice && <PriceDisplay price={documentPrice} discount={discount} pill margin='mb6' />}
          <p className='fm mid-grey mt0 mb6'>Select confirm to purchase <span className='b'>{documentName}</span>.</p>
          <Form initialValues={{ discountCode: '' }} onSubmit={handleSubmit}>
            <FormConsumer>
              {({ actions }) => (
                <DiscountCodeField
                  margin='mh0 mb8'
                  className='tl'
                  setDiscountCode={actions.setValue('discountCode')}
                  appliedDiscountCode={appliedDiscountCode}
                />
              )}
            </FormConsumer>
            <FormSubmit>Confirm</FormSubmit>
            {responseError && <ErrorMessages className='mt4' messages={[responseError]} />}
          </Form>
        </div>
      </ModalBox>
    </Modal>
  )
}
const ConnectedConfirmPurchaseModal = props => {
  const handleSubmit = props.confirmPurchase
  return (
    <DiscountCodeContainer {...props} onSubmit={handleSubmit}>
      <ConfirmPurchaseModal />
    </DiscountCodeContainer>
  )
}

export default ConnectedConfirmPurchaseModal
