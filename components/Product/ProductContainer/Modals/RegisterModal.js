import React, { useContext, useState } from 'react'
import { _startsWith, _isNumber } from 'libraries/lodash'
import { getBaseUrl } from 'front-end-core/core/history'
import { isValidResponse } from 'core/api/dataflow'
import { STRIPE } from 'constants/service-availability'
import { formatCurrency } from 'helpers/misc'
import { registerFreemiumUser, updateUserBilling } from 'services/PaymentsService'
import { onCheckoutOption } from 'services/AnalyticsService'
import { LoginContext } from 'components/LoginContext'
import Icon from 'components/Icon'
import Modal, { ModalBox } from 'components/Modal'
import CostCircle from 'components/CostCircle'
import AccountForm from 'components/Account/FreemiumAccountForm'
import StripeDown from 'components/Stripe/ServiceDown'
import { FormBox } from 'components/Form'
import Button, { themes, themeModifiers } from 'components/Button'
import BillingTab from './RegisterModalTabs/Billing'
import StripeWrapper from 'components/Stripe/wrap'
import { CardElement } from '@stripe/react-stripe-js'

export const TABS = { BILLING: 'billing', ACCOUNT: 'account', LOADING: 'loading' }

const RegisterModal = ({
  price,
  visible,
  isLogged,
  closeModal,
  currentTab,
  setCurrentTab,
  isStatic,
  paymentError,
  productType,
  isBundle,
  getFreeProduct,
  setRegisterModalVisible,
  onSubmitBilling,
  stripe,
  elements
}) => {
  const { handleSubmit } = useContext(LoginContext)
  const [accountData, setAccountData] = useState(null)
  const [responseError, setResponseError] = useState(null)

  const goToNext = () => {
    onCheckoutOption(price === 0 ? 'register' : 'purchase')
    setCurrentTab(isLogged ? TABS.BILLING : TABS.ACCOUNT)
  }

  const handleSubmitAccountForm = (e, { values, onFinishSubmit }) => {
    setAccountData(values)
    registerFreemiumUser(values)
      .then(() => handleSubmit({ username: values.email, password: values.password }))
      .then(response => {
        const isValid = isValidResponse(response)
        if (isValid) {
          setResponseError(null)
          if (price === 0) {
            getFreeProduct(false)
          } else {
            setCurrentTab(TABS.BILLING)
            setRegisterModalVisible(true)
          }
        } else {
          throw new Error(response)
        }
      })
      .catch(error => {
        console.error(error)
        setResponseError('There was a problem signing you up. Please try again later.')
      })
  }

  const handleSubmitBillingForm = (e, { values, onFinishSubmit }) => {
    if (!isLogged && !accountData) {
      setResponseError('')
      return onFinishSubmit()
    }

    if (!elements) {
      return
    }

    setResponseError(null)

    const billing_details = {
      name: values.billingName,
      address: {
        line1: values.addressLine1,
        line2: values.addressLine2,
        country: values.country,
        postal_code: values.postCode
      }
    }

    const card = elements.getElement(CardElement)

    stripe.createPaymentMethod({
      billing_details,
      card,
      type: 'card',
    }).then(({ paymentMethod, error }) => {
      if (error) {
        throw new Error('Error', error.message)
      }
      return updateUserBilling(paymentMethod.id)
    })
    .then(() => {
      onSubmitBilling(e, { values, onFinishSubmit })
    })
  }

  const handleResponseError = onFinishSubmit => ({ message }) => {
    setResponseError(message)
    onFinishSubmit()
  }

  const free = price === 0
  const titleComponent = (
    <Title
      {...{
        isLogged,
        currentTab,
        closeModal,
        free,
        isStatic,
        paymentError,
        productType,
        isBundle
      }}
    />
  )

  return (
    <Modal visible={visible}>
      <ModalBox className='relative' title={titleComponent} closeModal={closeModal}>
        <Content
          {...{
            price,
            free,
            isStatic,
            isLogged,
            currentTab,
            closeModal,
            accountData,
            paymentError,
            responseError,
            productType,
            isBundle,
            goToNext
          }}
          goToTab={tab => setCurrentTab(tab)}
          onSubmitAccountForm={handleSubmitAccountForm}
          onSubmitBillingForm={handleSubmitBillingForm}
        />
      </ModalBox>
    </Modal>
  )
}

const Title = ({ currentTab, ...props }) => {
  switch (currentTab) {
    case TABS.BILLING:
      return <BillingFormTitle {...props} />
    case TABS.ACCOUNT:
      return <AccountFormTitle {...props} />
    case TABS.LOADING:
      return <LoadingTitle paymentError={props.paymentError} />
    default:
      return null
  }
}

const Content = ({
  goToTab,
  currentTab,
  paymentError,
  onSubmitAccountForm,
  onSubmitBillingForm,
  ...props
}) => {
  switch (currentTab) {
    case TABS.BILLING:
      return STRIPE ? (
        <BillingTab onSubmit={onSubmitBillingForm} {...props} />
      ) : <StripeDown />
    case TABS.ACCOUNT:
      return <AccountFormContent onSubmit={onSubmitAccountForm} {...props} />
    case TABS.LOADING:
      return <LoadingContent paymentError={paymentError} goToBilling={() => goToTab(TABS.BILLING)} />
    default:
      return null
  }
}

const LoadingTitle = ({ paymentError }) => paymentError ? 'Error' : 'Processing...'

const BillingFormTitle = ({ productType }) => (
  <>
    Enter details to purchase {productType}
  </>
)

const AccountFormTitle = ({ free, isStatic, isBundle }) => (
  <>
    Enter details to {isBundle
    ? 'purchase toolkit'
    : !free ? 'purchase' : isStatic ? 'download' : 'start'}
  </>
)

const LoadingContent = ({ paymentError, goToBilling }) => (
  <div className='pa8 tc'>
    {paymentError ? (
      <>
        <p className='crimson mb8'>{paymentError}</p>
        <Button onClick={goToBilling} theme={themes.ROYAL_BLUE_3} themeModifier={themeModifiers.INVERT_BORDER_COLOR}>
          <Icon name='arrow-left' className='mr2' />
          Back to Billing Details
        </Button>
      </>
    ) : (
      <Icon name='spin2' className='spin-svg w-icon-10 grey' />
    )}
  </div>
)

const AccountFormContent = ({ onSubmit, buttonText, accountData, price }) => (
  <FormBox title='Your details'>
    {_isNumber(price) && <CostCircle cost={formatCurrency(price, true)} />}
    <AccountForm
      onSubmit={onSubmit}
      buttonText='Continue'
      initialValues={accountData || {}}
    />
  </FormBox>
)

const WrappedRegisterModal = props => {
  const path = _startsWith(location.href, getBaseUrl())
    ? location.href.substr(getBaseUrl().length)
    : location.pathname

  return (
    <StripeWrapper>
      <RegisterModal {...{ ...props, path }} />
    </StripeWrapper>
  )
  // return stripeWrap(RegisterModal, { ...props, path })
}

export default WrappedRegisterModal
