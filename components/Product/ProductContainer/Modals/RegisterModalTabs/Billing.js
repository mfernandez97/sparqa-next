import React from 'react'
import DiscountCodeField from 'components/Account/DiscountCodeField'
import DiscountCodeContainer from 'components/Account/DiscountCodeContainer'
import BillingForm from 'components/Account/BillingForm'
import { FormBox, FormConsumer } from 'components/Form'
import PriceDisplay from 'components/PriceDisplay'

const BillingFormContent = ({
  price,
  onSubmit,
  discount,
  responseError,
  appliedDiscountCode = ''
}) => {
  return (
    <FormBox title='Billing details'>
      {price && (
        <PriceDisplay
          pill
          price={price}
          discount={discount}
          bg='bg-steel-blue-lighten-25'
          className='absolute top8 right8 y--50'
          priceTextProps={{ fontSize: 'fxl' }}
        />
      )}
      <BillingForm
        onSubmit={onSubmit}
        responseError={responseError}
        initialValues={{ discountCode: '' }}
      >
        <div className='bt b--light-cyan pv8'>
          <FormConsumer>
            {({ actions }) => (
              <DiscountCodeField
                margin='mh0'
                setDiscountCode={actions.setValue('discountCode')}
                appliedDiscountCode={appliedDiscountCode}
              />
            )}
          </FormConsumer>
        </div>
      </BillingForm>
    </FormBox>
  )
}

const ConnectedBillingTab = props => (
  <DiscountCodeContainer {...props}>
    <BillingFormContent />
  </DiscountCodeContainer>
)

export default ConnectedBillingTab
