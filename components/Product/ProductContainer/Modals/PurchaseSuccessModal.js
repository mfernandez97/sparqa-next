import React from 'react'
import { _get } from 'libraries/lodash'
import { DOCUMENTSMENU } from 'constants/controllers'
import { getDocumentIcon } from 'services/DocumentService'
import Modal, { ModalBox } from 'components/Modal'
import Button, { themes, themeModifiers } from 'components/Button'
import TrackedButton from 'components/Product/overview/TrackedButton'

const PurchaseSuccessModal = props => {
  const { isBundle, visible, closeModal, title } = props
  return (
    <Modal visible={visible} onMaskClick={closeModal}>
      <ModalBox title='Success!' closeModal={closeModal}>
        <div className='tc pa8'>
          {isBundle
            ? <p className='fm b mv6 mt0'>You have successfully purchased {title}. </p>
            : <DocumentContent {...props} />}
        </div>
      </ModalBox>
    </Modal>
  )
}

const DocumentContent = ({
  price,
  title,
  documentId,
  documentType,
  documentName,
  isStatic,
  section,
  availableFiles
}) => {
  const isFree = price === 0
  return (
    <>
      <img
        className='db mhauto w-icon-12 h-icon-12 mb6'
        src={getDocumentIcon(_get(documentType, 'name'), { alt: true })} alt={`${_get(documentType, 'name')} icon`}
      />
      <p className='fm b mv6 mt0'>
        You {isFree ? 'now have access to' : 'have successfully purchased'} {title}.
      </p>
      <TrackedButton {...{
        isStatic,
        documentId,
        documentName,
        section,
        title,
        availableFiles,
        inModal: true
      }}
      />
      <Button
        link
        size='medium'
        controller={DOCUMENTSMENU}
        theme={themes.ROYAL_BLUE_3}
        themeModifier={themeModifiers.INVERT_BORDER_COLOR}
        margin='mt6 mb0 mhauto'
      >
        Save For Later
      </Button>
    </>
  )
}

export default PurchaseSuccessModal
