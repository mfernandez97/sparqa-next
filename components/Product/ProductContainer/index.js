import React from 'react'
import { ModalProvider } from './Modals/ModalContext'
import ProductContainerResponseWrapper from './ProductContainerResponseWrapper'

const WrappedProductContainer = props => (
  <ModalProvider productData={props.productData} isLogged={props.isLogged}>
    <ProductContainerResponseWrapper {...props} />
  </ModalProvider>
)

export default WrappedProductContainer
