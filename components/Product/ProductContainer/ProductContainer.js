import React, { Component } from 'react'
import { filter } from 'rxjs/operators'
import { _pick, _isEmpty } from 'libraries/lodash'
import { USER_BILLING, DOCUMENT_INTERVIEW, CREDIT } from 'constants/requests'
import { stripTags, formatCurrency, percentOff } from 'helpers/misc'
import { refresh } from 'services/ApiService'
import { $refreshPurchasedDocument } from 'services/DocumentService'
import { onClickCallToAction, onCheckout, onCheckoutComplete, onCheckoutOption } from 'services/AnalyticsService'
import Metadata from 'components/Metadata'
import { htmlText } from 'helpers/htmlText'
import ModalContext from './Modals/ModalContext'
import { TABS as REGISTER_MODAL_TABS } from './Modals/RegisterModal'
import Overview from '../overview/index'
import SupplementaryProductInfo from '../SupplementaryProductInfo'
import RelatedBundles from '../RelatedBundles'
import Modals from './ProductContainerModalWrapper'

class Main extends Component {
  static contextType = ModalContext

  state = {
    responseError: null
  }

  subscriptions = []

  pageDescription = stripTags(this.props.product.description).split('\n')[0]

  faqs = this.props.qa.map(({ answer, question }) => ({
    question,
    answer: htmlText(answer)
  }))

  get modalProps () {
    const {
      isLogged,
      hasAccess,
      section,
      productType,
      product,
      availableForPurchase,
      isBundle
    } = this.props
    const modalProps = Object.assign({},
      product,
      { isLogged, isBundle, hasAccess, section, availableForPurchase, productType },
      _pick(this.props, `${productType}Name`),
      this.state,
      _pick(this, [
        'purchaseProduct',
        'getFreeProduct',
        'handleSubmitBilling'
      ]))
    return modalProps
  }

  componentWillUnmount () {
    this.subscriptions.forEach(subscription => subscription.unsubscribe())
  }

  handleClickBuyNow = () => {
    const { isLogged, hasBillingDetails, product: { price }, credit: { remaining } } = this.props
    const { setRegisterModalTab, setRegisterModalVisible, setConfirmPurchaseModalVisible } = this.context
    const updatedPrice = remaining >= price ? 0 : price - remaining

    if (!this.state.registerModalVisited) {
      onClickCallToAction(this.props.productData)
      onCheckout({ ...this.props.productData, step: 1 })
    }
    if (!isLogged) {
      setRegisterModalTab(REGISTER_MODAL_TABS.ACCOUNT)
      return setRegisterModalVisible(true)
    }
    if (hasBillingDetails || updatedPrice <= 0) {
      return setConfirmPurchaseModalVisible(true)
    }
    return setRegisterModalVisible(true)
  }

  handleSubmitBilling = (e, { values, onFinishSubmit }) => {
    this.refreshBilling(() => this.purchaseProduct(e, { values, onFinishSubmit }))
  }

  getFreeProduct = (trackClick = true) => {
    const { id, name, variant } = this.props.productData
    trackClick && onClickCallToAction({
      id,
      name,
      price: 0,
      category: 'freemium',
      variant
    })
    this.setResponseError(null)
    this.context.setRegisterModalTab(REGISTER_MODAL_TABS.LOADING)
    const subscription = this.props.getFreeProduct().subscribe(response => {
      this.refreshPurchasedDocuments()
      if (response.status === 'OK') {
        this.context.setRegisterModalVisible(false)
        this.context.setPurchaseSuccessModalVisible(true)
      } else {
        this.setResponseError('Unable to grant access to document, please try again later')
      }
    })
    this.subscriptions.push(subscription)
  }

  purchaseProduct = (e, { values = {}, onFinishSubmit = () => null }) => {
    this.setResponseError(null)
    this.context.setRegisterModalTab(REGISTER_MODAL_TABS.LOADING)

    const { registerVisited, billingVisited } = this.context
    if (!registerVisited && !billingVisited) {
      onCheckoutOption('purchase')
    }

    const subscription = this.props.purchaseProduct({
      discountCode: values.discountCode,
      stripe: this.props.stripe
    }).subscribe(response => {
      if (response.error) {
        onFinishSubmit()
        return this.setResponseError(response.message)
      }

      const {
        partner,
        product: { price },
        productData: { id: productId, name, category, variant }
      } = this.props
      onCheckoutComplete({
        partner: partner,
        transactionId: response.transactionId || response.uuid,
        productId,
        name,
        price: formatCurrency(percentOff(price, values.discount || 0), true),
        category,
        variant,
        discountCode: values.appliedDiscountCode
      })

      const {
        setRegisterModalVisible,
        setConfirmPurchaseModalVisible,
        setPurchaseSuccessModalVisible
      } = this.context
      Promise.all([
        this.refreshPurchasedDocuments(),
        this.refreshCredit(),
        this.refreshDocumentInterview()
      ])
        .then(() => {
          setRegisterModalVisible(false)
          setConfirmPurchaseModalVisible(false)
          setPurchaseSuccessModalVisible(true)
        })
    })
    this.subscriptions.push(subscription)
  }

  setResponseError = responseError => this.setState({ responseError })

  refreshBilling = (callback = () => null) => {
    const subscription = refresh(USER_BILLING).subscribe(callback)
    this.subscriptions.push(subscription)
  }

  refreshDocumentInterview = () => {
    const { productId } = this.props
    return refresh(DOCUMENT_INTERVIEW, { documentId: productId }).toPromise()
  }

  refreshPurchasedDocuments = () => {
    const { productId } = this.props
    return $refreshPurchasedDocument({ uuid: productId })
      .pipe(filter(response => response && !isNaN(response)))
      .toPromise()
  }

  refreshCredit = () => {
    return refresh(CREDIT).toPromise()
  }

  render () {
    const { product: { title }, relatedBundles, user: { isSubscribed } } = this.props

    return (
      <div className='product-overview-spacing'>
        <Metadata
          pageTitle={`Sparqa Legal | ${title}`}
          pageDescription={this.pageDescription}
          faqs={this.faqs}
        />
        <Overview
          {...{
            ...this.props,
            onClickBuyNow: this.handleClickBuyNow,
            getFreeProduct: this.getFreeProduct
          }}
        />
        <SupplementaryProductInfo {...this.props} />
        {!_isEmpty(relatedBundles) && (
          <RelatedBundles
            bundles={relatedBundles}
            isSubscribed={isSubscribed}
          />
        )}
        <Modals {...this.modalProps} />
      </div>
    )
  }
}

export default Main
