import React, { useContext } from 'react'
import ModalContext from './Modals/ModalContext'
import RegisterModal, { TABS as REGISTER_MODAL_TABS } from './Modals/RegisterModal'
import ConfirmPurchaseModal from './Modals/ConfirmPurchaseModal'
import PurchaseSuccessModal from './Modals/PurchaseSuccessModal'

const Modals = ({
  id,
  title,
  documentName,
  documentType,
  isStatic,
  section,
  price,
  isLogged,
  hasAccess,
  responseError,
  purchaseProduct,
  getFreeProduct,
  handleSubmitBilling,
  isBundle,
  productType,
  availableFiles
}) => {
  const {
    registerModalTab,
    registerModalVisible,
    confirmPurchaseModalVisible,
    purchaseSuccessModalVisible,
    setRegisterModalTab,
    setRegisterModalVisible,
    setConfirmPurchaseModalVisible,
    setPurchaseSuccessModalVisible
  } = useContext(ModalContext)
  return (
    <>
      <RegisterModal
        {...{
          price,
          isStatic,
          isLogged,
          hasAccess,
          purchaseProduct,
          getFreeProduct,
          setRegisterModalVisible,
          visible: registerModalVisible,
          currentTab: registerModalTab,
          paymentError: responseError,
          onSubmitBilling: handleSubmitBilling,
          isBundle,
          productType
        }}
        setCurrentTab={setRegisterModalTab}
        openConfirmPurchaseModal={() => setConfirmPurchaseModalVisible(true)}
        closeModal={() => {
          setRegisterModalVisible(false)
          setRegisterModalTab(isLogged ? REGISTER_MODAL_TABS.BILLING : REGISTER_MODAL_TABS.ACCOUNT)
        }}
      />
      <ConfirmPurchaseModal
        {...{
          price,
          title,
          responseError,
          visible: confirmPurchaseModalVisible,
          confirmPurchase: purchaseProduct
        }}
        closeModal={() => setConfirmPurchaseModalVisible(false)}
      />
      <PurchaseSuccessModal
        {...{
          documentName,
          documentType,
          section,
          price,
          isStatic,
          title,
          visible: purchaseSuccessModalVisible,
          documentId: id,
          isBundle,
          availableFiles
        }}
        closeModal={() => setPurchaseSuccessModalVisible(false)}
      />
    </>
  )
}

export default Modals
