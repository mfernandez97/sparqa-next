import { mount } from 'enzyme'
import { MockedProvider, defaultState } from './Modals/ModalContext'
import ProductContainerModalWrapper from './ProductContainerModalWrapper'
import ConfirmModal, { mockedPropAccessor as ConfirmProps } from './Modals/ConfirmPurchaseModal'
import PurchaseModal, { mockedPropAccessor as SuccessProps } from './Modals/PurchaseSuccessModal'
import RegisterModal, { mockedPropAccessor as RegisterProps, TABS } from './Modals/RegisterModal'

jest.mock('./Modals/ModalContext')
jest.mock('./Modals/ConfirmPurchaseModal')
jest.mock('./Modals/PurchaseSuccessModal')
jest.mock('./Modals/RegisterModal')

/**
 * PROPS:
 *
 * id,
 * title,
 * documentName,
 * documentType,
 * isStatic,
 * section,
 * price,
 * isLogged,
 * hasAccess,
 * responseError,
 * purchaseProduct,
 * getFreeProduct,
 * handleSubmitBilling,
 * isBundle,
 * productType
 */

describe('ProductContainerModalWrapper Component', () => {
  let Container
  const props = {
    id: 'id-mock',
    title: 'title-mock',
    documentName: 'documentName-mock',
    documentType: 'documentType-mock',
    isStatic: 'isStatic-mock',
    section: 'section-mock',
    price: 'price-mock',
    isLogged: 'isLogged-mock',
    hasAccess: 'hasAccess-mock',
    responseError: 'responseError-mock',
    purchaseProduct: 'purchaseProduct-mock',
    getFreeProduct: 'getFreeProduct-mock',
    handleSubmitBilling: 'handleSubmitBilling-mock',
    isBundle: 'isBundle-mock',
    productType: 'productType-mock'
  }

  describe('with the default values', () => {
    beforeAll(() => {
      const wrappingComponent = ({ children }) => (
        <MockedProvider value={defaultState}>
          {children}
        </MockedProvider>
      )
      Container = mount(<ProductContainerModalWrapper {...props} />, { wrappingComponent })
    })

    afterAll(() => {
      Container.unmount()
    })

    describe('should render the RegisterModal with expected values', () => {
      it('renders the Register Modal', () => {
        expect(RegisterModal).toHaveBeenCalled()
      })
      // Below we see the best example ever of why you should write the tests first
      // Am I testing the implementation or the requirement? WHO KNOWS
      it('passes through the required props from the input props', () => {
        // rename not required
        expect(RegisterProps.isStatic).toEqual(props.isStatic)
        expect(RegisterProps.price).toEqual(props.price)
        expect(RegisterProps.isLogged).toEqual(props.isLogged)
        expect(RegisterProps.hasAccess).toEqual(props.hasAccess)
        expect(RegisterProps.purchaseProduct).toEqual(props.purchaseProduct)
        expect(RegisterProps.getFreeProduct).toEqual(props.getFreeProduct)
        expect(RegisterProps.isBundle).toEqual(props.isBundle)
        expect(RegisterProps.productType).toEqual(props.productType)
        // rename required
        expect(RegisterProps.paymentError).toEqual(props.responseError)
        expect(RegisterProps.onSubmitBilling).toEqual(props.handleSubmitBilling)
      })
      it('passes through the correct state from context', () => {
        expect(RegisterProps.visible).toEqual(defaultState.registerModalVisible)
        expect(RegisterProps.currentTab).toEqual(defaultState.registerModalTab)
      })
      it('should create a handler for set tab that updates the context', () => {
        RegisterProps.setCurrentTab('hello world')
        expect(defaultState.setRegisterModalTab).toHaveBeenCalledWith('hello world')
      })
      it('should create a handler for openning the confirm purchase modal', () => {
        RegisterProps.openConfirmPurchaseModal()
        expect(defaultState.setConfirmPurchaseModalVisible).toHaveBeenCalledWith(true)
      })
      // This is a nice example of a good test, where the child is expecting a close handler
      // and its up to our wrapper to make sure that handler calls the correct context updates.
      it('should create a handler for closing the modal that resets the modal flow', () => {
        RegisterProps.closeModal()
        expect(defaultState.setRegisterModalVisible).toHaveBeenCalledWith(false)
        expect(defaultState.setRegisterModalTab).toHaveBeenCalledWith(TABS.BILLING)
      })
    })
    describe('should render the ConfirmPurchaseModal with expected values', () => {
      it('renders the Confirm Purchase Modal', () => {
        expect(ConfirmModal).toHaveBeenCalled()
      })
      it('passes through the required props from the input props', () => {
        // rename not required
        expect(ConfirmProps.price).toEqual(props.price)
        expect(ConfirmProps.responseError).toEqual(props.responseError)
        expect(ConfirmProps.title).toEqual(props.title)
        // rename required
        expect(ConfirmProps.confirmPurchase).toEqual(props.purchaseProduct)
      })
      it('passes through the correct state from context', () => {
        expect(ConfirmProps.visible).toEqual(defaultState.confirmPurchaseModalVisible)
      })
      it('should create a close modal handler for the modal', () => {
        ConfirmProps.closeModal()
        expect(defaultState.setConfirmPurchaseModalVisible).toHaveBeenCalledWith(false)
      })
    })
    describe('should render the PurchaseSuccessModal with expected values', () => {
      it('should render the Modal', () => {
        expect(PurchaseModal).toHaveBeenCalled()
      })
      it('should pass through the expected props', () => {
        expect(SuccessProps.documentName).toEqual(props.documentName)
        expect(SuccessProps.documentType).toEqual(props.documentType)
        expect(SuccessProps.section).toEqual(props.section)
        expect(SuccessProps.price).toEqual(props.price)
        expect(SuccessProps.isStatic).toEqual(props.isStatic)
        expect(SuccessProps.title).toEqual(props.title)
        expect(SuccessProps.documentId).toEqual(props.id)
        expect(SuccessProps.isBundle).toEqual(props.isBundle)
      })
      it('should pass through the required context state', () => {
        expect(SuccessProps.visible).toEqual(defaultState.purchaseSuccessModalVisible)
      })
      it('should create a close modal handler that updates the context', () => {
        SuccessProps.closeModal()
        expect(defaultState.setPurchaseSuccessModalVisible).toHaveBeenCalledWith(false)
      })
    })
  })

  describe('with a logged out user', () => {
    beforeAll(() => {
      const wrappingComponent = ({ children }) => (
        <MockedProvider value={defaultState}>
          {children}
        </MockedProvider>
      )
      Container = mount(<ProductContainerModalWrapper {...{ ...props, ...{ isLogged: false } }} />, { wrappingComponent })
    })

    afterAll(() => {
      Container.unmount()
    })

    describe('should render the RegisterModal with expected values', () => { // just want to retest this modal as the only thing that should change
      it('renders the Register Modal', () => {
        expect(RegisterModal).toHaveBeenCalled()
      })
      // Below we see the best example ever of why you should write the tests first
      // Am I testing the implementation or the requirement? WHO KNOWS
      it('passes through the required props from the input props', () => {
        // rename not required
        expect(RegisterProps.isStatic).toEqual(props.isStatic)
        expect(RegisterProps.price).toEqual(props.price)
        // changed
        expect(RegisterProps.isLogged).toEqual(false)

        expect(RegisterProps.hasAccess).toEqual(props.hasAccess)
        expect(RegisterProps.purchaseProduct).toEqual(props.purchaseProduct)
        expect(RegisterProps.getFreeProduct).toEqual(props.getFreeProduct)
        expect(RegisterProps.isBundle).toEqual(props.isBundle)
        expect(RegisterProps.productType).toEqual(props.productType)
        // rename required
        expect(RegisterProps.paymentError).toEqual(props.responseError)
        expect(RegisterProps.onSubmitBilling).toEqual(props.handleSubmitBilling)
      })
      it('passes through the correct state from context', () => {
        expect(RegisterProps.visible).toEqual(defaultState.registerModalVisible)
        expect(RegisterProps.currentTab).toEqual(defaultState.registerModalTab)
      })
      it('should create a handler for set tab that updates the context', () => {
        RegisterProps.setCurrentTab('hello world')
        expect(defaultState.setRegisterModalTab).toHaveBeenCalledWith('hello world')
      })
      it('should create a handler for openning the confirm purchase modal', () => {
        RegisterProps.openConfirmPurchaseModal()
        expect(defaultState.setConfirmPurchaseModalVisible).toHaveBeenCalledWith(true)
      })
      // This is a nice example of a good test, where the child is expecting a close handler
      // and its up to our wrapper to make sure that handler calls the correct context updates.
      it('should create a handler for closing the modal that resets the modal flow', () => {
        RegisterProps.closeModal()
        expect(defaultState.setRegisterModalVisible).toHaveBeenCalledWith(false)
        expect(defaultState.setRegisterModalTab).toHaveBeenCalledWith(TABS.ACCOUNT)
      })
    })
  })
})
