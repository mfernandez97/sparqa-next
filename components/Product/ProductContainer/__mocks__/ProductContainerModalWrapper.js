import React from 'react'

/**
 * PROPS:
 *
 * id,
 * title,
 * documentName,
 * documentType,
 * isStatic,
 * section,
 * price,
 * isLogged,
 * hasAccess,
 * responseError,
 * purchaseProduct,
 * getFreeProduct,
 * handleSubmitBilling,
 * isBundle,
 * productType
 */
const mockedPropAccessor = {}

export default jest.fn().mockImplementation((props) => {
  Object.assign(mockedPropAccessor, props)
  return <div>PRODUCT CONTAINER MODAL WRAPPER</div>
})

export { mockedPropAccessor }
