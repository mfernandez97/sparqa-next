import React from 'react'

const mockedPropAccessor = {}

export default jest.fn().mockImplementation((props) => {
  Object.assign(mockedPropAccessor, props)
  return <div>PRODUCT CONTAINER RESPONSE WRAPPER</div>
})

export { mockedPropAccessor }
