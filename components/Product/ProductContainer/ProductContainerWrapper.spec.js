import React from 'react'
import { mount } from 'enzyme'
import { ModalProvider } from './Modals/ModalContext'
import ProductContainerResponseWrapper from './ProductContainerResponseWrapper'
import WrappedProductContainer from './index'

jest.mock('./Modals/ModalContext')
jest.mock('./ProductContainerResponseWrapper')

describe('WrappedProductContainer Component', () => {
  const testProps = {
    productData: 'product-data-mock', // only prop actually used
    hello: 'world',
    foo: 'bar' // some extra props to make sure all are passed down
  }
  let Component
  beforeAll(() => {
    Component = mount(<WrappedProductContainer {...testProps} />)
  })

  afterAll(() => {
    Component.unmount()
  })

  it('should render the Modal Provider', () => {
    expect(ModalProvider).toHaveBeenCalled()
  })
  it('should pass the props children and productData to the ModalProvider', () => {
    const ModalProviderComponents = Component.find(ModalProvider)
    expect(ModalProviderComponents.length).toEqual(1)
    expect(ModalProviderComponents.props().productData).toEqual(testProps.productData)
  })
  it('should render the ResponseWrapper', () => {
    expect(ProductContainerResponseWrapper).toHaveBeenCalled()
  })
  it('should pass all props to the response wrapper', () => {
    const responseWrapper = Component.find(ProductContainerResponseWrapper)
    expect(responseWrapper.length).toEqual(1)
    expect(responseWrapper.props().hello).toEqual(testProps.hello)
    expect(responseWrapper.props().foo).toEqual(testProps.foo)
  })
})
