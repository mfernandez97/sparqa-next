import React from 'react'
import PropTypes from 'prop-types'
import { map } from 'rxjs/operators'
import { _has } from 'libraries/lodash'
import { $checkProductPurchased } from 'services/DocumentService'
import { $user, $customerDetails } from 'services/UserService'
import { $hasBillingDetails, $credit } from 'services/PaymentsService'
import connect from 'helpers/connect'
import { LoginProvider } from 'components/LoginContext'
import ResponseConsumer from 'components/ResponseConsumer'
import ProductContainer from './ProductContainer'
import StripeWrapper from 'components/Stripe/wrap'

const ProductContainerResponseWrapper = connect(({ productId }) => ({
  user: $user,
  credit: $credit(),
  partner: $customerDetails.pipe(map(({ partner: { code } = {} }) => code)),
  hasBillingDetails: $hasBillingDetails(),
  productPurchased: $checkProductPurchased({ uuid: productId })
}))(props => {
  const renderContent = () => {
    const isFree = props.product.price === 0
    const hasAccess = user.isSubscribed || props.productPurchased
    const isBundle = props.productType === 'bundle'

    return (
      <LoginProvider path={props.pageId}>
        <StripeWrapper>
          <ProductContainer {...{ ...props, hasAccess, isBundle, isFree }} availableForPurchase={_has(props.product, 'price')} />
        </StripeWrapper>
      </LoginProvider>
    )
  }

  const { user, credit, partner, hasBillingDetails, productPurchased } = props
  const responseList = [user, credit, partner, productPurchased]

  if (!user.isSubscribed) {
    responseList.push(hasBillingDetails)
  }

  return (
    <ResponseConsumer
      responses={responseList}
      unauthorizedComponent={renderContent}
      notfoundComponent={renderContent}
      content={renderContent}
    />
  )
})

ProductContainerResponseWrapper.propTypes = {
  productId: PropTypes.string.isRequired,
  productType: PropTypes.oneOf(['document', 'bundle']).isRequired
}

export default ProductContainerResponseWrapper
