import React from 'react'
import cx from 'classnames'
import { _get } from 'libraries/lodash'
import { BUNDLEOVERVIEW } from 'constants/controllers'
import connect from 'helpers/connect'
import { getDocumentIcon } from 'services/DocumentService'
import { $sectionIdToUrlMap, $bundleSeoIdToUrl } from 'services/SeoService'
import Svg from 'components/SVG'
import Button, { themeModifiers, themes } from 'components/Button'
import Price from './Price'
import ResponseConsumer from 'components/ResponseConsumer'
import { DOCUMENTSMENU } from 'components/Link'
import responsiveHOC from 'react-lines-ellipsis/lib/responsiveHOC'
import ReactHTMLEllipsis from 'react-lines-ellipsis/lib/html'

const HTMLEllipsis = responsiveHOC()(ReactHTMLEllipsis)

const RelatedBundles = ({ bundles, isSubscribed }) => {
  const hasMultiple = bundles.length > 1

  return (
    <section className='relative bg-light-silver related-bundles-slant mt11'>
      <h2 className='f-graphic-38 fw4 tc dark-slate-grey pv8 ma0'>
        {`Related Toolkit${hasMultiple ? 's' : ''}`}
      </h2>
      <div className={cx('layout-width-constraint flex flex-wrap-t justify-evenly')}>
        {hasMultiple ? (
          <Bundles {...{ bundles, isSubscribed }} />
        ) : (
          <Bundle {...{
            ...bundles[0],
            isSubscribed,
            width: cx({ 'w-75 w-100-t': !hasMultiple })
          }}
          />
        )}
      </div>
      <Button
        link
        controller={DOCUMENTSMENU}
        params={{ category: 'toolkits' }}
        theme={themes.DARK_SLATE_GREY}
        themeModifier={themeModifiers.INVERT}
        margin='mhauto mt8'
        padding='pv6'
        width='w-measure-2'
      >
        See all Toolkits
      </Button>
    </section>
  )
}

const Bundles = ({ bundles, isSubscribed }) => {
  const sliceIndex = Math.ceil(bundles.length / 2)
  const slicedBundles = [bundles.slice(0, sliceIndex), bundles.slice(sliceIndex)]

  return slicedBundles.map((bundles, index) => (
    <div className='related-bundles' key={index}>
      {bundles.map(bundle => <Bundle key={bundle.id} {...{ ...bundle, isSubscribed }} />)}
    </div>
  ))
}

const Bundle = connect(() => ({
  sectionIdUrlMap: $sectionIdToUrlMap,
  bundleSeoIdToUrl: $bundleSeoIdToUrl
}))(({
  sectionIdUrlMap,
  bundleSeoIdToUrl,
  id,
  title,
  description,
  price,
  bundleDocuments: documents,
  topic,
  isSubscribed,
  width
}) => {
  const renderContent = () => (
    <div className={cx('flex flex-column bg-white shadow-3-black-fade-30 br2 mb6', width)}>
      <h2 className='flex ml6 mr6 mr4-t'>
        <img
          src='assets/toolkit.svg'
          className='w-icon-9 h-icon-9 mr6'
          alt='Documents icon'
        />
        {title}
      </h2>
      <div className='fm tracked-tight silver mh9 mh6-m'>
        <HTMLEllipsis unsafeHTML={description} maxLine={4} />
      </div>
      <div className={cx('flex items-center mt6 mh9 mh6-m flex-wrap-m', {
        'justify-between': !isSubscribed,
        'justify-end': isSubscribed
      })}
      >
        {!isSubscribed && <Price size='medium' {...{ price }} />}
        <Button
          link
          controller={BUNDLEOVERVIEW}
          params={{ section: sectionIdUrlMap[topic.bookmarkId], bundleName: bundleSeoIdToUrl[id] }}
          themeModifier={themeModifiers.SOLID}
          size='small'
          padding='pv6'
          margin='mh0'
        >
          Find out more
        </Button>
      </div>
      <DocumentList {...{ documents }} />
    </div>
  )

  return <ResponseConsumer responses={[sectionIdUrlMap, bundleSeoIdToUrl]} content={renderContent} />
})

const DocumentList = ({ documents }) => (
  <ul className='list-reset mt6 mb0 pa0 fs'>
    {documents.map(({ name, documentType }, index) => (
      <li key={index} className='flex pl5 pr7 pv5 bb b--light-silver'>
        <Svg
          className='w-icon-6 h-icon-6 shrink-0 mr5'
          url={getDocumentIcon(_get(documentType, 'name'))}
        />
        {name}
      </li>
    ))}
    <li className='pl5 pr7 pv5 light-sea-green'>
      <Svg
        className='w-icon-6 h-icon-6 mr5 shrink-0'
        url='assets/icons/pdf.svg'
      />
      How-to guide
    </li>
  </ul>
)

export default RelatedBundles
