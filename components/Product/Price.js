import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import { formatCurrency } from 'helpers/misc'

const Price = ({ size = 'large', price, className }) => {
  const isFree = price === 0

  const font = cx('fw8', {
    'f-graphic-92': size === 'large',
    'f-graphic-50': size === 'medium'
  })

  return (
    <div className={cx('flex items-start light-sea-green', className)}>
      {isFree ? (
        <span className={font}>Free</span>
      ) : (
        <div className='flex'>
          <span className='self-start mt8'>£</span>
          <span className={cx('tracked-tight mr1', font, {
            'f-graphic-92': size === 'large',
            'f-graphic-50': size === 'medium'
          })}
          >
            {formatCurrency(price, true)}
          </span>
          <span className='self-end mb8'>+VAT</span>
        </div>
      )}
    </div>
  )
}

Price.propTypes = {
  price: PropTypes.number.isRequired,
  size: PropTypes.oneOf(['small', 'medium'])
}

export default Price
