import React from 'react'
import cx from 'classnames'
import CollapsibleBox from 'components/CollapsibleBox'

const FloatingBox = ({ className, children, titleContainerClassName, titleClassName, ...props }) => (
  <div className={cx(
    'shrink-1 grow-0-t shrink-0-t basis0 bg-white w-100 br2',
    className
  )}
  >
    <CollapsibleBox
      {...props}
      expandable
      titleClassName={cx('fxxl fl-t fw6', titleClassName)}
      titleContainerClassName={cx('pv7 pv6-m', titleContainerClassName)}
      scrollingContainerClassName='pb7 pv6-m ph10 ph6-m'
    >
      {children}
    </CollapsibleBox>
  </div>
)

export default FloatingBox
