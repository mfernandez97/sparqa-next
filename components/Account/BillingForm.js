import React from 'react'
import insist from 'components/Form/esv-react-form/insist'
import { COUNTRY_CODE_OPTIONS } from 'constants/stripe'
import { URL_TERMS_OF_USE, URL_PRIVACY_POLICY } from 'constants/links'
import Form, { Field as FormField, FormSubmit, FormFooter, ErrorMessages } from 'components/Form'

const schema = {
  billingName: insist().required(),
  addressLine1: insist().required(),
  postCode: insist().required(),
  country: insist().required()
}

const defaultInitialValues = {
  billingName: '',
  cardElement: null,
  addressLine1: '',
  addressLine2: '',
  postCode: '',
  country: '',
  vatNo: ''
}

const BillingForm = ({ children = null, onChange, onSubmit, initialValues, responseError }) => (
  <Form
    schema={schema}
    onSubmit={onSubmit}
    onChange={onChange}
    initialValues={{ ...defaultInitialValues, ...initialValues }}
  >
    <Field label='Billing name' name='billingName' type='text' placeholder='Full name as it appears on the card' />
    <Field label='Credit or debit card' name='cardElement' type='stripe' />
    <Field label='Address line 1' name='addressLine1' type='text' placeholder='123 Main Street' />
    <Field label='Address line 2 (optional)' name='addressLine2' type='text' placeholder='Apt., Office, Suite' />
    <Field label='Postcode' name='postCode' type='text' />
    <Field label='Country' name='country' type='select' options={COUNTRY_CODE_OPTIONS} placeholder='Please select' />
    {children}
    <FormSubmit>Confirm</FormSubmit>
    {responseError && <ErrorMessages className='mt4' messages={[ responseError ]} />}
    <FormFooter>
      <span>By submitting this form, you confirm that you agree to the storage and processing of your personal data by Sparqa Legal as described in our <a href={URL_TERMS_OF_USE}>Terms of Use</a> and <a href={URL_PRIVACY_POLICY}>Privacy Policy</a>.</span>
    </FormFooter>
  </Form>
)

const Field = ({ className = null, ...props }) => (
  <div className={className || 'mb6'}><FormField {...props} /></div>
)

export default BillingForm
