import React from 'react'
import insist from 'components/Form/esv-react-form/insist'
import { isEmailValid, isEmailAvailable, isPasswordStrong } from 'components/Form/customRules'
import Form, { Field as FormField, FormSubmit, ErrorMessages } from 'components/Form'
import { TermsOfUseField, FormFooter } from 'components/Account/CustomFields'

const schema = {
  firstName: insist().required(),
  lastName: insist().required(),
  email: insist()
    .custom(isEmailValid.rule, isEmailValid.options)
    .custom(isEmailAvailable.rule, isEmailAvailable.options),
  password: insist()
    .custom(isPasswordStrong.rule, isPasswordStrong.options),
  termsAndConditions: insist().isTrue({ message: 'You must agree to the Terms of Use and Privacy Policy.' }),
  marketingConsent: insist().isTrue({ message: 'To be eligible for our free tier, you must agree to receive updates on the laws and relevant promotions that affect your business.' })
}

const defaultInitialValues = {
  firstName: '',
  lastName: '',
  email: '',
  password: '',
  termsAndConditions: false,
  marketingConsent: false
}

const FreemiumForm = ({
  onSubmit,
  onChange,
  buttonText = 'Complete Registration',
  responseError,
  initialValues,
  customTermsAndConditions,
  FooterComponent = <FormFooter isTrial={false} />
}) => {
  return (
    <Form
      schema={schema}
      onSubmit={onSubmit}
      onChange={onChange}
      initialValues={{ ...defaultInitialValues, ...initialValues }}
    >
      <Field label='First name' type='text' name='firstName' />
      <Field label='Last name' type='text' name='lastName' />
      <Field label='Email address' type='email' name='email' />
      <Field label='Password' type='password' name='password' allowUnmasked showStrengthBar showStrengthBadge />
      <TermsOfUseField className='mb4' label={customTermsAndConditions} />
      <MarketingConsentField />
      <FormSubmit>{buttonText}</FormSubmit>
      {responseError && (
        <ErrorMessages className='mt2' messages={[responseError]} />
      )}
      {FooterComponent}
    </Form>
  )
}

const Field = props => <div className='mb6'><FormField {...props} /></div>

export const MarketingConsentField = () => (
  <div className='mb8'>
    <FormField
      label={'I consent to receiving email marketing from Sparqa Legal in order to access Sparqa\'s legal guidance for free.'}
      type='checkbox'
      name='marketingConsent'
    />
  </div>
)

export default FreemiumForm
