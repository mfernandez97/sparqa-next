import React, { Component } from 'react'
import Form, { Field, Switch, ErrorMessages } from 'components/Form'

const freemiumMessage = `To continue to use the service for free, access thousands of legal answers and
purchase documents on a PAYG basis please confirm you’re happy to receive occasional
emails and information briefings which are beneficial to your business.`

const MarketingConsentForm = ({
  message = freemiumMessage,
  onSubmit,
  showMessage = false,
  responseError,
  initialValues,
  marketingConsent
}) => (
  <Form
    onSubmit={() => null} // The request happens onChange so just need a stub here.
    initialValues={initialValues}
  >
    {showMessage && (
      <p className='mb8 mb6-m grey'>{message}</p>
    )}
    <Field name='marketingConsent' label='Send me occasional emails about Sparqa product features and news.'>
      {fieldProps => (
        <MarketingConsentSwitch
          {...fieldProps}
          onChange={onSubmit}
          marketingConsent={marketingConsent}
        />
      )}
    </Field>
    {responseError && <ErrorMessages messages={[ responseError ]} />}
  </Form>
)

class MarketingConsentSwitch extends Component {
  componentDidUpdate (prevProps) {
    const { setValue, marketingConsent } = this.props
    if (prevProps.marketingConsent !== marketingConsent) {
      setValue(marketingConsent)
    }
  }

  render () {
    const { name, value: checked, label, onChange } = this.props
    return (
      <Switch
        name={name}
        value={checked}
        label={label}
        onChange={e => onChange(!checked)}
      />
    )
  }
}

export default MarketingConsentForm
