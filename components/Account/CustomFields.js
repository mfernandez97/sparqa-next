import React from 'react'
import { Field, FormFooter as BCFormFooter } from 'components/Form'
import { LOGIN } from 'constants/controllers'
import { URL_TERMS_OF_USE, URL_PRIVACY_POLICY } from 'constants/links'
import Link from 'components/Link'

export const IndustryField = ({ options }) => (
  <div className='mb8'>
    <Field label='Industry' type='select' name='companyIndustry' placeholder='Please select' options={options} />
  </div>
)

export const TermsOfUseField = ({ label = null, className = 'mb6' }) => (
  <div className={className}>
    <Field
      label={label || (
        <span>
          I have read and agree to the <a href={URL_TERMS_OF_USE} target='_blank' rel='noopener noreferrer'>Terms of Use</a> and <a href={URL_PRIVACY_POLICY} target='_blank' rel='noopener noreferrer'>Privacy Policy</a>, and I confirm that I am using Sparqa Legal for the purposes of my trade, business, craft or profession.
        </span>
      )}
      type='checkbox'
      name='termsAndConditions'
    />
  </div>
)

export const MarketingConsentField = () => (
  <div className='mb6'>
    <Field
      label='I would like to receive updates on the laws and relevant promotions that affect my business.'
      name='marketingConsent'
      type='radio'
      options={[
        { value: 'yes', label: 'Yes' },
        { value: 'no', label: 'No' }
      ]}
    />
  </div>
)

export const FormFooter = ({ isTrial }) => isTrial ? (
  <footer className='tl mt8 fs'>
    Sparqa Legal has been developed for businesses in England and Wales, but we welcome users from all corners of the globe.
    Sign up to find useful guidance and documents for your business and see our <a href={URL_TERMS_OF_USE} target='_blank' rel='noopener noreferrer'>Terms of Use</a> for more info.
  </footer>
) : (
  <BCFormFooter>
    Already have an account?&nbsp;
    <Link className='no-underline steel-blue' controller={LOGIN}>Sign in</Link>
  </BCFormFooter>
)
