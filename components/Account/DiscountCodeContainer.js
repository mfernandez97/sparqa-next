import React, { Component, cloneElement } from 'react'
import { isDiscountCodeValid } from 'services/PaymentsService'
import { DiscountProvider } from 'context'

export default class DiscountCodeContainer extends Component {
  state = {
    discount: 0,
    appliedDiscountCode: ''
  }

  get discountProviderValue () {
    const { discount, appliedDiscountCode } = this.state
    return {
      state: { discount, appliedDiscountCode },
      actions: {
        applyDiscountCode: this.applyDiscountCode,
        clearDiscountCode: this.clearDiscountCode
      }
    }
  }

  applyDiscountCode = discountCode => {
    return isDiscountCodeValid(discountCode).then(discount => {
      this.setState({ appliedDiscountCode: discountCode, discount })
      return discount
    })
  }

  clearDiscountCode = () => {
    this.setState({ appliedDiscountCode: '', discount: 0 })
  }

  onSubmit = (e, { values, onFinishSubmit }) => {
    const discountCode = this.state.appliedDiscountCode || values.discountCode
    this.props.onSubmit(e, { values: { ...values, discountCode, ...this.state }, onFinishSubmit })
  }

  render () {
    const { discount, appliedDiscountCode } = this.state
    return (
      <DiscountProvider value={this.discountProviderValue}>
        {cloneElement(this.props.children, { ...this.props, discount, appliedDiscountCode, onSubmit: this.onSubmit })}
      </DiscountProvider>
    )
  }
}
