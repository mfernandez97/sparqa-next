import React, { Component } from 'react'
import { getClassName } from 'helpers/misc'
import { Field, ErrorMessages, getTextFieldClassNames } from 'components/Form'
import Button, { themes, themeModifiers } from 'components/Button'

const DiscountCodeField = props => (
  <Field name='discountCode'>
    {fieldProps => <DiscountInputWrap {...fieldProps} {...props} />}
  </Field>
)

class DiscountInputWrap extends Component {
  componentDidUpdate (prevProps) {
    const { appliedDiscountCode, setValue } = this.props
    if (prevProps.appliedDiscountCode !== appliedDiscountCode) {
      setValue(appliedDiscountCode)
    }
  }

  render () {
    const {
      warning,
      onKeyPress,
      responseError,
      responseSuccess,
      discountApplied,
      applyDiscountCode,
      clearDiscountCode
    } = this.props

    const defaultClassNames = { margin: 'mh6 mh0-m mb6-m' }
    const className = getClassName(defaultClassNames, this.props)

    const showErrors = this.props.showErrors || responseError
    const errorMessages = [ responseError ].concat(this.props.errorMessages)
    const completedWithErrors = this.props.completedWithErrors || (this.props.completedWithoutErrors && responseError)
    const completedWithoutErrors = this.props.completedWithoutErrors && !responseError

    return (
      <div className={className}>
        <Input
          {...this.props}
          readOnly={discountApplied}
          onKeyPress={onKeyPress}
          applyDiscountCode={applyDiscountCode}
          clearDiscountCode={clearDiscountCode}
          completedWithErrors={completedWithErrors}
          completedWithoutErrors={completedWithoutErrors}
        />
        {responseSuccess && (
          <p className='mt4 mb0 medium-sea-green fxs'>{responseSuccess}</p>
        )}
        {showErrors && <ErrorMessages className='mt4' messages={errorMessages} />}
        {warning && <p className='mt4 mb0 dark-grey fxs'>{warning}</p>}
      </div>
    )
  }
}

const Input = ({
  value = '',
  onBlur,
  readOnly,
  onChange,
  onKeyPress,
  applyDiscountCode,
  clearDiscountCode,
  completedWithErrors,
  completedWithoutErrors
}) => {
  const onSubmit = readOnly ? clearDiscountCode : applyDiscountCode

  const defaults = getTextFieldClassNames({ completedWithErrors, completedWithoutErrors, readOnly })
  const className = getClassName(defaults, {
    padding: 'ph6 pv5 ph4-m pv5-m',
    margin: 'mv0 ml0 mr4',
    color: readOnly ? 'light-sea-green' : defaults.color,
    className: 'grow-1 shrink-0 basis0'
  })

  return (
    <div className='flex relative'>
      <input
        type='text'
        value={value}
        onBlur={onBlur}
        onChange={e => onChange(e.target.value.toUpperCase())}
        readOnly={readOnly}
        className={className}
        onKeyPress={e => onKeyPress(e, value)}
        placeholder='DISCOUNT CODE (optional)'
      />
      <Button
        type='button'
        onClick={() => onSubmit(value)}
        theme={themes.LIGHT_SEA_GREEN}
        themeModifier={themeModifiers.INVERT_BORDER_COLOR}
        width={null}
        display={null}
        padding='pv6 ph8 ph4-m pv5-m'
        className='self-start'
      >
        {readOnly ? 'Clear' : 'Apply'}
      </Button>
    </div>
  )
}

export default DiscountCodeField
