import React, { Component } from 'react'
import { _isString, _isFunction } from 'libraries/lodash'
import { DiscountConsumer } from 'context'
import Field from './Field'

class DiscountCodeField extends Component {
  state = { responseError: null, responseSuccess: null }

  get isFreemium () {
    const { selectedPlan } = this.props
    return selectedPlan === 'freemium'
  }

  componentDidMount () {
    const { discount, appliedDiscountCode } = this.props
    if (discount > 0 && _isString(appliedDiscountCode) && appliedDiscountCode.length > 0) {
      this.setState({
        responseError: null,
        responseSuccess: `Your ${discount}% discount for ${appliedDiscountCode} has been applied!`
      })
    }
  }

  clearDiscountCode = () => {
    this.props.clearDiscountCode()
    this.props.setDiscountCode('')
    this.setState({
      responseError: null,
      responseSuccess: null
    })
  }

  applyDiscountCode = discountCode => {
    if (!discountCode) return
    this.props.applyDiscountCode(discountCode)
      .then(discount => {
        this.setState({
          responseError: null,
          responseSuccess: `Your ${discount}% discount for ${discountCode} has been applied!`
        })
      })
      .catch(this.handleError)
  }

  handleError = error => {
    this.setState({
      responseError: error.status === 404
        ? 'Discount code not valid.'
        : 'Sorry, an unknown error occurred.',
      responseSuccess: null
    })
  }

  handleKeyPress = (e, discountCode) => {
    if (e.key === 'Enter') {
      e.preventDefault()
      this.applyDiscountCode(discountCode)
    }
  }

  render () {
    const discountApplied = !!this.props.appliedDiscountCode
    const warning = this.isFreemium && discountApplied
      ? 'Your discount code will have no effect on a free tier plan.'
      : null
    return (
      <Field
        {...this.props}
        warning={warning}
        onKeyPress={this.handleKeyPress}
        selectedPlan={this.props.selectedPlan}
        responseError={this.state.responseError}
        responseSuccess={warning ? null : this.state.responseSuccess}
        discountApplied={discountApplied}
        applyDiscountCode={this.applyDiscountCode}
        clearDiscountCode={this.clearDiscountCode}
        appliedDiscountCode={this.props.appliedDiscountCode}
      />
    )
  }
}

const ConnectedDiscountCodeField = props => (
  <DiscountConsumer>
    {({ state, actions }) => {
      return (
        <DiscountCodeField
          {...props}
          applyDiscountCode={value => {
            _isFunction(props.onApplyDiscountCode) && props.onApplyDiscountCode(value)
            return actions.applyDiscountCode(value)
          }}
          clearDiscountCode={() => {
            _isFunction(props.onClearDiscountCode) && props.onClearDiscountCode()
            return actions.clearDiscountCode()
          }}
          discount={state.discount}
          appliedDiscountCode={state.appliedDiscountCode}
        />
      )
    }}
  </DiscountConsumer>
)

export default ConnectedDiscountCodeField
