import React from 'react'
import { _omit } from 'libraries/lodash'
import insist from 'components/Form/esv-react-form/insist'
import { isPasswordStrong } from 'components/Form/customRules'
import Form, { Field as FormField, FormSubmit, ErrorMessages } from 'components/Form'

const defaultSchema = {
  oldPassword: insist().required(),
  newPassword: insist().custom(isPasswordStrong.rule, isPasswordStrong.options),
  confirmPassword: insist().sameAs('newPassword', { message: 'Passwords do not match.' })
}

const defaultInitialValues = {
  oldPassword: '',
  newPassword: '',
  confirmPassword: ''
}

const UpdatePasswordForm = ({
  schema: providedSchema = {},
  onSubmit: handleSubmit,
  buttonText = 'Change Password',
  oldPassword = true,
  initialValues: providedInitialValues = {},
  responseError
}) => {
  const schema = { ...defaultSchema, ...providedSchema }
  const initialValues = { ...defaultInitialValues, ...providedInitialValues }
  return (
    <Form
      schema={oldPassword ? schema : _omit(schema, 'oldPassword')}
      onSubmit={handleSubmit}
      initialValues={oldPassword ? initialValues : _omit(initialValues, 'oldPassword')}
    >
      {oldPassword && <Field label='Old Password' type='password' name='oldPassword' />}
      <Field showStrengthBar showStrengthBadge label='New Password' type='password' name='newPassword' />
      <Field label='Confirm New Password' type='password' name='confirmPassword' />
      <FormSubmit className='mt8'>{buttonText}</FormSubmit>
      {responseError && <ErrorMessages messages={[responseError]} />}
    </Form>
  )
}

const Field = props => (
  <div className='mb6'>
    <FormField {...props} />
  </div>
)

export default UpdatePasswordForm
