import React from 'react'
import insist from 'components/Form/esv-react-form/insist'
import Form, { FormConsumer, FormSubmit } from 'components/Form'
import PlanOptionsField from 'components/Account/PlanOptionsField'
import DiscountCodeField from 'components/Account/DiscountCodeField'

const schema = {
  selectedPlan: insist().required({ message: 'Please select a plan to continue.' })
}

const SelectPlanForm = ({
  discount,
  onChange,
  onSubmit,
  carousel = false,
  initialValues,
  appliedDiscountCode
}) => (
  <Form
    schema={schema}
    onChange={onChange}
    onSubmit={onSubmit}
    initialValues={initialValues}
  >
    <PlanOptionsField savePlanToSession carousel={carousel} discount={discount} />
    <div className='mb8 mb2-m max-w-measure-2 mhauto'>
      <FormConsumer>
        {({ state, actions }) => (
          <DiscountCodeField
            setDiscountCode={actions.setValue('discountCode')}
            appliedDiscountCode={appliedDiscountCode}
          />
        )}
      </FormConsumer>
    </div>
    <FormSubmit className='mhauto' maxWidth='max-w-measure-3'>Continue</FormSubmit>
  </Form>
)

export default SelectPlanForm
