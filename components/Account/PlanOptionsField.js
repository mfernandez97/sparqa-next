import React, { useRef, useEffect } from 'react'
import { Field, ErrorMessages } from 'components/Form'
import PaymentPlans from 'components/PaymentPlans'
import styleConfig from 'styles/config/config.js'

const {
  PRICE_PANEL_WIDTH
} = styleConfig

const PlanOptionsField = ({
  filter = plan => true,
  discount = 0,
  carousel = false,
  savePlanToSession = false
}) => {
  const ref = useRef()
  useEffect(() => {
    const el = ref.current
    if (el.scrollWidth > el.clientWidth) {
      el.scrollLeft = PRICE_PANEL_WIDTH / 2
    }
  }, [])

  return (
    <Field name='selectedPlan'>
      {field => (
        <div ref={ref} className='mb8 mb2-m overflow-x-scroll'>
          <PaymentPlans
            filter={filter}
            discount={discount}
            carousel={carousel}
            onChange={e => field.onChange(e.target.value)}
            selectedPlan={field.value}
            savePlanToSession={savePlanToSession}
          />
          {field.showErrors && (
            <ErrorMessages className='ph6 mt4' messages={field.errorMessages} />
          )}
        </div>
      )}
    </Field>
  )
}

export default PlanOptionsField
