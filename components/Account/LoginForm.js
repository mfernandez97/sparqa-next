import React from 'react'
import insist from 'components/Form/esv-react-form/insist'
import Link, { REGISTER, FORGOTPASSWORD } from 'components/Link'
import Form, { Field, FormSubmit, FormFooter, ErrorMessages } from 'components/Form'

const schema = {
  username: insist().required(),
  password: insist().required()
}

const defaultInitialValues = {
  username: '',
  password: ''
}

const LoginForm = ({
  onSubmit,
  initialValues,
  responseError,
  FooterComponent = (
    <FormFooter>
      Don&#39;t have an account yet?&nbsp;
      <Link className='no-underline steel-blue' controller={REGISTER}>Get started here.</Link>
    </FormFooter>
  )
}) => (
  <Form
    schema={schema}
    onSubmit={onSubmit}
    initialValues={{ ...defaultInitialValues, ...initialValues }}
  >
    <div className='mb6'>
      <Field label='Email address' type='email' name='username' />
    </div>
    <div className='mb6'>
      <Field
        label='Password'
        type='password'
        name='password'
        nodeAfterInput={(
          <Link
            className='fxs no-underline steel-blue'
            controller={FORGOTPASSWORD}
            tabIndex='0'
          >
            Forgot password?
          </Link>
        )}
      />
    </div>
    <FormSubmit>Log in</FormSubmit>
    {responseError && (
      <ErrorMessages className='mt2' messages={[ responseError ]} />
    )}
    {FooterComponent}
  </Form>
)

export default LoginForm
