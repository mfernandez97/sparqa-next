import React, { Component } from 'react'
import cx from 'classnames'
import getUrl from 'front-end-core/core/url'
import { pushUrl } from 'front-end-core/core/history'
import insist from 'components/Form/esv-react-form/insist'
import { $paymentPlans } from 'services/PaymentsService'
import { isEmailValid, isEmailAvailable } from 'components/Form/customRules'
import { REGISTER } from 'constants/controllers'
import { ACCOUNT_DETAILS } from 'constants/register-pages'
import sessionState from 'helpers/sessionState'
import connect from 'helpers/connect'
import Form, { FormConsumer, Field, ErrorMessages } from 'components/Form'
import Icon from 'components/Icon'
import ResponseConsumer from 'components/ResponseConsumer'
import Button, { themes, themeModifiers } from 'components/Button'

const initialValues = { email: '' }
const schema = {
  email: insist()
    .custom(isEmailValid.rule, isEmailValid.options)
    .custom(isEmailAvailable.rule, isEmailAvailable.options)
}

@sessionState(({ register }) => ({ register }), {})
@connect(() => ({ paymentPlans: $paymentPlans() }))
class StartNowForm extends Component {
  startNow = email => {
    const selectedPlan = 'freemium'
    const { setSessionState, paymentPlans, path } = this.props
    const planDetails = paymentPlans.find(({ stripeId }) => stripeId === selectedPlan)
    const url = getUrl(REGISTER, { step: ACCOUNT_DETAILS, path })
    setSessionState(sessionState => ({
      register: {
        email,
        plan: selectedPlan,
        planName: planDetails.name,
        selectedPlanCost: planDetails.price
      }
    }))
    pushUrl(url)
  }

  renderContent = () => {
    const {
      showText = true,
      textColor = 'white',
      className = '',
      buttonText = 'Start Now',
      placeholder = 'Enter your email address'
    } = this.props
    return (
      <div className={cx(className, 'w-100 layout-width-constraint')}>
        <Form
          schema={schema}
          className='relative max-w-measure-3 mhauto'
          initialValues={initialValues}
          onSubmit={(e, { values, onFinishSubmit }) => {
            this.startNow(values.email)
          }}
        >
          <div className='bg-white w-100-m db br-pill ma0 focus-outline-0 ba grow-1 shrink-0 basis0 transition-all flex shadow-3-black-fade-30 h-icon-12 h-icon-10-m'>
            <EmailField placeholder={placeholder} />
            <Submit buttonText={buttonText} />
          </div>
          <Errors />
        </Form>
        {showText && <p className={cx(textColor, 'tc fl lh-title mv6')}>Sign up for free - no credit card required</p>}
      </div>
    )
  }

  render () {
    return <ResponseConsumer content={this.renderContent} response={this.props.paymentPlans} />
  }
}

const EmailField = ({ placeholder }) => (
  <Field name='email'>
    {props => (
      <input
        type='text'
        value={props.value}
        onChange={e => props.onChange(e.currentTarget.value)}
        className='dark-grey fm fs-t pa6 w-100 db br-pill ma0 focus-outline-0 bn bg-transparent transition-all'
        placeholder={placeholder}
      />
    )}
  </Field>
)

const Errors = () => (
  <FormConsumer>
    {({ state: { errorMessages, showAllErrors } }) => {
      const errors = errorMessages.email
      return errors.length > 0 && showAllErrors
        ? <ErrorMessages className='bg-white mh6 pv1 ph2 br2 mt4 dib bg-white-fade-70' messages={errors} />
        : null
    }}
  </FormConsumer>
)

const Submit = ({ buttonText }) => {
  return (
    <FormConsumer>
      {() => (
        <Button
          type='submit'
          size='small'
          theme={themes.DARK_SLATE_GREY}
          themeModifier={themeModifiers.INVERT}
          width='w-image-6 w-icon-12-m'
          className='relative'
        >
          <span className='dn-m nowrap'>{buttonText}</span>
          <Icon className='dn db-m w-icon-5 h-icon-5 left-50 top-50 x--50-y--50 absolute' name='arrow-right' />
        </Button>
      )}
    </FormConsumer>
  )
}

export default StartNowForm
