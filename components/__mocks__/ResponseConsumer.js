import React from 'react'

export default jest.fn().mockImplementation(() => {
  return <div>RESPONSE CONSUMER</div>
})
