import React from 'react'

export const themes = {
  LIGHT_SEA_GREEN: 'light-sea-green',
  DARK_SLATE_GREY: 'dark-slate-grey',
  ROYAL_BLUE_3: 'royal-blue-3',
  CORAL: 'coral'
}
export const themeModifiers = {
  SOLID: 'solid',
  INVERT: 'invert',
  INVERT_BORDER_COLOR: 'invert-border-color'
}

export default jest.fn().mockImplementation(() => {
  return <div>BUTTON</div>
})
