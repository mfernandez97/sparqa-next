import React, { Component } from 'react'
import cx from 'classnames'
import { _isFunction, _includes, _isNull, _isString } from 'libraries/lodash'
import responsiveHOC from 'react-lines-ellipsis/lib/responsiveHOC'
import HTMLEllipsis from 'react-lines-ellipsis/lib/html'
import { MediaChanged } from 'services/WindowService'
import connect from 'helpers/connect'
import Link from 'components/Link'
import { plainText } from 'components/CMS/RichText'

const LinesEllipsis = responsiveHOC()(HTMLEllipsis)

export const GridItem = ({ link = null, linkParams, ...props }) => {
  if (link) {
    return (
      <a className='no-underline color-inherit' href={link}>
        <GridItemContent {...props} />
      </a>
    )
  }
  if (linkParams) {
    return (
      <Link className='no-underline dark-grey' {...linkParams}>
        <GridItemContent {...props} />
      </Link>
    )
  }
  return <GridItemContent {...props} />
}

@connect(() => ({ media: MediaChanged }))
class GridItemContent extends Component {
  render () {
    const {
      image = {},
      title = null,
      className = '',
      description = null,
      titleFontSize = 'large',
      titleAlignment = 'center',
      descriptionAlignment = 'center',
      imageAspectRatio = '1:1',
      imageBackgroundSize = 'contain',
      mobileCardType = 'column',
      desktopCardType = 'column'
    } = this.props
    const descriptionExists = !_isNull(description)
    return (
      <div className={cx('flex br2 overflow-hidden h-100', {
        'flex-column': desktopCardType === 'column',
        'flex-column-t': mobileCardType === 'column',
        'flex-row-t': mobileCardType === 'row'
      }, className)}
      >
        {(image && image.url) && (
          <Image
            {...image}
            backgroundSize={imageBackgroundSize}
            aspectRatio={imageAspectRatio}
            mobileCardType={mobileCardType}
            desktopCardType={desktopCardType}
          />
        )}
        <div className={cx('pa4 basis-auto grow-1 shrink-1 h-100', {
          'justify-center flex flex-column': !descriptionExists
        })}
        >
          {title && <Title title={title} size={titleFontSize} alignment={titleAlignment} descriptionExists={descriptionExists} />}
          {description && (
            <LinesEllipsis
              maxLine={_includes(this.props.media, 'mobile') ? 1 : 3}
              ellipsis='...'
              basedOn='words'
              className={cx('fs fw4 mv0 color-inherit', { tc: descriptionAlignment !== 'left' })}
              unsafeHTML={_isString(description) ? description : plainText(description)}
            />
          )}
        </div>
      </div>
    )
  }
}

const Image = ({ url, alt, backgroundSize, aspectRatio, desktopCardType, mobileCardType }) => {
  return (
    <div
      className={cx('bg-x-center shrink-0', {
        'w-100 mb4': desktopCardType === 'column',
        'w-100-t mb4-t min-h0-t': mobileCardType === 'column', // h-auto-m
        'w-30 pb-30 mb0': desktopCardType === 'row',
        'w-image-10-t h-100-t min-h-image-10-t pb0-t mb0-t': mobileCardType === 'row',
        'pb-100': aspectRatio === '1:1' && desktopCardType === 'column',
        'pb-100-t': aspectRatio === '1:1' && mobileCardType === 'column',
        'pb-75': aspectRatio === '4:3' && desktopCardType === 'column',
        'pb-75-t': aspectRatio === '4:3' && mobileCardType === 'column',
        'pb-two-thirds': aspectRatio === '3:2' && desktopCardType === 'column',
        'pb-two-thirds-t': aspectRatio === '3:2' && mobileCardType === 'column',
        'pb-56': aspectRatio === '16:9' && desktopCardType === 'column',
        'pb-56-t': aspectRatio === '16:9' && mobileCardType === 'column'
      })}
      style={{
        backgroundSize,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        backgroundImage: `url(${url})`
      }}
    >
      {alt && <p className='screen-reader-text'>{alt}</p>}
    </div>
  )
}

const Title = ({ title, size, alignment, descriptionExists }) => {
  return (
    <h3
      className={cx('fw6 mt0 lh-title color-inherit', {
        mb4: descriptionExists,
        mb0: !descriptionExists,
        'fm fs-m': size === 'small',
        'fl fm-m': size === 'medium',
        'fxl fm-m': size === 'large',
        tl: alignment === 'left',
        tc: alignment === 'center'
      })}
    >
      {title}
    </h3>
  )
}

const Grid = ({
  title = null,
  children,
  itemsPerRow,
  itemsPerRowMobile = '',
  keyExtractor,
  desktopCardType,
  mobileCardType,
  className = '',
  gutterWidth = '4',
  widthConstraint = false
}) => {
  const perRow = itemsPerRow.toString()
  const mobilePerRow = itemsPerRowMobile.toString()
  return (
    <section className={className}>
      {title && <h2 className={cx('tc fxxxl fw7 mt0 mb8 lh-solid', { 'layout-width-constraint': widthConstraint })}>{title}</h2>}
      <ul className={cx('pa0 mv0 list-reset flex', {
        'layout-width-constraint': widthConstraint,
        'grid-1-d': perRow === '1',
        [`grid-2-gutter-${gutterWidth}-d`]: perRow === '2',
        [`grid-3-gutter-${gutterWidth}-d`]: perRow === '3',
        [`grid-4-gutter-${gutterWidth}-d`]: perRow === '4',
        [`grid-5-gutter-${gutterWidth}-d`]: perRow === '5',
        'grid-1-t grid-1-m': mobilePerRow === '1' || mobileCardType === 'row',
        [`grid-2-gutter-${gutterWidth}-t grid-2-gutter-${gutterWidth}-m`]: mobilePerRow === '2',
        [`grid-3-gutter-${gutterWidth}-t grid-3-gutter-${gutterWidth}-m`]: mobilePerRow === '3',
        [`grid-4-gutter-${gutterWidth}-t grid-4-gutter-${gutterWidth}-m`]: mobilePerRow === '4',
        [`grid-5-gutter-${gutterWidth}-t grid-5-gutter-${gutterWidth}-m`]: mobilePerRow === '5'
      })}
      >
        {React.Children.map(children, (child, index) => (
          <li key={_isFunction(keyExtractor) ? keyExtractor(child) : index}>{child}</li>)
        )}
      </ul>
    </section>
  )
}

export default Grid
