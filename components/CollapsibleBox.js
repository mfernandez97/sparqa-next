import React, { Component, createRef } from 'react'
import cx from 'classnames'
import 'polyfills/closest'
import ScrollingContainer from 'components/ScrollingContainer'
import Icon from 'components/Icon'
import { Collapse as ReactCollapse } from 'react-collapse'
import { copyToClipboard } from 'helpers/misc'

export default class CollapsibleBox extends Component {
  static defaultProps = {
    expandable: false,
    startExpanded: false
  }

  constructor (props) {
    super(props)
    this.state = {
      notification: null,
      expanded: props.startExpanded
    }
    this.shareRef = createRef()
  }

  toggleExpanded = e => {
    const closest = e.target.closest('.share')
    closest
      ? !closest.isSameNode(this.shareRef.current) && this.setState({ expanded: !this.state.expanded })
      : this.setState({ expanded: !this.state.expanded })
  }

  share = () => {
    copyToClipboard(this.props.url)
      .then(result => {
        return result
          ? { message: 'Succesfully copied URL', className: 'bg-grey white' }
          : { message: 'Failed to copy URL', className: 'bg-crimson white' }
      })
      .then(({ message, className }) => {
        const notificationElement = <span className={cx(className, 'mr4 br1')}>{message}</span>
        this.setState({ notification: notificationElement })
      })
      .then(setTimeout(() => this.setState({ notification: false }), 2000))
  }

  handleWrapClick = e => {
    this.toggleExpanded(e)
  }

  handleShareClick = e => {
    this.share()
  }

  render () {
    const { expanded } = this.state
    const {
      title = null,
      icon = null,
      chevronClassName = '',
      scrollingContainerClassName = '',
      titleContainerClassName = '',
      outerTitleContainerClassName = '',
      titleClassName = '',
      scrollingStateKey,
      children,
      expandable,
      shareable,
      scrollable = false,
      seoDump = false
    } = this.props
    return (
      <>
        <div className={outerTitleContainerClassName}>
          <Wrap
            titleContainerClassName={titleContainerClassName}
            expandable={expandable}
            onClick={this.handleWrapClick}
            {...this.props}
            {...this.state}
          >
            <h2 className={cx(titleClassName, 'ma0 flex flex-row justify-between items-center', { 'grow-1': shareable })}>
              {icon}
              {title}
            </h2>
            {shareable && (
              <>
                {this.state.notification && this.state.notification}
                <span ref={this.shareRef} onClick={this.handleShareClick} className='share pointer hover-shadow-2-royal-blue-3 w-icon-9 h-icon-9 br-50'>
                  <Icon className='db mhauto mt3 color-inherit w-icon-6 h-icon-6' name='content-copy' />
                </span>
              </>
            )}
            {expandable &&
              <>
                {expanded && <Icon className={cx(chevronClassName, 'color-inherit w-icon-6 h-icon-6 pa1 collapse', { ml8: shareable })} name='chevron-down' />}
                {!expanded && <Icon className={cx(chevronClassName, 'color-inherit w-icon-6 h-icon-6 pa1', { ml8: shareable })} name='chevron-right' />}
              </>}
          </Wrap>
        </div>
        <Collapse expandable={expandable} isOpened={expanded}>
          {scrollable ? (
            <ScrollingContainer className={scrollingContainerClassName} stateKey={scrollingStateKey}>
              {children}
            </ScrollingContainer>
          ) : (
            children
          )}
        </Collapse>
        {seoDump &&
          <div style={{ display: 'none' }}>
            {children}
          </div>}
      </>
    )
  }
}

class Wrap extends Component {
  wrapRef = createRef()
  componentDidMount () {
    this.props.expanded && this.props.scrollIntoView && this.wrapRef.current.scrollIntoView()
  }

  render () {
    const { expandable, onClick, children, titleContainerClassName, track, dataTrackCategory, dataTrackLabel, expanded } = this.props
    return expandable ? (
      <button
        ref={this.wrapRef}
        onClick={onClick}
        className={cx(titleContainerClassName, 'pointer w-100 tl flex flex-row justify-between items-center bg-transparent')}
        data-track-click={track}
        data-track-category={dataTrackCategory}
        data-track-action={expanded ? 'Collapse' : 'Expand'}
        data-track-label={dataTrackLabel}
      >
        {children}
      </button>
    ) : (
      <div className={cx(titleContainerClassName, 'flex flex-row justify-between items-center')}>
        {children}
      </div>
    )
  }
}

const Collapse = ({ children, expandable, isOpened }) => {
  return expandable ? (
    <ReactCollapse isOpened={isOpened}>
      {children}
    </ReactCollapse>
  ) : (
    <>{children}</>
  )
}
