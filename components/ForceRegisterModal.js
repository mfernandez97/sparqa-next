import React, { Component } from 'react'
import { _join, _compact } from 'libraries/lodash'
import Grid from '@material-ui/core/Grid'
import Modal from '@material-ui/core/Modal'
import Hidden from '@material-ui/core/Hidden'
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import MuiDoneIcon from '@material-ui/icons/Done'
import withStyles from '@material-ui/core/styles/withStyles'
import { SELECT_PLAN } from 'constants/register-pages'
import StartNowForm from 'components/Account/StartNowForm'
import Link, { LOGIN, REGISTER } from 'components/Link'
import testimonials from 'constants/testimonials'

const personDetails = (name, position, company) => {
  return _join(_compact([name, position, company]), ', ') || ''
}

const theme = createMuiTheme({
  breakpoints: {
    // Define custom breakpoint values.
    // These will apply to Material-UI components that use responsive
    // breakpoints, such as `Grid` and `Hidden`. You can also use the
    // theme breakpoint functions `up`, `down`, and `between` to create
    // media queries for these breakpoints
    values: {
      xs: 0,
      sm: 800,
      md: 900,
      lg: 1000,
      xl: 1200
    }
  }
})

const TickIcon = withStyles({
  root: {
    color: '#f66f4e'
  }
})(MuiDoneIcon)

const Testimonial = () => {
  const testimonial = testimonials.find(testimonial => testimonial.name === 'Sean Nolan')
  return (
    <div className='h-100 pv6'>
      <div className='w-image-1 h-image-1 mr4 tc br-50 top10 overflow-hidden pa2 testimonial-border flt-l ml4'>
        <div className='testimonial-image' style={{ backgroundImage: `url(${testimonial.assetUrl})` }} />
      </div>
      <div>
        <p className='i mv0 fs fxs-m lh-copy fw4'>
          {testimonial.copy}
        </p>
        <p className='mt0 fw6 fs fxs-m'>
          {'- ' + personDetails(testimonial.name, testimonial.position, testimonial.company)}
        </p>
      </div>
    </div>

  )
}

const regInfoText = ['Free access', 'No credit card required', 'Access thousands of legal answers', <>Purchase documents and use <span className='i'>Ask a Lawyer</span> service</>]
const RegInfo = ({ text }) => (
  <Grid item xs={12} className='pv3'>
    <TickIcon className='flt-l mr4 mb3' />
    <span className='fs fxs-m fw6 lh-solid'>
      {text}
    </span>
  </Grid>
)

export default class ForceRegisterModal extends Component {
  path = location.pathname.substr(1)
  render () {
    return (
      <MuiThemeProvider theme={theme}>
        <Modal open={this.props.open} hideBackdrop={this.props.hideBackdrop}>
          <div className='h-100 flex flex-column justify-around layout-width-constraint outline-0'>
            <Grid
              container
              className='forced-register-modal-container br2-1 w-80 mv0 mhauto'
              style={{
                width: '80%'
              }}
            >
              <Grid item xs={12} sm={7} md={7} className='forced-register-input-container bg-white'>
                <Grid container justify='space-between' className='h-100'>
                  <Grid item xs={12}>
                    <div className='mt6 mb4 tc'>
                      <span className='forced-register-modal-title fw7 f-graphic-32 lh-title dark-slate-grey royal-blue-2'>Sign up for free to get answers</span>
                    </div>
                  </Grid>
                  <Grid item xs={12}>
                    <div className='forced-register-modal-input relative mt6 mb8 mh8 tc'>
                      <StartNowForm showText={false} className='mb6' path={this.path} />
                      <p className='fm b'><Link controller={REGISTER} params={{ step: SELECT_PLAN, path: this.path }}>Click here</Link> for unlimited access to our documents</p>
                    </div>
                  </Grid>
                  <Grid item xs={12} className='relative mt6'>
                    <div className='mb5 absolute w-100 bottom0 tc fw6'>
                      <span className='mr3 fs fxs-m grey'>Have an account?</span>
                      <Link className='no-underline steel-blue hover-underline fw6 fs fxs-m' controller={LOGIN} params={{ path: this.path }}>Log in here</Link>
                    </div>
                  </Grid>
                </Grid>
              </Grid>
              <Hidden xsDown>
                <Grid item xs={12} sm={5} className='forced-register-info-container'>
                  <Grid container direction='column' alignItems='stretch' justify='space-between' className='h-100 tl ph7'>
                    <Grid item className='pv6'>
                      <span className='fw7 lh-title fs-m'>Instantly access legal answers for your business</span>
                    </Grid>
                    <Grid item>
                      <Grid container justify='space-between'>
                        {regInfoText.map((text, index) =>
                          <RegInfo key={index} text={text} />
                        )}
                      </Grid>
                    </Grid>
                    <Grid item>
                      <Testimonial />
                    </Grid>
                  </Grid>
                </Grid>
              </Hidden>
            </Grid>
          </div>
        </Modal>
      </MuiThemeProvider>
    )
  }
}
