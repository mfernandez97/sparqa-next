import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import { FormConsumer } from './esv-react-form'
import Icon from 'components/Icon'
import Button, { themes, themeModifiers } from 'components/Button'

const FormSubmit = ({ className, isSubmitting, children, maxWidth = 'none', disabled = false }) => {
  const hoverBg = cx({
    'hover-bg-light-sea-green-saturate-15 hover-shadow-3-black-fade-30': !isSubmitting
  })

  const bg = cx({
    'bg-light-sea-green': !isSubmitting,
    'bg-light-sea-green-fade-40': isSubmitting
  })

  const shadow = cx({
    'shadow-3-black-fade-20': !isSubmitting,
    'shadow-none': isSubmitting
  })

  const cursor = cx({
    'not-allowed': disabled,
    pointer: !isSubmitting,
    'default-cursor': isSubmitting
  })

  return (
    <Button
      type='submit'
      theme={themes.LIGHT_SEA_GREEN}
      themeModifier={themeModifiers.SOLID}
      bg={bg}
      hoverBg={hoverBg}
      cursor={cursor}
      shadow={shadow}
      disabled={disabled || isSubmitting}
      maxWidth={maxWidth}
    >
      {children}
      {isSubmitting && (
        <>
          &nbsp;
          <Icon name='spin2' className='spin-svg mr4' />
        </>
      )}
    </Button>
  )
}

const ConnectedFormSubmit = props => (
  <FormConsumer>
    {({ state }) => (
      <FormSubmit {...props} isSubmitting={state.isSubmitting} />
    )}
  </FormConsumer>
)

FormSubmit.propTypes = {
  children: PropTypes.string.isRequired,
  className: PropTypes.string,
  isSubmitting: PropTypes.bool
}

FormSubmit.defaultProps = {
  className: '',
  isSubmitting: false
}

export default ConnectedFormSubmit
