import React, { Component, createRef } from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import { _debounce } from 'libraries/lodash'
import { Select as BCSelectField } from './esv-react-form/defaultComponents'
import { getBoundingClientRect } from 'front-end-core/helpers/document'
import { Resized, ScrollMain } from 'services/WindowService'
import connect from 'helpers/connect'
import Icon from 'components/Icon'
import Label from './Label'
import ErrorMessages from './ErrorMessages'

class Select extends Component {
  constructor (props) {
    super(props)
    this.state = { open: false }
    this.container = createRef()
  }

  // Listen for all clicks.
  componentDidMount () {
    window.addEventListener('click', this.handleClick)
  }

  // Remove click listener.
  componentWillUnmount () {
    window.removeEventListener('click', this.handleClick)
  }

  openSelect = () => this.setState({ open: true })

  closeSelect = () => this.setState({ open: false })

  handleClick = e => {
    const containerNode = this.container.current
    const clickedOnNode = containerNode.contains(e.target)
    const alreadyOpen = this.state.open
    // Open the dropdown if the user clicks on the select element and the
    // dropdown is currently closed. Otherwise, close it.
    this.setState({ open: clickedOnNode && !alreadyOpen })
  }

  handleClickOption = value => e => {
    e.preventDefault()
    this.props.onChange(value)
  }

  render () {
    const {
      name,
      value,
      label,
      onBlur,
      options,
      showErrors,
      placeholder,
      errorMessages,
      completedWithErrors,
      completedWithoutErrors
    } = this.props
    const selectedOption = options.find(o => o.value === value)
    const containerClassName = 'relative w-100 mb2'
    const notCompleted = !completedWithErrors && !completedWithoutErrors
    const inputClassName = {
      text: {
        'fs tl': true,
        'dark-grey': completedWithErrors,
        'light-sea-green': completedWithoutErrors,
        'dark-grey focus-light-sea-green': notCompleted
      },
      border: {
        'b--crimson': completedWithErrors,
        'br-pill ba focus-outline-0': true,
        'b--grey focus-b--light-sea-green': notCompleted,
        'b--light-sea-green focus-b--light-sea-green': completedWithoutErrors
      },
      layout: 'w-100 pa6'
    }
    return (
      <>
        {label && <Label htmlFor={name}>{label}</Label>}
        <div className={containerClassName} ref={this.container}>
          <button
            type='button'
            onBlur={onBlur}
            className={cx(
              'bg-transparent pointer dropdown-chevron',
              inputClassName.text,
              inputClassName.border,
              inputClassName.layout
            )}
          >
            {selectedOption ? selectedOption.label : placeholder}
          </button>
          {this.state.open && (
            <Dropdown
              onClick={this.handleClickOption}
              options={options}
              selectedOption={selectedOption}
            />
          )}
        </div>
        {showErrors && <ErrorMessages messages={errorMessages} />}
      </>
    )
  }
}

@connect(() => ({ windowDimensions: Resized }))
class Dropdown extends Component {
  /*
    This dropdown:
    a) Has a max height set on it and shows a scroll indicator when the content
       exceeds that height.
    b) Hides the scroll indicator once the user has scrolled all the way to
       the bottom.
    c) Has a smaller max height set if the dropdown overflows the bottom of the
       window.
    d) Recalculates the max height as the user scrolls down the main scroll
       container allowing more content to be visible.
  */
  constructor (props) {
    super(props)
    this.content = createRef()
    this.container = createRef()
    this.state = {
      maxHeight: null,
      scrolledToEnd: false,
      contentOverflow: false,
      isScrollingContent: false
    }
  }

  componentDidMount () {
    // Make sure we set the max height before working out whether the content
    // overflows the container.
    this.setMaxHeight()
    // Recalculate when the user scrolls on the main container.
    this.scrollSubscription = ScrollMain.subscribe(_debounce(async () => {
      this.setMaxHeight()
    }, 100))
  }

  componentWillUnmount () {
    if (this.scrollSubscription) {
      this.scrollSubscription.unsubscribe()
    }
  }

  setMaxHeight = async () => {
    const { windowHeight } = this.props.windowDimensions
    const containerNode = this.container.current
    const containerRect = getBoundingClientRect(containerNode)
    const { top, bottom } = containerRect
    // If the bottom of the dropdown is below the fold...
    if (bottom > windowHeight) {
      // ... then make it shorter so that it fits.
      const approxScrollIndicatorHeight = 28
      const maxHeight = windowHeight - top - approxScrollIndicatorHeight
      // Within reason! Set a min height of 40.
      const minHeight = 40
      await this.setState({
        maxHeight: Math.max(Math.floor(maxHeight), minHeight)
      })
    } else {
      // Otherwise we can just use the max height set by the CSS.
      await this.setState({ maxHeight: null })
    }
    // Check whether content is now overflowing.
    this.setContentOverflow(containerNode)
    // Might have to update scroll indicator visibility.
    this.setScrolledToEnd(containerNode)
  }

  setContentOverflow (containerNode) {
    // Check whether the content overflows the container, so we know whether or
    // not to show the scroll indicator.
    const contentHeight = this.content.current.offsetHeight
    const containerHeight = containerNode.offsetHeight
    this.setState({ contentOverflow: contentHeight > containerHeight })
  }

  setScrolledToEnd = node => {
    // Check whether the user's scrolled all the way to the end of the dropdown.
    // (We hide the scroll indicator when they reach the end.)
    const scrollEnd = node.scrollHeight - node.scrollTop === node.clientHeight
    this.setState({ scrolledToEnd: scrollEnd })
  }

  handleScroll = e => {
    // Stop scroll events firing on main scroll and messing everything up.
    e.stopPropagation()
    // Check whether user has scrolled to the end of the dropdown.
    this.setScrolledToEnd(e.target)
  }

  render () {
    const { contentOverflow, scrolledToEnd, maxHeight } = this.state
    const { onClick, options, selectedOption } = this.props
    const containerStyle = maxHeight ? { maxHeight: `${maxHeight}px` } : {}
    return (
      <div
        className={cx(
          'absolute z5 w-100 left0 bottom0 y-100 pt4 br2 shadow-3-black-fade-30',
          { pb7: contentOverflow }
        )}
      >
        <div
          ref={this.container}
          style={containerStyle}
          onScroll={this.handleScroll}
          className={cx(
            'bg-white w-100 br2 max-h-measure-1 overflow-y-auto',
            { 'br--top': contentOverflow }
          )}
        >
          <div ref={this.content}>
            {options.map((props, index) => (
              <Option
                {...props}
                key={props.value}
                last={index === options.length - 1}
                first={index === 0}
                onClick={onClick}
                selected={selectedOption ? props.value === selectedOption.value : false}
              />
            ))}
          </div>
        </div>
        {contentOverflow && <ScrollIndicator ref={this.scrollIndicator} hideIcon={scrolledToEnd} />}
      </div>
    )
  }
}

const Option = ({ last, first, label, value, onClick, selected }) => {
  const className = {
    text: { 'tl fs': true, 'dark-grey': !selected, 'light-sea-green fw6': selected },
    hover: 'hover-bg-light-sea-green-fade-20 pointer',
    border: { 'br--top br2': first, 'br2 br--bottom': last },
    layout: 'w-100 db pv4 ph6'
  }
  return (
    <button
      onClick={onClick(value)}
      className={cx(
        'pointer bg-white',
        className.text,
        className.hover,
        className.border,
        className.layout
      )}
    >
      {label}
    </button>
  )
}

const ScrollIndicator = ({ hideIcon }) => (
  <div className='w-100 flex justify-center items-center pv1 bg-white absolute bottom0 left0 br2 br--bottom'>
    <Icon className={cx('w-icon-5 h-icon-5', { grey: !hideIcon, white: hideIcon })} name='chevron-down' />
  </div>
)

Select.propTypes = {
  ...BCSelectField.propTypes,
  completedWithErrors: PropTypes.bool.isRequired,
  completedWithoutErrors: PropTypes.bool.isRequired
}

Select.defaultProps = {
  ...BCSelectField.defaultProps
}

export default Select
