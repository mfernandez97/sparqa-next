import React from 'react'

const FormInstructions = ({ children }) => (
  <p className='fm lh-copy grey mb8 last-child-mb0'>{children}</p>
)

export default FormInstructions
