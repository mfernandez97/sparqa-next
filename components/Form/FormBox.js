import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'

const FormBox = ({
  title = null,
  boxShadow = false,
  children,
  className = '',
  titleClassName = ''
}) => (
  <div
    className={cx(
      className,
      'min-w-measure-3 min-w-auto-m max-w-none-m br2 pa8 ph6-t ph5-m bg-white relative',
      { 'shadow-3-light-sea-green-fade-20': boxShadow }
    )}
  >
    {title && <h2 className={cx('ma0 mb8 b fm tracked grey mb6-m', titleClassName)}>{title}</h2>}
    {children}
  </div>
)

FormBox.propTypes = {
  title: PropTypes.node,
  boxShadow: PropTypes.bool,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  titleClassName: PropTypes.string
}

export default FormBox
