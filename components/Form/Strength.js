import React from 'react'
import cx from 'classnames'

const STRENGTH_LEVELS = [
  {
    level: 'Weak',
    bgColor: 'bg-crimson'
  },
  {
    level: 'Getting there',
    bgColor: 'bg-goldenrod'
  },
  {
    level: 'OK',
    bgColor: 'bg-medium-sea-green'
  },
  {
    level: 'Strong',
    bgColor: 'bg-forest-green'
  },
  {
    level: 'Very Strong',
    bgColor: 'bg-dark-green'
  }
]

export const StrengthBar = ({ value = '', strength }) => {
  const bgColor = STRENGTH_LEVELS[strength].bgColor
  return value.length > 0 ? (
    <div className='mt4 mb2 h-icon-1 relative'>
      <span
        className={cx('h-100 db transition-all', bgColor)}
        style={{
          width: `${(100 / (STRENGTH_LEVELS.length)) * (strength + 1)}%`
        }}
      />
    </div>
  ) : null
}

export const StrengthBadge = ({ value = '', strength }) => {
  const bgColor = STRENGTH_LEVELS[strength].bgColor
  const baseClassName = 'transition-all pv2 ph4 white br-pill fxs mt1 ml2 mr6'
  return value.length > 0 ? (
    <span className={cx(baseClassName, bgColor || 'bg-grey')}>
      {STRENGTH_LEVELS[strength].level}
    </span>
  ) : null
}
