import React, { useEffect, useState } from 'react'
import cx from 'classnames'
import { _debounce, _isFunction } from 'libraries/lodash'
import { getClassName } from 'helpers/misc'
import Icon from 'components/Icon'
import ErrorMessages from './ErrorMessages'
import TextField, { getDefaultClassNames } from './TextField'
import { StrengthBar, StrengthBadge } from './Strength'
import { isPasswordStrong } from 'services/UserService'

const PasswordField = ({
  allowUnmasked = false,
  showStrengthBar = false,
  showStrengthBadge = false,
  ...props
}) => (
  <TextField
    type='password'
    {...props}
    allowUnmasked={allowUnmasked}
    showStrengthBar={showStrengthBar}
    showStrengthBadge={showStrengthBadge}
    CustomInputComponent={CustomPasswordField}
    showErrors={false}
  />
)

const CustomPasswordField = React.forwardRef(({
  allowUnmasked,
  showStrengthBar,
  showStrengthBadge,
  containerClassName = '',
  completedWithErrors,
  completedWithoutErrors,
  ...props
},
ref
) => {
  const [focused, setFocused] = useState(false)
  const [unmasked, setUnmasked] = useState(false)

  const [strength, setStrength] = useState(0)
  const [errorMessages, setErrorMessages] = useState([])

  useEffect(() => {
    async function fetchStrength () {
      if (ref.current.value.length > 0) {
        const response = await isPasswordStrong(ref.current.value)
        setStrength(response.score)
        setErrorMessages([response.reason])
      }
    }
    fetchStrength()
  }, [])

  const type = allowUnmasked && unmasked ? 'text' : 'password'
  const focusedAndNotCompleted = focused && (!completedWithErrors && !completedWithoutErrors)

  const className = getClassName({}, { ...props, className: containerClassName })

  const defaults = getDefaultClassNames({ completedWithErrors, completedWithoutErrors })
  const innerClassName = getClassName(defaults, {
    className: 'justify-center items-center',
    display: 'flex',
    padding: 'pa0',
    color: cx({
      [defaults.color]: !focusedAndNotCompleted && !completedWithoutErrors,
      'light-sea-green': focusedAndNotCompleted || completedWithoutErrors
    }),
    borderColor: cx({
      [defaults.borderColor]: !focusedAndNotCompleted && !completedWithoutErrors,
      'b--light-sea-green': focusedAndNotCompleted || completedWithoutErrors
    })
  })

  // TODOs: This debouncing may not be up-to-scratch.
  const updateStrength = _debounce(event => {
    isPasswordStrong(event.target.value).then(response => {
      setStrength(response.score)
      const reason = (response.reason == null) ? [] : [response.reason]
      setErrorMessages(reason)
    })
  }, 200, { leading: true })

  return (
    <div className={className}>
      <div className={innerClassName}>
        <input
          ref={ref}
          {...props}
          className='grow-1 pa6 bn focus-outline-0 br-pill fs color-inherit'
          type={type}
          onFocus={e => {
            _isFunction(props.onFocus) && props.onFocus(e)
            setFocused(true)
          }}
          onBlur={e => {
            _isFunction(props.onBlur) && props.onBlur(e)
            setFocused(false)
          }}
          onChange={e => {
            _isFunction(props.onChange) && props.onChange(e)
            updateStrength(e)
          }}
        />
        {showStrengthBadge && <StrengthBadge value={props.value} strength={strength} />}
        {allowUnmasked && <ToggleUnmasked unmasked={unmasked} onClick={() => setUnmasked(!unmasked)} />}
      </div>
      {showStrengthBar && <StrengthBar value={props.value} strength={strength} />}
      {<ErrorMessages messages={errorMessages} />}
    </div>
  )
})

const ToggleUnmasked = ({ unmasked, onClick }) => (
  <button
    type='button'
    onClick={onClick}
    className='h-icon-7 flex grow-0 ml2 mr6 pv1 transition-all hover-bg-moon-grey ph4 dark-grey bg-light-grey br-pill fxs pointer ba b--light-grey focus-b--light-sea-green'
  >
    <Icon name={unmasked ? 'eye-off' : 'eye'} className='w-icon-5' />
  </button>
)

export default PasswordField
