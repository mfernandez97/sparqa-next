import React, { Fragment, useState } from 'react'
import cx from 'classnames'
import Icon from 'components/Icon'
import ErrorMessages from './ErrorMessages'

const CheckboxField = ({
  name,
  value: checked,
  label,
  onBlur: onBlurProp,
  onChange,
  className,
  showErrors,
  errorMessages,
  customInputProps,
  completedWithErrors,
  completedWithoutErrors
}) => {
  const [focused, setFocused] = useState(false)
  const onFocus = () => setFocused(true)
  const onBlur = e => {
    setFocused(false)
    onBlurProp(e)
  }

  const containerClassName = {
    misc: 'pointer outline-0 focus-outline-0 relative shadow-5-black-fade-10 shrink-0',
    layout: 'mr4 w-icon-5 h-icon-5 db',
    border: {
      'ba br2': true,
      'b--silver': !checked && !completedWithErrors && !focused,
      'b--light-sea-green': (checked && !completedWithErrors) || focused,
      'b--crimson': completedWithErrors
    },
    background: { 'bg-light-sea-green': checked }
  }
  return (
    <Fragment>
      <label className={cx('flex fs', { 'mb2': showErrors })}>
        <span
          className={cx(
            containerClassName.misc,
            containerClassName.layout,
            containerClassName.border,
            containerClassName.background
          )}
        >
          <input
            type='checkbox'
            name={name}
            onBlur={onBlur}
            onFocus={onFocus}
            checked={checked}
            onChange={e => onChange(!checked)}
            className='w-100 h-100 o-0 pointer'
            {...customInputProps}
          />
          {checked && (
            <span className={cx('flex items-center justify-center absolute center no-pointer-events transparent pb1')}>
              <Icon className='white' name='check' />
            </span>
          )}
        </span>
        {label}
      </label>
      {showErrors && <ErrorMessages messages={errorMessages} />}
    </Fragment>
  )
}

export default CheckboxField
