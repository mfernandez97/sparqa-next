import React, { Component, Fragment, createRef } from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import { getClassName } from 'helpers/misc'
import { Text as BCTextField } from './esv-react-form/defaultComponents'
import Label from './Label'
import ErrorMessages from './ErrorMessages'

class TextField extends Component {
  inputEl = createRef()
  autocompletePoll = null

  componentDidMount () {
    if (!this.props.readOnly) {
      this.autocompletePoll = setInterval(this.detectAutocomplete, 100)
    }
  }

  componentWillUnmount () {
    this.clearAutocompletePoll()
  }

  clearAutocompletePoll = () => {
    this.autocompletePoll && clearInterval(this.autocompletePoll)
    this.autocompletePoll = null
  }

  detectAutocomplete = () => {
    const inputNode = this.inputEl.current
    if (inputNode && inputNode.value.length > 0) {
      this.clearAutocompletePoll()
      this.props.onChange(inputNode.value)
    }
  }

  onChange = e => {
    this.clearAutocompletePoll()
    this.props.onChange(e.currentTarget.value)
  }

  render () {
    const {
      type,
      name,
      value,
      label,
      onBlur,
      readOnly,
      showErrors,
      placeholder,
      allowUnmasked,
      errorMessages,
      nodeAfterInput,
      nodeBeforeInput,
      showStrengthBar,
      customInputProps,
      showStrengthBadge,
      containerClassName,
      completedWithErrors,
      CustomInputComponent = null,
      completedWithoutErrors,
      ...props
    } = this.props

    if (readOnly) {
      return <ReadOnlyTextField {...this.props} />
    }

    const defaults = getDefaultClassNames({ completedWithErrors, completedWithoutErrors })
    const className = getClassName(defaults, props)

    return (
      <Fragment>
        {!!label && <Label htmlFor={name}>{label}</Label>}
        {nodeBeforeInput}
        {CustomInputComponent ? (
          <CustomInputComponent
            ref={this.inputEl}
            type={type}
            className={className}
            name={name}
            value={value}
            onBlur={onBlur}
            onChange={this.onChange}
            placeholder={placeholder}
            allowUnmasked={allowUnmasked}
            showStrengthBar={showStrengthBar}
            showStrengthBadge={showStrengthBadge}
            containerClassName={containerClassName}
            completedWithErrors={completedWithErrors}
            completedWithoutErrors={completedWithoutErrors}
            {...customInputProps}
          />
        ) : (
          <input
            ref={this.inputEl}
            type={type}
            className={className}
            name={name}
            value={value}
            onBlur={onBlur}
            onChange={this.onChange}
            placeholder={placeholder}
            {...customInputProps}
          />
        )}
        {nodeAfterInput}
        {showErrors && <ErrorMessages messages={errorMessages} />}
      </Fragment>
    )
  }
}

TextField.propTypes = {
  ...BCTextField.propTypes,
  type: PropTypes.string,
  readOnly: PropTypes.bool,
  nodeAfterInput: PropTypes.node,
  nodeBeforeInput: PropTypes.node,
  customInputProps: PropTypes.object,
  completedWithErrors: PropTypes.bool.isRequired,
  completedWithoutErrors: PropTypes.bool.isRequired
}

TextField.defaultProps = {
  ...BCTextField.defaultProps,
  type: 'text',
  readOnly: false,
  nodeAfterInput: null,
  nodeBeforeInput: null,
  customInputProps: {}
}

const ReadOnlyTextField = ({
  type,
  name,
  value,
  label,
  placeholder,
  nodeAfterInput,
  nodeBeforeInput,
  customInputProps,
  ...props
}) => {
  const defaults = getDefaultClassNames({
    readOnly: true,
    completedWithErrors: false,
    completedWithoutErrors: false
  })
  const className = getClassName(defaults, props)
  return (
    <Fragment>
      {!!label && <Label htmlFor={name}>{label}</Label>}
      {nodeBeforeInput}
      <input
        type={type}
        className={className}
        name={name}
        value={value}
        readOnly
        placeholder={placeholder}
        {...customInputProps}
      />
      {nodeAfterInput}
    </Fragment>
  )
}

export const getDefaultClassNames = ({ completedWithErrors, completedWithoutErrors, readOnly = false }) => {
  const notCompleted = !completedWithErrors && !completedWithoutErrors
  return {
    width: 'w-100',
    display: 'db',
    br: 'br-pill',
    margin: 'mt0 mh0 mb2',
    padding: 'pa6',
    outline: 'focus-outline-0',
    fontSize: 'fs',
    border: cx({ 'ba': !readOnly, 'bn': readOnly }),
    bg: cx({ 'bg-near-white': readOnly }),
    color: cx({
      'focus-light-sea-green': notCompleted && !readOnly,
      'dark-grey': !completedWithoutErrors || readOnly,
      'light-sea-green': completedWithoutErrors && !readOnly
    }),
    borderColor: cx({
      'b--grey focus-b--light-sea-green': notCompleted,
      'b--light-sea-green focus-b--light-sea-green': completedWithoutErrors,
      'b--crimson': completedWithErrors
    })
  }
}

export default TextField
