import React, { Fragment, useState } from 'react'
import { getClassName } from 'helpers/misc'
import CardInput from 'components/Stripe/CardInput'
import { getDefaultClassNames as getTextFieldClassNames } from './TextField'
import ErrorMessages from './ErrorMessages'

const defaults = getTextFieldClassNames({
  completedWithErrors: false,
  completedWithoutErrors: false
})

const StripeCardField = ({ label, disabled }) => {
  const [ stripeErrorMessage, setStripeErrorMessage ] = useState(null)
  const showErrors = !!stripeErrorMessage
  const className = getClassName(defaults, { className: 'h-100' })
  return (
    <Fragment>
      <span className='fxs grey db mb4'>{label}</span>
      <CardInput
        disabled={disabled}
        baseClassName={className}
        setErrorMessage={setStripeErrorMessage}
      />
      {showErrors && <ErrorMessages messages={[ stripeErrorMessage ]} />}
    </Fragment>
  )
}

export default StripeCardField
