import React from 'react'
import Form from './esv-react-form'
import ErrorMessages from './ErrorMessages'
import FormSubmit from './FormSubmit'
import FormBox from './FormBox'
import FormFooter from './FormFooter'
import TextField, { getDefaultClassNames as getTextFieldClassNames } from './TextField'
import PasswordField from './PasswordField'
import EmailField from './EmailField'
import Switch from './Switch'
import SelectField from './SelectField'
import CheckboxField from './CheckboxField'
import RadioField from './RadioField'
import StripeCardField from './StripeCardField'
import FormInstructions from './FormInstructions'

const CustomForm = props => (
  <Form
    {...props}
    customComponents={{
      text: TextField,
      email: EmailField,
      radio: RadioField,
      select: SelectField,
      stripe: StripeCardField,
      password: PasswordField,
      checkbox: CheckboxField
    }}
  />
)

export {
  FormConsumer,
  Field,
  FieldConsumer,
  insist
} from './esv-react-form'

export { FormSubmit, FormFooter, ErrorMessages, getTextFieldClassNames, FormBox, Switch, FormInstructions }

export default CustomForm
