import React, { useState } from 'react'
import PropTypes from 'prop-types'
import ErrorMessages from './ErrorMessages'
import cx from 'classnames'
import Icon from 'components/Icon'

const Radio = ({
  name,
  label,
  options,
  showErrors,
  errorMessages,
  ...props
}) => (
  <div className='flex flex-column mb4 fs'>
    {label && <label className='mb2' htmlFor={name}>{label}</label>}
    {options.map(({ label, value }, index) => (
      <RadioOption
        {...props}
        key={value}
        last={index === options.length}
        name={name}
        label={label}
        inputValue={value}
      />
    ))}
    {showErrors && <ErrorMessages messages={errorMessages} />}
  </div>
)

const RadioOption = ({
  last,
  name,
  value,
  label,
  onBlur: onBlurProp,
  onChange,
  inputValue,
  completedWithErrors
}) => {
  const [focused, setFocused] = useState(false)
  const onFocus = () => setFocused(true)
  const onBlur = e => {
    setFocused(false)
    onBlurProp(e)
  }

  const checked = inputValue === value
  const containerClassName = {
    misc: 'pointer outline-0 focus-outline-0 relative shadow-5-black-fade-10',
    layout: 'mr4 w-icon-5 h-icon-5 db',
    border: {
      'ba br-100': true,
      'b--silver': !checked && !completedWithErrors && !focused,
      'b--light-sea-green': (checked && !completedWithErrors) || focused,
      'b--crimson': completedWithErrors
    },
    background: { 'bg-light-sea-green': checked }
  }
  return (
    <label className={cx('fs flex', { 'mb2': !last })}>
      <span
        className={cx(
          containerClassName.misc,
          containerClassName.layout,
          containerClassName.border,
          containerClassName.background
        )}
      >
        <input
          type='radio'
          name={name}
          value={inputValue}
          onBlur={onBlur}
          onFocus={onFocus}
          checked={checked}
          onChange={e => onChange(e.target.value)}
          className='w-100 h-100 pointer o-0'
        />
        {checked && (
          <span className={cx('flex items-center justify-center absolute center no-pointer-events transparent')}>
            <Icon className='white' name='check' />
          </span>
        )}
      </span>
      {label}
    </label>

  )
}

Radio.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.string
    })
  ).isRequired,
  showErrors: PropTypes.bool,
  errorMessages: PropTypes.arrayOf(PropTypes.string)
}

Radio.defaultProps = {
  label: null,
  showErrors: false,
  errorMessages: []
}

RadioOption.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  label: PropTypes.string,
  onBlur: PropTypes.func,
  onChange: PropTypes.func.isRequired,
  inputValue: PropTypes.string.isRequired
}

RadioOption.defaultProps = {
  label: null,
  value: null,
  onBlur: () => null
}

export default Radio
