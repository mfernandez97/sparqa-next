import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'

const Switch = ({ name, value: checked, label, onChange }) => {
  const containerClassName = 'flex'
  const switchBackgroundClassName = cx(
    'relative dib left0 min-w-icon-8 w-icon-10 h-icon-5 br-pill mr5',
    { 'bg-light-sea-green': checked, 'bg-silver': !checked }
  )
  const switchCircleClassName = cx(
    'absolute no-pointer-events br-100 h-icon-3 w-icon-3 w-icon-3-m bg-white center-v transition-all',
    { 'left2': !checked, 'switch-on-pill-left': checked }
  )
  const labelClassName = cx('grey', { 'fw6': checked })
  return (
    <div className={containerClassName}>
      <span className={switchBackgroundClassName}>
        <input
          type='checkbox'
          name={name}
          checked={checked}
          onChange={onChange}
          className='pointer w-100 h-100 o-0'
        />
        <span className={switchCircleClassName} />
      </span>
      <label className={labelClassName} htmlFor={name}>{label}</label>
    </div>
  )
}

Switch.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
}

export default Switch
