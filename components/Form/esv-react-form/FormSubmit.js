import React from 'react'
import PropTypes from 'prop-types'
import { FormConsumer } from './Form'

const FormSubmit = ({ value, isSubmitting }) => (
  <input type='submit' value={value} disabled={isSubmitting} />
)

const ConnectedFormSubmit = props => (
  <FormConsumer>
    {({ state }) => (
      <FormSubmit {...props} isSubmitting={state.isSubmitting} />
    )}
  </FormConsumer>
)

FormSubmit.propTypes = {
  value: PropTypes.string,
  isSubmitting: PropTypes.bool
}

FormSubmit.defaultProps = {
  value: 'Submit',
  isSubmitting: false
}

export default ConnectedFormSubmit
