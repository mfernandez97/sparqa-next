import { _difference, _keys, _reduce, _pick, _mergeWith, _compact } from 'libraries/lodash'

export const initSchema = ({ values, initialSchema }) => {
  // You know what's easier than dealing with "cannot read property 'rules' of
  // undefined" errors all over the place?
  const missingValidation = _difference(_keys(values), _keys(initialSchema))
  const schema = {
    ...initialSchema,
    // Going through all the fields without validation rules...
    ...missingValidation.reduce((result, name) => {
      // And setting rules to an empty array.
      result[name] = { rules: [] }
      return result
    }, {})
  }
  return schema
}

export const initErrorMessages = ({ schema }) => {
  // Initial shape for error messages.
  return _keys(schema)
    .reduce((result, name) => {
      result[name] = []
      return result
    }, {})
}

export const initErrorChecker = ({ schema }) => {
  // Some fields have a 'sameAs' validation method on them, i.e. they are valid
  // only if they have the same value as some other specified field.
  // This means that we not only have to check whether they pass their rules when
  // they are being edited, but also when the field they are supposed to match
  // is being edited.
  // We start initialising the error checking method by recording which other
  // fields we need to check when a given field is being edited.

  // For every entry in the schema...
  const sameAsFields = _reduce(schema, (result, { rules }, name) => {
    // Go through all its rules...
    const sameAsFieldsForEntry = rules.reduce(
      (result, { name: ruleName, param: fieldToMatch }) => {
        // And if any have the name 'sameAs'...
        if (ruleName === 'sameAs') {
          // Then make a note of the pair.
          // E.g. if `repeatPassword` should match `password`, then this will
          // add: { password: repeatPassword } }
          result[fieldToMatch] = name
        }
        return result
      }, {})
    // This returns an object like: { password: [ repeatPassword ] }
    // Just in case some fields have more than one other field to match!
    return _mergeWith(
      result,
      sameAsFieldsForEntry,
      (valueOne, valueTwo) => _compact([valueOne].concat([valueTwo]))
    )
  }, {})

  return errorChecker({ schema, sameAsFields })
}

const errorChecker = ({ schema, sameAsFields }) => async ({ name = null, values }) => {
  // If the name is provided then we only check the error messages for that field.
  if (name) {
    const alsoCheck = sameAsFields[name] || []
    const valuesToCheck = _pick(values, [name, ...alsoCheck])
    return checkForErrors({ valuesToCheck, schema, values })
  }
  // If no name is provided then we check all for error messages for every field.
  return checkForErrors({ valuesToCheck: values, schema, values })
}

const checkForErrors = ({ valuesToCheck, values, schema }) => {
  // Go through all the values we need to validate...
  const errorMessages = _reduce(
    valuesToCheck,
    // And do it in a way that's async because we're advanced like that...
    async (accumulator, _, name) => {
      const result = await Promise.resolve(accumulator)
      // For each value, collect the error messages...
      result[name] = await checkValueAgainstRules({
        rules: schema[name].rules,
        value: valuesToCheck[name],
        values
      })
      return Promise.resolve(result)
    }, Promise.resolve({}))
  // All the error messages for all the values we were checking, keyed by name.
  return errorMessages
}

const checkValueAgainstRules = async ({ rules, value, values }) => {
  // For each rule...
  return rules.reduce(
    // And again, rules can be async...
    async (accumulator, { rule, message }) => {
      const result = await Promise.resolve(accumulator)
      // Check whether the value provided passes the rule...
      const check = await Promise.resolve(rule(value, values))
      if (!check) {
        // And if it doesn't then add the error message for that rule to the
        // results array.
        result.push(message)
      }
      return Promise.resolve(result)
    }, Promise.resolve([]))
}
