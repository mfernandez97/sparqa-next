import React, { Component, createContext } from 'react'
import PropTypes from 'prop-types'
import { _pick } from 'libraries/lodash'
import { createErrorLogger } from './logger'
import { FormConsumer } from './Form'
import { Text, Password, Radio, Checkbox, Select } from './defaultComponents'

const { Provider, Consumer: FieldConsumer } = createContext()

const { logError } = createErrorLogger('FIELD ERROR:')

const defaultComponents = {
  text: Text,
  radio: Radio,
  select: Select,
  checkbox: Checkbox,
  password: Password
}

class Field extends Component {
  state = { touched: false }

  get showErrors () {
    const { touched } = this.state
    const { errorMessages, showAllErrors } = this.props
    return (touched && errorMessages.length > 0) || showAllErrors
  }

  get completedWithErrors () {
    return this.state.touched && this.props.errorMessages.length > 0
  }

  get completedWithoutErrors () {
    return this.state.touched && this.props.errorMessages.length <= 0
  }

  onBlur = e => {
    this.setState({ touched: true })
  }

  render () {
    const { type = 'text', children, customComponents } = this.props

    const componentProps = {
      ...this.props,
      onBlur: this.onBlur,
      setValue: this.props.onChange,
      showErrors: this.showErrors,
      completedWithErrors: this.completedWithErrors,
      completedWithoutErrors: this.completedWithoutErrors
    }

    // For creating one-off custom fields
    if (children) {
      return (
        <Provider value={componentProps}>
          <FieldConsumer>
            {children}
          </FieldConsumer>
        </Provider>
      )
    }

    // For custom fields defined in the config
    const CustomComponent = customComponents[type] || null
    if (CustomComponent) {
      return <CustomComponent {...componentProps} />
    }

    // The default field component
    const DefaultComponent = defaultComponents[type] || null
    if (DefaultComponent) {
      return <DefaultComponent {...componentProps} />
    }

    switch (type) {
      case 'submit':
        logError(
          `There is no default field component for ${type} input types.
          Please consider using the FormSubmit component or creating a custom component.`
        )
        break
      default:
        logError(
          `${type} input fields are not yet supported by default; please create your own custom component.`
        )
    }

    return null
  }
}

const ConnectedField = props => (
  <FormConsumer>
    {({ state, actions, config }) => {
      const { name, type, overrideValue = null } = props
      actions.registerField(name, type)
      const value = state.values[name]
      const onChange = actions.onChange(name)
      const errorMessages = state.errorMessages[name]
      const rules = config.schema[name].rules.map(
        rule => _pick(rule, ['name', 'param'])
      )
      return (
        <Field
          {...props}
          value={overrideValue || value}
          rules={rules}
          onChange={onChange}
          errorMessages={errorMessages}
          showAllErrors={state.showAllErrors}
          customComponents={config.customComponents}
        />
      )
    }}
  </FormConsumer>
)

Field.propTypes = {
  value: PropTypes.any,
  rules: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      param: PropTypes.any
    })
  ).isRequired,
  onChange: PropTypes.func.isRequired,
  errorMessages: PropTypes.arrayOf(PropTypes.string),
  showAllErrors: PropTypes.bool,
  customComponents: PropTypes.object
}

Field.defaultProps = {
  showAllErrors: false,
  customComponents: {}
}

export {
  FieldConsumer,
  ConnectedField as default
}
