import { _isNil, _isArray, _isString, _isBoolean, _includes } from 'libraries/lodash'
import { createErrorLogger } from './logger'

const EMAIL_REGEX = /^[^\s@]+@[^\s@]+\.[^\s@]+$/

const { logError, throwError } = createErrorLogger('VALIDATION ERROR:')

const validation = () => {
  const rules = []
  return {
    rules,

    custom (rule, { message = null, name = null } = {}) {
      if (!message || !name) {
        throwError('Please supply a name and message for your custom validation rule.')
      }
      rules.push({ name, rule, message })
      return this
    },

    email ({ message = 'Please enter a valid email address.' } = {}) {
      rules.push({ name: 'email', rule: value => EMAIL_REGEX.test(value), message })
      return this
    },

    isTrue ({ message = 'This is required.' } = {}) {
      rules.push({ name: 'isTrue', rule: value => _isBoolean(value) && value, message })
      return this
    },

    length (x, options = {}) {
      if (_isNil(x)) {
        logError('The `length` validation method requires a length.')
      }
      const message = options.message ? options.message : `Must be at least ${x} characters.`
      rules.push({ name: 'length', param: x, rule: value => _isString(value) && value.length >= x, message })
      return this
    },

    oneOf (values, options = {}) {
      if (_isNil(values) || !_isArray(values)) {
        logError('The `oneOf` validation method requires an array of values to match.')
      }
      const message = options.message ? options.message : `Must be one of ${values.map(value => `'${value}'`).join(', ')}.`
      rules.push({ name: 'oneOf', param: values, rule: value => _includes(values, value), message })
      return this
    },

    regex (expression, { message = null } = {}) {
      if (!message || !expression) {
        throwError('The regex method requires a regular expression to match and a message.')
      }
      rules.push({ name: 'regex', param: expression, rule: value => expression.test(value), message })
      return this
    },

    required ({ message = 'This field is required.' } = {}) {
      rules.push({ name: 'required', rule: value => !_isNil(value) && value.length > 0, message })
      return this
    },

    sameAs (fieldToMatch, options = {}) {
      if (_isNil(fieldToMatch)) {
        logError('The `matches` validation method requires the name of a field to match.')
      }
      const message = options.message ? options.message : `Must be the same as ${fieldToMatch}.`
      rules.push({
        name: 'sameAs',
        param: fieldToMatch,
        rule: (value, values) => {
          if (_isNil(values[fieldToMatch])) {
            logError(`There is no value with the name ${fieldToMatch}.`)
          }
          return value === values[fieldToMatch]
        },
        message
      })
      return this
    }
  }
}

export default validation
