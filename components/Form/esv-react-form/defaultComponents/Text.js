import React from 'react'
import PropTypes from 'prop-types'
import ErrorBox from './ErrorBox'

const Text = ({
  name,
  value,
  label,
  onBlur,
  onChange,
  showErrors,
  placeholder,
  errorMessages
}) => (
  <label className='mb6' htmlFor={name}>
    {label}
    <br />
    <input
      type='text'
      name={name}
      value={value}
      onBlur={onBlur}
      placeholder={placeholder}
      onChange={e => onChange(e.currentTarget.value)}
    />
    <ErrorBox showErrors={showErrors} errorMessages={errorMessages} />
  </label>
)

Text.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  value: PropTypes.string.isRequired,
  onBlur: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  showErrors: PropTypes.bool,
  placeholder: PropTypes.string,
  errorMessages: PropTypes.arrayOf(PropTypes.string)
}

Text.defaultProps = {
  label: null,
  showErrors: false,
  placeholder: null,
  errorMessages: []
}

export default Text
