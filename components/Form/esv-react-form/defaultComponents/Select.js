import React from 'react'
import PropTypes from 'prop-types'
import ErrorBox from './ErrorBox'

const Select = ({
  name,
  value,
  label,
  onBlur,
  options,
  onChange,
  showErrors,
  placeholder,
  errorMessages
}) => (
  <div className='mb6'>
    <label htmlFor={name}>{label}</label>
    <br />
    <select
      name={name}
      value={value}
      onChange={e => onChange(e.target.value)}
    >
      {placeholder && <option value=''>{placeholder}</option>}
      {options.map(({ label, value }) => <option key={value} value={value}>{label}</option>)}
    </select>
    <ErrorBox showErrors={showErrors} errorMessages={errorMessages} />
  </div>
)

Select.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  label: PropTypes.string,
  onBlur: PropTypes.func,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.string
    })
  ).isRequired,
  onChange: PropTypes.func.isRequired,
  showErrors: PropTypes.bool,
  placeholder: PropTypes.string,
  errorMessages: PropTypes.arrayOf(PropTypes.string)
}

Select.defaultProps = {
  label: null,
  onBlur: () => null,
  showErrors: false,
  placeholder: null,
  errorMessages: []
}

export default Select
