import React from 'react'
import PropTypes from 'prop-types'
import ErrorBox from './ErrorBox'

const Radio = ({
  name,
  label,
  options,
  showErrors,
  errorMessages,
  ...props
}) => (
  <div className='flex flex-column mb4'>
    {label && <label htmlFor={name}>{label}</label>}
    {options.map(({ label, value }) => (
      <RadioOption
        {...props}
        key={value}
        name={name}
        label={label}
        inputValue={value}
      />
    ))}
    <ErrorBox showErrors={showErrors} errorMessages={errorMessages} />
  </div>
)

const RadioOption = ({
  name,
  value,
  label,
  onBlur,
  onChange,
  inputValue
}) => (
  <label className='fs'>
    <input
      type='radio'
      name={name}
      value={inputValue}
      onBlur={onBlur}
      checked={inputValue === value}
      onChange={e => onChange(e.target.value)}
      className='mr4'
    />
    {label}
  </label>
)

Radio.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.string
    })
  ).isRequired,
  showErrors: PropTypes.bool,
  errorMessages: PropTypes.arrayOf(PropTypes.string)
}

Radio.defaultProps = {
  label: null,
  showErrors: false,
  errorMessages: []
}

RadioOption.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  label: PropTypes.string,
  onBlur: PropTypes.func,
  onChange: PropTypes.func.isRequired,
  inputValue: PropTypes.string.isRequired
}

RadioOption.defaultProps = {
  label: null,
  value: null,
  onBlur: () => null
}

export default Radio
