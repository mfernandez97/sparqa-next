import React from 'react'
import PropTypes from 'prop-types'

const ErrorBox = ({ showErrors, errorMessages }) => showErrors ? (
  <div className='mt2'>
    {errorMessages.map(message => (
      <span key={message} className='crimson db fs'>{message}</span>
    ))}
  </div>
) : null

ErrorBox.propTypes = {
  showErrors: PropTypes.bool.isRequired,
  errorMessages: PropTypes.arrayOf(PropTypes.string)
}

ErrorBox.defaultProps = {
  errorMessages: []
}

export default ErrorBox
