import React from 'react'
import PropTypes from 'prop-types'
import ErrorBox from './ErrorBox'

const Checkbox = ({
  name,
  value,
  label,
  onChange,
  showErrors,
  errorMessages
}) => {
  return (
    <label className='mb4'>
      <input
        type='checkbox'
        name={name}
        checked={value}
        onChange={e => onChange(!value)}
        className='mr4'
      />
      {label}
      <ErrorBox showErrors={showErrors} errorMessages={errorMessages} />
    </label>
  )
}

Checkbox.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  value: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  showErrors: PropTypes.bool,
  errorMessages: PropTypes.arrayOf(PropTypes.string)
}

Checkbox.defaultProps = {
  label: null,
  showErrors: false,
  errorMessages: []
}

export default Checkbox
