import React, { Component, createContext } from 'react'
import PropTypes from 'prop-types'
import { _has, _values, _concat, _flatten, _debounce } from 'libraries/lodash'
import { initSchema, initErrorChecker, initErrorMessages } from './validation'

const { Provider, Consumer: FormConsumer } = createContext()

class FormContainer extends Component {
  constructor (props) {
    super(props)
    const { initialValues } = props
    // Initialise the schema.
    const schema = initSchema({
      values: initialValues,
      initialSchema: this.props.schema
    })
    // All the form inputs are controlled inputs and their values are provided
    // by the FormContainer.
    this.state = {
      values: initialValues,
      isSubmitting: false,
      errorMessages: initErrorMessages({ schema }),
      showAllErrors: false
    }
    // Save a reference to the initialised schema.
    this.schema = schema
    // Error checker initialised here with the schema so we can do something
    // clever with the 'sameAs' rules.
    this.errorChecker = initErrorChecker({ schema })
    this.errorCheckers = {}

    this.mounted = false
  }

  componentDidMount () {
    this.mounted = true
    // Check for errors when the component mounts in case they try to submit the
    // form before interacting with it.
    this.checkForErrors(null)({ values: this.state.values })
  }

  componentWillUnmount () {
    this.mounted = false
  }

  registerField = (name, type) => {
    // Each field gets its own errorChecker function so the debouncing doesn't
    // inadvertently skip checking fields onChange. (E.g. when different fields
    // are completed near simultaneously, as in the case of autocomplete.)
    if (!_has(this.errorCheckers, name)) {
      // We don't use a debounced error checker for select fields because they
      // briefly show an error message if we do.
      const notSelect = type !== 'select'
      this.errorCheckers[name] = notSelect
        ? this.debouncedCheckForErrors(name)
        : this.checkForErrors(name)
    }
  }

  get hasErrors () {
    // Add up all the errorMessage arrays to work out whether there are any
    // errors on the form.
    const { errorMessages } = this.state
    return _flatten(_concat(_values(errorMessages))).length > 0
  }

  checkForErrors = name => async ({ values }) => {
    // Update the errorMessages asynchronously.
    const errorMessages = await this.errorChecker({ name, values })
    this.mounted && this.setState({ errorMessages: { ...this.state.errorMessages, ...errorMessages } })
  }

  // Debounced so this function isn't called on literally every keystroke.
  debouncedCheckForErrors = name => {
    return _debounce(this.checkForErrors(name), 150)
  }

  // The ConnectedField component fixes the name before providing this function
  // to the Field itself.
  onChange = name => value => {
    // Every time a value changes, update the state...
    const values = { ...this.state.values, [name]: value }
    this.mounted && this.setState({ values }, () => {
      // Wait for setState to finish! And check for errors.
      this.errorCheckers[name]({ values })
    })
    this.props.onChange && this.props.onChange(values)
  }

  onFinishSubmit = () => this.mounted && this.setState({ isSubmitting: false })

  handleSubmit = e => {
    e.preventDefault()
    if (this.props.disabled) {
      return
    }
    if (this.hasErrors) {
      // Don't let the user submit a form if it still contains errors.
      this.mounted && this.setState({ showAllErrors: true })
      return
    }
    // Update form to 'isSubmitting' (i.e. loading state).
    this.mounted && this.setState({ isSubmitting: true })
    // Call the provided onSubmit function.
    this.props.onSubmit(e, {
      values: this.state.values,
      onFinishSubmit: this.onFinishSubmit
    })
  }

  render () {
    const { children, className, customComponents } = this.props
    const CustomForm = customComponents.form
    const state = { ...this.state, hasErrors: this.hasErrors }
    const config = { schema: this.schema, customComponents }
    const actions = { onChange: this.onChange, setValue: this.onChange, registerField: this.registerField }
    const dataAttributes = getDataAttributes(this.props)
    return (
      <Provider value={{ state, config, actions }}>
        <Form onSubmit={this.handleSubmit} className={className} CustomForm={CustomForm} dataAttributes={dataAttributes}>
          {children}
        </Form>
      </Provider>
    )
  }
}

const Form = ({ onSubmit, children, className, CustomForm, dataAttributes }) => (
  CustomForm ? (
    <CustomForm className={className} onSubmit={onSubmit} {...dataAttributes}>
      {children}
    </CustomForm>
  ) : (
    <form className={className} onSubmit={onSubmit} {...dataAttributes}>
      {children}
    </form>
  )
)

const getDataAttributes = props => {
  return Object.keys(props).reduce((result, key) => {
    if (key.indexOf('data-') > -1) {
      result[key] = props[key]
    }
    return result
  }, {})
}

FormContainer.propTypes = {
  schema: PropTypes.object,
  children: PropTypes.node.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func,
  className: PropTypes.string,
  initialValues: PropTypes.object,
  customComponents: PropTypes.object
}

FormContainer.defaultProps = {
  schema: {},
  onChange: null,
  className: '',
  initialValues: {},
  customComponents: {}
}

Form.propTypes = {
  children: PropTypes.node.isRequired,
  onSubmit: PropTypes.func.isRequired,
  className: PropTypes.string,
  CustomForm: PropTypes.node
}

Form.defaultProps = {
  className: '',
  CustomForm: null
}

export {
  FormConsumer,
  FormContainer as default
}
