export const createErrorLogger = prefix => ({
  logError: error => {
    console.error(`%c [esv-react-form] ${prefix} ${error}`, 'background: #cc0033; color: #fff')
  },
  throwError: error => {
    throw new Error(`[esv-react-form] ${prefix} ${error}`)
  }
})
