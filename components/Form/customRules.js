import {
  isEmailValid as isEmailValidRequest,
  isEmailAvailable as isEmailAvailableRequest,
  isServicedAccountCodeValid as isServicedAccountCodeValidRequest
} from 'services/PaymentsService'
import {
  isPasswordStrong as isPasswordStrongRequest
} from 'services/UserService'

export const isEmailValid = {
  rule: async value => {
    try {
      const response = await isEmailValidRequest(value)
      return response.status === 'OK' && response.data === 'true'
    } catch (error) {
      console.error(error)
    }
  },
  options: {
    name: 'emailValid',
    message: 'Please enter a valid email address.'
  }
}

export const isEmailAvailable = {
  rule: async value => {
    try {
      if (value) {
        const response = await isEmailAvailableRequest(value)
        return response
      }
      return Promise.resolve(true)
    } catch (error) {
      console.error(error)
    }
  },
  options: {
    name: 'emailAvailable',
    message: 'There is already an account registered with this email.'
  }
}

export const isEmailTaken = {
  rule: async value => {
    try {
      if (value) {
        const response = await isEmailAvailableRequest(value)
        return !response
      }
      return Promise.resolve(false)
    } catch (error) {
      console.error(error)
    }
  },
  options: {
    name: 'emailTaken',
    message: 'There is no account associated with this email address.'
  }
}

export const isPasswordStrong = {
  rule: async value => {
    try {
      const response = await isPasswordStrongRequest(value)
      return response.acceptable === true
    } catch (error) {
      console.error(error)
    }
  },
  options: {
    name: 'passwordWeak',
    message: 'The password you have entered is considered too common. We suggest using a password generator.'
  }
}

export const isServicedPartnerCodeValid = {
  rule: async value => {
    try {
      const response = await isServicedAccountCodeValidRequest(value)
      return response
    } catch (error) {
      console.error(error)
    }
  },
  options: {
    name: 'invalidCode',
    message: 'The code you have entered has not been recognised. Please check the code again, and if the problem persists please contact us or the company you received the code from.'
  }
}
