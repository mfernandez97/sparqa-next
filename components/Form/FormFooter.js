import React from 'react'
import { getClassName } from 'helpers/misc'

const FormFooter = ({ children, ...props }) => {
  const defaults = getDefaults()
  const className = getClassName(defaults, props)
  return (
    <footer className={className}>
      {children}
    </footer>
  )
}

const getDefaults = () => ({
  color: 'grey',
  margin: 'mt5',
  fontSize: 'fxs',
  textAlign: 'tc'
})

export default FormFooter
