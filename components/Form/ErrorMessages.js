import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'

const ErrorMessages = ({ className, messages }) => (
  <Fragment>
    {messages.map(
      message => <p key={message} className={cx(className, 'mb0 crimson fxs tl')}>{message}</p>
    )}
  </Fragment>
)

ErrorMessages.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.node).isRequired,
  className: PropTypes.string
}

ErrorMessages.defaultProps = {
  className: ''
}

export default ErrorMessages
