import React from 'react'

const Label = ({ htmlFor, children }) => (
  <label className='db mb2 mh0 fxs grey' htmlFor={htmlFor}>{children}</label>
)

export default Label
