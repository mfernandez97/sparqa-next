import React, { Component } from 'react'
import { _toLower } from 'libraries/lodash'
import { map } from 'rxjs/operators'
import { $user, updateMarketingConsent, $customerDetails } from 'services/UserService'
import Modal, { ModalBox } from 'components/Modal'
// import stripeWrap from 'components/Stripe/wrap'
import StripeWrapper from 'components/Stripe/wrap'
import BillingForm from 'components/Account/BillingForm'
import PlanOptionsField from 'components/Account/PlanOptionsField'
import Form, { FormSubmit, FormConsumer } from 'components/Form'
import { fetch, refresh } from 'services/ApiService'
import { Media } from 'services/WindowService'
import connect from 'helpers/connect'
import { USER, USER_BILLING, USER_CURRENT_SUBSCRIPTION } from 'constants/requests'
import { PLAN_NAMES } from 'components/PaymentPlans'
import { onCheckoutOption, onCheckout, onCheckoutComplete } from 'services/AnalyticsService'
import { resubscribe, $paymentPlans } from 'services/PaymentsService'
import insist from 'components/Form/esv-react-form/insist'
import Svg from 'components/SVG'
import MarketingConsentForm from 'components/Account/MarketingConsentForm'
import DiscountCodeField from 'components/Account/DiscountCodeField'
import DiscountCodeContainer from 'components/Account/DiscountCodeContainer'

const steps = selectedPlan => [
  'Select Plan',
  (selectedPlan === 'freemium' ? 'Marketing Consent' : 'Billing Details'),
  'Welcome Back'
]

@connect(() => ({
  user: $user,
  partner: $customerDetails.pipe(map(({ partner: { code } = {} }) => code)),
  billing: fetch(USER_BILLING),
  subscription: fetch(USER_CURRENT_SUBSCRIPTION),
  tablet: Media.Tablet,
  paymentPlans: $paymentPlans()
}))
class ResubscribeModal extends Component {
  constructor (props) {
    super(props)
    const { billing: { name, line1, line2, country, postcode } } = props
    this.state = {
      step: 0,
      selectedPlan: null,
      planDetails: { name: '' },
      register: {
        billingName: name,
        addressLine1: line1,
        addressLine2: line2,
        country,
        postCode: postcode
      }
    }
  }

  nextStep = () => {
    this.setState({
      step: this.state.step + 1
    })
  }

  setSelectedPlan = plan => {
    const planDetails = this.props.paymentPlans.find(({ stripeId }) => stripeId === plan)
    this.setState({
      selectedPlan: plan,
      planDetails: planDetails
    })
  }

  onSubmit = (e) => {
    e.preventDefault()
    e.stopPropagation()
    if (this.state.selectedPlan !== 'freemium') {
      const { planDetails: { name, price, stripeId } } = this.state
      onCheckoutOption('unlimited')
      onCheckout({
        id: stripeId,
        name: 'unlimited',
        price,
        category: 'unlimited',
        variant: name,
        step: 3
      })
    }
    this.nextStep()
  }

  updateFormState = (newState) => {
    this.setState({
      register: Object.assign({}, this.state.register, newState)
    })
  }

  onCloseModal = () => {
    this.setState({ step: 0 }, this.props.closeModal)
  }

  render () {
    const { step, selectedPlan, register, planDetails: { name: selectedPlanName } } = this.state
    const {
      user, tablet, partner, discount, discountCode, appliedDiscountCode, visible, className
    } = this.props

    const props = {
      handleSubmit: this.onSubmit,
      selectedPlan,
      selectedPlanName,
      setSelectedPlan: this.setSelectedPlan,
      register,
      updateFormState: this.updateFormState,
      nextStep: this.nextStep,
      planDetails: this.state.planDetails,
      user,
      tablet,
      partner,
      discount,
      discountCode,
      appliedDiscountCode
    }

    return (
      <Modal
        {...{ visible, className }}
        onMaskClick={() => null}
        useFlex={step === 0}
        usePortal={false}
        forceFullWidth
      >
        <ModalBox title={steps(selectedPlan)[step]} closeModal={this.onCloseModal} fullWidth={step === 0}>
          <div className='pa7'>
            <Step step={step} props={props} />
          </div>
        </ModalBox>
      </Modal>
    )
  }
}

const Step = ({ step, props }) => {
  switch (step) {
    case 0:
      return <PickPlan {...props} />
    case 2:
      return <Success />
    default:
      return props.selectedPlan === 'freemium' ? (
        <MarketingConsent {...props} />
      ) : (
        <StripeWrapper>
          <EnterBillingInformationBase {...props} />
        </StripeWrapper>
      )
      // ) : stripeWrap(EnterBillingInformationBase, props)
  }
}

const schema = {
  selectedPlan: insist().required({ message: 'Please select a plan to continue.' })
}
const PickPlan = ({ user, tablet, handleSubmit, setSelectedPlan, discount = 0, appliedDiscountCode }) => (
  <Form
    className='resubscribe-form'
    schema={schema}
    onChange={values => {
      values.selectedPlan && setSelectedPlan(values.selectedPlan)
    }}
    onSubmit={handleSubmit}
    initialValues={{ selectedPlan: null, discountCode: '' }}
  >
    <PlanOptionsField
      filter={({ name }) => user.isFreemium ? _toLower(name) !== PLAN_NAMES.FREEMIUM : true}
      discount={discount}
      carousel={tablet}
    />
    <div className='mb8 mb2-m max-w-measure-2 mhauto'>
      <FormConsumer>
        {({ actions }) => (
          <DiscountCodeField
            setDiscountCode={actions.setValue('discountCode')}
            appliedDiscountCode={appliedDiscountCode}
          />
        )}
      </FormConsumer>
    </div>
    <FormSubmit maxWidth='max-w-measure-3'>Continue</FormSubmit>
  </Form>
)

class EnterBillingInformationBase extends Component {
  state = { error: '' }

  get initialValues () {
    const { register } = this.props
    return {
      billingName: register.billingName || '',
      cardElement: null,
      addressLine1: register.addressLine1 || '',
      addressLine2: register.addressLine2 || '',
      postCode: register.postCode || '',
      country: register.country || '',
      vatNo: register.vatNo || ''
    }
  }

  handleSubmit = (e, { values, onFinishSubmit }) => {
    this.setState({ error: '' })

    this.props.stripe.createToken({
      name: values.billingName,
      address_line1: values.addressLine1,
      address_line2: values.addressLine2,
      address_country: values.country,
      address_zip: values.postCode
    })
      .then(({ token, error }) => {
        if (error) {
          throw new Error(error.message)
        } else if (token) {
          const { vatNo } = values
          const { selectedPlan, user: { isFreemium }, partner, appliedDiscountCode } = this.props

          const userData = {
            planId: selectedPlan,
            token: token.id,
            vatNo,
            discountCode: appliedDiscountCode
          }

          resubscribe(userData, isFreemium)
            .then(response => {
              onCheckoutComplete({
                partner,
                transactionId: response.data.uuid,
                productId: this.props.planDetails.stripeId,
                name: 'unlimited',
                price: this.props.planDetails.price,
                category: 'unlimited',
                variant: this.props.planDetails.name
              })
              return response
            }).then(() => refresh(USER))
            .then(() => refresh(USER_CURRENT_SUBSCRIPTION))
            .then(this.props.nextStep)
            .catch(({ message }) => {
              this.setState({ error: `Error: ${message}` })
              onFinishSubmit()
            })
        }
      })
      .catch(({ message }) => {
        this.setState({ error: `Error: ${message}` })
        onFinishSubmit()
      })
  }

  render () {
    return (
      <BillingForm
        onSubmit={this.handleSubmit}
        initialValues={this.initialValues}
        responseError={this.state.error}
      />
    )
  }
}

class MarketingConsent extends Component {
  state = { responseError: null }

  componentWillUnmount () {
    this.subscription && this.subscription.unsubscribe()
  }

  message = 'To subscribe to our free plan simply enable marketing consent.'

  handleSubmit = value => {
    this.subscription = updateMarketingConsent({ marketingConsent: value })
      .subscribe(response => {
        try {
          if (response.marketingConsent === value) {
            this.setState({ responseError: null })
            refresh(USER)
            this.props.nextStep()
          } else {
            throw new Error('There was a problem updating your marketing preferences. Please try again later.')
          }
        } catch (error) {
          this.setState({ responseError: error.message })
        }
      })
  }

  render () {
    const { responseError } = this.state
    const { user: { marketingConsent } } = this.props
    return (
      <MarketingConsentForm
        message={this.message}
        onSubmit={this.handleSubmit}
        showMessage
        responseError={responseError}
        initialValues={{ marketingConsent }}
        marketingConsent={marketingConsent}
      />
    )
  }
}

const Success = () => (
  <>
    <Svg
      url='assets/smile.svg'
      alt='Smiling face'
      className='light-sea-green db mhauto mt0 mb7 w-image-5 h-image-5'
    />
    <div className='tc b fxxl tracked light-sea-green lh-title mb7 mhauto mt0'>
      Thanks for resubscribing!
    </div>
  </>
)

const ConnectedResubscribeModal = props => (
  <DiscountCodeContainer {...props}>
    <ResubscribeModal />
  </DiscountCodeContainer>
)

export default ConnectedResubscribeModal
