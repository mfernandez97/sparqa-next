import React, { Component } from 'react'
import cx from 'classnames'
import PropTypes from 'prop-types'
import nullIcon from '../public/assets/icons/crop-free.svg'

const cache = new Map()
const resolve = new Map()

export default class SVG extends Component {
  constructor (props) {
    super(...arguments)
    this.state = {
      svg: this.getSVG(props.url)
    }
    this.update(props.url)
  }

  mounted = false

  componentDidMount () {
    this.mounted = true
  }

  componentWillUnmount () {
    this.mounted = false
  }

  componentDidUpdate (prevProps) {
    const { url } = this.props
    if (prevProps.url !== url) {
      this.setState({ svg: this.getSVG(url) })
      this.update(url)
    }
  }

  getSVG (url) {
    if (!url) {
      return nullIcon
    }
    // If we've got the file in the cache then use that.
    return cache.has(url) ? cache.get(url) : nullIcon
  }

  async update (url) {
    // Check whether there's already an inflight request.
    const inflight = resolve.has(url)
    // Save to resolve map.
    resolve.set(url, [
      // When the request has returned, then we can set the state.
      () => this.mounted && this.setState({ svg: cache.get(url) }),
      ...(resolve.get(url) || [])
    ])
    // If there isn't already a request...
    if (!inflight) {
      // (quickly check the cache again)
      if (cache.has(url)) {
        return this.mounted && this.setState({ svg: cache.get(url) })
      }
      // ...then make one.
      const [result] = await preloadSVGs([url])
      // If it goes wrong...
      if (result.status === 'error') {
        // ...then set the nullIcon as the svg.
        this.setState({ svg: nullIcon })
      } else {
        // Otherwise go through the resolve map and set the state.
        resolve.get(url).forEach(r => r())
        resolve.delete(url)
      }
    }
  }

  render () {
    return (
      <span
        className={cx('svg-base', this.props.className)}
        dangerouslySetInnerHTML={{ __html: this.state.svg }}
      />
    )
  }
}

const checkExists = url => {
  return new Promise((resolve, reject) => {
    const img = new Image()
    img.onload = () => resolve({ url, status: 'ok' })
    img.onerror = () => reject(new Error(`error loading file with url ${url}`))
    img.src = url
  })
}

export const preloadSVGs = async urls => {
  return Promise.all(urls.map(async url => {
    try {
      // Make sure the file exists.
      await checkExists(url)
      // Fetch its contents.
      const file = await fetch(url, { method: 'get', credentials: 'same-origin' })
      const svg = await file.text()
      // Save it in the cache.
      cache.set(url, svg)
      // Return it, idk, might be useful.
      return { status: 'success', url, svg }
    } catch (error) {
      console.error(`SVG ${error}`)
      return { status: 'error' }
    }
  }))
}

SVG.propTypes = {
  url: PropTypes.string.isRequired,
  className: PropTypes.string
}

SVG.defaultProps = {
  className: ''
}
