import React, { Component, Fragment } from 'react'
import RichText from 'components/CMS/RichText'
import Modal from 'components/Modal'
import Icon from 'components/Icon'

export default class TeamViewer extends Component {
  state = {
    detailsOpen: false,
    selectedBio: null
  }

  bioClickHandler = person => () => {
    this.setState({
      detailsOpen: true,
      selectedBio: person
    })
  }

  maskClickHandler = () => {
    this.setState({ detailsOpen: false })
  }

  render () {
    const { title } = this.props
    const { detailsOpen, selectedBio } = this.state
    const teamMembers = this.props.team.results
    return (
      <Fragment>
        <div
          id={title}
          className='layout-width-constraint w-100 mhauto pt7 pb9 pv7-t max-w-measure-5 w-100-t grid-4-gutter-6-d grid-3-gutter-4-t grid-2-gutter-4-m'
        >
          {teamMembers.map(({
            id,
            uid,
            data: {
              name,
              biography: bio,
              job_title: desc,
              headshot: { url: imgUrl, alt }
            }
          }) => (
            <div key={id}>
              <div className='relative pb-100 w-100' onClick={this.bioClickHandler({ id, name, desc, imgUrl, alt, bio, uid })}>
                <img className='w-100 h-100 fit-image absolute' src={imgUrl} alt={alt} />
                <div
                  className='fs pointer absolute white left0 top0 w-100 h-100 bg-near-black o-0 hover-o-90 tc flex flex-column justify-center pa4'
                >
                  <span>{name}</span>
                  <span>{desc}</span>
                </div>
              </div>
            </div>
          ))}
        </div>
        <Modal visible={detailsOpen} onMaskClick={this.maskClickHandler}>
          {selectedBio && (
            <TeamMember bio={selectedBio} close={this.maskClickHandler} />
          )}
        </Modal>
      </Fragment>
    )
  }
}

class TeamMember extends Component {
  render () {
    const { close, bio } = this.props
    return (
      <div className='relative flex flex-column-t w-measure-5 h-measure-4 max-w-100 w-measure-2-t w-100-m h-auto-t bg-white br2 overflow-hidden'>
        <div className='w-50 w-100-t'>
          <img className='fit-image h-measure-1-t position-image-50-30 w-100 h-100' src={bio.imgUrl} alt={bio.alt} />
        </div>
        <div className='flex flex-column w-50 w-100-t pa5 dark-slate-grey'>
          <h1 className='fxl'>{bio.name}</h1>
          <h2 className='fl'>{bio.desc}</h2>
          <RichText className='fm'>{bio.bio}</RichText>
        </div>
        <button className='pointer absolute top6 right5 pa0 bg-transparent' onClick={close} >
          <Icon name='close' className='w-icon-5 h-icon-5' />
        </button>
      </div>
    )
  }
}
