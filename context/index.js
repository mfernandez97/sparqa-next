import { createContext } from 'react'

export const {
  Provider: DiscountProvider,
  Consumer: DiscountConsumer
} = createContext()

export const {
  Provider: SearchProvider,
  Consumer: SearchConsumer
} = createContext({
  query: {},
  category: null,
  pageLimit: null,
  pageNumber: null,
  defaultQuery: {},
  searchPhrase: null
})
