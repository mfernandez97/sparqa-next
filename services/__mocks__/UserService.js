import { BehaviorSubject } from 'rxjs'

const userService = {}

const userObj = (roles, marketingConsent = false) => ({
  email: 'FooBar@test.com',
  firstName: 'Foo',
  lastName: 'Bar',
  name: 'Foo Bar',
  passwordStatus: 'Password never changed',
  pin: 1234,
  roles,
  uri: '',
  userId: 'some-user-id',
  analyticsId: 'some-analytics-id',
  marketingConsent,
  isSubscribed: roles.includes('subscribed'),
  isFreemium: !roles.includes('subscribed') && roles.includes('free-tier'),
  isServiced: roles.includes('serviced-accounts'),
  isLinked: !roles.includes('customer_admin')
})
userService.$user = new BehaviorSubject(-100)
export const __resetUser__ = () => userService.$user.next(-100)
export const __subscribedUser__ = () => userService.$user.next(userObj(['subscribed']))
export const __fremiumUser__ = () => userService.$user.next(userObj(['free-tier'], true))
export const __servicedUser__ = () => userService.$user.next(userObj(['serviced-accounts']))
export const __linkedUser__ = () => userService.$user.next(userObj(['']))

userService.$customerDetails = new BehaviorSubject(-100)
export const __resetCustomerDetails__ = () => userService.$customerDetails.next(-100)
export const __customerDetailsNoResult__ = () => userService.$customerDetails.next(-404) // accounts like test
export const __customerDetailsPartner__ = () => userService.$customerDetails.next({
  email: 'FooBar@test.com',
  uuid: 'some-uuid-goes-here',
  partner: {
    name: 'partner-name-mock',
    code: 'partner-code-mock',
    servicedAccounts: 'serviced-accounts-mock',
    logoLg: 'partner-logo-lg-mock',
    logoSm: 'partner-logo-sm-mock'
  }
})
export const __customerDetailsNoPartner__ = () => userService.$customerDetails.next({
  email: 'FooBar+partnerFree@test.com',
  uuid: 'a-different-uuuid-to-that-other-one'
})

const subscriptionObj = ({
  name = 'Annual',
  price = 24000,
  cancelling = false,
  expiryDate = '2020-10-01T11:20:05Z',
  discount = null,
  monthsRemaining = -12
}) => ({
  name,
  price,
  cancelling,
  expiryDate,
  status: 'active',
  discount,
  monthsRemaining
})

userService.$subscription = new BehaviorSubject(-100)
export const __annualSubscription__ = () => userService.$subscription.next(subscriptionObj())
export const __monthlySubscription__ = () => userService.$subscription.next(subscriptionObj({
  name: 'Monthly',
  price: 2500,
  monthsRemaining: 11
}))

export const {
  $user,
  $userAuth,
  $customerDetails,
  $subscription,
  $isLogged,
  $userData,
  updateMarketingConsent,
  changePassword,
  passwordReset,
  requestPasswordReset,
  $isSubscribedUser,
  $isLinkedUser,
  $isFreemiumUser,
  $isExpiredUser,
  isPasswordStrong
} = userService
