import { Observable, BehaviorSubject } from 'rxjs'
import { fullDocumentList, mockDocument } from '__testdata__/documents'

const documentService = jest.genMockFromModule('../DocumentService')

/**
 * EXPORTS
 * $documents () -> list of docs from fetch
 *
 * $topicDocuments ({ topics: [topicId, ...] }) -> list of docs based on topic
 *
 * $publishedTopicDocuments ({ topicId }) -> list of above filtered by isPublished
 *
 * $document ({ documentId }) -> specific document
 *
 * $bundle ({ bundleId }) -> specific bundle
 *
 * $bundles () -> all bundles
 *
 * $documentInterview ({ documentId }) -> the hotdoc
 *
 * $documentResults ({ searchPhrase, pageNumber, pageLimit, query }) -> document search results
 *
 * $bundleResults ({ searchPhrase, pageNumber, pageLimit, query }) -> bundle search results
 *
 * $relatedDocuments ({ documentId }) -> document list of docs related to the id of the current document
 *
 * $relatedBundles ({ documentId, bundleId, topicId }) -> bundle list related to either the current bundle, doc, or topic
 *
 * $savedDocuments (parameters) -> gets users saved documents
 *
 * $savedDocumentsFacets (parameters) -> gets the filters available based on what documents are saved
 *
 * $documentFacets ({ searchPhrase, pageNumber, pageLimite, query }) -> filters available for documents
 * This is an implementation of Search Services $facets which uses encapsulation to support similar functionality
 * accross the different services that use facet style filters.
 *
 * $purchasedDocuments () -> list of purchased docs
 *
 * $purchasedDocumentsIds () -> list of ?
 *
 * $checkProductPurchased ({ uuid }) -> true/false? I think?
 *
 * getDocumentIcon (typeName, { alt }) -> path/to/icon/file
 *
 * $documentLabel (id, url) -> generates the analytics label for the doc
 *
 * $saveDocumentAs ({ data }) -> fetch response to check success/fail
 *
 * $saveDocument ({ uuid, data }) -> fetch response to check success/fail
 *
 * $savedDocument ({ uuid }) -> retrieves saved document
 *
 * $savedDocumentRawText ({ uuid }) -> as above but as raw text I guess?
 *
 * $deleteSavedDocument ({ uuid }) -> fetch response to check success/fail
 *
 * $refreshSavedDocument (uuid) -> I guess it refreshes the data, I don't understand why its merging the results
 *
 * $refreshPurchasedDocument ({ uuid }) -> same as above but for purchased doc and same q
 */

const documentsObservable = new Observable(subscriber => {
  subscriber.next(fullDocumentList)
})
documentService.$documents.mockImplementation(() => documentsObservable)

documentService.$topicDocuments.mockImplementation(() => documentsObservable)

const publishedListObservable = new Observable(subscriber => {
  subscriber.next([mockDocument])
})
documentService.$publishedTopicDocuments.mockImplementation(() => publishedListObservable)

const $documentObservable = new Observable(subscriber => {
  subscriber.next(mockDocument)
})
documentService.$document.mockImplementation(() => $documentObservable)
// TODO most of the mocking

const checkProductPurchasedObservable = new BehaviorSubject(-100)
export const __setProductPurchasedFalse = () => checkProductPurchasedObservable.next(false)
export const __setProductPurchaseTrue = () => checkProductPurchasedObservable.next(true)
export const __resetProductPurchased = () => checkProductPurchasedObservable.next(-100)
documentService.$checkProductPurchased.mockImplementation(() => checkProductPurchasedObservable)

export const {
  $documents,
  $topicDocuments,
  $publishedTopicDocuments,
  $document,
  $bundle,
  $bundles,
  $documentInterview,
  $documentResults,
  $bundleResults,
  $relatedDocuments,
  $relatedBundles,
  $savedDocuments,
  $savedDocumentsFacets,
  $documentFacets,
  $purchasedDocuments,
  $purchasedDocumentsIds,
  $checkProductPurchased,
  getDocumentIcon,
  $documentLabel,
  $saveDocumentAs,
  $saveDocument,
  $savedDocument,
  $savedDocumentRawText,
  $deleteSavedDocument,
  $refreshSavedDocument,
  $refreshPurchasedDocument
} = documentService
