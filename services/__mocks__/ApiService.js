/* eslint-env jest */
import { of } from 'rxjs'
import { RESPONSE_NOTFOUND } from 'front-end-core/constants/responses'
import { mockDocument, mockDocuments, mockTopicTree, mockIdForTopicContent, mockTopicContent } from '__testdata__'
import { DOCUMENTS, DOCUMENT, TOPICS, TOPIC_CONTENT } from 'constants/requests'

const getDocument = documentId => {
  if (documentId === mockDocument.id) {
    return mockDocument
  }
  return { statusCode: RESPONSE_NOTFOUND }
}

const getTopicContent = topicId => {
  if (topicId === mockIdForTopicContent) {
    return mockTopicContent
  }
  return { statusCode: RESPONSE_NOTFOUND }
}

const mockFetch = (name, values) => {
  switch (name) {
    case DOCUMENTS:
      return of(mockDocuments)
    case DOCUMENT:
      return of(getDocument(values.documentId))
    case TOPICS:
      return of(mockTopicTree)
    case TOPIC_CONTENT:
      return of(getTopicContent(values.topicIds))
    default:
      return null
  }
}

export const fetch = mockFetch
