/* eslint-env jest */
import { _keys, _has, _isArray } from 'libraries/lodash'
import { RESPONSE_NOTFOUND } from 'front-end-core/constants/responses'
import { mockTopicTree, mockRootTopic, mockIdForTopicContent } from '__testdata__'
import { $rootTopics, $topic, $flattenedTopics } from 'services/TopicService'

jest.mock('services/ApiService')

describe('$rootTopics', () => {
  it('returns an observable of the root topics', done => {
    $rootTopics()
      .subscribe(rootTopics => {
        // Slightly mad way to test they all have parentId of -1
        const parentIdsSummed = rootTopics.reduce((sum, { parentId }) => sum + Number(parentId), 0)
        expect(parentIdsSummed).toEqual(rootTopics.length * -1)
        expect(rootTopics).toEqual(expect.arrayContaining([mockRootTopic]))
        done()
      })
  })
})

describe('$topic', () => {
  it('returns an observable of a topic tree with the topic requested as its root', done => {
    $topic({ topicId: mockIdForTopicContent })
      .subscribe(topic => {
        const mockTopic = mockTopicTree.nodes[mockIdForTopicContent]
        // The root should be the topic specified
        expect(topic.id).toBe(mockIdForTopicContent)
        expect(topic.title).toBe(mockTopic.title)
        expect(topic.bookmarkId).toBe(mockTopic.bookmarkId)
        expect(topic.overview).toBe(mockTopic.overview)
        expect(topic.description).toBe(mockTopic.description)
        expect(topic.active).toBe(mockTopic.active)
        // It should have children, questions and overview properties
        expect(_has(topic, 'children')).toBe(true)
        expect(_has(topic, 'questions')).toBe(true)
        expect(_has(topic, 'overview')).toBe(true)
        // All children should have question and children arrays, even if they're empty
        // So should the children of children but cba with a recursive fn
        expect(topic.children.reduce((result, child) => {
          return result && _isArray(child.children) && _isArray(child.questions)
        }, true)).toBe(true)
        expect()
        done()
      })
  })

  it('returns a 404 when the topic doesn\'t exist', done => {
    $topic({ topicId: '???' })
      .subscribe(response => {
        expect(response).toStrictEqual({ statusCode: RESPONSE_NOTFOUND })
        done()
      })
  })
})

describe('$flattenedTopics', () => {
  it('returns an observable of all nodes with a common S1 ancestor', done => {
    $flattenedTopics({ topicId: mockIdForTopicContent })
      .subscribe(topicNodes => {
        // The object returned should contain all the descendants of the root topic
        // of the topic requested (ie if you request an S3 topic id you should get
        // all the descendants of that topic's S1)
        const rootTopicId = mockTopicTree.nodes[mockIdForTopicContent].path[0]
        const { descendants } = mockTopicTree.nodes[rootTopicId]
        const topicIds = _keys(topicNodes)
        expect(topicIds).toStrictEqual(descendants)

        // Any randomly selected topic in the response info should have all the info we need
        const randomIndex = getRandomIndex(descendants)
        const randomId = topicIds[randomIndex]
        expect(topicNodes[randomId]).toStrictEqual(mockTopicTree.nodes[randomId])
        done()
      })
  })

  it('returns a 404 when the topic doesn\'t exist', done => {
    $flattenedTopics({ topicId: '???' })
      .subscribe(response => {
        expect(response).toStrictEqual({ statusCode: RESPONSE_NOTFOUND })
        done()
      })
  })
})

const getRandomIndex = arr => Math.floor(Math.random() * Math.floor(arr.length))
