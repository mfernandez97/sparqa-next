/* eslint-env jest */
import { RESPONSE_NOTFOUND } from 'front-end-core/constants/responses'
import { mockDocument } from '__testdata__'
import { $document } from 'services/DocumentService'

jest.mock('services/ApiService')

describe('$document', () => {
  it('successfully returns the correct document', done => {
    $document({ documentId: 'e3db5c98-78a4-4b00-8102-b86f85a780fa' })
      .subscribe(response => {
        expect(response).toStrictEqual(mockDocument)
        done()
      })
  })

  it('returns a 404 when the document doesn\'t exist', done => {
    $document({ documentId: '???' })
      .subscribe(response => {
        expect(response).toStrictEqual({ statusCode: RESPONSE_NOTFOUND })
        done()
      })
  })
})
