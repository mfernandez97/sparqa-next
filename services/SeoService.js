import { map } from 'rxjs/operators'
import { RESPONSE_PENDING } from 'front-end-core/constants/responses'
import { fetch } from 'services/ApiService'
import { DOCUMENT_SEO, SECTION_SEO, TOPIC_QUESTION_SEO, BUNDLE_SEO } from 'constants/requests'

const filterPending = (response, onContent) => response.statusCode === RESPONSE_PENDING ? response : onContent(response)

const mapIdToUrl = mapping => mapping.reduce((prev, { id, seo_id: url }) => Object.assign(prev, { [id]: url }), {})
const mapUrlToId = mapping => mapping.reduce((prev, { id, seo_id: url }) => Object.assign(prev, { [url]: id }), {})

const idToUrl = () => map(mapping => filterPending(mapping, mapIdToUrl))
const urlToId = () => map(mapping => filterPending(mapping, mapUrlToId))

const $bundleSeoMap = fetch(BUNDLE_SEO)
export const $bundleSeoIdToUrl = $bundleSeoMap.pipe(idToUrl(map))
export const $bundleSeoUrlToId = $bundleSeoMap.pipe(urlToId(map))

const $documentSeoMap = fetch(DOCUMENT_SEO)
export const $documentSeoIdToUrl = $documentSeoMap.pipe(idToUrl(map))
export const $documentSeoUrlToId = $documentSeoMap.pipe(urlToId(map))

const $sectionSeoMap = fetch(SECTION_SEO)
export const $sectionIdToUrlMap = $sectionSeoMap.pipe(idToUrl(map))
export const $sectionUrlToIdMap = $sectionSeoMap.pipe(urlToId(map))

const $topicsQuestionSeoMap = fetch(TOPIC_QUESTION_SEO)
export const $topicQuestionIdToUrlMap = $topicsQuestionSeoMap.pipe(idToUrl(map))
export const $topicQuestionUrlToIdMap = $topicsQuestionSeoMap.pipe(urlToId(map))
