import { _capitalize, _last, _split, _pick } from 'libraries/lodash'
import Route from 'route-parser'
import { merge, combineLatest, of } from 'rxjs'
import { map, switchMap, filter, first } from 'rxjs/operators'
import { mapResponse, isValidResponse } from 'core/api/dataflow'
import { RESPONSE_UNAUTHORIZED, RESPONSE_NOTFOUND } from 'front-end-core/constants/responses'
import { SNIPPET_LENGTH_MOBILE, SNIPPET_LENGTH_DESKTOP } from 'constants/search'
import {
  URL_SAVE_DOCUMENT_INTERVIEW_AS,
  URL_SAVE_DOCUMENT_INTERVIEW,
  URL_DELETE_SAVE_DOCUMENT
} from 'constants/urls'
import {
  DOCUMENTS,
  DOCUMENT,
  TOPIC_DOCUMENTS,
  DOCUMENT_INTERVIEW,
  DOCUMENT_SEARCH,
  SAVED_DOCUMENTS,
  SAVED_DOCUMENT_INTERVIEW,
  SAVED_DOCUMENT_INTERVIEW_RAW_TEXT,
  PURCHASED_DOCUMENTS,
  DOCUMENT_PURCHASED,
  RELATED_DOCUMENTS,
  BUNDLE,
  BUNDLES,
  DOCUMENT_RELATED_BUNDLES,
  PURCHASED_DOCUMENTS_IDS,
  BUNDLE_RELATED_BUNDLES,
  BUNDLE_SEARCH,
  TOPIC_RELATED_BUNDLES
} from 'constants/requests'
import { $facets, combineFacets, getFacets } from 'services/SearchService'
import { fetch, securePost, securePut, secureDelete, refresh } from 'services/ApiService'
import { Media } from 'services/WindowService'

export const $documents = () => fetch(DOCUMENTS, {}, {})

export const $topicDocuments = ({ topics } = { topics: null }) => fetch(TOPIC_DOCUMENTS, {}, { topics })

export const $publishedTopicDocuments = ({ topicId }) => $topicDocuments({ topics: [topicId] })
  .pipe(
    filter(isValidResponse),
    map(documents => documents.filter(({ isPublished }) => isPublished))
  )

export const $document = ({ documentId }) => fetch(DOCUMENT, { documentId })

export const $bundle = ({ bundleId }) => fetch(BUNDLE, { bundleId })

export const $bundles = () => fetch(BUNDLES)

export const $documentInterview = ({ documentId }) => fetch(DOCUMENT_INTERVIEW, { documentId })

export const $documentResults = ({ searchPhrase: queryString, pageNumber = 1, pageLimit = 20, query }) => {
  const limit = +pageLimit
  const init = (+pageNumber - 1) * limit
  return Media.Mobile.pipe(
    switchMap(isMobile => fetch(
      DOCUMENT_SEARCH,
      { queryString, limit, init },
      { ...query, q: queryString, i: init, s: limit, d: isMobile ? SNIPPET_LENGTH_MOBILE : SNIPPET_LENGTH_DESKTOP }
    ))
  )
}

export const $bundleResults = ({ searchPhrase: queryString, pageNumber = 1, pageLimit = 20, query }) => {
  const limit = +pageLimit
  const init = (+pageNumber - 1) * limit
  return Media.Mobile.pipe(
    switchMap(isMobile => fetch(
      BUNDLE_SEARCH,
      { queryString, limit, init },
      { ...query, q: queryString, i: init, s: limit, d: isMobile ? SNIPPET_LENGTH_MOBILE : SNIPPET_LENGTH_DESKTOP }
    ))
  )
}

export const $relatedDocuments = ({ documentId }) => fetch(RELATED_DOCUMENTS, { documentId })

export const $relatedBundles = ({ documentId = null, bundleId = null, topicId = null }) => {
  if (documentId) {
    return fetch(DOCUMENT_RELATED_BUNDLES, { documentId })
  }
  if (bundleId) {
    return fetch(BUNDLE_RELATED_BUNDLES, { bundleId })
  }
  if (topicId) {
    return fetch(TOPIC_RELATED_BUNDLES, { topicId })
  }
  return of({ statusCode: RESPONSE_NOTFOUND })
}

export const $publishedTopicBundles = ({ topicId }) => $relatedBundles({ topicId }) // really just a helper to "clean" output from net code stuff
  .pipe(filter(isValidResponse))

export const $savedDocuments = (parameters = {}) => {
  const params = Object.assign({}, parameters, { limit: 1000, search: 'name' }) // TODO: temporary (until we do pagination)
  return fetch(SAVED_DOCUMENTS, {}, params)
}

export const $savedDocumentsFacets = (parameters = {}) => {
  const q = _pick(parameters, 'q')
  return combineLatest(
    getFacets(fetch(SAVED_DOCUMENTS, {}, q)),
    getFacets(fetch(SAVED_DOCUMENTS, {}, parameters)),
    mapResponse(combineFacets)
  )
}

export const $documentFacets = $facets({ searchUrl: DOCUMENT_SEARCH, $results: $documentResults })

export const $purchasedDocuments = () => fetch(PURCHASED_DOCUMENTS, {}, {})

export const $purchasedDocumentsIds = () => fetch(PURCHASED_DOCUMENTS_IDS)

export const $checkProductPurchased = ({ uuid }) => (
  fetch(DOCUMENT_PURCHASED, { uuid }, {}).pipe(
    map(response => isValidResponse(response) ? response.success : response),
    map(response => response.statusCode === RESPONSE_NOTFOUND ? false : response),
    map(response => response.statusCode === RESPONSE_UNAUTHORIZED ? false : response)
  )
)
// I'm very disappointed in you Adam for this arguments definition.
// TODO: introduce some sanity (probably migrate to fully named params)
export const getDocumentIcon = (typeName, { alt } = { alt: false }) => {
  const folder = 'assets/document-type-icons'
  const iconPath = name => `${folder}/${name}${alt ? '-alt' : ''}.svg`
  switch (typeName) {
    case 'Legal agreements/contracts and terms':
      return iconPath('legal-agreements')
    case 'Legal letters and notices':
      return iconPath('letters')
    case 'Company resolutions':
      return iconPath('company-docs')
    case 'Forms':
    case 'Checklists':
    case 'Step-by-step guides':
      return iconPath('checklists')
    default:
      return alt ? 'assets/icons/help-circle.svg' : 'assets/icons/help.svg'
  }
}

export const $documentLabel = (id, url) => {
  return $document({ documentId: id }).pipe(
    filter(isValidResponse),
    map(doc => `[${doc.topic.title}] [${doc.isStatic ? 'Static' : 'HotDocs'}] [${_capitalize(_last(_split(url, '/')))}] [${doc.title}]`),
    first()
  )
}

export const $saveDocumentAs = ({ data }) => securePost(URL_SAVE_DOCUMENT_INTERVIEW_AS, data)
export const $saveDocument = ({ uuid, data }) => securePut(new Route(URL_SAVE_DOCUMENT_INTERVIEW).reverse({ uuid }), data)
export const $savedDocument = ({ uuid }) => fetch(SAVED_DOCUMENT_INTERVIEW, { uuid }, {})
export const $savedDocumentRawText = ({ uuid }) => fetch(SAVED_DOCUMENT_INTERVIEW_RAW_TEXT, { uuid }, {})
export const $deleteSavedDocument = ({ uuid }) => secureDelete(new Route(URL_DELETE_SAVE_DOCUMENT).reverse({ uuid }))

export const $refreshSavedDocument = uuid => {
  return merge(
    refresh(SAVED_DOCUMENTS, {}, { limit: 1000, search: 'name' }),
    refresh(SAVED_DOCUMENT_INTERVIEW, { uuid }),
    refresh(SAVED_DOCUMENT_INTERVIEW_RAW_TEXT, { uuid })
  )
}

export const $refreshPurchasedDocument = ({ uuid }) => {
  return merge(
    refresh(PURCHASED_DOCUMENTS),
    refresh(DOCUMENT_PURCHASED, { uuid }),
    refresh(PURCHASED_DOCUMENTS_IDS)
  )
}
