import { Subject, of, merge, fromEvent } from 'rxjs'
import { debounceTime, startWith, map, distinctUntilChanged, filter, scan, flatMap, take } from 'rxjs/operators'
import { _isEqual, _filter, _keys, _includes, _upperFirst, _reduce, _last, _split, _isNull, _has } from 'libraries/lodash'
import { getPathTo } from 'front-end-core/helpers/document'
import { Resize, DOMReady, Click } from 'front-end-core/services/WindowService'
import { getNode } from 'helpers/dom'
import styleConfig from 'styles/config/config.js'

const {
  PAGE_MAX_WIDTH,
  TABLET_MAX_WIDTH,
  MOBILE_MAX_WIDTH,
  DESKTOP_MIN_WIDTH
} = styleConfig

const mqMax = width => `screen and (max-width: ${width}px)`
const mqMin = width => `screen and (min-width: ${width}px)`

export const mediaQueries = {
  tablet: mqMax(TABLET_MAX_WIDTH),
  mobile: mqMax(MOBILE_MAX_WIDTH),
  desktop: mqMin(DESKTOP_MIN_WIDTH),
  desktopFullscreen: mqMin(PAGE_MAX_WIDTH),
  fullscreen: mqMax(PAGE_MAX_WIDTH)
}

const getMatchedMediaQueries = () =>
  _filter(_keys(mediaQueries), key => {
    return window.matchMedia(mediaQueries[key]).matches
  })

export const Resized = Resize.pipe(
  debounceTime(10),
  startWith({ windowWidth: window.innerWidth, windowHeight: window.innerHeight }),
  map(() => ({ windowWidth: window.innerWidth, windowHeight: window.innerHeight }))
)

export const MediaChanged = Resized.pipe(
  map(getMatchedMediaQueries),
  distinctUntilChanged(_isEqual)
)

// e.g. Media.Tablet emits true or false when window resized
export const Media = _reduce(
  _keys(mediaQueries),
  (acc, key) => {
    acc[_upperFirst(key)] = MediaChanged.pipe(map(media => _includes(media, key)))
    return acc
  },
  {}
)

export const overflowed = element => element.scrollHeight > element.clientHeight

export { DOMReady, Click }

export const ScrollMain = new Subject()

export const getCrossReferenceNode = getNode(node => node.classList.contains('fc-cross-reference'))

export const $crossReferenceId = Click.pipe(
  filter(({ target }) => getCrossReferenceNode(target)),
  map(({ target }) => getCrossReferenceNode(target)),
  map(node => _last(_split(node.getAttribute('href'), '/')))
)

export const $toggle = ({
  selectNode,
  excludeNode = node => false,
  showMapping = e => true,
  hideMapping = e => false,
  initialValue = false,
  startOpen = false
}) => {
  const node = getNode(selectNode)
  const ignoreNode = getNode(excludeNode)
  return Click.pipe(
    filter(({ target }) => !ignoreNode(target)),
    scan((acc, e) => {
      const { target } = e
      const nodePath = getPathTo(target)
      const wasOpen = acc.shouldOpen
      const differentNode = !_isNull(acc.nodePath) && nodePath !== acc.nodePath
      // if a relevant node was clicked and either the toggled element wasn't open before
      // or it was but a new relevant node was clicked (see popover for such a case) then
      // the toggled element should open
      return { e, shouldOpen: node(target) && (!wasOpen || (wasOpen && differentNode)), nodePath }
    }, { e: null, shouldOpen: startOpen, nodePath: null }),
    map(result => {
      const { e, shouldOpen } = result
      const { target } = e
      return node(target) && shouldOpen ? showMapping(e) : hideMapping(e)
    }),
    startWith(initialValue)
  )
}

// Opening and closing the mobile menu
const getToggleMenu = getNode(node => node.hasAttribute('data-toggle-menu'))
const getSearchSubmit = getNode(node => node.hasAttribute('data-mobile-search'))

const $toggleMenuClicks = Click.pipe(
  filter(e => getToggleMenu(e.target))
)

const $mobileMenuClicks = $toggleMenuClicks.pipe(
  map(e => getToggleMenu(e.target).dataset.toggleMenu.toString() === 'true')
)

const $mobileSearchSubmits = fromEvent(document, 'submit').pipe(
  filter(e => getSearchSubmit(e.target)),
  map(e => false)
)

export const $mobileMenuVisible = merge($mobileMenuClicks, $mobileSearchSubmits).pipe(startWith(false))

// Toggling mobile menu search mode
const getMobileMenu = getNode(node => node.classList.contains('mobile-menu'))

const $menuTransitions = fromEvent(document, 'transitionend').pipe(filter(e => getMobileMenu(e.target)))

export const $mobileSearchMode = $toggleMenuClicks.pipe(
  map(e => _has(getToggleMenu(e.target).dataset, 'search')),
  flatMap(bool => bool ? of(true) : $menuTransitions.pipe(map(() => false), take(1)))
)

// https://css-tricks.com/the-trick-to-viewport-units-on-mobile/
Resized.subscribe(({ windowHeight }) => {
  const vh = windowHeight * 0.01
  document.documentElement.style.setProperty('--vh', `${vh}px`)
})
