import { observeRequest as observeRequestCore } from 'core/api/api'
import { Online } from 'front-end-core/services/WindowService'
import { Api } from 'core/api/ApiService'
import {
  Documents,
  Document,
  TopicDocuments,
  DocumentInterview,
  SavedDocumentInterview,
  SavedDocumentInterviewRawText,
  DocumentSearch,
  SavedDocuments,
  PurchasedDocuments,
  PurchasedDocumentsIDs,
  DocumentPurchased,
  RelatedDocuments,
  Bundle,
  Bundles,
  DocumentRelatedBundles,
  BundleRelatedBundles,
  BundleSearch,
  TopicRelatedBundles
} from 'requests/document'
import {
  PaymentPlans,
  StripeId,
  CompanySizeOptions,
  IndustryOptions,
  UserCurrentSubscription,
  UserBilling,
  Credit
} from 'requests/payments'
import { User, UserAuth, CustomerDetails } from 'requests/user'
import { Topics, TopicContent, TopicQuestions, TopicSearch, TopicAnswers } from 'requests/topic'
import { DocumentSeoMappings, SectionSeo, TopicQuestionSeo, BundleSeo } from 'requests/seo'

const observeReq = observeRequestCore()

const ApiStorage = new Api(
  [
    Documents,
    Document,
    TopicDocuments,
    DocumentInterview,
    SavedDocumentInterview,
    SavedDocumentInterviewRawText,
    DocumentSearch,
    PurchasedDocuments,
    PurchasedDocumentsIDs,
    DocumentPurchased,
    SavedDocuments,
    Topics,
    TopicContent,
    TopicQuestions,
    TopicSearch,
    TopicAnswers,
    SectionSeo,
    TopicQuestionSeo,
    User,
    UserAuth,
    CustomerDetails,
    UserCurrentSubscription,
    UserBilling,
    PaymentPlans,
    StripeId,
    CompanySizeOptions,
    IndustryOptions,
    DocumentSeoMappings,
    Credit,
    RelatedDocuments,
    Bundle,
    Bundles,
    BundleSeo,
    DocumentRelatedBundles,
    BundleRelatedBundles,
    BundleSearch,
    TopicRelatedBundles
  ],
  { Online, observeRequest: observeReq }
)

const observeRequest = method => (url, body) => (
  observeReq({
    url,
    method,
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(body)
  })
)

export const fetch = ApiStorage.fetch
export const refresh = ApiStorage.refresh
export const observe = ApiStorage.observe
export const securePost = observeRequest('POST')
export const securePut = observeRequest('PUT')
export const secureDelete = observeRequest('DELETE')
export const secureGet = observeRequest('GET')
