import { of, combineLatest } from 'rxjs'
import { map, switchMap, tap, flatMap, debounceTime, filter, first } from 'rxjs/operators'
import { _has, _result, _get, _isObject, _pick, _forEach, _find, _isArray } from 'libraries/lodash'
import debug from 'front-end-core/core/debug'
import { RESPONSE_NOTFOUND, RESPONSE_BADDATA } from 'front-end-core/constants/responses'
import { mapResponse, flatMapResponse } from 'core/api/dataflow'
import { $facets } from 'services/SearchService'
import { fetch } from 'services/ApiService'
import { TOPICS, TOPIC_CONTENT, TOPIC_QUESTIONS, TOPIC_SEARCH, TOPIC_ANSWERS } from 'constants/requests'
import { Media } from 'services/WindowService'
import { TOPIC_FRAGMENT_COUNT_MOBILE, TOPIC_FRAGMENT_COUNT_DESKTOP } from 'constants/search'

const TopicContentStore = {}

export const $topics = () => fetch(TOPICS)

export const $rootTopics = () => $topics().pipe(
  map(mapResponse(topics => _result(
    topics,
    ['nodes', topics.root, 'children'],
    []
  ).map(child => topics.nodes[child.id])))
)

const $topicContent = ({ topicId }) => {
  if (!topicId || +topicId < 0) {
    return $topics()
  }
  return $topics().pipe(
    switchMap(
      flatMapResponse(response => {
        // If the response object doesn't have the topic in it, then it's probably
        // restricted content and has been filtered out. Make a request to the endpoint
        // that will give you the proper error response to confirm.
        if (!_has(response, ['nodes', topicId])) {
          return fetch(TOPIC_CONTENT, { topicIds: topicId })
        }
        const rootId = _result(response, ['nodes', topicId, 'path', 0], null)
        if (rootId === null) {
          return of({ statusCode: RESPONSE_NOTFOUND })
        }
        if (TopicContentStore.hasOwnProperty(topicId)) { // eslint-disable-line
          return of(TopicContentStore[topicId])
        }
        // Get all the topic ids that share the same rootId (where rootId is the id of the S1 topic)
        const topicIds = _result(response, ['nodes', rootId, 'descendants'], [])
        const paths = {}
        // Use the ids to get all the topics from the topic nodes
        const nodes = _pick(response.nodes, topicIds)
        // Build an object that has the topicId as key and the topic path as value
        _forEach(topicIds, topicId => {
          paths[topicId] = response.nodes[topicId].path
        })
        // Get all the questions... we call them 'content' for some reason
        return fetch(TOPIC_CONTENT, { topicIds: topicId }).pipe(map(
          // content is an object like { questions: { ... } }
          mapResponse(content => ({ ...content, paths, nodes, rootId }))
        ))
      })
    ),
    tap(
      mapResponse(response => {
        TopicContentStore[topicId] = response
      })
    )
  )
}

export const $topic = ({ topicId }) => {
  // topicId is compulsory for this function!
  if (!topicId) {
    return of({ statusCode: RESPONSE_NOTFOUND })
  }
  return $topicContent({ topicId }).pipe(map(
    mapResponse(response => {
      const getNode = id => _get(response, ['nodes', id], null)
      const getQuestions = node => ({
        ...node,
        questions:
          _get(response, ['topics', node.id], []) // Get all the question ids for the topic (there might not be any!)
            .map(qId => _get(response, ['questions', qId], null)) // Use the question ids to get all the questions
            .filter(x => !!x)
      })
      const getChildren = node => {
        // Recursively get all the node's children and their questions (if any)
        if (node.children.length > 0) {
          return {
            ...node,
            children: node.children
              .map(getQuestions)
              .map(getChildren)
          }
        }
        return node
      }

      // Get the node you're interested in
      const node = getNode(topicId)
      // Check if it's got an overview
      const overview = node.overview || null

      return node
        // Get the tree, including all child objects and questions, with the
        // topic id specified as its root.
        ? { ...getChildren(getQuestions(node)), overview }
        : { statusCode: RESPONSE_NOTFOUND }
    })
  ))
}

export const $topicWithAnswers = (...args) => $topic(...args).pipe(switchMap(
  flatMapResponse((node) =>
    fetch(TOPIC_ANSWERS, { topicIds: [node.id] }).pipe(
      map(mapResponse(({ answers }) => {
        const mapNode = (node) => ({
          ...node,
          questions: node.questions.map(mapQuestion),
          children: node.children.map(mapNode)
        })
        const mapQuestion = (question) => ({ ...question, ...answers[question.tocId] })
        return mapNode(node)
      }))
    )
  )
))

export const $topicPath = ({ topicId }) =>
  $topic({ topicId }).pipe(map(mapResponse(node => node.path)))

export const $flattenedTopics = ({ topicId }) => $topicContent({ topicId })
  .pipe(map(mapResponse(response => response.nodes)))

const $topicQuestions = function () {
  return combineLatest(
    fetch(TOPIC_QUESTIONS),
    $topics(),
    mapResponse((questions, topics) => {
      const {
        all: allQuestions,
        nodes: questionNodes,
        sections: topicReferences,
        topics: topicQuestions
      } = questions
      const { nodes: topicNodes, root: topicRoot } = topics
      return {
        allQuestions,
        questionNodes,
        topicNodes,
        topicQuestions,
        topicReferences,
        topicRoot
      }
    })
  )
}

export const $questions = ({ topicId }) =>
  $topicContent({ topicId }).pipe(map(
    mapResponse(response => response.questions)
  ))

export const $questionList = ({ topicId }) =>
  $topicQuestions().pipe(map(
    mapResponse(response => {
      const questions = _get(response, ['topicQuestions', topicId], null)
      return questions
        ? questions
          .map(qId => _get(response, ['questionNodes', qId], null))
          .filter(x => !!x)
        : { statusCode: RESPONSE_NOTFOUND }
    })
  ))

export const $topicResults = ({ searchPhrase: queryString, pageNumber = 1, pageLimit = 20, query }) => {
  const limit = +pageLimit
  const init = (+pageNumber - 1) * limit + 1
  return Media.Mobile.pipe(switchMap((isMobile) => combineLatest(
    fetch(
      TOPIC_SEARCH,
      {},
      { ...query, q: queryString, i: init, s: limit, f: isMobile ? TOPIC_FRAGMENT_COUNT_MOBILE : TOPIC_FRAGMENT_COUNT_DESKTOP }
    ),
    fetch(TOPICS),
    mapResponse(addPaths)
  )))
}

export const $topicFacets = $facets({ searchUrl: TOPIC_SEARCH, $results: $topicResults })

export const getParentTopicId = ({ topics, questions = {}, topicId = null, questionId = null }) => {
  const getParentTopicOfDepth = (depth, topics, topicId) => {
    const topic = topics[topicId]
    return topics[topic.path[depth - 1]] || null
  }
  const topic = topicId
    ? getParentTopicOfDepth(3, topics, topicId)
    : questions && questionId
      ? getParentTopicOfDepth(3, topics, questions[questionId].topicId)
      : null
  return _isObject(topic) ? topic.id : null
}

const addPaths = (search, topics) => {
  try {
    const getPathElement = id => ({ id, title: topics.nodes[id].title, parentId: topics.nodes[id].parentId })
    const getPath = id => topics.nodes[id] ? topics.nodes[id].path.map(getPathElement) : null
    const results = search.results.map(q => {
      const path = getPath(q.topicId)
      const moreData = path ? { path } : { restricted: true }
      return {
        ...q,
        ...moreData
      }
    })
    return { ...search, results }
  } catch (e) {
    debug.error('Search results parent ids don\'t match section ids.')
    return { statusCode: RESPONSE_BADDATA }
  }
}

export const $topicHierarchy = id => {
  const { topicId, questionId } = id
  return combineLatest(
    $topicPath({ topicId }),
    $topicPath({ topicId }).pipe(flatMap(path => $flattenedTopics({ topicId: path[1] }))),
    $questions({ topicId }).pipe(
      map(questions => _find(questions, ({ tocId }) => tocId === questionId) || null),
      map(question => question ? `Q${question.questionNumber}: ${question.question}` : null),
      map(question => questionId === 'overview' || questionId === 'documents' ? questionId : question)
    )
  ).pipe(
    debounceTime(0),
    filter(([path, topics]) => _isArray(path) && _isObject(topics)),
    map(([path, topics, question]) => [...path.slice(1).map(id => topics[id].title), ...(question ? [question] : [])]),
    map(hierarchy => hierarchy.join(' --> ')),
    first()
  )
}
