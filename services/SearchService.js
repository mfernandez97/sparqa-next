import { combineLatest } from 'rxjs'
import { map, filter } from 'rxjs/operators'
import { _omit, _xor, _isArray, _isUndefined, _find } from 'libraries/lodash'
import { mapResponse } from 'core/api/dataflow'
import { fetch } from 'services/ApiService'

export const combineFacets = (allFacets, availableFacets) => allFacets.map(facet => {
  const matchName = nameToMatch => ({ name }) => name === nameToMatch
  const availableFacet = _find(availableFacets, matchName(facet.name)) || { options: [] }
  return {
    ...facet,
    options: facet.options.map(option => {
      const { selected = false } = _find(availableFacet.options, matchName(option.name)) || {}
      return {
        ...option,
        selected
      }
    })
  }
})

export const getFacets = $stream => $stream.pipe(
  map(({ facets }) => facets),
  filter(facets => !_isUndefined(facets))
)

export const $facets = ({ searchUrl, $results }) => ({
  searchPhrase: queryString,
  pageNumber,
  pageLimit,
  query
}) => {
  return combineLatest(
    getFacets(fetch(searchUrl, {}, { q: queryString })),
    getFacets($results({ searchPhrase: queryString, pageNumber, pageLimit, query })),
    mapResponse(combineFacets)
  )
}

export const updateQuery = (query, remove, parameterName, value) => {
  const withoutParam = (query[parameterName] || []).length > 1
    ? _xor(query[parameterName], [value])
    : _omit(query, parameterName)
  const withParam = (query[parameterName] || []).concat([value])
  return {
    ...query,
    [parameterName]: remove ? withoutParam : withParam
  }
}

const removeQueryParam = (query, parameterName, currentValue, value) => {
  const current = _isArray(currentValue) ? currentValue : [currentValue]
  return current.length <= 1
    ? _omit(query, parameterName)
    : { ...query, [parameterName]: _xor(query[parameterName], [value]) }
}

const addQueryParam = (query, parameterName, currentValue, value) => {
  const current = _isArray(currentValue) ? currentValue : [currentValue]
  return {
    ...query,
    [parameterName]: current.concat([value])
  }
}

export const updateQueryParams = (query, parameterName, value, { removeParam = false }) => {
  const currentValue = query[parameterName] || []
  return removeParam
    ? removeQueryParam(query, parameterName, currentValue, value)
    : addQueryParam(query, parameterName, currentValue, value)
}
