import { fromEvent, merge, combineLatest } from 'rxjs'
import Cookies from 'js-cookie'
import { _isEmpty, _has } from 'libraries/lodash'
import { filter, skip, map, distinctUntilChanged, first } from 'rxjs/operators'
import { mergeResponses as $mergeResponses } from 'front-end-core/helpers/dataflow'
import { $user, $userAuth } from './UserService'
import { Click } from 'front-end-core/services/WindowService'
import { getNode } from 'helpers/dom'

window.dataLayer = window.dataLayer || []

const getTrackedClick = getNode(node => node.hasAttribute('data-track-click'))
const getTrackedSubmit = getNode(node => node.hasAttribute('data-track-submit'))

const $trackedClicks = Click.pipe(
  filter(e => getTrackedClick(e.target)),
  map(e => {
    const tracked = getTrackedClick(e.target)
    return {
      category: tracked.dataset.trackCategory || 'Uncategorised',
      action: tracked.dataset.trackAction || 'Click',
      label: tracked.dataset.trackLabel || null
    }
  })
)

const $trackedSubmits = fromEvent(document, 'submit').pipe(
  filter(e => getTrackedSubmit(e.target)),
  map(e => {
    const tracked = getTrackedSubmit(e.target)
    return {
      category: tracked.dataset.trackCategory || 'Uncategorised',
      action: tracked.dataset.trackAction || 'Submit',
      label: tracked.dataset.trackLabel || null
    }
  })
)

const zeroLead = (timeComponent) => timeComponent < 10 ? `0${timeComponent}` : timeComponent

const formattedDate = function () {
  const date = new Date()
  const year = date.getUTCFullYear()
  const month = date.getUTCMonth() + 1
  const day = date.getUTCDate()
  const hour = date.getUTCHours()
  const minutes = date.getUTCMinutes()
  const seconds = date.getUTCSeconds()
  const milliseconds = date.getUTCMilliseconds()
  return `${zeroLead(year)}/${zeroLead(month)}/${zeroLead(day)} ${zeroLead(hour)}:${zeroLead(minutes)}:${zeroLead(seconds)}:${zeroLead(milliseconds)}`
}

// Disabling as linter doesn't like key in quotes
/* eslint-disable */

combineLatest(
  $user,
  $userAuth,
  $mergeResponses
).pipe(
  filter(d => d && !!d.email),
  first()
).subscribe(({ analyticsId }) => window.dataLayer.push({
  'userId': analyticsId,
  'clientId': Cookies.get('_ga') ? Cookies.get('_ga').slice(6) : null
}))

const pageView = ({ url, userType = '' }) => window.dataLayer.push({
  'event': 'pageView',
  'pageView': '/' + url,
  'userType': userType,
  'dateTime': formattedDate()
}) 

export const pushEvent = ({ category, action, label }) => window.dataLayer.push({
  'event': category,
  'eventCategory': category,
  'eventAction': action,
  'eventLabel': label,
  'dateTime': formattedDate()
})

merge(
  $trackedClicks,
  $trackedSubmits
).subscribe(pushEvent)

// e-commerce stuff

export const trackPages = app => {
  app.pipe(
    skip(1),
    distinctUntilChanged(({ 
      url: prevUrl, queryString: prevQueryString
    },{
      url: currUrl, queryString: currQueryString
    }) => {
      return _isEmpty(currQueryString)
        ? prevUrl === currUrl
        : prevUrl === currUrl && prevQueryString == currQueryString
    }),
    map(({ url: basePath, isLogged, user, queryString }) => {
      const url = `${basePath}${!_isEmpty(queryString) ? `?${queryString}` : ''}`
      return isLogged && !_isEmpty(user) && _has(user, 'isSubscribed')
        ? { url, userType: user.isSubscribed ? 'unlimited': 'free' }
        : { url }
    })
  ).subscribe(pageView)
}

export const onClickCallToAction = ({ id, name, price, category, variant }) => window.dataLayer.push({
  'event': 'addToCart',
  'ecommerce': {
    'currencyCode': 'GBP',
    'add': {
      'products': [{
        'name': name,
        'id': id,
        'price': price,
        'brand': 'Sparqa',
        'category': category,
        'variant': variant,
        'quantity': 1
      }]
    }
  },
  'dateTime': formattedDate()
})

export const onCheckout = ({ id, name, price, category, variant, step }) => window.dataLayer.push({
  'event': 'checkout',
  'ecommerce': {
    'currencyCode': 'GBP',
    'checkout': {
      'actionField': { step },
      'products': [{
        'name': name,
        'id': id,
        'price': price,
        'brand': 'Sparqa',
        'category': category,
        'variant': variant
      }]
    }},
  'dateTime': formattedDate()
})

export const onCheckoutOption = checkoutOption => window.dataLayer.push({
  'event': 'checkoutOption',
  'ecommerce': {
    'checkout_option': {
      'actionField': {
        'step': 1,
        'option': checkoutOption
      }
    }
  },
  'dateTime': formattedDate()
})

export const onCheckoutComplete = ({ transactionId = '', productId, partner = '', name, price, category, variant, discountCode = '' }) => window.dataLayer.push({
  'event': 'purchase',
  'ecommerce': {
    'currencyCode': 'GBP',
    'purchase': {
      'actionField': {
        'id': transactionId,
        'affiliation': partner,
        'revenue': price,
        'coupon': ''
      },
      'products': [{
        'name': name,
        'id': productId,
        'price': price,
        'brand': 'Sparqa',
        'category': category,
        'variant': variant,
        'quantity': 1,
        'coupon': discountCode
      }]
    }
  },
  'dateTime': formattedDate()
})
/* eslint-enable */
