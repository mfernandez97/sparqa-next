import { combineLatest } from 'rxjs'
import { map, filter, startWith } from 'rxjs/operators'
import { fetch as fetchService, securePut, securePost } from './ApiService'
import { Media } from './WindowService'
import { USER, USER_AUTH, CUSTOMER_DETAILS, USER_CURRENT_SUBSCRIPTION } from 'constants/requests'
import { RESPONSE_UNAUTHORIZED, RESPONSE_PENDING, RESPONSE_BADRESPONSE } from 'front-end-core/constants/responses'
import { mapResponse, isValidResponse } from 'core/api/dataflow'
import { URL_USER_PASSWORD_CHANGE, URL_PASSWORD_RESET, URL_UPDATE_MARKETING_CONSENT, URL_CHECK_PASSWORD_STRENGTH } from 'constants/urls'

export const $user = fetchService(USER)
export const $userAuth = fetchService(USER_AUTH)
export const $customerDetails = fetchService(CUSTOMER_DETAILS)
export const $subscription = fetchService(USER_CURRENT_SUBSCRIPTION)

const defaultLogo = {
  logoLg: 'assets/logo.svg',
  logoSm: 'assets/logo-sm-alt.svg'
}

export const $logoUrl = combineLatest(
  [
    Media.Mobile,
    $customerDetails.pipe(
      startWith({ partner: defaultLogo }),
      filter((res) => isValidResponse(res)),
      map(mapResponse(({ partner }) => ({ ...defaultLogo, ...partner })))
    )
  ]
).pipe(
  map(([isMobile, logoUrls]) => isMobile ? logoUrls.logoSm : logoUrls.logoLg)
)

export const $isLogged = $user.pipe(
  map(({ statusCode }) =>
    statusCode === RESPONSE_PENDING
      ? { statusCode }
      : statusCode === RESPONSE_UNAUTHORIZED
        ? false
        : isValidResponse({ statusCode })
  ),
  map(mapResponse(isLogged => ({ isLogged })))
)

export const $userData = $user.pipe(
  filter(({ statusCode }) => statusCode !== RESPONSE_PENDING),
  map(response => {
    const { statusCode } = response
    if (statusCode === RESPONSE_PENDING) {
      return { statusCode }
    }
    if (statusCode === RESPONSE_UNAUTHORIZED || !isValidResponse({ statusCode })) {
      return {}
    }
    return response
  }),
  map(mapResponse(user => ({ user })))
)

export const updateMarketingConsent = (marketingConsent) => {
  return securePut(URL_UPDATE_MARKETING_CONSENT, marketingConsent)
}

export const changePassword = (oldPassword, newPassword, mail) => {
  return securePost(URL_USER_PASSWORD_CHANGE, {
    oldPassword,
    newPassword,
    mail
  }).pipe(map(response => ({ ...response, success: response.statusCode !== RESPONSE_BADRESPONSE }))) // TODO: can to in transforms
}

export const passwordReset = (email, token, newPassword) => {
  const path = encodeURI(`${URL_PASSWORD_RESET}?email=${email}&token=${token}`)
  return fetch(path, {
    method: 'PUT',
    headers: {
      'Content-Type': 'text/plain'
    },
    body: newPassword
  }).then(res => {
    if (!res.ok) {
      throw new Error('Something went wrong, please try again later.')
    }
    return res.text()
  })
}

export const requestPasswordReset = email => {
  const path = encodeURI(`${URL_PASSWORD_RESET}?email=${email}`)
  return fetch(path, {
    method: 'GET',
    credentials: 'include',
    headers: {
      'Content-Type': '*/*'
    }
  }).then(res => {
    if (!res.ok) {
      throw new Error('Something went wrong, please try again later.')
    }
    return res.text()
  })
}

export const $isSubscribedUser = $user.pipe(map(({ roles }) => roles && roles.includes('subscribed')))

export const $isLinkedUser = $user.pipe(map(({ roles }) => roles && !roles.includes('customer_admin')))

export const $isFreemiumUser = $user.pipe(map(({ roles }) => roles && (!roles.includes('subscribed') && roles.includes('free-tier'))))

export const $isExpiredUser = $user.pipe(map(({ roles }) => roles && (!roles.includes('subscribed') && !roles.includes('free-tier'))))

export const $isServicedUser = $user.pipe(map(({ roles }) => roles && (roles.includes('serviced-account'))))

export const $isTrialUser = $subscription.pipe(map(({ status }) => status === 'trialing'))

export const isPasswordStrong = password => {
  const body = JSON.stringify({ password })
  return fetch(URL_CHECK_PASSWORD_STRENGTH, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body
  }).then(res => res.json())
}
