import CMS from 'prismic-javascript'
import { Observable } from 'rxjs'
import { startWith } from 'rxjs/operators'
import qs from 'qs'
import Route from 'route-parser'
import { _toPairs, _sortBy, _startsWith, _pickBy, _mapKeys, _isArray } from 'libraries/lodash'
import Cookies from 'js-cookie'
import { RESPONSE_PENDING, RESPONSE_NOTFOUND } from 'front-end-core/constants/responses'
import { PARTNER_COOKIE } from 'constants/cookies'
import { route as blogRoute } from 'pages/blog'
import { route as blogPostRoute } from 'pages/blogpost'
import { route as blogTopicRoute } from 'pages/blogtopic'
import { route as blogAudienceRoute } from 'pages/blogaudience'
import { route as teamMemberRoute } from 'pages/teammember'

// Document types
export const BLOG_HOME = 'blog_home'
export const BLOG_POST = 'blog_post'
export const TEAM_MEMBER = 'team_member'
export const AUDIENCE = 'audience'
export const LANDING = 'landing'
export const TOPIC = 'topic'
export const ARBITRARY_CMS = 'static_page'
export const PARTNERSHIP_BANNER = 'partnership_banner'

const getApi = () => {
  if (!process.env.CMS_API_ENDPOINT) {
    console.log('Env variable CMS_API_ENDPOINT is not defined.')
    return
  }
  if (!process.env.CMS_ACCESS_TOKEN) {
    console.log('Env variable CMS_ACCESS_TOKEN is not defined.')
    return
  }
  return (
    CMS.getApi(process.env.CMS_API_ENDPOINT, { accessToken: process.env.CMS_ACCESS_TOKEN })
      .then(api => {
        const { previewRef } = qs.parse(window.location.search.slice(1))
        const ref = previewRef ? { ref: previewRef } : {}
        return { api, ref }
      })
  )
}

const initCache = () => {
  const store = new Map()
  const storeByUid = new Map()
  return (documentType = '', fn) => options => {
    return Observable.create(async observer => {
      const key = `${documentType}-${JSON.stringify(_sortBy(_toPairs(options), 0))}`
      const { uid } = options
      if (store.has(key)) {
        // Check if the same request has been made before.
        // console.log(`%c FETCHING FROM CACHE BY REQUEST KEY: ${key}`, 'background: #2ecc71; color: #fff')
        observer.next(store.get(key))
      } else if (uid && storeByUid.has(uid)) {
        // Check if a different request returned and cached the object.
        // console.log(`%c FETCHING FROM CACHE BY UID: ${uid}`, 'background: #3498db; color: #fff')
        observer.next(storeByUid.get(uid))
      } else {
        // Request object from API.
        // console.log(`%c FETCHING FROM API: ${JSON.stringify(options)}`, 'background: #e74c3c; color: #fff')
        const result = await fn(options)
        if (result) {
          store.set(key, result)
          if (uid) {
            storeByUid.set(uid, result)
          }
          if (_isArray(result.results)) {
            result.results.forEach(item => {
              if (item.uid) {
                storeByUid.set(item.uid, item)
              }
            })
          }
          observer.next(result)
        } else {
          observer.next({ statusCode: RESPONSE_NOTFOUND })
        }
      }
    }).pipe(startWith({ statusCode: RESPONSE_PENDING }))
  }
}

const $initCachedRequests = () => {
  const cache = initCache()
  return {
    $documents: (documentType, defaultOptions) => cache(
      documentType,
      ({ ids = [], documentLinks = [], ...options } = {}) => {
        return getApi().then(({ api, ref }) => {
          const allOptions = { ...ref, ...defaultOptions, ...options }
          if (ids.length) {
            return api.getByIDs(ids, allOptions)
          }
          return api.query([
            CMS.Predicates.at('document.type', documentType),
            ...documentLinks.map(({ documentType: linkType, id: linkId }) => {
              // NOTE: ${linkType}s plural because so far all the links are nested in group blocks
              return CMS.Predicates.at(`my.${documentType}.${linkType}s.${linkType}`, linkId)
            })
          ], allOptions).catch(error => {
            console.log(`Error fetching ${documentType} from CMS:`, error)
          })
        })
      }
    ),
    $document: (documentType, defaultOptions) => cache(
      documentType,
      ({ uid = null, ...options }) => {
        return getApi().then(({ api, ref }) => {
          return api.getByUID(documentType, uid, {
            ...ref,
            ...defaultOptions,
            ...options
          })
        }).catch(error => {
          console.log(`Error fetching ${documentType} with uid ${uid} from CMS:`, error)
        })
      }
    )
  }
}

const { $documents, $document } = $initCachedRequests()

export const $blogPosts = $documents(BLOG_POST, {
  page: 1,
  orderings: '[document.first_publication_date desc]'
})

export const $audiences = $documents(AUDIENCE, {
  page: 1,
  orderings: '[document.name]'
})

export const $topics = $documents(TOPIC, {
  page: 1,
  orderings: '[document.name]'
})

export const $teamMembers = $documents(TEAM_MEMBER, {
  page: 1
})

export const $blogHome = $document(BLOG_HOME, {})
export const $blogPost = $document(BLOG_POST, {})
export const $teamMember = $document(TEAM_MEMBER, {})
export const $audience = $document(AUDIENCE, {})
export const $topic = $document(TOPIC, {})
export const $arbitraryCMS = $document(ARBITRARY_CMS, {})
export const $partnershipBanner = $document(PARTNERSHIP_BANNER, {})

export const extractData = (type, data) => {
  return _mapKeys(
    _pickBy(data, (value, key) => _startsWith(key, `${type}_`)),
    (value, key) => key.replace(`${type}_`, '')
  )
}

export const linkResolver = doc => {
  switch (doc.type) {
    case BLOG_HOME:
      return blogRoute
    case BLOG_POST:
      return new Route(blogPostRoute).reverse({ uid: doc.uid })
    case TEAM_MEMBER:
      return new Route(teamMemberRoute).reverse({ uid: doc.uid })
    case TOPIC:
      return new Route(blogTopicRoute).reverse({ uid: doc.uid })
    case AUDIENCE:
      return new Route(blogAudienceRoute).reverse({ uid: doc.uid })
    case ARBITRARY_CMS:
      return doc.uid
    case PARTNERSHIP_BANNER:
      return resolvePartnerWithCookie(doc)
    default:
      return '/404'
  }
}

const resolvePartnerWithCookie = doc => {
  // Usually this is set on the backend, but in the case of CMS previews we
  // bypass the backend.
  Cookies.set(PARTNER_COOKIE, doc.uid)
  return `?pt=${doc.uid}`
}
