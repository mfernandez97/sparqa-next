import { catchError, concatMap, map } from 'rxjs/operators'
import { RESPONSE_BADRESPONSE } from 'front-end-core/constants/responses'
import { URL_WORDPRESS_MEDIA, URL_WORDPRESS_POSTS } from 'constants/urls'
import { isValidResponse } from 'core/api/dataflow'
import { _reduce, _get } from 'libraries/lodash'
import { stripTags } from 'helpers/misc'
import { forkJoin, of } from 'rxjs'
import { secureGet } from './ApiService'
import Route from 'route-parser'

export const requestPosts = ({ limit }) => secureGet(new Route(URL_WORDPRESS_POSTS).reverse({ limit })).pipe(
  map(response => {
    if (!isValidResponse(response)) {
      throw new Error('Error')
    }
    const posts = _reduce(response, (acc, curr) => {
      if (!curr.type || curr.type !== 'post') return acc

      return [
        ...acc,
        {
          title: curr.title.rendered,
          path: curr.slug,
          excerpt: stripTags(curr.excerpt.rendered),
          mediaId: curr.featured_media
        }
      ]
    }, [])

    return posts
  }),
  concatMap(posts => forkJoin(posts.map(post => {
    const url = new Route(URL_WORDPRESS_MEDIA).reverse({ id: post.mediaId })

    return secureGet(url).pipe(
      map(response => {
        const imageUrl = isValidResponse(response)
        // try and find a smaller image and fall back to source_ if there is no medium size for the image
        // which in turn falls back to empty string if there is no source_url
          ? _get(response, 'media_details.sizes.medium.source_url', _get(response, 'source_url', ''))
          : '/assets/icons/image-filter-hdr.svg'

        return {
          ...post,
          imageUrl
        }
      })
    )
  }))),
  catchError(() => {
    return of({ statusCode: RESPONSE_BADRESPONSE })
  })
)
