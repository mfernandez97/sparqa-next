import { _isArray, _find, _get } from 'libraries/lodash'
import { of, from } from 'rxjs'
import { map, concatMap, catchError, tap } from 'rxjs/operators'
import Route from 'route-parser'
import { pushEvent } from 'services/AnalyticsService'
import { RESPONSE_NOTFOUND, RESPONSE_UNAUTHORIZED } from 'front-end-core/constants/responses'
import { isValidResponse } from 'core/api/dataflow'
import {
  USER_BILLING,
  PAYMENT_PLANS,
  STRIPE_ID,
  COMPANY_SIZE_OPTIONS,
  INDUSTRY_OPTIONS,
  CREDIT
} from 'constants/requests'
import { fetch as fetchReturnStream, securePut, securePost } from 'services/ApiService'
import {
  URL_CHECK_EMAIL_AVAILABLE,
  URL_CHECK_EMAIL_VALID,
  URL_REGISTER_CUSTOMER,
  URL_REGISTER_FREEMIUM_CUSTOMER,
  URL_REGISTER_SERVICED_CUSTOMER,
  URL_BILLING_DETAILS,
  URL_CHECK_DISCOUNT_CODE_VALID,
  URL_CHECK_SERVICED_ACCOUNT_CODE_VALID,
  URL_DISCOUNT_CODE_TRIAL,
  URL_UPDATE_SUBSCRIPTION,
  URL_DEFAULT_TOKEN,
  URL_RESUBSCRIBE,
  URL_PURCHASE_DOCUMENT,
  URL_DOCUMENT_CONFIRM_PURCHASE,
  URL_FREE_DOCUMENT,
  URL_FREE_BUNDLE,
  URL_PURCHASE_BUNDLE
} from 'constants/urls'
import { getPartnerCode } from 'helpers/misc'

const freemiumPlan = {
  stripeId: 'freemium',
  name: 'Free',
  price: 0,
  recommended: false,
  benefits: ['Full, unlimited access to all our Q&A', 'Pay only for the documents you need', 'Save and store your documents', 'Blogs, guides & tips for your business']
}

export const $stripeId = () => fetchReturnStream(STRIPE_ID)
export const $paymentPlans = () => (
  fetchReturnStream(PAYMENT_PLANS).pipe(
    map(paymentPlans => _isArray(paymentPlans) ? paymentPlans.concat([freemiumPlan]) : paymentPlans)
  )
)
export const $companySizeOptions = () => fetchReturnStream(COMPANY_SIZE_OPTIONS)
export const $industryOptions = () => fetchReturnStream(INDUSTRY_OPTIONS)
export const $billing = () => fetchReturnStream(USER_BILLING)
export const $credit = () => fetchReturnStream(CREDIT)

export const $hasBillingDetails = () => $billing().pipe(
  map(response => isValidResponse(response) ? !!response.last4 : response),
  map(response => response.statusCode === RESPONSE_NOTFOUND ? false : response),
  map(response => response.statusCode === RESPONSE_UNAUTHORIZED ? false : response)
)

export const isEmailAvailable = (email = '') => {
  const url = `${URL_CHECK_EMAIL_AVAILABLE}?email=${encodeURIComponent(email)}`
  return fetch(url)
    .then(res => res.json())
    .then((data) => {
      return Promise.resolve(data.identityAlreadyExists === false)
    })
}

export const isEmailValid = (email = '') => {
  const body = JSON.stringify({ email })
  return fetch(URL_CHECK_EMAIL_VALID, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body
  }).then(res => res.json())
}

const discountCodeRequest = ({ url, mapResponse }) => (discountCode = '') => {
  const route = new Route(url).reverse({ discountCode })
  return fetch(route, {
    method: 'GET',
    credentials: 'include'
  })
    .then(response => {
      if (response.status !== 200) {
        throw response
      }
      return response.json()
    })
    .then(mapResponse)
}

export const isDiscountCodeValid = discountCodeRequest({
  url: URL_CHECK_DISCOUNT_CODE_VALID,
  mapResponse: ({ data: { amount } }) => amount
})

export const isServicedAccountCodeValid = discountCodeRequest({
  url: URL_CHECK_SERVICED_ACCOUNT_CODE_VALID,
  mapResponse: ({ data: { servicedAccounts } }) => servicedAccounts
})

export const getDiscountCodeTrial = discountCodeRequest({
  url: URL_DISCOUNT_CODE_TRIAL,
  mapResponse: ({ data: { code, duration } }) => ({ code, duration })
})

export const getDefaultTrialCode = () =>
  fetch(URL_DEFAULT_TOKEN, {
    method: 'GET',
    credentials: 'include'
  }).then(response => {
    if (response.status !== 200) {
      throw response
    }
    return response.json()
  }).then(({ data }) => data)

export const registerUser = ({
  plan,
  planName,
  token,
  firstName,
  lastName,
  email,
  vatNo,
  discountCode,
  password,
  companyName,
  companySize,
  companyIndustry,
  marketingConsent
}) => {
  const body = JSON.stringify({
    stripeInfo: {
      planId: plan,
      token
    },
    company: {
      name: companyName,
      size: parseInt(companySize),
      industry: parseInt(companyIndustry)
    },
    customer: {
      email,
      lastName,
      firstName,
      password,
      marketingConsent,
      vatNo
    },
    partner: getPartnerCode(),
    discountCode: discountCode || undefined
  })

  return fetch(URL_REGISTER_CUSTOMER, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body
  })
    .then(res => {
      if (res.status === 'OK') {
        pushEvent({
          category: 'Session',
          action: 'Create Account',
          label: `Paid (${planName})`
        })
      }
      return res.json()
    })
}

export const registerTrialUser = ({
  firstName,
  lastName,
  email,
  password,
  companyName,
  companySize,
  companyIndustry,
  marketingConsent,
  token
}) => {
  const body = JSON.stringify({
    _type: 'createcustomer',
    _ver: 'v1.0',
    company: {
      _type: 'company',
      _ver: 'v1.0',
      name: companyName,
      size: parseInt(companySize),
      industry: parseInt(companyIndustry)
    },
    customer: {
      _type: 'customer',
      _ver: 'v1.0',
      email,
      lastName,
      firstName,
      password,
      marketingConsent
    },
    discountCode: undefined, // I think we need this because the BE needs the field to exist but be empty?
    partner: getPartnerCode()
  })
  let response
  return fetch(`${URL_REGISTER_CUSTOMER}/${token}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body
  })
    .then(res => {
      response = res
      if (response.status >= 500) {
        throw new Error('Unable to register at this time, please try again later.')
      }
      return res.json()
    }).then((data) => {
      if (!response.ok) {
        throw new Error(data.data.errors[0].message)
      } else {
        pushEvent({ category: 'Session', action: 'Create Account', label: 'Free Trial' })
      }
    })
}

export const registerFreemiumUser = ({
  email,
  lastName,
  firstName,
  password,
  marketingConsent,
  stripeCardDetailsToken = null
}, companyNumber = null) => {
  const customer = {
    _type: 'customer',
    _ver: 'v1.0',
    email,
    lastName,
    firstName,
    password,
    marketingConsent
  }
  const withCompany = {
    _type: 'createcustomer',
    _ver: 'v1.0',
    company: {
      _type: 'company',
      _ver: 'v1.0',
      name: '',
      companyRegNumber: companyNumber
    },
    customer: customer,
    partner: getPartnerCode()
  }
  const withoutCompany = {
    _type: 'createcustomer',
    _ver: 'v1.0',
    customer: customer,
    partner: getPartnerCode()
  }
  const data = companyNumber ? withCompany : withoutCompany
  const body = JSON.stringify(
    stripeCardDetailsToken ? { ...data, stripeCardDetailsToken } : data
  )
  let response
  return fetch(URL_REGISTER_FREEMIUM_CUSTOMER, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body
  })
    .then(res => {
      response = res
      if (response.status >= 500) {
        throw new Error('Unable to register at this time, please try again later.')
      }
      return res.json()
    }).then((data) => {
      if (!response.ok) {
        throw new Error(data.data.errors[0].message)
      } else {
        pushEvent({ category: 'Session', action: 'Create Account', label: 'Free Tier' })
      }
    })
}

export const registerServicedUser = ({
  email,
  lastName,
  firstName,
  password,
  marketingConsent,
  partnerCode: partner
}) => {
  const customer = {
    _type: 'customer',
    _ver: 'v1.0',
    email,
    lastName,
    firstName,
    password,
    marketingConsent: marketingConsent === 'yes'
  }
  const payload = {
    _type: 'createcustomer',
    _ver: 'v1.0',
    customer: customer,
    partner
  }
  const body = JSON.stringify(payload)
  let response
  return fetch(URL_REGISTER_SERVICED_CUSTOMER, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body
  })
    .then(res => {
      response = res
      if (response.status >= 500) {
        throw new Error('Unable to register at this time, please try again later.')
      }
      return res.json()
    }).then((data) => {
      if (!response.ok) {
        throw new Error(data.data.errors[0].message)
      } else {
        pushEvent({ category: 'Session', action: 'Create Account', label: 'Serviced Account' })
      }
    })
}

export const updateUserBilling = paymentMethod => {
  const body = JSON.stringify({
    paymentMethod
  })

  return fetch(URL_BILLING_DETAILS, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body
  })
    .then(response => response.json())
    .then(response => {
      console.log('URL_BILLING_DETAILS', response)
      const errors = _get(response, 'data.errors', [])
      if (errors.length > 0) {
        const message = errors.map(({ message }) => message).join(', ')
        throw new Error(message)
      }
      return response
    })
}

export const updateSubscriptionStatus = ({ cancel }) => securePut(URL_UPDATE_SUBSCRIPTION, { cancel })
  .toPromise()
  .then(extractErrorMessage)

export const resubscribe = ({ token, planId, vatNo, plan, discountCode }, conversion) => {
  const body = JSON.stringify({
    stripeInfo: {
      planId,
      token
    },
    customerDetails: {
      vatNo: vatNo || ''
    },
    discountCode: discountCode || undefined
  })
  let response
  return fetch(URL_RESUBSCRIBE, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body
  }).then(res => {
    response = res
    if (response.status >= 500) {
      throw new Error('Unable to register at this time, please try again later.')
    }
    return res.json()
  }).then((data) => {
    if (!response.ok) {
      throw new Error(data.data.errors[0].message)
    }

    const trackingData = {
      category: 'Session',
      action: conversion ? 'Conversion' : 'Resubscribe',
      label: plan
    }
    pushEvent(trackingData)
    return data
  })
}

// Purchase document actually purchases the document.
const $purchaseDocument = ({ uuid, discountCode = null }) => from(
  fetch(new Route(URL_PURCHASE_DOCUMENT).reverse({ documentId: uuid }), {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(discountCode ? { discountCode } : {})
  }).then(response => response.json())
)

// Confirm purchase document gives the document to them immediately; otherwise they just need to wait a few seconds.
const $confirmPurchaseDocument = ({ data }, { transactionId }) => from(
  fetch(URL_DOCUMENT_CONFIRM_PURCHASE, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  }).then(response => {
    const retJson = response.json()
    return retJson
  })
).pipe(
  map(({ status, data: responseMessage }) => Object.assign({}, { status }, {
    data: { responseMessage, transactionId }
  }))
)

export const $getFreeDocument = ({ uuid }) => securePost(new Route(URL_FREE_DOCUMENT).reverse({ documentId: uuid }), {})

export const $getFreeBundle = ({ uuid }) => securePost(new Route(URL_FREE_BUNDLE).reverse({ bundleId: uuid }), {})

export const $buyDocument = ({ uuid, discountCode, stripe }) => $purchaseDocument({ uuid, discountCode }).pipe(
  concatMap(response => {
    console.log('response', response)
    if (response.status !== 'OK') {
      // HANDLE ERROR
    }
    const { data: { secret, uuid } } = response
    const confirmation = stripe.confirmCardPayment(secret, {
      payment_method: uuid
    })

    return confirmation
  }),
  tap(response => console.log('confirmation', response))
)

// export const $buyDocument = ({ uuid, discountCode }) => $purchaseDocument({ uuid, discountCode }).pipe(
//   map(extractPlainErrorMessage),
//   concatMap(data => {
//     if (data.status === 'succeeded') {
//       return of({ status: 'OK', data })
//     }
//     return $confirmPurchaseDocument({ data: { paymentIntent: data.id } }, { transactionId: data.uuid })
//   }),
//   map(extractErrorMessage),
//   catchError(error => {
//     const result = { error: true, message: error.message }
//     return of(result)
//   })
// )

export const $buyBundle = ({ bundleId, discountCode }) => from(
  fetch(new Route(URL_PURCHASE_BUNDLE).reverse({ bundleId }), {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(discountCode ? { discountCode } : {})
  }).then(response => response.json())
).pipe(
  map(extractPlainErrorMessage),
  concatMap(data => {
    if (data.status === 'succeeded') {
      return of({ status: 'OK', data })
    }
    return $confirmPurchaseDocument({ data: { paymentIntent: data.id } }, { transactionId: data.uuid })
  }),
  map(extractErrorMessage),
  catchError(error => {
    const result = { error: true, message: error.message }
    return of(result)
  })
)

const extractPlainErrorMessage = response => {
  const statusOk = ['OK', 'Created']
  if (!statusOk.includes(response.status)) {
    const message = _get(response, 'data', [])
    throw new Error(message)
  }
  return response.data
}

const extractErrorMessage = response => {
  const statusOk = ['OK', 'Created']
  const errors = _get(response, 'data.errors', [])
  if (errors.length > 0 || !statusOk.includes(response.status)) {
    const message = errors.length > 0 ? errors.map(({ message }) => message).join(', ') : 'An unknown error occurred! Please try again later.'
    throw new Error(message)
  }
  return response.data
}

export const getPlanPrice = ({ name = 'monthly', paymentPlans }) => (
  _find(paymentPlans, plan => plan.name.toLowerCase() === name) ||
  { price: null }
).price
