import { stringify } from 'qs'
import { getBaseUrl } from 'front-end-core/core/history'

const BASE_URL = getBaseUrl()

const handleResponse = response => {
  if (response.status < 200 || response.status > 302) {
    // This will handle any errors that aren't network related (network related
    // errors are handled automatically)
    return response.json().then(body => {
      console.error('An error occurred while making a HTTP request: ', body)
      return Promise.reject(new Error(body.error_description))
    })
  }
  return response.json()
}

const get = (flow, challenge) => {
  const queryString = stringify({ [flow + '_challenge']: challenge })
  const url = `${BASE_URL}oauth2/auth/requests/${flow}?${queryString}`
  return fetch(url, { method: 'GET' }).then(handleResponse)
}

const put = (flow, action, challenge, body) => {
  const queryString = stringify({ [flow + '_challenge']: challenge })
  const url = `${BASE_URL}oauth2/auth/requests/${flow}/${action}?${queryString}`
  const options = {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return fetch(url, options).then(handleResponse)
}

export const getLoginRequest = challenge => get('login', challenge)
export const acceptLoginRequest = (challenge, body) => put('login', 'accept', challenge, body)
export const rejectLoginRequest = (challenge, body) => put('login', 'reject', challenge, body)
export const getConsentRequest = challenge => get('consent', challenge)
export const acceptConsentRequest = (challenge, body) => put('consent', 'accept', challenge, body)
export const rejectConsentRequest = (challenge, body) => put('consent', 'reject', challenge, body)
export const getLogoutRequest = challenge => get('login', challenge)
export const acceptLogoutRequest = challenge => put('login', 'accept', challenge, {})
export const rejectLogoutRequest = challenge => put('login', 'reject', challenge, {})
